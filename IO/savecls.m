function savecls(Class, Dataset)
  % Save trial class information in a CTF dataset.
  %
  % savecls(Class, Dataset)
  %
  % Saves data from Class structure in file ClassFile.cls in directory
  % Dataset.  Makes a backup copy of original ClassFile.cls as
  % ClassFile_Original.cls if it doesn't already exist.  Also saves a copy
  % of the new file with a timestamp: ClassFile_yyyymmddThhmmss.cls.
  % Class structure is defined in opencls.m.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2012-04
  
  % Parse input arguments.
  if ~exist('Class', 'var') || isempty(Class)
    error('Missing argument: Class.');
  end
  if ~exist('Dataset', 'var') 
    Dataset = pwd;
    WriteDir = pwd;
    fprintf('Writing new class file in directory: %s\n', Dataset);
  elseif ~exist(Dataset, 'file')
    WriteDir = pwd;
    fprintf(['Provided Dataset is not a local directory: %s. \n', ...
      'It will be written in the class file, but the file will be saved in the current directory.\n'], Dataset);
  elseif ~isdir(Dataset)
    % Only keep directory part.  Cannot choose name for class file.  This
    % ensures that we always keep a backup of the original class file. 
    Dataset = fileparts(Dataset);
    WriteDir = Dataset;
  else
    WriteDir = Dataset;
  end
  
  % Prepare string formats.
  FileHeaderFormat = [ ...
    'PATH OF DATASET:\n', ...
    '%s\n\n\n', ... % Dataset name with full path starting with /
    'NUMBER OF CLASSES:\n', ...
    '%1.0f\n\n\n']; % Number of classes.
  
  ClassHeaderFormat = [ ...
    'CLASSGROUPID:\n', ...
    '3\n', ... % 3 is only "normal" value?
    'NAME:\n', ...
    '%s\n', ... % Class name.
    'COMMENT:\n', ...
    '\n', ... 
    'COLOR:\n', ...
    'Red\n', ...
    'EDITABLE:\n', ...
    'Yes\n', ...
    'CLASSID:\n', ...
    '+%1.0f\n', ... 
    'NUMBER OF TRIALS:\n', ...
    '%1.0f\n', ... % Number of recorded instances of this class.
    'LIST OF TRIALS:\n', ...
    'TRIAL NUMBER\n'];

  ClassFormat = '%20.0f\n'; % Trial number (indexed from 0).
  ClassFooterFormat = '\n\n';
  
  
  % Always make sure there is a backup of original ClassFile.
  ClassFile = [WriteDir, filesep, 'ClassFile.cls'];
  BackupOriginal(ClassFile);
  
  % Create the new file with time stamp.
  ClassFileStamped = [ClassFile(1:end-4), '_', datestr(now, 30), '.cls'];
  fid = fopen(ClassFileStamped, 'w', 'ieee-be'); % If using text mode 'wt' on Windows, '\n' will be replaced by '\r\n'!
  if fid<0
    error('Can''t write to file %s\n', ClassFile);
  end
  
  % Write file header.
  nClass = length(Class);
  fprintf(fid, FileHeaderFormat, Dataset, nClass);
  % Write class data.
  fprintf('Class ');
  for m = 1:nClass
    fprintf('%s (%1.0f), ', Class(m).Name, Class(m).Count);
    fprintf(fid, ClassHeaderFormat, ...
      Class(m).Name, Class(m).Id, Class(m).Count);
    if Class(m).Count > 0
      % Trials are saved 0-indexed; subtract 1.
      fprintf(fid, ClassFormat, Class(m).Trials - 1); % fprintf prints columnwise.
    end
    fprintf(fid, ClassFooterFormat);
  end
  fclose(fid);
  
  % Copy time stamped file to regular file.
  CopyFile(ClassFileStamped, ClassFile);
  %   if ~copyfile(ClassFileStamped, ClassFile)
  %     error('Copy failed: %s to %s', ClassFileStamped, ClassFile);
  %   end
  fprintf('\n saved in file %s \n and %s.\n\n', ClassFileStamped, ClassFile);
  
end


