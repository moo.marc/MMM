function savehdm(File, Sphere, Labels, Overwrite, Verbose, CTFHeader)
  % Save a hdm head model file from pre-calculated spheres.
  %
  %  savehdm(File, Sphere, Labels, Overwrite, Verbose)
  %
  % Saves a head model file with or without (default) a CTF .hdm format
  % header.  Overwrites existing files only if Overwrite is set to true.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-07
  
  if nargin < 3
    error('Missing input arguments.');
  end
  if ~exist('Verbose', 'var') || isempty(Verbose)
    if ~exist('Overwrite', 'var')
      % Since default is now not to overwrite, which is opposite as before,
      % give some feedback if user didn't specify both.
      Verbose = 1;
    else
      Verbose = 0;
    end
  end
  if ~exist('Overwrite', 'var') || isempty(Overwrite)
    Overwrite = false;
  end
  if ~exist('CTFHeader', 'var') || isempty(CTFHeader)
    WriteHeader = false;
    warning('savehdm:NoHeader', ...
      'Saving a .hdm file without header, this file will likely not be compatible with other software.  Provide a CTFHeader structure to write a header.  (This warning will not appear again.)');
    warning('off', 'savehdm:NoHeader');
  else
    WriteHeader = true;
    if ~isstruct(CTFHeader)
      if Verbose > 0
        fprintf('Unrecognized CTFHeader structure.  Will use default (fake) header values.');
      end
      CTFHeader = struct();
    end
    if ~isfield(CTFHeader, 'File')
      CTFHeader.File = File;
    end
  end
  if ~ischar(File) || isempty(File)
    error('Invalid file name.');
  end
  
  if exist(File, 'file')
    if ~Overwrite
      % Do nothing.
      if Verbose > 0
        fprintf('Keeping existing head model file %s\n', File);
      end
      return;
    elseif Verbose > 0
      fprintf('Overwriting head model file %s\n', File);
    end
  elseif Verbose > 0
    fprintf('Saving head model file %s.\n', File);
  end
  
  N = length(Labels);
  if size(Sphere, 2) ~= N
    error('Number of spheres doesn''t match number of labels.')
  end
  
  fid = fopen(File, 'w');
  if (fid < 0)
    error('Failed to open file %s.\nMay be a permissions issue (try running the script from your home directory).', ...
      File);
  end
  
  if WriteHeader
    CTFHeader.SingleSphere = mean([reshape([Sphere(:).Center], 3, [])', [Sphere(:).Radius]'], 1);
    WriteCTFHeader(fid, CTFHeader);
  end
  
  for s = 1:N
    fprintf(fid, '%s:    %1.5f    %1.5f    %1.5f    %1.5f\n', ...
      Labels{s}, Sphere(s).Center, Sphere(s).Radius);
  end
  
  if WriteHeader
    fprintf(fid, '\n}\n');
  end
  
  fclose(fid);
  
end



function WriteCTFHeader(fid, Info)
  
  Dir = fileparts(Info.File);
  
  % Check for missing inputs, in which case use fake or default values.
  if ~isfield(Info, 'ShapeFile') || isempty(Info.ShapeFile)
    Info.ShapeFile = fullfile(Dir, 'Fake.shape');
  end
  if ~isfield(Info, 'MRIFile') || isempty(Info.MRIFile)
    Info.MRIFile = fullfile(Dir, 'Fake.mri');
  end
  if ~isfield(Info, 'Fiducials') || isempty(Info.Fiducials) || ...
      any(size(Info.Fiducials) ~= [3, 3])
    % At least one fiducial missing, use these fake but somewhat plausible
    % numbers in case it matters.
    Na = [125, 35, 175];
    LE = [35, 135, 175];
    RE = [215, 135, 175];
  else
    Na = Info.Fiducials(1, :);
    LE = Info.Fiducials(2, :);
    RE = Info.Fiducials(3, :);
  end
  if ~isfield(Info, 'VoxelRes') || isempty(Info.VoxelRes)
    Info.VoxelRes = 0.93750000;
  end
  if ~isfield(Info, 'PatchR') || isempty(Info.PatchR)
    Info.PatchR = 9;
  end
  
  HeaderFormat = ['File_Info\n',...
    '{\n',...
    '	VERSION:		CTF_HEAD_MODEL_FILE_VERSION_5.0\n',...
    '	DATE:			%s %2.0f:%2.0f\n',... % Date in dd-mmm-yyyy format, time in hh:mm format.
    '	PATIENT:		none\n',...
    '	STATUS:		\n',...
    '}\n',...
    '\n',...
    'Model\n',...
    '{\n',...
    '	MODEL_TYPE:		MEG_ONLY\n',...
    '	MRI_FILE:		%s\n',... % File name with full path starting with /
    '}\n',...
    '\n',...
    'MEG_Sphere\n',...
    '{\n',...
    '	ORIGIN_X:		%1.3f\n',... % Single sphere parameters.
    '	ORIGIN_Y:		%1.3f\n',... % "
    '	ORIGIN_Z:		%1.3f\n',... % "
    '	RADIUS:			%1.3f\n',... % "
    '\n',...
    '}\n',...
    '\n',...
    'Voxel_Resolution\n',...
    '{\n',...
    '	// Resolution in mm per voxel\n',...
    '	SAGITTAL:	%1.8f\n',... % Voxel resolution
    '	CORONAL:	%1.8f\n',... % "
    '	AXIAL:	%1.8f\n',... % "
    '\n',...
    '}\n',...
    '\n',...
    'MRI_Fid_Points\n',...
    '{\n',...
    '	// Fid. Pts	Sag	Cor	Axi\n',...
    '	NASION:		%1.0f	%1.0f	%1.0f\n',... % Fiducial MRI coordinates (integers).
    '	LEFT_EAR:	%1.0f	%1.0f	%1.0f\n',... % "
    '	RIGHT_EAR:	%1.0f	%1.0f	%1.0f\n',... % "
    '\n',...
    '}\n',...
    '\n',...
    'MultiSphere_Data\n',...
    '{\n',...
    '	SEARCH_RADIUS:	%1.3f\n',... % Sphere fitting patch radius.
    '\n',...
    '	HEADSHAPE_FILE:		%s\n',... % File name with full path starting with /
    '\n',...
    '	// Multiple Sphere locations in cm\n',...
    '	// 		X		Y		Z		Radius\n'];
  
  Time = clock;
  Time = Time(4:5);
  
  fprintf(fid, HeaderFormat, ...
    date, Time, Info.MRIFile, Info.SingleSphere, ...
    Info.VoxelRes, Info.VoxelRes, Info.VoxelRes, Na, LE, RE, ...
    Info.PatchR, Info.ShapeFile);
  
end





