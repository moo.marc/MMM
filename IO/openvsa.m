function [Data, Time] = openvsa(File)
  % Load LCMV "virtual sensor" timeseries from a .vsa file.
  % 
  % [Data, Time] = openvsa(File)
  %
  % These files contain two time series corresponding to the two
  % "tangential" source directions for a specific location.  There is a
  % second copy of these time series, where the orientations are rotated
  % such that the first has the maximal power.
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-02-08
  
  if nargin < 1 || isempty(File) || ~exist(File, 'file')
    error('Vsa file not provided or not found.');
  end
  
  % Time + 4 columns of data, not sure which pair is "rotated".
  Data = load(File);
  Time = Data(:, 1);
  Data(:, [1, 4, 5]) = [];

end