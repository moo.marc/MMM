function NiiFiles = svl2nii(SvlFiles, NiiFiles, Overwrite)
  % Convert CTF SAM (.svl) functional image file(s) to NIfTI format.
  %
  % NiiFiles = svl2nii(SvlFiles, NiiFiles, Overwrite)
  %
  % Converts CTF .svl file to NIfTI format, with real-world coordinates
  % set to "RAS-CTF-head", i.e. 90 degree rotated CTF-head coordinate
  % system (which is ALS before the rotation).  Also convert to 'mm' units,
  % since 'cm' is not an option in NIfTI header.
  %
  % SvlFiles and NiiFiles can each be a single file name or a cell array of
  % file names.  However the output is always a cell array even for a
  % single file.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-09-03
  
  if ~nargin || isempty(SvlFiles)
    error('No SvlFiles to process.');
  end
  if ischar(SvlFiles)
    SvlFiles = {SvlFiles};
  end
  nF = numel(SvlFiles);
  if nargin < 3 || isempty(Overwrite)
    Overwrite = false;
  end
  if nargin < 2 || isempty(NiiFiles)
    % Save under the same name with '_RAS-CTF.nii'.
    NiiFiles = cell(size(SvlFiles));
    for f = 1:nF
      [Path, Name] = fileparts(SvlFiles{f});
      NiiFiles{f} = fullfile(Path, [Name, '_RAS-CTF.nii']);
    end
  elseif ischar(NiiFiles)
    NiiFiles = {NiiFiles};
  end
  
  for f = 1:size(NiiFiles)
    if Overwrite || ~exist(NiiFiles{f}, 'file')
      
      if ~exist(SvlFiles{f}, 'file')
        warning('Svl file not found: %s', SvlFiles{f});
        continue;
      end
      % CTF Matlab code only works for version 4.
      % [MRItag, MRIdata] = readCTFMRI(SvlFile);
      
      [Data, SvlInfo] = opensvl(SvlFiles{1, 1});
      % transform goes from voxels to CTF-mm (ALS), make it "RAS-CTF-mm".
      % As usual, it applies to column vectors from the right: T * [i;j;k;1].
      % Use ctf2rasctf, but that assumes cm, so divide by 10 after.
      T = [ctf2rasctf(SvlInfo.transform(1:3, :)')'/10;  0, 0, 0, 1]; % verified with [1,1,1] and SvlInfo.dim
      % This previous T transform applies to 1-indexed voxel coordinates.
      % Although the transformation matrices encoded in the nifti file apply to
      % 0-indexed voxels, the nifti matlab library converts them to apply to
      % 1-indexed voxel coordinates (nii.mat, nii.mat0)!  So do not adjust T
      % here.
      
      % niftimatlib does not support changing units.  xyzt_units is always 10:
      % mm, s.
      
      DataType = class(Data);
      switch DataType
        case 'uint16'
          % uint16 is not that common and not all programs can read it (e.g.
          % FSL). Also, it looks like the ctf .mri data is scaled to 15 bits
          % (i.e. max=32767), which fits in a signed int16.
          % mri.hdr.clippingRange=32767.  Rescale it to int16 max anyway in
          % case it isn't.
          Data = int16((2.^15 - 1) / double(max(Data(:))) * Data);
          DataType = 'int16';
        case 'double'
          DataType = 'float64';
        case 'single'
          DataType = 'float32';
      end
      
      Nii = nifti;
      Nii.dat = file_array(NiiFiles{f}, SvlInfo.dim, [DataType, '-le'], ceil(348/8)*8);
      % The offset is where the data starts in the .nii file.  Not sure why it's
      % put on an 8 byte boundary after the end of the header, but seems conventional.
      % Last two (missing) fields are scl_slope and scl_inter (scaling and offset
      % of data such that dat = raw*slope + inter)
      
      % nii.cal = [0 1]; % Range of data.
      % sform (12-parameter affine transform)
      Nii.mat = T;
      Nii.mat_intent = 'Aligned';
      % qform (9-parameter affine transform)
      Nii.mat0 = T;
      Nii.mat0_intent = 'Aligned';
      Nii.descrip = 'NIfTI-1 file created by svl2nii';
      Nii.intent.code = 'NONE';
      % Nii.timing.toffset = [];
      % Nii.timing.tspace = [];
      create(Nii);
      Nii.dat(:, :, :) = Data;
      
    end
  end
  
end

