function Events = openlog(LogFile)
  % Get event codes from a Presentation .log file.
  %
  %  Events = openlog(LogFile)
  %
  % Returns the same structure as the one obtained by openmrk.m, with
  % fields: Name, Bit, Count, Samples, but with 6 additional columns in
  % Samples: Trial, Tr. time, Time, Uncert., Req. time, Dur., Dur. unc.,
  % Req. dur.  Name is a concatenation of event type (e.g. 'Picture' or 
  % 'Response') and the Presentation code (string or number), e.g.:
  % "Picture Go" or "Response 4".
  %
  % Trials are now indexed from 1, matching what we do with marker files.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-05-25
  
  if ~exist('LogFile', 'var') || isempty(LogFile)
    error('No log file provided.')
  end
  
  fid = fopen(LogFile, 'r');
  if (fid == -1)
    error('Failed to open file %s', LogFile);
  end
  % Seems older versions (e.g. on clinical side) don't have Subject so need
  % to check.
  fgetl(fid); fgetl(fid); fgetl(fid); 
  Check = fgetl(fid);
  Check = strtok(Check, char(9)); % '\t' (see help strtok)
  if strcmp(Check, 'Trial')
    % "Old" log file.
    ScanFormat = '%f %s %s %f %f %f %f %f %f %s %*[^\n]';
    NoSubject = true;
  else
    ScanFormat = '%s %f %s %s %f %f %f %f %f %f %s %*[^\n]';
    NoSubject = false;
  end
  frewind(fid);
  % Subject, Trial, Event type, Code (can be string), Time, Trial time,
  % Uncertainty, Duration, Uncertainty, Requested time, Requested duration
  % (can be string, e.g. "next").  Presentation version 15 added 2 columns,
  % ignored for now: the rest of the line is skipped.
  LogData = textscan(fid, ScanFormat, 'HeaderLines', 5, 'Delimiter', '\t');
  fclose(fid);
  
  if NoSubject
    LogData = [{[]}, LogData];
  end
  if length(LogData) < 11 || ~iscell(LogData)
    error('Empty log file: %s', LogFile);
  end
  
  % textscan will read until the format doesn't match.  This means for
  % logfiles that have a second section, there will be a "type" called
  % "Event Type" with no data which we must ignore.
  if ~isempty(LogData{1}) && strncmpi(LogData{1}{end}, 'Event', 5)
    LogData{1}(end, :) = [];
  end
  
  % Ignore non-numeric requested durations.
  LogData{11} = str2double(LogData{11});
    
  % Separate events by type and code.
  TypeCodes = strcat(LogData{3}, {' '}, LogData{4});
  % Even if LogData is empty, strcat returns 1 element.  Ignore it.
  if strcmp(TypeCodes{end}, ' ')
    TypeCodes(end) = [];
  end
  TypeCodeList = unique(TypeCodes);
  nTCL = length(TypeCodeList);
  
  % Assign unique integer codes to each different event.  Useful to combine
  % events in large array, e.g. to create a list of time sorted events.
  Bits = unique(round(str2double(unique(LogData{4}))));
  if length(Bits) < nTCL || any(isnan(Bits))
    % Generate new codes.
    Bits = 1:nTCL;
    % Otherwise keep existing integer codes.
  end
  
  % Reorganize numeric data into array. 
  % To make this somewhat compatible with openmrk, which has only "trial"
  % and "trial time" fields.  Since the Presentation trials won't
  % correspond to the dataset trials and the data is often continuously
  % recorded, Presentation "time" makes more sense to match with the
  % dataset "trial time".
  % Trial, TTime, Time, Uncert., Req. time, Dur., Dur. unc., Req. dur.
  LogData = [LogData{[2, 6, 5, 7, 10, 8, 9, 11]}];
  % Convert times to seconds.
  LogData(:, 2:end) = LogData(:, 2:end) / 10000;
  % Index trials from 1 to match what we do with marker files!
  %   LogData(:, 1) = LogData(:, 1) - 1;
  
  % Create same structure as the one obtained by openmrk.m:
  % Name, Bit, Count, Samples, but with additional columns in Samples.
  Events(1:nTCL) = struct('Name', '', 'Bit', 0, 'Count', 0, 'Samples', []);
  for t = 1:nTCL
    I = find(strcmp(TypeCodeList{t}, TypeCodes));
    if isempty(I)
      error('Type code %d not found: %s; %s', t, TypeCodeList{t}, LogFile);
    elseif isempty(LogData)
      error('Zero data for %d: %s; %s', t, TypeCodeList{t}, LogFile);
    end
    Events(t).Name = TypeCodes{I(1)}; 
    Events(t).Bit = Bits(t);
    Events(t).Count = length(I);
    Events(t).Samples = LogData(I, :);
  end
    
end