function [Status, Dir] = CommonPath(File1, File2)
  % Find the common part of 2 files, i.e. their deepest common folder.
  %
  % [Status, Dir] = CommonPath(File1, File2)
  %
  % Status =
  %  0 no common path found or one or both files did not have a path.
  %  1 found and is a real directory.
  %  2 found and is root directory.
  %  3 found but does not exist.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2013-04
  
  if nargin ~= 2
    error('2 files or directories required as inputs.');
  end
  
  Status = 0;
  Dir = '';
  % Find the deepest common folder of 2 files.
  Parts1 = textscan(File1, '%s', 'Delimiter', filesep);
  Parts1 = Parts1{1};
  Parts2 = textscan(File2, '%s', 'Delimiter', filesep);
  Parts2 = Parts2{1};
  nP = min(numel(Parts1), numel(Parts2));
  if nP < 1
    warning('Unexpected string, perhaps only a file name without path.');
    return;
  end
  iFirstDiff = find(~strcmp(Parts1(1:nP), Parts2(1:nP)), 1, 'first');
  if iFirstDiff == 1
    return;
  end
  iSame = iFirstDiff - 1;
  if isempty(iSame)
    iSame = nP;
  end
  Dir = cell(1, iSame);
  Dir(:) = Parts1(1:iSame);
  Dir(2, :) = {filesep};
  Dir = [Dir{:}];
  Dir(end) = [];
  
  if isempty(Dir)
    % Only root folder was common.
    Status = 2;
  elseif isdir(Dir)
    Status = 1;
  else
    Status = 3;
  end
  
end


