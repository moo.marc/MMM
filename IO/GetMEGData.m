function [Data, Info] = GetMEGData(Dataset, InfoOnly, Geometry, Units, ...
    ChannelSubset, IncludeRefs)
  % Get MEG channel data and/or information from CTF dataset, single precision.
  %
  % [Data, Info] = GetMEGData(Dataset, InfoOnly, Geometry, Units, ...
  %     ChannelSubset, IncludeRefs)
  % 
  % Wrapper for readCTFds and getCTFdata to get all sensor data, basic
  % dataset information and optionally sensor geometry.  All but first
  % argument are optional, InfoOnly and Geometry are boolean (both false by
  % default), Units 'fT' or 'T' (default).  All positions in Info are in
  % 'cm'.
  %
  % IMPORTANT: This function returns the data as saved, gradient is not
  % applied or verified (but noted in Info.Grad).
  %
  % ChannelSubset is one or two letters in the channel names indicating
  % which region: first letter 'L' (left), 'R' (right) or 'Z' (midline),
  % second letter 'F' (frontal), 'T' (temporal), 'C' (central), 'P'
  % (parietal), 'O' (occipital).
  %
  % IncludeRefs [default false]: If true, reference channels are included,
  % and placed before sensors.  Only those used in the current balancing
  % order are kept.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-02-15
  
  % [TO DO: Deal with bad channels from class file.  Change gradient to
  % desired: setCTFDataBalance()]
  
  if nargin < 6 || isempty(IncludeRefs)
    IncludeRefs = false;
  end
  
  if nargin > 4 && ischar(ChannelSubset) && ~isempty(ChannelSubset)
    
  else
    % Get MEG channels only.
    ChannelSubset = 'M';
  end
  
  ds = readCTFds(Dataset);

  % Types: 0 ref mag, 1 ref grad, 5 MEG sens, 9 EEG sens, 10 ADC, 11 stim.
  MEGSens = find([ds.res4.senres.sensorTypeIndex] == 5);
  % getCTFdata seems to return the data as saved in the file, it does not
  % apply gradient correction nor verifies it.  So we must always verify!
  Info.Grad = unique([ds.res4.senres(MEGSens).grad_order_no]);
  if numel(Info.Grad) > 1
    error('Channels have different grad order in %s\n', Dataset);
  end

  if nargin > 3
    Info.Unit = Units;
  else
    Info.Unit = 'T';
  end
    
  % Find sensors and possibly reference channels.
  if IncludeRefs
    if nargin > 2 && Geometry
      if Info.Grad == 13 % Not completely sure, should reverify this is how 3rd adaptive is coded.
        Info.Grad = 4;
      end
      GradNames = {'G1BR', 'G2BR', 'G3BR', 'G3AR'};
      % Include current gradiometer coefficients.  Needed e.g. for forward
      % solutions.
      [Info.GradCoefs, MEGSens, MEGRefs] = getCTFBalanceCoefs(ds, GradNames{Info.Grad}, Info.Unit);
      %       balanced_data(:,MEGindex)=data(:,MEGindex)-data(:,MEGbalanceindex)*alphaMEG
    else
      MEGRefs = find([ds.res4.senres.sensorTypeIndex] <= 1);
    end
  else
    MEGRefs = [];
  end
  Info.nSens = numel(MEGSens);
  Info.nRefs = numel(MEGRefs);
  EEGSens = find([ds.res4.senres.sensorTypeIndex] == 9);
  Info.nEEG = numel(EEGSens);
  iChan = [MEGRefs, MEGSens, EEGSens];

  Info.ChanNames = cellstr(ds.res4.chanNames(iChan, :));
  Info.ChanNames = strtok(Info.ChanNames, '-'); % Removes site ID '-1706'; could just do 1:5 for MEG channel names.
  isSubset = ~cellfun(@isempty, strfind(Info.ChanNames, ChannelSubset));
  iChan = iChan(isSubset);
  if isempty(iChan)
    error('No channels found matching subset "%s".', ChannelSubset);
  end
  Info.nChan = sum(isSubset);
  Info.nEEG = sum(isSubset((Info.nSens + Info.nRefs + 1):end));
  Info.nRefs = sum(isSubset((Info.nSens + 1):(Info.nSens + Info.nRefs)));
  Info.nSens = sum(isSubset(1:Info.nSens));
  Info.ChanNames = Info.ChanNames(isSubset);
  
  Info.SampleRate = ds.res4.sample_rate;
  Info.nPreTrig = ds.res4.preTrigPts;
  Info.nSamplesPerTrial = ds.res4.no_samples;
  Info.nTrials = ds.res4.no_trials;
  Info.nTrialsAveraged = ds.res4.no_trials_avgd;
  %   Info.nChannels = ds.res4.no_channels; % All channels, not just MEG sensors

  if nargin > 2 && Geometry
    Info.nCoils = zeros(Info.nChan, 1);
    Info.ChanPos = zeros(Info.nChan, 3, 2);
    Info.ChanOri = zeros(Info.nChan, 3, 2);
    for i = 1:Info.nChan
      s = iChan(i);
      Info.nCoils(i) = ds.res4.senres(s).numCoils;
      %       if ds.res4.senres(iSens).numCoils ~= 2
      %         error('Unexpected number of coils: %d', ds.res4.senres(iSens).numCoils);
      %       end
      for c = 1:Info.nCoils(i)
        Info.ChanPos(i, :, c) = ds.res4.senres(s).pos(:, c);
        Info.ChanOri(i, :, c) = ds.res4.senres(s).ori(:, c) ...
          .* -sign(ds.res4.senres(s).properGain);
      end
    end
  end

  % Get the actual data.
  if ~(nargin > 1 && InfoOnly)
    Data = getCTFdata(ds, [], iChan, Units, 'single'); % [nSamples, nChannels, nTrials] % ? units, single precision for memory.
    if isempty(Data)
      error('No MEG data found in %s\n', Dataset);
    end
    if Info.nSamplesPerTrial ~= size(Data, 1) || Info.nTrials ~= size(Data, 3)
      error('Data size doesn''t match dataset info in %s\n', Dataset);
    end
  else
    Data = [];
  end
  
end

