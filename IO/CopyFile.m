function [Status, Message] = CopyFile(Source, Destination)
  % Copy a file from Source to (and possibly overwriting) Destination.
  % 
  % [Status, Message] = CopyFile(Source, Destination)
  %
  % Similar to Matlab's copyfile, but with additional checks, and warnings
  % for symbolic links on Linux, and other special strange cases.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2013
  
  % If on Linux, verify if Destination is a symbolic link, and if so delete
  % it first and give a warning. Note that the 'test' command exit status
  % is 0 if the test is true and 1 otherwise.
  if isunix && ~system(sprintf('test -L "%s"', Destination)) % Destination is a symlink
    delete(Destination);
    warning(['Destination already existed and was a symbolic link.  It is now being replaced ', ...
      'with a regular file, thus the original linked file is not overwritten.']);
  end
  
  [Status, Message] = copyfile(Source, Destination, 'f');
  if ~Status
    % "Feature" in Matlab, returns status 0 even when it works sometimes.
    % Check manually. First check if there is a message.  If so it did
    % fail.
    
    % This is documented and has to do with trying to preserve timestamps.
    % Linux doesn't allow it if source and destination owners are
    % different. The workaround is to put a copy in the destination folder
    % and move it onto the destination file.
    S = dir(Source);
    D = dir(Destination);
    if ~isempty(Message) || isempty(D) || ...
        S.bytes ~= D.bytes || S.datenum > D.datenum
      % Try the workaround.
      File = fullfile(fileparts(Destination), tempname);
      if ~copyfile(Source, File)
        error('Copy failed: %s to %s\n (also workaround copy)\n%s', Source, Destination, Message);
      end
      if ~movefile(File, Destination)
        error('Copy failed: %s to %s\n (also workaround move)\n%s', Source, Destination, Message);
      end
    else
      Status = 1;
    end
  end
end

