function [mri, nii, NiiFile] = mri2nii(MRIFile, NiiFile)
  % Convert a CTF .mri or .svl file to NIfTI format.
  %
  % [mri, nii, NiiFile] = mri2nii(MRIFile, NiiFile)
  %
  % Converts a CTF .mri (version 2 or 4), or .svl file to NIfTI format,
  % with real-world coordinates set to "RAS-CTF-head", i.e. 90 degree
  % rotated CTF-head coordinate system (which is in ALS orientation before
  % the rotation). 
  %
  % Requires Fieldtrip to be on the Matlab path: uses fr_read_mri to read.
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2013-07

  % Parse inputs.
  if nargin < 1 || isempty(MRIFile) || ~exist(MRIFile, 'file')
    [fileName, filePath] = uigetfile('*.mri', 'Locate CTF .mri file');
    MRIFile = fullfile(filePath, fileName);
  end
  
  if nargin < 2 || isempty(NiiFile)
    [Path, Name] = fileparts(MRIFile);
    NiiFile = [Path, filesep, Name, '_RAS-CTF.nii'];
  end
  
  % ------------------------------------------------------------------------
  % Open .mri or .svl file.
  
  % CTF Matlab code only works for version 4.
  % [MRItag, MRIdata] = readCTFMRI(MRIFile);
  % Now uses spm/fieldtrip fileio library, in which I added a function to
  % read svl files.
  if ~exist('ft_read_mri', 'file')
    addpath(fullfile(spm('dir'), 'external', 'fieldtrip', 'fileio'));
  end
  mri = ft_read_mri(MRIFile);
  
  % Check transformation matrix.
  %
  % Use this before permuting and flipping axes in read_ctf_mri (debug).
  % Fiducials = [hdr.HeadModel.Nasion_Sag hdr.HeadModel.Nasion_Cor hdr.HeadModel.Nasion_Axi; ...
  %   hdr.HeadModel.LeftEar_Sag hdr.HeadModel.LeftEar_Cor hdr.HeadModel.LeftEar_Axi; ...
  %   hdr.HeadModel.RightEar_Sag hdr.HeadModel.RightEar_Cor hdr.HeadModel.RightEar_Axi];
  % Scaling = [hdr.mmPerPixel_sagittal, hdr.mmPerPixel_coronal,hdr.mmPerPixel_axial];
  % [Origin, Rotation] = ChangeCoordinates([], Fiducials, Scaling, -1);
  % % PIR voxel coordinate system is left-handed!
  % Transformation = [Rotation * diag(Scaling), - Rotation * Origin'; zeros(1, 3), 1]
  % diag([Scaling, 1]) * inv(transformMatrix)
  %
  % Use this after.
  % Scaling = [mri.hdr.mmPerPixel_sagittal, mri.hdr.mmPerPixel_coronal, mri.hdr.mmPerPixel_axial];
  % Fiducials = [mri.hdr.fiducial.mri.nas; mri.hdr.fiducial.mri.lpa; mri.hdr.fiducial.mri.rpa];
  % [Origin, Rotation] = ChangeCoordinates([], Fiducials, Scaling);
  % % This goes from voxels to CTF in mm.
  % Transformation = [Rotation * diag(Scaling), - Rotation * Origin'; zeros(1, 3), 1];
  %
  % (Transformation * [Fiducials, ones(3, 1)]')';
  % ChangeCoordinates(Fiducials, Fiducials, Scaling)
  
  % Convert transformation to "RAS-CTF-head" coordinates.
  % 90 degree rotation in x-y.
  R = zeros(4);
  R(1, 2) = -1;
  R(2, 1) = 1;
  R(3, 3) = 1;
  R(4, 4) = 1;
  
  T = R * mri.transform;
  
  % This previous T transform applies to 1-indexed voxel coordinates.
  % Although the transformation matrices encoded in the nifti file apply to
  % 0-indexed voxels, the nifti matlab library converts them to apply to
  % 1-indexed voxel coordinates (nii.mat, nii.mat0)!  So do not adjust T
  % here.
  %   IndexShift = eye(4);
  %   IndexShift(:, 4) = 1;
  %   T = T * IndexShift;
  
  % ------------------------------------------------------------------------
  % Write nifti file.
  
  % niftimatlib does not support changing units.  xyzt_units is always 10:
  % mm, s.
  
  % Consider incorporating .nii output in ft_volumewrite.m
  
  DataType = class(mri.anatomy);
  switch DataType
    case 'uint16'
      % uint16 is not that common and not all programs can read it (e.g.
      % FSL). Also, it looks like the ctf .mri data is scaled to 15 bits
      % (i.e. max=32767), which fits in a signed int16.
      % mri.hdr.clippingRange=32767.  Rescale it to int16 max anyway in
      % case it isn't.
      mri.anatomy = int16((2.^15 - 1) / double(max(mri.anatomy(:))) * mri.anatomy);
      DataType = 'int16';
    case 'double'
      DataType = 'float64';
    case 'single'
      DataType = 'float32';
  end
    
  nii = nifti;
  nii.dat = file_array(NiiFile, mri.dim, [DataType, '-le'], ceil(348/8)*8);
  % The offset is where the data starts in the .nii file.  Not sure why it's
  % put on an 8 byte boundary after the end of the header, but seems conventional.
  % Last two (missing) fields are scl_slope and scl_inter (scaling and offset
  % of data such that dat = raw*slope + inter)
  
  % nii.cal = [0 1]; % Range of data.
  
  % sform (12-parameter affine transform)
  nii.mat = T;
  nii.mat_intent = 'Aligned';
  % qform (9-parameter affine transform)
  nii.mat0 = T;
  nii.mat0_intent = 'Aligned';
  
  nii.descrip = 'NIFTI-1 file converted with mri2nii.m';
  nii.intent.code = 'NONE'; % Not statistical data.
  
  create(nii); % Write header info to file.
  nii.dat(:,:,:) = mri.anatomy; % Write data to file.
  
end
