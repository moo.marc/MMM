function savemrk(Markers, Dataset, Quiet)
  % Create a MarkerFile.mrk CTF dataset marker file.
  %
  % savemrk(Markers, Dataset, Quiet)
  %
  % Saves data from Markers structure in file MarkerFile.mrk in directory
  % Dataset.  Makes a backup copy of original MarkerFile.mrk as
  % MarkerFile_Original.mrk if it doesn't already exist.  Also saves a copy
  % of the new file with a timestamp: MarkerFile_yyyymmddThhmmss.mrk.
  %
  % Markers structure is defined in openmrk.m.
  % Quiet [false]: true = no output on command line.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-20

  % There is a different header format for non-editable markers in
  % DataEditor. I don't see any use for these, so just make them all
  % editable. 

  % Parse input arguments.
  if ~exist('Markers', 'var') || isempty(Markers)
    error('Missing argument: Markers.');
  end
  if ~exist('Quiet', 'var') || isempty(Quiet)
    Quiet = false;
  end
  if ~exist('Dataset', 'var') 
    Dataset = pwd;
    WriteDir = pwd;
    if ~Quiet
      fprintf('Writing new marker file in directory: %s\n', Dataset);
    end
  elseif ~exist(Dataset, 'file')
    WriteDir = pwd;
    warning(['Provided Dataset is not a directory: %s. \n', ...
      'It will be written in the marker file, but the file will be saved in the current directory.\n'], Dataset);
  elseif ~isdir(Dataset)
    % Only keep directory part.  Cannot choose name for marker file.  This
    % ensures that we always keep a backup of the original marker file. 
    Dataset = fileparts(Dataset);
    WriteDir = Dataset;
  else
    WriteDir = Dataset;
  end
  
  % Verify there are no duplicate marker names.
  Names = unique({Markers(:).Name});
  if length(Names) < length(Markers)
    error('Duplicate marker names.');
  end
  
  % Verify counts match number of samples.
  nMarkers = length(Markers);
  for m = 1:nMarkers
    if size(Markers(m).Samples, 1) ~= Markers(m).Count
      error('Incorrect sample count for %s.', Markers(m).Name);
    end
  end

  % Add potentially missing field Editable.
  if ~isfield(Markers, 'Editable')
    [Markers(:).Editable] = deal(false);
  end
  
  % Prepare string formats.
  FileHeaderFormat = [ ...
    'PATH OF DATASET:\n', ...
    '%s\n\n\n', ... % Dataset name with full path starting with /
    'NUMBER OF MARKERS:\n', ...
    '%1.0f\n\n\n']; % Number of markers.
  
  MarkerHeaderFormat = [ ...
    'CLASSGROUPID:\n', ...
    '3\n', ... % 0 would requires long version below, 3 is only other "normal" value?
    'NAME:\n', ...
    '%s\n', ... % Marker name.
    'COMMENT:\n', ...
    'Trigger Marker corresponding to %s (CondBitPattern=0x%x, CondBitMask=0x%x, Flag0to1Only=1,MinStableLength=0,TransientSearchMask=0)\n', ... % Bit # in hexadecimal.
    'COLOR:\n', ...
    'Red\n', ...
    'EDITABLE:\n', ...
    '%s\n', ... % Editable
    'CLASSID:\n', ...
    '+%1.0f\n', ... % Index starting at 1.
    'NUMBER OF SAMPLES:\n', ...
    '%1.0f\n', ... % Number of recorded instances of this marker.
    'LIST OF SAMPLES:\n', ...
    'TRIAL NUMBER		TIME FROM SYNC POINT (in seconds)\n'];

  %   MarkerNoEditHeaderFormat = [ ... % Long version (with bitnumber, polarity, source, threshold).
  %     'CLASSGROUPID:\n', ...
  %     '0\n', ...
  %     'NAME:\n', ...
  %     '%s\n', ... % Marker name.
  %     'COMMENT:\n', ...
  %     'Trigger Marker corresponding to bit %1.0f\n', ... % Bit #
  %     'COLOR:\n', ...
  %     'Red\n', ...
  %     'EDITABLE:\n', ...
  %     '%s\n', ... % Editable
  %     'CLASSID:\n', ...
  %     '+%1.0f\n', ... % Index starting at 1.
  %     'BITNUMBER:\n', ...
  %     '%1.0f\n', ... % Bit #
  %     'POLARITY:\n', ...
  %     '-\n', ...
  %     'SOURCE:\n', ...
  %     'Source is not defined\n', ...
  %     'THRESHOLD:\n', ...
  %     '0.0\n', ...
  %     'NUMBER OF SAMPLES:\n', ...
  %     '%1.0f\n', ... % Number of recorded instances of this marker.
  %     'LIST OF SAMPLES:\n', ...
  %     'TRIAL NUMBER		TIME FROM SYNC POINT (in seconds)\n'];
  MarkerFormat = '%20d\t\t\t\t%+20.12g\n'; % Trial number (from 0), 4 tabs, trial time. '%+20.0f				%+20.5f\n'
  MarkerFooterFormat = '\n\n';
  
  
  % Always make sure there is a backup of original MarkerFile.
  MarkerFile = [WriteDir, filesep, 'MarkerFile.mrk'];
  % In rare cases where the marker file was missing, don't give error to
  % still allow saving.
  if ~exist(MarkerFile, 'file')
    warning('No marker file found, can''t back it up.');
  else
    BackupOriginal(MarkerFile);
  end
  
  % Create the new file with time stamp.
  MarkerFileStamped = [MarkerFile(1:end-4), '_', datestr(now, 30), '.mrk'];
  fid = fopen(MarkerFileStamped, 'w', 'ieee-be'); % If using text mode 'wt' on Windows, '\n' will be replaced by '\r\n'!
  if fid<0
    error('Can''t write to file %s\n', MarkerFileStamped);
  end
  
  % Write file header.
  fprintf(fid, FileHeaderFormat, Dataset, nMarkers);
  if ~Quiet
    % Write marker data.
    fprintf('Markers ');
  end
  for m = 1:nMarkers
    if ~Quiet
      fprintf('%s (%1.0f), ', Markers(m).Name, Markers(m).Count);
    end
    Ed = 'Yes';
    fprintf(fid, MarkerHeaderFormat, Markers(m).Name, Markers(m).Name, ...
      Markers(m).Bit, Markers(m).Bit, Ed, m, Markers(m).Count);
    if Markers(m).Count > 0
      % Trials are saved 0-indexed; subtract 1.
      Markers(m).Samples(:, 1) = Markers(m).Samples(:, 1) - 1;
      fprintf(fid, MarkerFormat, Markers(m).Samples'); % fprintf prints columnwise.
    end
    fprintf(fid, MarkerFooterFormat);
  end
  fclose(fid);
  
  CopyFile(MarkerFileStamped, MarkerFile);
  if ~Quiet
    fprintf('\n saved in file %s \n and %s.\n\n', MarkerFileStamped, MarkerFile);
  end
  
end
