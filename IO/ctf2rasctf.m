function Coords = ctf2rasctf(Coords)
  % Convert (possibly homogeneous) coordinates from "real" CTF (ALS, cm) to "NIfTI-ized" CTF (RAS, mm).
  %
  % Coords = ctf2rasctf(Coords)
  %
  % Expects and returns row vector coordinates.
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-11-11
  
  VectLength = size(Coords, 2);
  if VectLength < 3 || VectLength > 4
    error('Expecting row vector coordinates (3 or 4 columns).');
  end
  
  % Rotate and scale to mm.
  % This R is the inverse (transpose) of the one in rasctf2ctf.

  R = zeros(VectLength);
  R(1, 2) = 1;
  R(2, 1) = -1;
  R(3, 3) = 1;
  if VectLength == 4
    R(4, 4) = 1;
  end
  
  Coords = Coords * R;
  % Keep the homogeneous part 1, not 10.
  Coords(:, 1:3) = Coords(:, 1:3) * 10;
end