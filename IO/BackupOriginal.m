function FileOriginal = BackupOriginal(File)
  % Make a backup of any file, in the same directory.
  %
  % FileOriginal = BackupOriginal(File)
  %
  % Creates a copy of File with the same name and "_Original" appended
  % before the extension.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-05-31
  
  [Dir, Name, Ext] = fileparts(File);
  FileOriginal = [Dir, filesep, Name, '_Original', Ext];
  if ~exist(FileOriginal, 'file')
    if ~exist(File, 'file')
      error('BackupOriginal:NotFound', 'File not found: %s', File);
    else
      CopyFile(File, FileOriginal);
      fprintf('Backed up original %s file as %s.\n', Ext, FileOriginal);
    end
  end
  
end
