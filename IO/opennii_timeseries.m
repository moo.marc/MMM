function [Data, Time, Coordinates] = opennii_timeseries(NiftiFile)
  % Load time series data from NIfTI file.
  %
  % [Data, Time, Coordinates] = opennii_timeseries(NiftiFile)
  % 
  % Coordinates is 2 row vectors: q-form and s-form coordinates.
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-11-10

  if ~exist(NiftiFile, 'file')
    error('File not found: %s', NiftiFile);
  end
  nii = nifti(NiftiFile);
  
  % Checks that it is a time series.
  if any(nii.dat.dim(1:3) > 1) || nii.dat.dim(1) <= 1
    error('File does not seem to contain time series: %s', NiftiFile);
  end
  
  if nargout > 0
    Data = squeeze(nii.dat(:, :, :, :)); % Haven't verified if squeeze is needed.
  end
  if nargout > 1
    nS = numel(Data);
    Time = nii.timing.toffset + ( (0:(nS-1)) * nii.timing.tspace );
  end
  if nargout > 2
    Coordinates = [nii.mat0 * [1;1;1;1], nii.mat * [1;1;1;1]]';
    Coordinates(:, 4) = [];
  end
  
end