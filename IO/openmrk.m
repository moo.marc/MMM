function [Markers, FileDataset] = openmrk(Dataset, Original, Quiet)
  % Generates a data structure array from MarkerFile.mrk in a CTF dataset.
  %
  % Markers = openmrk(Dataset, Original, Quiet)
  %
  % Each element of the array (each marker) contains the fields: Name, Bit,
  % Count, Samples.  Samples is a (Count x 2) array of marker
  % data which contains trial numbers (indexed from 1) and times (from the
  % start of the trial). It is allowed to give
  % directly a .mrk file instead of a Dataset.
  %
  % Original (open backup marker file) and Quiet (no output on command
  % line) are optional arguments, both false by default.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-20
  
  % This field was unnecessary, just make everything editable... [Editable
  % is a boolean that controls whether the marker is editable manually in
  % DataEditor.]
  
  % Parse input arguments.
  if ~exist('Dataset', 'var') || isempty(Dataset)
    Dataset = pwd;
  end
  if ~exist('Original', 'var') || isempty(Original)
    Original = false;
  end
  if ~exist('Quiet', 'var') || isempty(Quiet)
    Quiet = false;
  end
  % Allow specifying the file directly.
  if strcmpi(Dataset(end-3:end), '.mrk')
    MarkerFile = Dataset;
    Dataset = fileparts(Dataset);
  else
    MarkerFile = [Dataset, filesep, 'MarkerFile.mrk'];
  end
  if ~exist(Dataset, 'file') || ~exist(MarkerFile, 'file') || Original
    % See if there was a backup.
    MarkerFileOriginal = [MarkerFile(1:end-4), '_Original.mrk'];
    if exist(MarkerFileOriginal, 'file')
      CopyFile(MarkerFileOriginal, MarkerFile);
      %       if ~copyfile(MarkerFileOriginal, MarkerFile)
      %         error('Copy failed: %s to %s', MarkerFileOriginal, MarkerFile);
      %       end
      if ~Quiet
        fprintf('MarkerFile.mrk restored from %s.\n', MarkerFileOriginal);
      end
    else
      warning('Marker file not found in %s.', Dataset);
      Markers = struct('Name', {}, 'Bit', {}, 'Count', {}, 'Samples', {});
      FileDataset = '';
      return;
    end
  end
  
  % Open file for reading.
  if ~Quiet
    fprintf('Opening marker file: %s\n', MarkerFile);
  end
  fid = fopen(MarkerFile, 'rt', 'ieee-be');
  if (fid == -1)
    error('Failed to open file %s.', MarkerFile);
  end
  
  
  % Prepare string formats.
  FileHeaderFormat = [ ...
    'PATH OF DATASET:\n'];%, ...
  %     '%s\n']; % Dataset name with full path (starting with / on Linux).
  FileHeaderFormat2 = [ ...
    '\n\nNUMBER OF MARKERS:\n', ...
    '%d\n'];%, ... % Number of markers.
  %     '\n\n']; % This brings us to beginning of first CLASSGROUPID line.
  
  % Read file header.  There can be a space in the path or file name so
  % need to use fgetl
  fscanf(fid, FileHeaderFormat, 1); % Finds and goes past that string.
  FileDataset = fgetl(fid);
  %   FileDataset = char(FileDataset'); % Because there were mixed types, it was saved as column of char numbers.
  nMarkers = fscanf(fid, FileHeaderFormat2, 1);
  if isempty(FileDataset) || isempty(nMarkers)
    FileDataset = [];
    nMarkers = 0;
    warning('Error reading marker file header.  Possibly missing header: %s\n', Dataset);
    MissingHeader = true;
    %     Markers = [];
    %     return;
    %     error('Error reading marker file header.  Possibly missing header.');
  else
    MissingHeader = false;
  end
  
  % Read marker data.
  if ~Quiet
    fprintf('Found markers ');
  end
  % Note: no use preallocating the Markers structure since we don't know
  % how many samples we have for each marker.
  %   for m = 1:nMarkers
  m = 1;
  while ~feof(fid)
    Field = fgetl(fid);
    switch Field(1:end-1)
      case 'NAME'
        Line = fgetl(fid);
        Markers(m).Name = sscanf(Line, '%s', 1); %#ok<*AGROW>
      case 'COMMENT'
        Line = fgetl(fid);
        Pattern = 'BitPattern=';
        Index = strfind(Line, Pattern) + length(Pattern);
        if ~isempty(Index)
          Markers(m).Bit = sscanf(Line(Index(1):end), '%i', 1); % Bit # in hexadecimal.
        else
          Pattern = 'bit ';
          Index = strfind(Line, Pattern) + length(Pattern);
          if ~isempty(Index)
            Markers(m).Bit = sscanf(Line(Index(1):end), '%f', 1);
          end
        end
        if ~isfield(Markers, 'Bit') || isempty(Markers(m).Bit) || ...
            ~isnumeric(Markers(m).Bit)
          Markers(m).Bit = 0;
        end
        %       case 'EDITABLE'
        %         Line = fgetl(fid);
        %         Markers(m).Editable = strncmpi(Line, 'Yes', 2);
      case 'BITNUMBER' % Maybe redundant, but this is a proper field, so use it.
        Line = fgetl(fid);
        Markers(m).Bit = sscanf(Line, '%f', 1);
      case 'NUMBER OF SAMPLES'
        Line = fgetl(fid);
        Markers(m).Count = sscanf(Line, '%f', 1);
      case 'LIST OF SAMPLES'
        % Get data
        fgetl(fid); % 'TRIAL NUMBER		TIME FROM SYNC POINT (in seconds)\n'
        Markers(m).Samples = fscanf(fid, '%f', [2, inf])'; % fscanf fills in column order.
        % Trials are indexed from 0 in file, but in CTF programs they are
        % indexed from 1, so add 1 to match what users expect, as well as
        % Matlab indexing.
        if ~isempty(Markers(m).Samples)
          Markers(m).Samples(:, 1) = 1 + Markers(m).Samples(:, 1);
        end
        % Using inf instead of the expected Count brings us past the empty
        % lines, at the start of the next CLASSGROUPID line.  
        %
        % However, if Count is 0, it (sometimes? depending on Matlab
        % version?) also grabs the "C" of that line so we'd need to seek
        % back a character. But when it doesn't grab the "C", seeking back
        % would bring us back on an empty line and break the sequence.  So
        % ignore this for now.
        %         if Markers(m).Count == 0
        %           fseek(fid, -1, 'cof');
        %         end
        if size(Markers(m).Samples, 1) ~= Markers(m).Count
          warning('Number of samples for marker %s doesn''t match expected count.', Markers(m).Name);
          Markers(m).Count = size(Markers(m).Samples, 1);
        end
        if ~Quiet
          fprintf('%s (%1.0f), ', Markers(m).Name, Markers(m).Count);
        end
        m = m + 1;
      case []
        % Safeguard for empty line.  Shouldn't happen, but if it does,
        % don't skip and just move on to the next line.
      otherwise
        fgetl(fid); % Skip other fields.
    end
  end
  
  fclose(fid);
  if ~Quiet
    fprintf('\b\b.\n\n');
  end
  
  if length(Markers) ~= nMarkers
    if MissingHeader
      fprintf('Found %d markers, saving new marker file with header.', numel(Markers));
      savemrk(Markers, Dataset, true);
    else
      warning('Expected %d markers, found %d.', nMarkers, numel(Markers));
    end
  end
  
  
end
