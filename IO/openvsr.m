function [Data, Time] = openvsr(File)
  % Load LCMV "virtual sensor" timeseries from a .vsr file.
  % 
  % [Data, Time] = openvsr(File)
  %
  % These files contain two time series corresponding to the two
  % "tangential" source directions for a specific location. Returns Data
  % with size [nSamples, nTrials, 2], and a column Time vector.
  %
  %
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-02-08
  
  if nargin < 1 || isempty(File) || ~exist(File, 'file')
    error('Vsr file not provided or not found.');
  end
  
  fid = fopen(File, 'rt', 'ieee-be');
  if (fid == -1)
    error('Failed to open file %s.', File);
  end
  Header = fscanf(fid, '%f', 4);
  %   nT = Header(1); % This is not number of trials, maybe number of sensors?
  nS = Header(2);
  nPreTrig = Header(3);
  SampleRate = Header(4);
  
  % 2 columns of data, no time in this file.
  %   Data = load(File);
  Data = fscanf(fid, '%f %f \n', [2, inf]);
  nT = numel(Data)/nS/2;
  Data = reshape(Data', [nS, nT, 2]);
  
  Time = SampleToTime((1:nS)', SampleRate, nPreTrig);
  fclose(fid);

end