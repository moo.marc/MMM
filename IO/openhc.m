function Fiducials = openhc(Dataset, DewarCoords, StandardCoords, ...
    Original, Verbose)
  % Get the MEG fiducial locations (in cm) from a CTF head coil (.hc) file. 
  %
  % Fiducials = openhc(Dataset, DewarCoords, StandardCoords, ...
  %     Original, Verbose)
  %
  % The provided Dataset can be either the dataset directory, or directly
  % the .hc file. Fiducials is a 3x3 matrix where rows are in order:
  % nasion, left ear, right ear, and columns are x, y, z coordinates.
  %
  % Optional inputs (defaults in brackets):
  % DewarCoords [false]: If true, get dewar coordinates instead of the
  % usual CTF head coordinates. 
  % StandardCoords [false]: If true, get standard values instead of the
  % measured coordinates.
  % Original [false]: If true, open the backed up original hc file.
  % Verbose [false]: If true, display information on command line.
  %
  % Fiducials is a 3x3 matrix: [Na_x, y, z; LE_x, y, z; RE_x, y, z]
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-26
  
  if ~exist(Dataset, 'file')
    error('File not found: %s.', Dataset);
  end
  if ~exist('DewarCoords', 'var') || isempty(DewarCoords)
    DewarCoords = false; % Default is to use CTF head coordinates.
  end
  if ~exist('StandardCoords', 'var') || isempty(StandardCoords)
    StandardCoords = false; % Default is to use measured coordinates.
  end
  if ~exist('Original', 'var') || isempty(Original)
    Original = false; % Default is to use current hc file.
  end
  if ~exist('Verbose', 'var') || isempty(Verbose)
    Verbose = false; % Default is to be quiet.
  end
  
  % Allow specifying the file directly.
  if strcmpi(Dataset(end-2:end), '.hc')
    HCFile = Dataset;
    Dataset = fileparts(Dataset);
  else
    [Path, Name, Ext] = fileparts(Dataset); %#ok<ASGLU>
    if strcmpi(Ext, '.ds')
      HCFile = [Dataset, filesep, Name, '.hc'];
    else
      error('Unrecognized dataset: %s.', Dataset);
    end
  end
  if ~exist(Dataset, 'file') || ~exist(HCFile, 'file') || Original
    % See if there was a backup.
    HCFileOriginal = [HCFile(1:end-3), '_Original.hc'];
    if exist(HCFileOriginal, 'file')
      % We cannot restore the file here blindly since the fiducial
      % locations must match with sensor coordinates in the res4 file.  So
      % just try opening it.
      HCFile = HCFileOriginal;
      if ~Original
        warning('%s missing, using backup: %s.', HCFile, HCFileOriginal);
      end
    else
      error('Couldn''t find the head coil file: %s', HCFileOriginal);
    end
  end
  
  % Open file for reading.
  if Verbose
    fprintf('Opening head coil file: %s\n', HCFile);
  end
  fid = fopen(HCFile, 'rt', 'ieee-be');
  if (fid == -1)
    error('Failed to open file %s.', HCFile);
  end

  
  if StandardCoords
    StartPattern = 'standard '; % Some versions of ACQ have a typo on some lines: 'stadard ', so stick to 3 char for comparison below.
  else % Default.
    StartPattern = 'measured ';
  end
  if DewarCoords
    Pattern = 'dewar'; % 'nasion coil position relative to dewar (cm):';
  else % Default.
    Pattern = 'head'; % 'nasion coil position relative to head (cm):';
  end
  
  NextLine = '';
  while ~strncmpi(NextLine, StartPattern, 3) || ...
      ~any(strfind(NextLine, Pattern))
    NextLine = fgetl(fid);
    % Account for problematic or empty file.
    if strncmp(NextLine, 'Unable to make head coil file.', 6) || feof(fid)
      warning('Head coil file may be empty.');
      Fiducials = [];
      return;
    end
  end

  Fiducials(1, :) = fscanf(fid, '%*s %*s %f', 3); % Nasion.
  fgetl(fid);
  fgetl(fid);
  Fiducials(2, :) = fscanf(fid, '%*s %*s %f', 3); % Left ear.
  fgetl(fid);
  fgetl(fid);
  Fiducials(3, :) = fscanf(fid, '%*s %*s %f', 3); % Right ear.

end
