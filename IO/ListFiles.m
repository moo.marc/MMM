function List = ListFiles(Mask, Type, StrictQ, StrictS)
  % Returns a cell array of file and/or directory names matching a mask.
  %
  %  List = ListFiles(Mask, Type, StrictQ, StrictS)
  %
  % Since Matlab doesn't provide a function for this that is platform
  % independent, recursively search for matching files and folders. The
  % search is case insensitive on Windows, and case sensitive otherwise.
  % All input arguments are optional, default values in brackets below.
  %
  % Mask [*]: String pattern with optional wildcards representing a folder
  % and/or file name.  If the "root" or "drive" (e.g. '/' on Linux or 'c:\'
  % on Windows) is not present at the start of the Mask, it is interpreted
  % as a relative path to the current folder.  UNC network shares on
  % Windows must be given without wildcards, e.g. '\\server\share\*', or
  % they won't be recognized.  Valid wildcards: '?' and '*'.
  %   '?' represents a valid file name character, so not folder separators
  %       or other reserved characters.  See StrictQ below for variations.
  %   '*' represents 0 or more characters and are allowed or not to span
  %       subfolders depending on StrictS (see below).  However, those
  %       after the last slash in the search string are never allowed to
  %       match folder separators. In other words, the last desired slash
  %       must be explicitely given.  This allows for searching for files
  %       on a specific directory "level" (see examples).  
  %
  % Type ['b']: 'd' only return directories, 'f' only return files, 'b'
  % returns both types that match the mask.
  %
  % StrictQ [true]: When true, '?' represents exactly 1 valid file name
  % character.  When false, 0 or 1 character.
  % 
  % StrictS [false]: When true, '*' represents 0 or more characters and is
  % not allowed to span subfolders (i.e. it cannot match the folder
  % separators '/', '\' or ':').  When false, '*' before the last folder
  % separator in the mask can span subfolders, but '*' after the last
  % separator cannot.
  %
  % Examples, with default wildcard options, assuming '/' is the folder
  % separator as in linux:
  %   /moo/*oo*   will match the file:  /moo/boo.ext
  %               and the directory:    /moo/goo
  %               but NOT:              /moo/goo/boo.ext
  %
  %   /moo*/*oo*  will match the 3 above and:   /moo/glub/blub/boo.ext
  %
  %   *           is interpreted as ./* (relative to current directory);
  %               as in the first example, this will not match files in
  %               subdirectories, but */* would.
  %
  % Inspired by wildcardsearch.m by B.C. Hamans and other similar functions
  % found on Matlab File Exchange.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2012-04
  
  % -----------------------------------------------------------------------
  % Verify inputs.
  
  if ~exist('Mask', 'var') || isempty(Mask)
    Mask = '*';
  end
  
  if ~exist('StrictQ', 'var') || isempty(StrictQ)
    StrictQ = true;
  end
  
  if ~exist('StrictS', 'var') || isempty(StrictS)
    StrictS = false;
  end
  
  if ~exist('Type', 'var') || isempty(Type)
    Type = 'b';
  end
  switch Type
    case 'b'
      GetFiles = true;
      GetDirs = true;
    case 'f'
      GetFiles = true;
      GetDirs = false;
    case 'd'
      GetFiles = false;
      GetDirs = true;
    otherwise
      error('Unrecognized "Type", should be ''f'', ''d'', ''b'' or empty.');
  end; clear Type
  
  % -----------------------------------------------------------------------
  % Prepare search.
  
  % Find static part of search directory to use as starting point. 
  BaseDir = fileparts(Mask);
  while ~isdir(BaseDir)
    if isempty(BaseDir)
      % Interpret this as relative path from current directory.
      BaseDir = pwd;
      Mask = [BaseDir, filesep, Mask];
      break
    end
    [BaseDir, Name] = fileparts(BaseDir);
    % Special case for UNC paths: note that fileparts('\\server') returns
    % '\\server' and not '\', but isdir('\\server') returns false.  Make
    % sure we don't get an infinite loop.
    if isempty(Name)
      break
    end
  end
  
  % Add full path if relative.
  if strcmp('.', BaseDir)
    BaseDir = pwd;
    Mask = [BaseDir, Mask(2:end)];
  elseif ~( strncmp(filesep, BaseDir, 1) || ... % neither filesep nor
      ( ispc && ~isempty(regexp(Mask, '^.:\', 'once')) ) ) % (pc and drive)
    BaseDir = [pwd, filesep, BaseDir];
    Mask = [pwd, filesep, Mask];
  % else it starts with filesep or drive, either root or network path, try
  % it as is.
  end
  
  % Translate Mask to a regular expression (regexp).
  NonFileSep = ['[^', regexptranslate('escape', filesep), ']']; %'[^\\/:]'; % Allowed file-name characters (no filesep: / \ :).
  if StrictS
    % '*' cannot span subfolders anywhere, don't match filesep.
    RMask = Mask; clear Mask
    RMask = regexptranslate('escape', RMask);
    %     RMask = strrep(RMask, '\', '\\'); % Escape backslashes.
    %     RMask = strrep(RMask, '.', '\.'); % Escape periods.
    RMask = strrep(RMask, '\*', [NonFileSep, '*']);
  else
    % We want different behavior for '*' before and after the last filesep,
    % so separate.
    [DMask, RMask, Ext] = fileparts(Mask); clear Mask
    % Directory part. '*' can match filesep.
    DMask = regexptranslate('escape', DMask);
    %     DMask = strrep(DMask, '\', '\\'); % Escape backslashes.
    %     DMask = strrep(DMask, '.', '\.'); % Escape periods.
    DMask = strrep(DMask, '\*', '.*'); % 0 or more characters, filesep allowed.
    % File part. '*' cannot span subfolders, don't match filesep.
    RMask = [RMask, Ext]; clear Ext
    RMask = regexptranslate('escape', RMask);
    %     RMask = strrep(RMask, '\', '\\'); % Escape backslashes. Rare linux case...
    %     RMask = strrep(RMask, '.', '\.'); % Escape periods.
    RMask = strrep(RMask, '\*', [NonFileSep, '*']);
    % Put together.
    RMask = [DMask, regexptranslate('escape', filesep), RMask];
  end
  if StrictQ
    RMask = strrep(RMask, '\?', [NonFileSep, '{1}']); % Exactly one file-name character.
  else
    RMask = strrep(RMask, '\?', [NonFileSep, '?']); % Zero or one file-name character.
  end
  RMask = ['^', RMask, '$']; % Force full string match.
  if ispc
    % Respect the case-insensitivity of the file system.
    RMask = ['(?i)', RMask];
  end
  
  % Partial mask for directories.  We are being lazy here and making every
  % explicit subfolder part optional, though those before the first
  % wildcard don't need to be.  
  % Need to escape '\' twice to search for filesep in RMask now.
  EscFileSep = regexptranslate('escape', regexptranslate('escape', filesep));
  % Look for filesep, but not initial or initial double for UNC or Windows
  % drive.  So must be preceded by non-filesep but not initial 'c:'
  % pattern. Also have to avoid finding those we introduced with
  % NonFileSep, so not preceded by '[^'.
  FileSepSearchPattern = ['(?<=[^', EscFileSep, '])', '(?<!\[\^)', ... % preceded by non-filesep, not by '[^'
    '(?(?=\\)(?<!^\(\?i\)\^[a-zA-Z]:))', EscFileSep]; % if '\' then not preceded by starting 'c:' pattern (with our added extra junk), 
  FileSeps = regexp(RMask, FileSepSearchPattern); 
  if isempty(FileSeps)
    % There is only the "root" filesep, which means this is a one level
    % search in the "root" directory.
    DMask = RMask;
  else
    nFS = numel(FileSeps);
    DMask = regexprep(RMask, FileSepSearchPattern, ['(', EscFileSep]);
    DMask = [DMask(1:end-1), repmat(')?', [1, nFS]), '$'];
  end
    
  % -----------------------------------------------------------------------
  % Recursively find all matching directories, and list matching files in
  % each.
  List = {};
  List = [List; RecursiveFileFind(BaseDir)];
  
  
  
  function List = RecursiveFileFind(BaseDir)
    List = {};
    LevelList = dir(BaseDir);
    LevelList = {LevelList(:).name}';
    for f = 1:length(LevelList)
      if ~ismember(LevelList{f}, {'.', '..'})
        LevelList{f} = [BaseDir, filesep, LevelList{f}];
        if ~isempty(regexp(LevelList{f}, RMask, 'once'))
          if (isdir(LevelList{f}) && GetDirs) || ...
              (~isdir(LevelList{f}) && GetFiles)
            % Add this to the output list.
            List = [List; LevelList{f}]; %#ok<*AGROW>
          end
        elseif isdir(LevelList{f}) && ~isempty(regexp(LevelList{f}, DMask, 'once'))
          % If we don't want special treatment of * after the last filesep,
          % i.e. the last star can include subdirectories, this 'elseif' by
          % 'end if'. Enter the partially matched directory.
          List = [List; RecursiveFileFind(LevelList{f})];
        end
      end
    end
  end
  
  
end

  % Previous methods that were inconsistent between platforms and/or Matlab
  % versions.
  
  % 1.
  %   List = dir(Mask);
  %   List = {List(:).name}';
  
  % 2.
  %   if isunix
  %     % On Linux, need to use '-d' to avoid listing directory contents.
  %     List = ls(ListMask, '-d'); % Gives a single row, files separated by space.
  %   else
  %     List = ls(ListMask); % Gives a character array.
  %     List(:,end+1) = ' '; % On Windows, necessary to add space after longest line (other lines are padded).
  %   end
  %   List = textscan(List', '%s'); % textscan reads column-wise and returns a single cell containing a cell array.
  %   List = sort(List{1});
