function Status = CopyDs(Dataset, NewDs, Anonymize, Overwrite, Full)
  % Load and save a dataset to current CTF format, possibly removing patient info.
  %
  % Status = CopyDs(Dataset, NewDs, Anonymize, Overwrite, Full)
  %
  % Anonymize [false by default]: if true, or positive number, will remove
  % any information entered by users, potentially containing sensitive or
  % personal information.  If a negative number (and Full is true), will
  % not anonymize the head zeroing sub-datasets, which in theory should not
  % contain any personal information.  (This last option is because the
  % head zeroing .acq file is needed to calculate the head position, and it
  % seems the .acq files from the clinical system have bugs and Matlab
  % can't read or write them properly, leading to failure of head position
  % calculation.)
  %
  % Full [true by default]: if false, will ignore head zeroing
  % sub-datasets, i.e. they are not copied.
  %
  % Status (optional) is 1 for success, 0 for failure (following Matlab's
  % convention for file operations).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-09
  
  if nargin < 2
    error('Not enough input arguments.');
  end
  if nargin < 3 || isempty(Anonymize)
    Anonymize = false;
    AnonymizeHz = false;
  elseif Anonymize < 0
    Anonymize = true;
    AnonymizeHz = false;
  elseif Anonymize
    Anonymize = true;
    AnonymizeHz = true;
  end
  if nargin < 4 || isempty(Overwrite)
    Overwrite = false;
  end
  if nargin < 5 || isempty(Full)
    Full = true; % Always attempt to keep as much of the data and info as possible.
  end
  
  if exist(NewDs, 'file') 
    if Overwrite
      rmdir(NewDs, 's');
    else
      if Anonymize
        fprintf('Not overwriting existing dataset.  Anonymization not confirmed.\n');
      end
      Status = 1; % Still considered success, following desired options...
      return;
    end
  end
  
  % Convert.
  ds = readCTFds(Dataset);
  FullData = getCTFdata(ds, [], [], 'fT', 'single');
  
  % Anonymize
  if Anonymize
    ds = AnonymizeDs(ds, NewDs);
    % Attempt to fix .acq bug in clinical datasets.
    ds = FixAcqBugDs(ds);
  end
  
  % Create new dataset.
  writeCTFds(NewDs, ds, FullData);
  
  % Attempt to fix .acq bug in clinical datasets.
  % Just copy when not anonymizing.  Needed for head position calculation.
  if ~Anonymize
    [~, DsName] = fileparts(Dataset);
    [~, NewDsName] = fileparts(NewDs);
    CopyFile(fullfile(Dataset, [DsName, '.acq']), fullfile(NewDs, [NewDsName, '.acq']));
  end
  
  % Verify if successful.
  if exist(NewDs, 'file')
    Status = 1;
  else
    Status = 0;
    return;
  end
  
  if Full
    % Also copy head localization sub-datasets.
    HeadDs = ListFiles(fullfile(Dataset, '*.ds'), 'd');
    for h = 1:numel(HeadDs)
      [~, Name] = fileparts(HeadDs{h});
      Status = CopyDs(HeadDs{h}, fullfile(NewDs, [Name, '.ds']), ...
        AnonymizeHz, Overwrite, false);
    end
    
    % Possibly missing: VirtualChannel, BadChannel, default.de, *.avg (data
    % selector parameters?), .dsc?
    
    % Just copy all other files, if Anonymize is false.
    %     if ~Anonymize
    %       % TO DO
    %     end
  end
end


function ds = AnonymizeDs(ds, NewDs)
  % Empty all fields that can contain information entered by the user.
  [~, NewName] = fileparts(NewDs);
  
  if isfield(ds, 'baseName')
    ds.baseName = NewName; % Would probably be changed anyway by writeCTFds.
  end
  
  if isfield(ds, 'res4')
    for F = {'nf_run_name', 'nf_collect_descriptor', 'nf_run_title', ...
        'nf_subject_id', 'nf_operator', 'run_description'}
      if isfield(ds.res4, F{1})
        ds.res4.(F{1}) = ' ';
      end
    end
  end
  
  if isfield(ds, 'infods')
    Names = {'_PATIENT_NAME_FIRST', '_PATIENT_NAME_MIDDLE', ...
      '_PATIENT_NAME_LAST', '_PATIENT_ID', '_PATIENT_INSTITUTE', ...
      '_PROCEDURE_ACCESSIONNUMBER', '_PROCEDURE_TITLE', ...
      '_PROCEDURE_LOCATION', '_DATASET_PROCSTEPTITLE', ...
      '_DATASET_PROCSTEPPROTOCOL', '_DATASET_PROCSTEPDESCRIPTION', ...
      '_DATASET_KEYWORDS', '_DATASET_COMMENTS', '_DATASET_OPERATORNAME'};
    % for i = [5, 6, 7, 8, 13, 19, 20, 27, 38, 39, 40, 45, 46, 47]
    for i = 1:numel(ds.infods)
      if ismember(ds.infods(i).name, Names)
        ds.infods(i).data = ' ';
      end
    end
  end
  
  if isfield(ds, 'acq')
    % With newDs -anon, .acq file still contains other info: patient ID,
    % operator, run title...  But with writeCTFds used here, and if we also
    % empty the genRes field (which contains multiple pieces of
    % information, but isn't read as characters in readCTFds), all that
    % info will be gone.
    
    % .acq file data changes between versions.  Go with names. 
    % for i = [6, 9, 1416] % Research, [6, 9, 10, 11, 12, 629, 630] % Clinical
    Names = {'_genRes', '_run_title', '_subject_id', '_operator', ...
      '_run_description', '_run_purpose', '_keywords', '_institute'}; % 
    for i = 1:numel(ds.acq)
      if ismember(ds.acq(i).name, Names)
        ds.acq(i).data = ' ';
      end
    end
  end
  
  if isfield(ds, 'hist')
    ds.hist = ' ';
  end
  
  % Contains useful info, but should all be redundant.  Will have to
  % confirm if ok to remove this.  (newDs -anon does a better job at
  % cleaning only what is needed, but leaves keywords, as everywhere else.)
  if isfield(ds, 'newds') 
    ds = rmfield(ds, 'newds');
    %     ds.newds = [];
  end
    
end


function   ds = FixAcqBugDs(ds)
  % Somehow doesn't properly read the end of .acq file.  Not much missing
  % (FileVersion, DatasetFiles?), but add missing closing "bracket" at
  % least.
  if sum(ismember({ds.acq.name}, 'WS1_')) - ...
      sum(ismember({ds.acq.name}, 'EndOfParameters')) == 1
    ds.acq(end+1).name = 'EndOfParameters';
  end
end

