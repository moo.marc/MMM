function FindStrInDs(Dataset, SearchStr)
  % Prints all fields and subfields where SearchStr is found in ds structure.
  %
  % Status = FindStrInDs(Dataset, SearchStr)
  %
  % Note that for fields that are arrays (e.g. infods.data), we need to
  % check all elements.
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-12-01
  
  ds = readCTFds(Dataset);
  WhereStrInDs(ds, SearchStr, 'ds');
  
end

function WhereStrInDs(Struct, String, StructName, Index)
  if nargin < 3
    VarName = @(x) inputname(1);
    StructName = VarName(Struct);
    if isempty(StructName)
      error('Unable to find variable name of struct.');
    end
  end
  if nargin < 4
    Index = '0';
  end

  F = fieldnames(Struct);
  for f = 1:numel(F)
    if strcmpi(String, 'ListAllFields')
      fprintf([StructName, '.', F{f}, ' \n']);
    end
    for i = 1:numel(Struct)
      if isstruct(Struct(i).(F{f}))
        WhereStrInDs(Struct(i).(F{f}), String, [StructName, '.', F{f}], ...
          [Index, '(', num2str(i), ').', num2str(f)]);
      elseif ischar(Struct(i).(F{f})) || iscellstr(Struct(i).(F{f}))
        if any(strfind(Struct(i).(F{f})(:)', String))
          fprintf([Index, '.' num2str(f), ': ', StructName, ...
            '(', num2str(i), ').', F{f}, ' = ', Struct(i).(F{f})(:)']);
          fprintf('\n'); % Strangely, if in previous fprintf statement, doesn't go to new line after some fields.
        end
      elseif isinteger(Struct(i).(F{f}))
        try
          CharF = char(typecast(swapbytes(Struct(i).(F{f})), 'int8'));
          if any(strfind(CharF(:)', String))
            fprintf([Index, '.' num2str(f), ': ', StructName, ...
              '(', num2str(i), ').', F{f}, ' =(cast) \n']);
            disp(CharF); % fprintf seems to stop at the first character it doesn't like.
          end
        catch
        end
      end % Field type if
    end % Field loop
  end % Element loop
end


% function sf = subfieldnames(ThisStruct)
%   sf = fieldnames(ThisStruct);
%   for fnum = 1:length(sf)
%     if isstruct(ThisStruct.(sf{fnum}))
%       cn = subfieldnames(ThisStruct.(sf{fnum}));
%       sf = cat(1, sf, strcat(sf(fnum), '.', cn));
%       % sf(end+1:end+length(cn)) = cellstr( horzcat(repmat([sf{fnum} '.'], length(cn), 1),char(cn)));
%     end
%   end
% end

