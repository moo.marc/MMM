function nii = nii2ctfnii(NiiFile, NewNiiFile, FidsFile)
  % Create new NIfTI file with CTF_RAS coordinates as both S- and Q-Forms.
  %
  % nii = nii2ctfnii(NiiFile, NewNiiFile, FidsFile)
  %
  % Uses fiducial coordinates manually picked from an MRI NIfTI file to
  % change the real-world coordinates to RAS-CTF-head coordinates, i.e. the
  % CTF-head coodinate system, but rotated 90 degrees in x-y to be in RAS
  % orientation (required by Nifti specification) and in mm.
  % 
  % FidsFile (optional) is the .mat file name where the fiducial
  % coordinates are/will be saved.
  %
  % The (optional) output is the new NIfTI structure, but it is already
  % saved to file.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-06
  
  if ~exist('NiiFile', 'var') || isempty(NiiFile) || ~exist(NiiFile, 'file')
    [fileName, filePath] = ...
      uigetfile({'*.nii; *.NII', 'NIfTI Files (*.nii)'}, 'Locate NIfTI file');
    NiiFile = fullfile(filePath, fileName);
  end
  
  if ~exist('FidsFile', 'var')
    FidsFile = '';
  end
  
  % Structure with fields: VoxelIndex0, VoxelIndex1, SForm, QForm,
  % VoxelSize, VoxelHandedness.
  Fids = GetFiducials(NiiFile, FidsFile); 
  
  % Use 1-indexed voxel coordinates scaled to mm. Although the
  % transformation matrices encoded in the nifti file apply to 0-indexed
  % voxels, the nifti matlab library converts them and presents 1-indexed
  % transformation matrices to the user in nii.mat and nii.mat0.
  [Origin, Rotation] = ChangeCoordinates([], ...
    Fids.VoxelIndex1, Fids.VoxelSize, Fids.VoxelHandedness);
  % This goes from voxels to CTF in mm.
  T = [Rotation * diag(Fids.VoxelSize), - Rotation * Origin'; ...
    zeros(1, 3), 1];
  %   clear Fids
  
  if ~exist('NewNiiFile', 'var') || isempty(NewNiiFile)
    [Path, Name] = fileparts(NiiFile);
    NewNiiFile = [Path, filesep, Name, '_RAS-CTF.nii'];
  end
  
  % From nifti1.h specification:
  %   The (x,y,z) coordinates refer to the CENTER of a voxel.  In methods
  %   2 and 3, the (x,y,z) axes refer to a subject-based coordinate system,
  %   with
  %   +x = Right  +y = Anterior  +z = Superior.
  %   This is a right-handed coordinate system.  However, the exact direction
  %   these axes point with respect to the subject depends on qform_code
  %   (Method 2) and sform_code (Method 3).
  % This can be interpreted to mean that whatever the coordinate system
  % chosen, it must be approximately RAS.
  
  % Convert transformation to "RAS-CTF-head" coordinates.
  % 90 degree rotation in x-y.
  R = zeros(4);
  R(1, 2) = -1;
  R(2, 1) = 1;
  R(3, 3) = 1;
  R(4, 4) = 1;
  
  T = R * T;
  
  % Write nifti file.
  
  % niftimatlib does not support changing units.  xyzt_units is always 10:
  % mm, s.
  
  % Consider incorporating .nii output in ft_volumewrite.m
  % Or use spm's functions? (lacking too... only minc2nifti)
  
  nii = nifti(NiiFile);
  
  Vol = nii.dat(:, :, :);
  nii.dat.fname = NewNiiFile;
  % sform (12-parameter affine transform)
  nii.mat = T;
  nii.mat_intent = 'Aligned';
  % qform (9-parameter affine transform)
  nii.mat0 = T;
  nii.mat0_intent = 'Aligned';
  nii.descrip = 'NIFTI-1 file converted with nii2ctfnii.m';
  nii.intent.code = 'NONE'; % Not statistical data.
  
  create(nii); % Create new file and write header info.
  % This is needed othewise the volume data is not written to the new file.
  nii.dat(:,:,:) = Vol; % Write data to file.

end
















