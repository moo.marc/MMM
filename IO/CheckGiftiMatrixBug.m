function gii = CheckGiftiMatrixBug(gii)
  % Verify, warn and compensate for GIfTI matlab library coordinate matrix bug.
  %
  % gii = CheckGiftiMatrixBug(gii)
  %
  % gii input and output are gifti class objects.
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-26

  if any(gii.mat(4, 1:3) ~= 0)
    warning(['There was a bug in the GIfTI matlab library, which is included in SPM.  ', ...
      'Either update it or make sure the fixed version has higher priority in your path.  ', ...
      'Attempting to compensate.']);
    gii.mat = gii.mat';
  end
end