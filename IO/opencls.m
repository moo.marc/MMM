function [Class, FileDataset] = opencls(Dataset, Original, Verbose)  
  % Load the trial class information from a CTF dataset.
  %
  % Class = opencls(Dataset, Original, Verbose)
  %
  % Generates a data structure array from ClassFile.cls in Dataset.  Each
  % element of the array (each trial class) contains the fields: Name, Id,
  % Count, Trials.  Trials is a vector of trial numbers, indexed from 1, of
  % length Count. It is allowed to give directly a .cls file instead of a
  % Dataset.
  %
  % Original [default false]: If true, opens the original class file, if it
  % was previously modified and backed up, AND overwrites the current class
  % file with the original.
  %
  % Verbose [default true]: Output information to the command window.
  %
  % FileDataset: Optional output argument to capture the dataset that was
  % named in the cls file.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-03-25

  % Parse input arguments.
  if nargin < 1 || isempty(Dataset)
    Dataset = pwd;
  end
  if nargin < 2 || isempty(Original)
    Original = false;
  end
  if nargin < 3 || isempty(Verbose)
    Verbose = false;
  end
  
  % Allow specifying the file directly.
  if strcmpi(Dataset(end-3:end), '.cls')
    ClassFile = Dataset;
    Dataset = fileparts(Dataset);
  else
    ClassFile = [Dataset, filesep, 'ClassFile.cls'];
  end
  if ~exist(Dataset, 'file') || ~exist(ClassFile, 'file') || Original
    % See if there was a backup.
    ClassFileOriginal = [ClassFile(1:end-4), '_Original.cls'];
    if exist(ClassFileOriginal, 'file')
      CopyFile(ClassFileOriginal, ClassFile);
      if Verbose
        fprintf('ClassFile.cls restored from %s.\n', ClassFileOriginal);
      end
    else
      warning('Class file not found in %s.', Dataset);
      Class = [];
      FileDataset = '';
      return;
    end
  end
  
  % Open file for reading.
  if Verbose
    fprintf('Opening trial class file: %s\n', ClassFile);
  end
  fid = fopen(ClassFile, 'rt', 'ieee-be');
  if (fid == -1)
    error('Failed to open file %s.', ClassFile);
  end
  
  % Prepare string formats.  This makes reading simpler.
  FileHeaderFormat = [ ...
    'PATH OF DATASET:\n', ...
    '%s\n\n\n']; % Dataset name with full path (starting with / on Linux).
  FileHeaderFormat2 = [ ...
    'NUMBER OF CLASSES:\n', ...
    '%d', ... % Number of trial classes.
    '\n\n\n']; % This brings us to beginning of first CLASSGROUPID line.
  
  % Read file header.
  FileDataset = fscanf(fid, FileHeaderFormat);
  nClasses = fscanf(fid, FileHeaderFormat2);
  
  
  % Read marker data.
  if Verbose
    fprintf('Found classes ');
  end
  % Note: no use preallocating the structure since we don't know
  % how many trials we have for each marker.
  c = 1;
  while ~feof(fid)
    Field = fgetl(fid);
    switch Field(1:end-1)
      case 'NAME'
        Line = fgetl(fid);
        Class(c).Name = sscanf(Line, '%s', 1); %#ok<*AGROW>
        %       case 'COMMENT'
        %         Line = fgetl(fid);
      case 'CLASSID'
        Line = fgetl(fid);
        Class(c).Id = sscanf(Line, '%f', 1);
      case 'NUMBER OF TRIALS'
        Line = fgetl(fid);
        Class(c).Count = sscanf(Line, '%f', 1);
      case 'LIST OF TRIALS'
        % Get data
        fgetl(fid); % 'TRIAL NUMBER \n'
        % Trials are indexed from 0 in file, but in CTF programs they are
        % indexed from 1, so add 1 to match what users expect, as well as
        % Matlab indexing.
        Class(c).Trials = 1 + fscanf(fid, '%f', inf); % fscanf fills in column order.
        % Using inf instead of the expected Count brings us past the empty
        % lines, at the start of the next CLASSGROUPID line.  
        %
        % However, if Count is 0, it (sometimes? depending on Matlab
        % version?) also grabs the "C" of that line so we'd need to seek
        % back a character. But when it doesn't grab the "C", seeking back
        % would bring us back on an empty line and break the sequence.  And
        % since we don't care about CLASSGROUPID anyway, ignore this.
        if size(Class(c).Trials, 1) ~= Class(c).Count
          warning('Number of trials for class %s doesn''t match expected count.', Class(c).Name);
          Class(c).Count = size(Class(c).Trials, 1);
        end
        if Verbose
          fprintf('%s (%1.0f), ', Class(c).Name, Class(c).Count);
        end
        c = c + 1;
      case []
        % Safeguard for empty line.  Shouldn't happen, but if it does,
        % don't skip and just move on to the next line.
      otherwise
        fgetl(fid); % Skip other fields.
    end
  end
  
  fclose(fid);
  if Verbose
    fprintf('\b\b.\n\n');
  end
  
  if length(Class) ~= nClasses
    error('Expected %d classes, found %d.', nClasses, length(Class));
  end
  
  
end

