function savehc(Fiducials, Dataset)
  % Save a CTF head coil (.hc) file.
  %
  % savehc(Fiducials, Dataset)
  %
  % Save the 3 MEG fiducial coil locations in a CTF .hc file, in directory
  % Dataset.  Makes a backup copy of original <dataset_name>.hc file as
  % <dataset_name>_Original.hc if it doesn't already exist.  Also saves a
  % copy of the new file with a timestamp:
  % <dataset_name>_yyyymmddThhmmss.hc.
  %
  % Fiducials should be given in cm, as a single vector (row or column):
  %   [Na_x, y, z, LE_x, y, z, RE_x, y, z]
  % or as a 3x3 matrix:
  %   [Na_x, y, z; LE_x, y, z; RE_x, y, z]
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-05-31

  % Parse input arguments.
  if nargin < 1 || isempty(Fiducials)
    error('Missing Fiducials.');
  end
  if nargin < 2 
    error('Missing Dataset.');
  end
  [Path, Name, Ext] = fileparts(Dataset); %#ok<ASGLU>
  % Allow specifying file name and location.
  if strcmpi(Ext, '.hc')
    HCFile = Dataset;
    %     Dataset = Path;
  else
    if ~strcmpi(Ext, '.ds')
      error('Unrecognized dataset: %s.', Dataset);
    elseif ~isdir(Dataset)
      error('Dataset not found: %s', Dataset);
    end
    HCFile = [Dataset, filesep, Name, '.hc'];
  end
  if size(Fiducials, 1) == 3
    Fiducials = Fiducials';
  end
  
  % Compute head coodinates.
  HeadFids = reshape(Fiducials, [3,3])';
  HeadFids = ChangeCoordinates(HeadFids, HeadFids)'; % Note the ', so ready for linear indexing.

  % Verify locations are in cm and in dewar coordinates.
  if any(abs(HeadFids(:)) > 15) || ~any(abs(HeadFids(:)) > 4)
    error('Fiducials should be given in cm.');
  elseif any(Fiducials([3, 6, 9]) > -15) || Fiducials(2) == 0
    error('Fiducials should be given in dewar coordinates.');
  end
  
  % Put all coordinates together in 3x9 matrix: standard, dewar, head.
  Fiducials = [5.65685, 5.65685, -27, -5.65685, 5.65685, -27, 5.65685, -5.65685, -27; ...
    Fiducials(:)'; HeadFids(:)'];
  
  % Prepare string formats.
  HcFormat = ['%s %s coil position relative to %s (cm):\n', ...
  	'\tx = %7.5f\n', '\ty = %7.5f\n', '\tz = %7.5f\n'];
  Type = {'standard', 'measured', 'measured'};
  System = {'dewar', 'dewar', 'head'};
  CoilName = {'nasion', 'left ear', 'right ear'};

  % Always make sure there is a backup of original hc file, if it exists.
  if exist(HCFile, 'file')
    BackupOriginal(HCFile);
  end
  
  % Create the new file with time stamp.
  HCFileStamped = [HCFile(1:end-3), '_', datestr(now, 30), '.hc'];
  fid = fopen(HCFileStamped, 'w', 'ieee-be', 'UTF-8'); % If using text mode 'wt' on Windows, '\n' will be replaced by '\r\n'!
  if fid<0
    error('Can''t write to file %s\n', HCFile);
  end
  
  % Write file data.
  for m = 1:numel(Type)
    for c = 1:numel(CoilName)
      fprintf(fid, HcFormat, Type{m}, CoilName{c}, System{m}, ...
        Fiducials(m, 3*(c-1) + (1:3)));
    end
  end
  fclose(fid);
  
  % Copy time stamped file to regular file.
  CopyFile(HCFileStamped, HCFile);
  %   if ~copyfile(HCFileStamped, HCFile)
  %     error('Copy failed: %s to %s', HCFileStamped, HCFile);
  %   end
  fprintf('Fiducial locations saved in file %s \n and %s.\n\n', HCFileStamped, HCFile);
  
end



