function [Sphere, Labels] = openhdm(File)
  % Loads the content of a head model file (with or without header).
  %
  % [Sphere, Labels] = openhdm(File)
  %
  % Positions (Sphere.Center and Sphere.Radius) are in CTF head coordinates
  % (ALS, 'cm'), thus they are associated with a specific MEG dataset.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2009-09-29

  if ~exist(File, 'file')
    error('Didn''t find file %s.', File)
  end
  fid = fopen(File, 'r');
  if (fid < 0)
    error('Failed to open file %s.', File);
  end

  % Verify if header is present.
  FirstWord = fscanf(fid, '%s', 1);
  if any(strfind(FirstWord, 'File')) % File_Info
    % Go to start of sphere data.
    Line = fgetl(fid);
    Words = sscanf(Line, '%s', 2); % Remove white spaces for easier comparison (and only look at first 2 strings).
    while ~strcmp(Words, '//X')
      Line = fgetl(fid);
      Words = sscanf(Line, '%s', 2);
    end
    FileStartPosition = ftell(fid);
  else
    % Rewind
    fseek(fid, 0, 'bof');
    FileStartPosition = 0;
  end
    
  Spheres4 = fscanf(fid, '%*s %f %f %f %f', [4, inf]);
  Spheres4 = Spheres4';
  N = size(Spheres4, 1);
  
  % Read in labels.
  fseek(fid, FileStartPosition, 'bof');
  %   LabelsN = fscanf(fid, '%s %*f %*f %*f %*f', inf);
  %   % This would return a long vector with each character as a number and
  %   % is more complicated to deal with (see commented code below).
  Labels = cell(N, 1);
  Labels{1} = fscanf(fid, '%s', 1);
  for l = 2:N
    Labels{l} = fscanf(fid, '%*s %*s %*s %*s %s', 1); % Skip numbers on previous line.
  end
  
  fclose(fid);
  
  % Convert sphere data to structure.
  Centers = num2cell(Spheres4(:, 1:3), 2);
  [Sphere(1:N).Center] = Centers{:};
  Radiuses = num2cell(Spheres4(:, 4), 2);
  [Sphere(1:N).Radius] = Radiuses{:};
  clear Spheres4
  
  % Remove column character from each label.
  for l = 1:N
    Labels{l} = Labels{l}(1:end-1);
  end
    
end
