function PlotData(Dataset, Trial, Scale, ChannelSubset, Projection)
  % Plot a single trial of MEG data from CTF dataset.
  %
  % PlotData(Dataset, Trial, Scale, ChannelSubset, Projection)
  %
  % Scale [default 500] is in fT. 
  %
  % ChannelSubset is one or two letters in the channel names indicating
  % which region: first letter 'L' (left), 'R' (right) or 'Z' (midline),
  % second letter 'F' (frontal), 'T' (temporal), 'C' (central), 'P'
  % (parietal), 'O' (occipital).
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-02-15
  
  if ~exist('Dataset', 'var') || isempty(Dataset)
    error('No dataset provided.');
  elseif ~exist(Dataset, 'file')
    error('Dataset not found: %s', Dataset);
  end
  
  if ~exist('Projection', 'var') || isempty(Projection)
    DoProj = false;
  else
    DoProj = true;
  end
  
  if ~exist('ChannelSubset', 'var')
    ChannelSubset = '';
  end
  
  if ~exist('Scale', 'var') || isempty(Scale)
    Scale = 500; % 0.5 pT
  end
  
  [Data, Info] = GetMEGData(Dataset, false, false, 'fT', ...
    ChannelSubset);
  
  Data = Data(:, :, Trial);
  
  if DoProj
    %     for t = 1:Info.nTrials
    Data(:, :) = Data(:, :) * Projection';
      %     end
    %     Data = permute(reshape(...
    %       Proj * reshape(permute(Data, [2, 1, 3]), Info.nChan, []), ...
    %       [Info.nChan, Info.nSamplesPerTrial, Info.nTrials]), [2, 1, 3]);
  end
  
  Time = SampleToTime(1:Info.nSamplesPerTrial, Info.SampleRate, Info.nPreTrig)';
  
  %   for s = 1:Info.nSens
  plot(Time, bsxfun(@plus, Data/Scale, 1:Info.nChan), '-k');
  axis tight;
  set(gca, 'YDir', 'reverse', 'YLim', [0, Info.nChan+1]);
  
end

