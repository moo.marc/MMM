function Patch = TopoPlot(B, Dataset)
  % Topoplot using Kavrayskiy VII projection.
  %
  % Patch = Topoplot(B, Dataset)
  %
  % Arguments, all optional [defaults in brackets]:
  % B ["relative distance of sensor to center of 'sensor-sphere'"]: Sensor
  % data to plot.
  %
  % Dataset ["averaged symetric position, noise dataset"]: Dataset from
  % which to get sensor positions.
  %
  % If the output argument Patch is returned, the data is not plotted and
  % the fields faces, vertices, and facevertexcdata (if B is provided) are
  % returned.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-03-16
  
  % TO DO: Save "default dataset" sensor info in mat file.
  
  if nargin < 1
    B = [];
  end
  if nargin < 2 || isempty(Dataset)
    Path = fileparts(which('TemplateInfo.txt'));
    Dataset = fullfile(Path, '0.ds');
    if ~exist(Dataset, 'dir')
      error('Can''t find default dataset.');
    end
    SensorSphere.Center = [-0.2759 0.0089 5.5782];
    SensorSphere.Radius = 10.4488;
  end  
  [~, Info] = GetMEGData(Dataset, true, true);
  %   Info.ChanPos(:, 1:3);
  %   Info.ChanNames{Info.nSens - (0:8)}
  
  % ---------------------------------------------------------------------
  if ~exist('SensorSphere', 'var')
    % Find sphere that's close to sensors without including them.
    % Used for sphere validation.
    SensorSphere.Center = [0, 0, 5]; % mean(SensorLocations, 1);
    SensorSphere.Center = fminsearch(@InsideMeanSquareDist, SensorSphere.Center);
    SensorSphere.Radius = min( sqrt(sum( ...
      bsxfun(@minus, Info.ChanPos(:, 1:3), SensorSphere.Center).^2 , 2)) );
    % Results: Center: [-0.70 0.04 5.43], Radius: 10.39
  end
  
  % Subfunction to fit a sphere to the sensors, while not overlapping them.
  % Need to calculate this everytime in head coordinates, used to constrain
  % the sphere minimization in FictitiousCurrents.  Could be used in other
  % methods too.
  function D = InsideMeanSquareDist(Center)
    Distances = sqrt(sum( bsxfun(@minus, Info.ChanPos(:, 1:3), Center).^2 , 2));
    Rad = min(Distances);
    D = mean((Distances - Rad).^2);
  end
  % ---------------------------------------------------------------------
  
  % Verified all inner coils.
  %   plot3(Info.ChanPos(:, 1), Info.ChanPos(:, 2), Info.ChanPos(:, 3), '.k');
  %   axis square

  % Subtract sphere center, but keep Z positive.
  Info.ChanPos(:, 1:3) = bsxfun(@minus, Info.ChanPos(:, 1:3), ...
    [SensorSphere.Center(1:2), 0]);
  % If midline (Z) sensors have negative Z, they end up near the right or
  % left edges.
  
  % Convert to spherical coordinates, with x, y, z = CTF Z, -Y, X so that
  % "equator" is on ears and vertex, nose at "north pole".
  [r, t, p] = cart2spher(Info.ChanPos(:, 3), -Info.ChanPos(:, 2), Info.ChanPos(:, 1));
  % Latitude = pi/2 - theta;
  % Longitude = phi;
  y = pi/2 - t;
  x = 3/2 .* p .* sqrt(1/3 - (y./pi).^2);
  
  % Verified ok.
  %   plot(x, y, '.k');
  
  % Need a triangulation to plot a patch.
  Tri = delaunay(x, y);
  
  
  if nargout < 1
    if isempty(B)
      B = r/max(r);
    end
    patch('Faces', Tri, 'Vertices', [x, y, zeros(Info.nSens, 1)], ...
      'FaceVertexCData', B, 'FaceColor', 'interp', 'EdgeColor', 'none');
    axis square
    axis tight
    axis off
  else
    Patch.faces = Tri;
    Patch.vertices = [x, y, zeros(Info.nSens, 1)];
    if ~isempty(B)
      Patch.facevertexcdata = B;
    end
  end
  
end







