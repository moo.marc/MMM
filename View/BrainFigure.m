function varargout = BrainFigure(varargin)
  % Generate interpolated and antialiased anatomical and functional overlay figure.
  %
  % [] = BrainFigure('BaseFile', 'file.nii', ...
  %                                    'OverlayFile', 'file.nii')
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-04-04
  
  % Based on NiftiViewer, but GUI and some options not implemented.
  
  %BRAINFIGURE M-file for BrainFigure.fig
  %      BRAINFIGURE, by itself, creates a new BRAINFIGURE or raises the existing
  %      singleton*.
  %
  %      H = BRAINFIGURE returns the handle to a new BRAINFIGURE or the handle to
  %      the existing singleton*.
  %
  %      BRAINFIGURE('Property','Value',...) creates a new BRAINFIGURE using the
  %      given property value pairs. Unrecognized properties are passed via
  %      varargin to BrainFigure_OpeningFcn.  This calling syntax produces a
  %      warning when there is an existing singleton*.
  %
  %      BRAINFIGURE('CALLBACK') and BRAINFIGURE('CALLBACK',hObject,...) call the
  %      local function named CALLBACK in BRAINFIGURE.M with the given input
  %      arguments.
  %
  %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
  %      instance to run (singleton)".
  %
  % See also: GUIDE, GUIDATA, GUIHANDLES
  
  % Edit the above text to modify the response to help BrainFigure
  
  % Last Modified by GUIDE v2.5 29-Oct-2015 15:13:00
  
  % Begin initialization code - DO NOT EDIT
  gui_Singleton = 0;
  gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @BrainFigure_OpeningFcn, ...
    'gui_OutputFcn',  @BrainFigure_OutputFcn, ...
    'gui_LayoutFcn',  [], ...
    'gui_Callback',   []);
  if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
  end
  
  if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
  else
    gui_mainfcn(gui_State, varargin{:});
  end
  % End initialization code - DO NOT EDIT
  
end


% --- Executes just before BrainFigure is made visible.
function BrainFigure_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
  % This function has no output args, see OutputFcn.
  % hObject    handle to figure
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  % varargin   unrecognized PropertyName/PropertyValue pairs from the
  %            command line (see VARARGIN)
  
  % Transparency requires opengl.
  set(hObject, 'Renderer', 'opengl', 'PaperPositionMode', 'auto', ...
    'InvertHardcopy', 'off');
  % PaperPositionMode auto is needed to prevent resizing when saving.
  
  % Create all variables and assign default values, some of which will be
  % evaluated later: those in the EvalDefault structure below which are
  % temporarily assigned '=' here.  (Order doesn't matter here, but try to
  % keep in same order as in Validate structure below.)
  BaseFile = '';
  OverlayFile = '';
  SurfaceFile = ''; % Not implemented yet.
  SaveFile = '';
  Overwrite = false;
  CoordSystem = 'B';
  Coordinates = [];
  BaseContrast = [0.12, 0.88]; % Darker end harder to see, so having a symmetric default probably a good idea.
  OverlayThresh = [-inf, inf];
  OverlayTransp = 0.4;
  FigureWidth = 765; % Pixels
  LeftRight = 'N';
  InvertBaseColors = true;
  InvertOverlayColors = false;
  % Not implemented yet.
  %   OverlayInterpMethod = 'nearest'; % 'nearest' avoids "erosion" or edge artefacts when NaN present, but aliasing.
  ShowDirections = true;
  ShowCrosshair = false;
  CrosshairSize = 0.1; % Relative axis length.
  PrintValues = true;
  if ispc
    WindowFrameSize = 8; % Pixels (trial and error depending on platform...)
  else
    WindowFrameSize = [2, 4];
  end
  
  % Structure of variable validation functions.  If validation is not
  % required, simply use @(x) true. Variables with evaluated defaults must
  % be AFTER other variables they depend on. Because we need a list of
  % variables with specific order (and 'who' returns them sorted), we need
  % all optional variables listed again here.
  ScreenWidth = get(0, 'ScreenSize');
  ScreenWidth = ScreenWidth(3);
  Validate = struct(...
    VarName(BaseFile), @(x) ischar(x) && strcmpi(x(end-3:end), '.nii'), ...
    VarName(OverlayFile), @(x) ischar(x) && strcmpi(x(end-3:end), '.nii'), ...
    VarName(SurfaceFile), @(x) ischar(x) && strcmpi(x(end-3:end), '.gii'), ...
    VarName(SaveFile), @(x) ischar(x), ...
    VarName(Overwrite), @(x) isscalar(x), ...
    VarName(CoordSystem), @(x) ischar(x) && any(strncmpi(x, {'Voxel', 'SForm', 'QForm'}, 1)), ...
    VarName(Coordinates), @(x) isnumeric(x) && numel(x) == 3, ...
    VarName(BaseContrast), @(x) isnumeric(x) && numel(x) == 2 && all(x >= 0 & x <= 1), ...
    VarName(OverlayThresh), @(x) isnumeric(x) && numel(x) == 2, ...
    VarName(OverlayTransp), @(x) isnumeric(x) && isscalar(x), ...
    VarName(FigureWidth), @(x) isnumeric(x) && isscalar(x) && x >= 150 && x <= ScreenWidth, ...
    VarName(LeftRight), @(x) ischar(x) && any(strncmpi(x, {'Neurological', 'Radiological'}, 1)), ...
    VarName(InvertBaseColors), @(x) isscalar(x), ...
    VarName(InvertOverlayColors), @(x) isscalar(x), ...
    VarName(ShowDirections), @(x) isscalar(x), ...
    VarName(ShowCrosshair), @(x) isscalar(x), ...
    VarName(CrosshairSize), @(x) isnumeric(x) && isscalar(x) && x >= 0 && x <= 1, ...
    VarName(PrintValues), @(x) isscalar(x), ...
    VarName(WindowFrameSize), @(x) isnumeric(x) && numel(x)<3 && all(x >= 0 & x <= ScreenWidth) ...
    );
    
  % Not sure we can use struct to pass input arguments in GUIDE GUIs; this
  % accepts both...
  ParseVarargin(varargin, Validate); 
  clear Validate 
  
  if isempty(Coordinates)
    CoordSystem = 'B';
    Coordinates = [1, 1, 1];
  end

  if isempty(BaseFile) || ~exist(BaseFile, 'file')
    [fileName, filePath] = uigetfile({'*.nii; *.NII', 'NIfTI Files (*.nii)'}, 'Select NIfTI file');
    BaseFile = fullfile(filePath, fileName);
  end
  
  % Must have these 2 fields to prepare to hold dialog figure handles.
  handles.figure_CoordinatesDialog = [];
  handles.figure_ImageOptionsDialog = [];
  
  if InvertBaseColors
    handles.Background = [1, 1, 1];
    handles.Foreground = [0, 0, 0];
  else
    handles.Background = [0, 0, 0]; %#ok<*UNRCH>
    handles.Foreground = [1, 1, 1];
  end
  
  handles.Text = [handles.text_Cx1, handles.text_Cx2, handles.text_Sx1, ...
    handles.text_Sx2, handles.text_Ax1, handles.text_Ax2, ...
    handles.text_Ay1, handles.text_Ay2];
  set(handles.Text, 'BackgroundColor', handles.Background, ...
    'ForegroundColor', handles.Foreground);
  if ShowDirections
    set(handles.menu_ViewDirections, 'Checked', 'on');
    set(handles.Text, 'Visible', 'on');
  else
    set(handles.menu_ViewDirections, 'Checked', 'off');
    set(handles.Text, 'Visible', 'off');
  end
  if ShowCrosshair
    set(handles.menu_ViewCrosshair, 'Checked', 'on');
  else
    set(handles.menu_ViewCrosshair, 'Checked', 'off');
  end
  
  Overlay.Thresh = OverlayThresh;
  if Overlay.Thresh(2) <= Overlay.Thresh(1)
    error('OverlayThresh should be 2 values in increasing order, e.g. [-1, 1] or [0, 100].');
  end
  Overlay.Transp = OverlayTransp;
  handles.Radiological = strcmpi(LeftRight(1), 'R');
  % Open file.
  NiiBase = nifti(BaseFile);
  % Also needed later for interpolating the overlay so keep file name.
  handles.BaseFile = BaseFile;

  handles.SaveFile = SaveFile;
  handles.Overwrite = Overwrite;
  if numel(WindowFrameSize) == 1
    WindowFrameSize = WindowFrameSize * [1, 1];
  end
  handles.WindowFrameSize = WindowFrameSize;
  
  % To display images consistently and in a "natural" way (e.g. Superior
  % up).  Use "anatomical" (Coronal, Sagittal, Axial) coordinates, and save
  % volume and transformation matrices in that permuted order.  Then voxel
  % coordinates are only used for displaying in the coordinates pannel
  % anymore.

  % Although the transformation matrices encoded in the nifti file apply to
  % 0-indexed voxels, the nifti matlab library converts them to apply to
  % 1-indexed voxel coordinates (nii.mat, nii.mat0)!
  % For simplicity, let's use and display 1-indexed voxel coordinates here,
  % (as opposed to 0-indexed in NiftiViewer).
  Base.VoxToAnat = [NiiBase.mat(:, 1:3), [0; 0; 0; 1]];
  handles.VoxSize = double(NiiBase.hdr.pixdim(2:4));
  % TO DO: verify VoxSize is same as column norms of transf matrix below.
  for c = 1:3
    Base.VoxToAnat(:, c) = round(Base.VoxToAnat(:, c) ./ norm(Base.VoxToAnat(:, c)));
  end
  if any(sum(abs(Base.VoxToAnat)) ~= 1)
    error('Anatomical orientation difficult to determine from transformation matrix.  Consider doing a rigid coregistration to a template.');
  end
  % Our "Anat" coordinate system is A(R/L)S, with R/L depending on
  % radiological or neurological choice.  Here we start with ARS.
  Base.VoxToAnat = [0, 1, 0, 0; 1, 0, 0, 0; 0, 0, 1, 0; 0, 0, 0, 1] * Base.VoxToAnat;
  % Voxel dimension corresponding to anatomical ones, e.g. iVoxDim(1) is
  % voxel dim for coronal.  Negative numbers means the dimension needs to
  % be flipped.  The number is the Voxel dimension, the position will be
  % the Base dimension.
  iVoxDim = (Base.VoxToAnat(1:3, 1:3) * [1; 2; 3])';
  handles.iVoxDim = abs(iVoxDim);
  handles.iBaseFlipDim = find(iVoxDim < 0);
  handles.VoxSize = handles.VoxSize(handles.iVoxDim);
  % Dim is needed in multiple places so keep in handles.
  handles.Dim = NiiBase.dat.dim(handles.iVoxDim);
  Base.Volume = permute(NiiBase.dat(:, :, :), handles.iVoxDim);
  for d = handles.iBaseFlipDim
    % Add offset for dimensions that get flipped.
    Base.VoxToAnat(d, 4) = handles.Dim(d) + 1;
    Base.Volume = flipdim(Base.Volume, d);
  end
  % Correct left-right flip for radiological orientation.
  if handles.Radiological
    d = 2;
    % May already have been flipped, so this works in either case.
    Base.VoxToAnat(d, :) = [-Base.VoxToAnat(d, 1:3), handles.Dim(d) + 1 - Base.VoxToAnat(d, 4)];
    Base.Volume = flipdim(Base.Volume, d);
    set(handles.text_Cx1, 'String', 'R');
    set(handles.text_Cx2, 'String', 'L');
    set(handles.text_Ax1, 'String', 'R');
    set(handles.text_Ax2, 'String', 'L');
  end
  % Save copy in handles for RefreshCoordinates.
  handles.VoxToAnat = Base.VoxToAnat;
  handles.VoxToS = NiiBase.mat;
  handles.VoxToQ = NiiBase.mat0;
  handles.AnatToVox = inv(handles.VoxToAnat);
  handles.SToVox = inv(handles.VoxToS);
  handles.QToVox = inv(handles.VoxToQ);
  %   Base.VoxToS = NiiBase.mat;
  %   Base.VoxToQ = NiiBase.mat0;
  %   Base.AnatToS = Base.VoxToAnat / Base.VoxToS;
  %   Base.AnatToQ = Base.VoxToAnat / Base.VoxToQ;
  
  % Compute max value for consistent scaling of data.
  %   BaseHist = sort(Base.Volume(:));
  %   nBase = numel(BaseHist);
  %   Base.Range = [BaseHist(1), BaseHist(nBase)];
  Base.Range = [min(Base.Volume(:)), max(Base.Volume(:))];
  %   % Convert percentiles to values.
  %   Base.Contrast = BaseHist(round((nBase-1) * BaseContrast + 1));
  % Convert range proportion to values.
  Base.Contrast = BaseContrast * (Base.Range(2) - Base.Range(1)) + Base.Range(1);
  
  % Convert to [0, 1] range.  (Overflows, but assume it gets clipped.)
  Base.Volume = (Base.Volume - Base.Contrast(1)) ./ (Base.Contrast(2) - Base.Contrast(1));
  
  if InvertBaseColors
    Base.Volume = 1 - Base.Volume;
  end

  % Always need to save changes to guidata/appdata.
  %   guidata(hObject, handles); % Done below.
  setappdata(hObject, VarName(Base), Base);
  setappdata(hObject, VarName(Overlay), Overlay);

  % Set slider ranges and step.  RefreshCoordinates will initialize them.
  set(handles.slider_C, 'Min', 1, 'Max',  handles.Dim(1), ...
    'SliderStep', [1/(handles.Dim(1)-1), 0.1]);
  set(handles.slider_S, 'Min', 1, 'Max', handles.Dim(2), ...
    'SliderStep', [1/(handles.Dim(2)-1), 0.1]);
  set(handles.slider_A, 'Min', 1, 'Max', handles.Dim(3), ...
    'SliderStep', [1/(handles.Dim(3)-1), 0.1]);
  
  % Save B, V, S, Q coordinates of starting point.  It saves guidata, and
  % returns it, so no need to reload.
  handles.Coord.(upper(CoordSystem(1))) = Coordinates(:)';
  handles = RefreshCoordinates(handles, CoordSystem);

  % Set axes ranges: pixel centers are from 1 to n, so display half before
  % and after.
  
  % Issue with DataAspectRatioMode, possibly axes width/height wrong.
  % http://www.mathworks.com/matlabcentral/answers/241766-dataaspectratio-dataaspectratiomode-image-display-bug
  set(handles.axes_Coronal, 'XLim', [0, handles.Dim(2)]+0.5, ...
    'YLim', [0, handles.Dim(3)]+0.5, ... %'DataAspectRatio', [1 1 1], ...
    'DataAspectRatioMode', 'auto', 'XAxisLocation', 'bottom', 'YDir', 'normal', ...
    'NextPlot', 'add'); % 'CLim', Base.Contrast, 
  set(handles.axes_Sagittal, 'XLim', [0, handles.Dim(1)]+0.5, ...
    'YLim', [0, handles.Dim(3)]+0.5, ... %'DataAspectRatio', [1 1 1], ...
    'DataAspectRatioMode', 'auto', 'XAxisLocation', 'bottom', 'YDir', 'normal', ...
    'NextPlot', 'add'); % 'CLim', Base.Contrast, 
  set(handles.axes_Axial, 'XLim', [0, handles.Dim(2)]+0.5, ...
    'YLim', [0, handles.Dim(1)]+0.5, ... %'DataAspectRatio', [1 1 1], ...
    'DataAspectRatioMode', 'auto', 'XAxisLocation', 'bottom', 'YDir', 'normal', ...
    'NextPlot', 'add'); % 'CLim', Base.Contrast, 

  % Initialize volume slice images.  Later only CData is updated.
  Slice = max(0, min(1, squeeze(Base.Volume(handles.Coord.B(1), :, :))'));
  handles.image_C = image('Parent', handles.axes_Coronal, ...
    'CData', Slice(:, :, [1, 1, 1]), ...
    'XData', [1 handles.Dim(2)], ...
    'YData', [1 handles.Dim(3)], ...
    'Tag', 'image_C', 'HitTest', 'off');
  %     'CDataMapping', 'Scaled', ...
  % XData and YData are positions of centers of first and last pixels.
  Slice = max(0, min(1, squeeze(Base.Volume(:, handles.Coord.B(2), :))'));
  handles.image_S = image('Parent', handles.axes_Sagittal, ...
    'CData', Slice(:, :, [1, 1, 1]), ...
    'XData', [1 handles.Dim(1)], ...
    'YData', [1 handles.Dim(3)], ...
    'Tag', 'image_S', 'HitTest', 'off');
  Slice = max(0, min(1, squeeze(Base.Volume(:, :, handles.Coord.B(3)))));
  handles.image_A = image('Parent', handles.axes_Axial, ...
    'CData', Slice(:, :, [1, 1, 1]), ...
    'XData', [1 handles.Dim(2)], ...
    'YData', [1 handles.Dim(1)], ...
    'Tag', 'image_A', 'HitTest', 'off');

  handles.Axes = [handles.axes_Coronal, handles.axes_Sagittal, handles.axes_Axial];
  axis(handles.Axes, 'off');
  handles.Images = [handles.image_C, handles.image_S, handles.image_A];
  
  % Set the color map for overlay (not base image, but set here once).
  handles.nColors = SetColormap(InvertOverlayColors);
  set(hObject, 'Color', handles.Background);
  
  % Constant used for matching coordinate of GIfTI surface with NIfTI
  % volume.
  handles.CoordSystems = {'NIFTI_XFORM_UNKNOWN', 'NIFTI_XFORM_SCANNER_ANAT', ...
    'NIFTI_XFORM_ALIGNED_ANAT', 'NIFTI_XFORM_TALAIRACH', 'NIFTI_XFORM_MNI_152'};
  % #define NIFTI_XFORM_UNKNOWN      0 /*! Arbitrary coordinates (Method 1). */
  % #define NIFTI_XFORM_SCANNER_ANAT 1 /*! Scanner-based anatomical coordinates */
  % #define NIFTI_XFORM_ALIGNED_ANAT 2 /*! Coordinates aligned to another file's,
  % #define NIFTI_XFORM_TALAIRACH    3 /*! Coordinates aligned to Talairach-
  % #define NIFTI_XFORM_MNI_152      4 /*! MNI 152 normalized coordinates. */
  handles.BaseSystems = [NiiBase.hdr.qform_code, NiiBase.hdr.sform_code] + 1;

  % Inform of intents and units.
  handles.BaseUnits = SpaceUnits(NiiBase);
%   set(handles.uipanel_s, 'Title', ['SForm (', SpaceUnits(NiiBase), ') ', NiiBase.mat_intent]);
%   set(handles.uipanel_q, 'Title', ['QForm (', SpaceUnits(NiiBase), ') ', NiiBase.mat0_intent]);
  
  % Write file name, now in title bar.
  %   set(handles.text_File, 'String', NiiFile);
  set(handles.figure_BrainFigure, 'Name', BaseFile);
  
  % No longer need to change aspect ratio.  We interpolate to pixel
  % precision instead.
  % Adjust aspect ratio.  Setting a radio button programmatically (through
  % button or buttongroup) respects exclusivity, but it doesn't trigger the
  % group's selection change function.
  %   set(handles.uipanel_AR, 'SelectedObject', handles.radio_S);
  %   Event.EventName = 'SelectionChanged';
  %   Event.OldValue = handles.radio_V;
  %   Event.NewValue = handles.radio_S;
  %   uipanel_AR_SelectionChangeFcn(handles.radio_S, Event, handles);
  %   AR = 1 ./ handles.VoxSize;
  %   set(handles.axes_Coronal, 'DataAspectRatio', AR([2, 3, 1]));
  %   set(handles.axes_Sagittal, 'DataAspectRatio', AR([1, 3, 2]));
  %   set(handles.axes_Axial, 'DataAspectRatio', AR([2, 1, 3]));
  
  % Resize axes to have same physical display size, and figure height to
  % match.
  
  % Make sure we're in pixels.
  if ~strcmpi(get(handles.figure_BrainFigure, 'Units'), 'pixels')
    set(handles.figure_BrainFigure, 'Units', 'pixels');
  end
  if any(~strcmpi(get(handles.Axes, 'Units'), 'pixels'))
    set(handles.Axes, 'Units', 'pixels');
  end
  handles.Sliders = [handles.slider_C, handles.slider_S, handles.slider_A];
  if any(~strcmpi(get(handles.Sliders, 'Units'), 'pixels'))
    set(handles.Sliders, 'Units', 'pixels');
  end
  VolRealSize = handles.Dim .* handles.VoxSize;
  % Calculate sizes, first based on normalized figure width, and then
  % converted to pixels.  [2, 1, 2] are the 3 axes width dimensions.
  %   [MaxRealHeight, iMRH] = max(VolRealSize([1, 3]));
  handles.AxesNormSizes = [VolRealSize, max(VolRealSize([1, 3]))] ./ ...
    sum(VolRealSize([2, 1, 2]));
  % No longer needed.
  %   % Adjust y-axes ranges to match in physical length.
  %   iAdj = setdiff([1, 3], iMRH);
  %   switch iAdj
  %     case 1 % y of Axial plot
  %       AddHeigh = (MaxRealHeight ./ handles.VoxSize(1) - handles.Dim(1));
  %       set(handles.axes_Axial, 'YLim', ...
  %         get(handles.axes_Axial, 'YLim') + [-AddHeigh*2/3, AddHeigh/3]);
  %     case 3 % y of Coronal and Sagittal plots
  %       AddHeigh = (MaxRealHeight ./ handles.VoxSize(3) - handles.Dim(3));
  %       set(handles.axes_Coronal, 'YLim', ...
  %         get(handles.axes_Coronal, 'YLim') + [-AddHeigh*2/3, AddHeigh/3]);
  %       set(handles.axes_Sagittal, 'YLim', ...
  %         get(handles.axes_Sagittal, 'YLim') + [-AddHeigh*2/3, AddHeigh/3]);
  %   end
  
  % Draw crosshair.  Used to correct lengths for aspect ratio, but now we
  % have true pixel axes limits, thus with 1:1 aspect ratio. 
  
  % Z = 1 so it will be above overlay even though overlay gets drawn after.
  % However, changing the colormap would bring the overlay back on top, so
  % force with uistack as well after both are plotted.
  
  % Pick kind of random length here, gets adjusted once we've resized.
  handles.CrossSize = CrosshairSize;
  CrossSize = handles.CrossSize .* handles.Dim(2); % Left-right, probably smallest dim.
  handles.Cross_C = line(...
    'XData', [-0.5, 0.5, NaN, 0, 0] * CrossSize + handles.Coord.B(2), ...
    'YData', [0, 0, NaN, -0.5, 0.5] * CrossSize + handles.Coord.B(3), ...
    'ZData', [1, 1, NaN, 1, 1], ...
    'Parent', handles.axes_Coronal, 'Color', 'red', 'HitTest', 'off');
  handles.Cross_S = line(...
    'XData', [-0.5, 0.5, NaN, 0, 0] * CrossSize + handles.Coord.B(1), ...
    'YData', [0, 0, NaN, -0.5, 0.5] * CrossSize + handles.Coord.B(3), ...
    'ZData', [1, 1, NaN, 1, 1], ...
    'Parent', handles.axes_Sagittal, 'Color', 'red', 'HitTest', 'off');
  handles.Cross_A = line(...
    'XData', [-0.5, 0.5, NaN, 0, 0] * CrossSize + handles.Coord.B(2), ...
    'YData', [0, 0, NaN, -0.5, 0.5] * CrossSize + handles.Coord.B(1), ...
    'ZData', [1, 1, NaN, 1, 1], ...
    'Parent', handles.axes_Axial, 'Color', 'red', 'HitTest', 'off');
  handles.Cross = [handles.Cross_C, handles.Cross_S, handles.Cross_A];
  % Hide if not desired.
  if strcmpi(get(handles.menu_ViewCrosshair, 'Checked'), 'off')
    set(handles.Cross, 'Visible', 'off');
  end

  % Draw overlay if loaded from command line.  Saves guidata.
  if ~isempty(OverlayFile) && exist(OverlayFile, 'file')
    LoadOverlay(OverlayFile, handles);
    set(handles.menu_ViewOverlay, 'Checked', 'on');
    % Need to reload to get changes saved in LoadOverlay.
    handles = guidata(hObject);
  else
    % Need to save!
    guidata(hObject, handles);
  end
  
  % Bring crosshairs back on top of overlay.  Stays on top even if colormap
  % is changed later.
  uistack(handles.Cross_C, 'top');
  uistack(handles.Cross_S, 'top');
  uistack(handles.Cross_A, 'top');

  % ResizeFcn also readjusts axes limits and calls the refresh image
  % function.  Image slices are now properly interpolated (with
  % antialiasing when needed) to pixel precision, and thus differing ranges
  % when resizing.
  FigPos = get(handles.figure_BrainFigure, 'Position');
  % Setting the position does not call the ResizeFcn, but it gets executed
  % as soon as the figure is drawn.  Do it here explicitely anyway for
  % potentially saving automatically.
  set(handles.figure_BrainFigure, 'Position', [FigPos([1, 2]), FigureWidth, FigPos(4)]);
  figure_BrainFigure_ResizeFcn(handles.figure_BrainFigure, struct(), handles);
  
  
  %   % Draw surface if loaded from command line.  Saves guidata again.
  %   if ~isempty(SurfaceFile) && exist(VarName(SurfaceFile), 'file')
  %     LoadGii(SurfaceFile, handles);
  %     handles = guidata(hObject);
  %   end
  
  %   guidata(hObject, handles); 
  % Need to reload if modified in a previous callback and needed again.
  %   handles = guidata(hObject);
  
  % This (and a few changes in CloseRequestFcn and OutputFcn) makes the GUI
  % modal, i.e. execution will not continue until the GUI is closed and the
  % output will be set after the user interacted with the GUI.
  % UIWAIT makes BrainFigure wait for user response (see UIRESUME)
  %   uiwait(handles.figure_BrainFigure);
  
  if PrintValues
    % Print values for verification, in original units.
    Overlay = getappdata(hObject, VarName(Overlay));
    BaseValue = Base.Volume(handles.Coord.B(1), handles.Coord.B(2), handles.Coord.B(3));
    if InvertBaseColors
      BaseValue = 1 - BaseValue;
    end
    BaseValue = BaseValue .* (Base.Contrast(2) - Base.Contrast(1)) + Base.Contrast(1);
    if isfield(Overlay, 'Volume')
      OverlayValue = ...
        (Overlay.Volume(handles.Coord.B(1), handles.Coord.B(2), handles.Coord.B(3)) - 1) ./ ...
        (handles.nColors - 1) .* (Overlay.Thresh(2) - Overlay.Thresh(1)) + ...
        Overlay.Thresh(1);
      fprintf('Base: %1.4f, Overlay: %1.4f\n', BaseValue, OverlayValue);
    else
      fprintf('Base: %1.4f\n', BaseValue);
    end
  end
  
  if ~isempty(handles.SaveFile)
    menu_Save_Callback(handles.menu_Save, [], handles)
    % Close the figure immediately.  It will flash on screen but that is
    % unavoidable and not much of an issue.
    figure_BrainFigure_CloseRequestFcn(hObject, struct(), handles);
  end
  
end % OpeningFcn

% --- Executes when user attempts to close figure_BrainFigure.
function figure_BrainFigure_CloseRequestFcn(hObject, eventdata, handles) %#ok<*INUSD>
  % hObject    handle to figure_BrainFigure (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  %   if isequal(get(hObject, 'waitstatus'), 'waiting')
  %     % The GUI is "in UIWAIT", so "UIRESUME"
  %     uiresume(hObject);
  %   else
  %     % The GUI is no longer waiting.
  %     % Hint: delete(hObject) closes the figure
  %     delete(hObject);
  %   end
  %   if isfield(handles, 'SaveFile') && ~isempty(handles.SaveFile)
  %     if handles.Overwrite || ~exist(handles.SaveFile, 'file')
  %       print(hObject, handles.SaveFile, '-dpng', '-r0')
  %       %       export_fig(handles.SaveFile, '-png', '-nocrop', '-opengl', '-m1', hObject);
  %       %       screencapture(hObject, get(hObject, 'Position'), handles.SaveFile);
  %
  %     else
  %       warning('Image file already exist and Overwrite is false.');
  %     end
  %   end
  
  delete(hObject);
  
end

function varargout = BrainFigure_OutputFcn(hObject, eventdata, handles)
  varargout{1} = hObject; % handles.figure_BrainFigure didn't work somehow.
end

% --- Executes when figure_BrainFigure is resized.
function figure_BrainFigure_ResizeFcn(hObject, eventdata, handles)
  % hObject    handle to figure_BrainFigure (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  if ~isfield(handles, 'AxesNormSizes')
    % This got called before even running OpeningFnc?  Do nothing.
    warning('Unexpected call to resize function with missing info.');
    return;
  end
  
  FigSize = get(hObject, 'Position');
  FigPos = FigSize(1:2);
  FigSize = FigSize(3:4);
  
  % We want pixel precision, so round the sizes here.
  handles.PixDim = round(handles.AxesNormSizes .* FigSize(1));
  set(hObject, 'Position', [FigPos, FigSize(1), handles.PixDim(4)]);
  iX = [2, 1, 2]; % Order of volume dimensions for axes x-axes.
  iY = [3, 3, 1];
  for ih = 1:3
    set(handles.Axes(ih), 'Position', [sum(handles.PixDim(iX(1:(ih-1)))), ...
      0, handles.PixDim(iX(ih)), handles.PixDim(4)]);
  end
  SliderMargin = 27.5; % Pixels
  for ih = 1:3
    Pos = get(handles.Sliders(ih), 'Position');
    set(handles.Sliders(ih), 'Position', [sum(handles.PixDim(iX(1:(ih-1)))) + SliderMargin, ...
      0, handles.PixDim(iX(ih)) - 2 * SliderMargin, Pos(4)]);
  end
  TextArea = [20, 20]; % Pixels
  set(handles.Text(1), 'Position', [0, 0, TextArea]);
  set(handles.Text(2), 'Position', [sum(handles.PixDim(iX(1:1))) - TextArea(1), 0, TextArea]);
  set(handles.Text(3), 'Position', [sum(handles.PixDim(iX(1:1))), 0, TextArea]);
  set(handles.Text(4), 'Position', [sum(handles.PixDim(iX(1:2))) - TextArea(1), 0, TextArea]);
  set(handles.Text(5), 'Position', [sum(handles.PixDim(iX(1:2))), 0, TextArea]);
  set(handles.Text(6), 'Position', [sum(handles.PixDim(iX(1:3))) - 1.5*TextArea(1), 0, TextArea]);
  set(handles.Text(7), 'Position', [sum(handles.PixDim(iX(1:3))) - TextArea(1), TextArea(2), TextArea]);
  set(handles.Text(8), 'Position', [sum(handles.PixDim(iX(1:3))) - TextArea(1), handles.PixDim(4) - TextArea(2), TextArea]);
  
  % Image slices are now properly interpolated (with antialiasing when
  % needed) to pixel precision, and thus differing ranges when resizing.
  MaxPix = max(handles.PixDim(1:3));
  set(handles.axes_Coronal, 'XLim', [0, handles.PixDim(iX(1))]+0.5, ...
    'YLim', [0, handles.PixDim(4)]+0.5 - floor(0.67 * (MaxPix - handles.PixDim(iY(1)))));
  set(handles.axes_Sagittal, 'XLim', [0, handles.PixDim(iX(2))]+0.5, ...
    'YLim', [0, handles.PixDim(4)]+0.5 - floor(0.67 * (MaxPix - handles.PixDim(iY(2)))));
  set(handles.axes_Axial, 'XLim', [0, handles.PixDim(iX(3))]+0.5, ...
    'YLim', [0, handles.PixDim(4)]+0.5 - floor(0.67 * (MaxPix - handles.PixDim(iY(3)))));
  
  % Save new sizes.  Could be recovered from axes limits, but ugly.
  guidata(handles.figure_BrainFigure, handles);
  
  % Refresh all displayed content.
  RefreshImages(handles);
end

% ========================================================================
% Object control functions.

% TO DO
% --- Executes on slider movement.
function slider_C_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
  % hObject    handle to slider_C (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'Value') returns position of slider
  %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
  
  % Round.
  Value = round(get(hObject, 'Value'));
  set(hObject, 'Value', Value);
  % Set corresponding text box.
  set(handles.edit_i, 'String', num2str(Value));
  % Call Callback on that text box to update coordinates and images.
  edit_i_Callback(handles.edit_i, 0, handles);
end

% --- Executes during object creation, after setting all properties.
function slider_C_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to slider_C (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: slider controls usually have a light gray background.
  if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
  end
end

% --- Executes on slider movement.
function slider_S_Callback(hObject, eventdata, handles)
  % hObject    handle to slider_S (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'Value') returns position of slider
  %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
  
  % Round.
  Value = round(get(hObject, 'Value'));
  set(hObject, 'Value', Value);
  % Set corresponding text box.
  set(handles.edit_j, 'String', num2str(Value));
  % Call Callback on that text box to update coordinates and images.
  edit_j_Callback(handles.edit_j, 0, handles);
end

% --- Executes during object creation, after setting all properties.
function slider_S_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to slider_S (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: slider controls usually have a light gray background.
  if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
  end
end

% --- Executes on slider movement.
function slider_A_Callback(hObject, eventdata, handles)
  % hObject    handle to slider_A (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'Value') returns position of slider
  %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
  
  % Round.
  Value = round(get(hObject, 'Value'));
  set(hObject, 'Value', Value);
  % Set corresponding text box.
  set(handles.edit_k, 'String', num2str(Value));
  % Call Callback on that text box to update coordinates and images.
  edit_k_Callback(handles.edit_k, 0, handles);
end

% --- Executes during object creation, after setting all properties.
function slider_A_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to slider_A (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: slider controls usually have a light gray background.
  if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
  end
end


% ========================================================================
% Coordinates figure object callbacks.  These are used by the
% CoordinatesDialog figure.

function edit_i_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_i (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'V');
  RefreshImages(handles, 1);
end

function edit_j_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_j (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'V');
  RefreshImages(handles, 2);
end

function edit_k_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_k (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'V');
  RefreshImages(handles, 3);
end

function edit_x_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_x (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'S');
  RefreshImages(handles);
end

function edit_y_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_x (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'S');
  RefreshImages(handles);
end

function edit_z_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_x (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'S');
  RefreshImages(handles);
end

function edit_xq_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_x (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'Q');
  RefreshImages(handles);
end

function edit_yq_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_x (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'Q');
  RefreshImages(handles);
end

function edit_zq_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_x (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles = RefreshCoordinates(handles, 'Q');
  RefreshImages(handles);
end

% --- Executes when selected object is changed in uipanel_AR.
function uipanel_AR_SelectionChangeFcn(hObject, eventdata, handles)
  % hObject    handle to the selected object in uipanel_AR
  % eventdata  structure with the following fields (see UIBUTTONGROUP)
  %	EventName: string 'SelectionChanged' (read only)
  %	OldValue: handle of the previously selected object or empty if none was selected
  %	NewValue: handle of the currently selected object
  % handles    structure with handles and user data (see GUIDATA)
  
  switch eventdata.NewValue % selected object
    case handles.radio_V
      AR = [1, 1, 1];
    case handles.radio_S
      AR = 1 ./ handles.VoxSize;
    case handles.radio_Q
      AR = 1 ./ handles.VoxSize;
    otherwise
      error('Unknown AR button.');
  end
  set(handles.axes_Coronal, 'DataAspectRatio', AR([2, 3, 1]));
  set(handles.axes_Sagittal, 'DataAspectRatio', AR([1, 3, 2]));
  set(handles.axes_Axial, 'DataAspectRatio', AR([2, 1, 3]));
  
end


% TO DO
% --- Executes on mouse press over axes background.
function axes_Coronal_ButtonDownFcn(hObject, eventdata, handles)
  % hObject    handle to axes_Coronal (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Get mouse coordinates.
  Mouse = get(handles.axes_Coronal, 'CurrentPoint');
  %   fprintf('x %1.1f, y %1.1f\n', Mouse);
  % Set corresponding text boxes.
  set(handles.edit_j, 'String', num2str(Mouse(1, 1, 1)));
  % Call Callback on those text boxes to update coordinates and images.
  edit_j_Callback(handles.edit_j, 0, handles);
  set(handles.edit_k, 'String', num2str(Mouse(1, 2, 1)));
  edit_k_Callback(handles.edit_k, 0, handles);
end

% --- Executes on mouse press over axes background.
function axes_Sagittal_ButtonDownFcn(hObject, eventdata, handles)
  % hObject    handle to axes_Sagittal (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Get mouse coordinates.
  Mouse = get(handles.axes_Sagittal, 'CurrentPoint');
  % Set corresponding text boxes.
  set(handles.edit_i, 'String', num2str(Mouse(1, 1, 1)));
  % Call Callback on those text boxes to update coordinates and images.
  edit_i_Callback(handles.edit_i, 0, handles);
  set(handles.edit_k, 'String', num2str(Mouse(1, 2, 1)));
  edit_k_Callback(handles.edit_k, 0, handles);
end

% --- Executes on mouse press over axes background.
function axes_Axial_ButtonDownFcn(hObject, eventdata, handles)
  % hObject    handle to axes_Axial (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Get mouse coordinates.
  Mouse = get(handles.axes_Axial, 'CurrentPoint');
  % Set corresponding text boxes.
  set(handles.edit_i, 'String', num2str(Mouse(1, 1, 1)));
  % Call Callback on those text boxes to update coordinates and images.
  edit_i_Callback(handles.edit_i, 0, handles);
  set(handles.edit_j, 'String', num2str(Mouse(1, 2, 1)));
  edit_j_Callback(handles.edit_j, 0, handles);
end


% ========================================================================
% Refresh functions.

function handles = RefreshCoordinates(handles, System)
  
  % Get "Base" coordinates: the volume image voxels, as displayed
  % (ALS or ARS, 1-indexed).
  switch upper(System(1))
    % case 'B' % Nothing to do.
    case 'V'
      handles.Coord.B = (handles.VoxToAnat(1:3, :) * [handles.Coord.V, 1]')';
    case 'S'
      handles.Coord.B = (handles.VoxToAnat(1:3, :) * handles.SToVox * [handles.Coord.S, 1]')';
    case 'Q'
      handles.Coord.B = (handles.VoxToAnat(1:3, :) * handles.QToVox * [handles.Coord.Q, 1]')';
  end

  % Round B as it is used as indices.  Also enforce limits.
  handles.Coord.B = round(handles.Coord.B);
  if any(handles.Coord.B < 1 | handles.Coord.B > handles.Dim)
    warning('Coordinates outside volume.');
    handles.Coord.B = min(handles.Dim, max(1, handles.Coord.B));
  end
  
  % Adjust other coordinates.
  handles.Coord.V = (handles.AnatToVox(1:3, :) * [handles.Coord.B, 1]')';
  handles.Coord.S = (handles.VoxToS(1:3, :) * [handles.Coord.V, 1]')';
  handles.Coord.Q = (handles.VoxToQ(1:3, :) * [handles.Coord.V, 1]')';
  
  % Save new coordinates.  We also return the updated structure to avoid
  % always having to load guidata.
  guidata(handles.figure_BrainFigure, handles);
  
  % Adjust sliders.
  set(handles.slider_C, 'Value', handles.Coord.B(1));
  set(handles.slider_S, 'Value', handles.Coord.B(2));
  set(handles.slider_A, 'Value', handles.Coord.B(3));

  if ~isempty(handles.figure_CoordinatesDialog)
    % Voxel coordinates.
    set(handles.edit_i, 'String', sprintf('%1.2f', handles.Coord.V(1)));
    set(handles.edit_j, 'String', sprintf('%1.2f', handles.Coord.V(2)));
    set(handles.edit_k, 'String', sprintf('%1.2f', handles.Coord.V(3)));
    % SForm world coordinates.
    set(handles.edit_x, 'String', sprintf('%1.2f', handles.Coord.S(1)));
    set(handles.edit_y, 'String', sprintf('%1.2f', handles.Coord.S(2)));
    set(handles.edit_z, 'String', sprintf('%1.2f', handles.Coord.S(3)));
    % QForm world coordinates.
    set(handles.edit_xq, 'String', sprintf('%1.2f', handles.Coord.Q(1)));
    set(handles.edit_yq, 'String', sprintf('%1.2f', handles.Coord.Q(2)));
    set(handles.edit_zq, 'String', sprintf('%1.2f', handles.Coord.Q(3)));
    % TO DO incomplete.
    % Also refresh voxel value.
    set(handles.edit_BaseVoxelValue, 'String', sprintf('%1.4f', ...
      Base.Volume(handles.Coord.B(1), handles.Coord.B(2), handles.Coord.B(3))));
  end
  
  % TO DO: Also refresh displayed coordinates and voxel values in main figure.
end % RefreshCoordinates


function RefreshImages(handles, Which)
  if nargin == 1 || isempty(Which) % exist('Which', 'var')
    Which = [1, 2, 3]; % Do all 3.
  end
  % Only refresh plot that changes for speed.
  CrossSize = handles.CrossSize .* min(handles.PixDim(1:3));
  Base = struct();
  Base = getappdata(handles.figure_BrainFigure, VarName(Base));
  Overlay = struct();
  Overlay = getappdata(handles.figure_BrainFigure, VarName(Overlay));
  DoOverlay = isfield(Overlay, 'Volume'); % Otherwise not loaded.
  % Get precise crosshair coordinates.  B was rounded so start from V.
  Coord = (handles.VoxToAnat(1:3, :) * [handles.Coord.V, 1]')';
  if any(Coord < 1 | Coord > handles.Dim)
    Coord = min(handles.Dim, max(1, Coord));
  end
  for d = Which
    switch d
      case 1
        Slice = squeeze(Base.Volume(handles.Coord.B(1), :, :))';
        % Slice dimensions are in [y, x] order, = matrix [row, column].
        iDim = [3, 2];
        if DoOverlay
          OverSlice = squeeze(Overlay.Volume(handles.Coord.B(1), :, :))';
        end
      case 2
        Slice = squeeze(Base.Volume(:, handles.Coord.B(2), :))';
        iDim = [3, 1];
        if DoOverlay
          OverSlice = squeeze(Overlay.Volume(:, handles.Coord.B(2), :))';
        end
      case 3
        Slice = squeeze(Base.Volume(:, :, handles.Coord.B(3)));
        iDim = [1, 2];
        if DoOverlay
          OverSlice = squeeze(Overlay.Volume(:, :, handles.Coord.B(3)));
        end
      otherwise
        error('Bad which.');
    end
    [Slice, PixCoord] = ImageResize(Slice, handles.PixDim(iDim), Coord(iDim));
    Slice = max(0, min(1, Slice));
    set(handles.Images(d), 'CData', Slice(:, :, [1, 1, 1]));
    if DoOverlay
      OverSlice = ImageResize(OverSlice, handles.PixDim(iDim));
      set(handles.ImagesO(d), 'CData', OverSlice, ...
        'AlphaData', (OverSlice >= 1 & OverSlice <= handles.nColors) * Overlay.Transp);
    end
    % Remember Slice coordinates are [y, x].
    set(handles.Cross(d), ...
      'XData', [-0.5, 0.5, NaN, 0, 0] * CrossSize + PixCoord(2), ...
      'YData', [0, 0, NaN, -0.5, 0.5] * CrossSize + PixCoord(1));
  end
  
  %   % Debugging.
  %   for d = 1:3
  %     AxPos{d} = get(handles.Axes(d), 'Position');
  %   end
  %   fprintf('Slices should be: %dx%d, %dx%d, %dx%d; Axes are: %1.1fx%1.1f, %1.1fx%1.1f, %1.1fx%1.1f; Last Slice: %1.1fx%1.1f.\n', ...
  %     handles.PixDim([2, 3, 1, 3, 2, 1]), AxPos{1}([3, 4]), AxPos{2}([3, 4]), AxPos{3}([3, 4]), ...
  %     size(Slice));

end



% Load overlay volume and display.
function LoadOverlay(OverlayFile, handles)
  NiiOver = nifti(OverlayFile);
  if isempty(NiiOver)
    warning('Could not load overlay file: %s', OverlayFile);
    return;
  end
  
  if ~strcmp(SpaceUnits(NiiOver), handles.BaseUnits)
    warning('Overlay has different spatial units than base image.');
    return;
  end
  SysMatch = ([NiiOver.hdr.qform_code, NiiOver.hdr.sform_code] + 1) == ...
    handles.BaseSystems;
  if ~any(SysMatch)
    warning('Overlay has different coordinate systems (q/s-form codes) than base image.');
    set(handles.menu_ViewOverlay, 'Checked', 'off');
    return;
  % We give priority to S-form.
  elseif SysMatch(2)
    handles.Form = 'Sform';
  else % SysMatch(1)
    handles.Form = 'Qform';
  end
  
  RefCoords = RegisterInterpNii(handles.BaseFile, OverlayFile, [], handles.Form);
  Overlay = struct();
  Overlay = getappdata(handles.figure_BrainFigure, VarName(Overlay));
  Overlay.Volume = RegisterInterpVol(NiiOver.dat(:, :, :), RefCoords, ...
    'linear', NaN);
  % interp3 expects x and y inverted (really stupid "feature").  We let it
  % use the automatic grid for Nii, and explicitely supply Y, then X and Z
  % for the RefCoords.
  %     Overlay.Volume = interp3(NiiOver.dat(:, :, :), ...
  %       RefCoords(:, :, :, 2), RefCoords(:, :, :, 1), RefCoords(:, :, :, 3), ...
  %       'nearest', NaN); % Last arg is extrapolation value.
  
  % Transform to "B orientation", i.e. like Base.Volume was.
  Overlay.Volume = permute(Overlay.Volume, handles.iVoxDim);
  for d = handles.iBaseFlipDim
    Overlay.Volume = flipdim(Overlay.Volume, d);
  end
  if handles.Radiological
    d = 2;
    Overlay.Volume = flipdim(Overlay.Volume, d);
  end
  
  % Scale according to thresholds and colormap to get full color range.
  % Then apply full transparency outside the range later.  Min and max
  % ignore NaN.
  Overlay.Range = [min(Overlay.Volume(:)), max(Overlay.Volume(:))];
  % NaN and +-inf are now handled in an acceptable way by ImageResize,
  % though might not be adequate for all situations. Otherwise they used to
  % propagate and erode the image.
  %   Overlay.Volume(isnan(Overlay.Volume(:))) = -Inf;
  Overlay.Thresh = [max(Overlay.Thresh(1), Overlay.Range(1)), ...
    min(Overlay.Thresh(2), Overlay.Range(2))];
  if any(Overlay.Thresh == [-inf, inf])
    error('Select a finite threshold when infinity is present in the data, or colormap scaling cannot work.');
  end
  Overlay.Volume = (Overlay.Volume - Overlay.Thresh(1)) ./ ...
    (Overlay.Thresh(2) - Overlay.Thresh(1)) .* (handles.nColors - 1) + 1;

  Slice = squeeze(Overlay.Volume(handles.Coord.B(1), :, :))';
  handles.image_CO = image('Parent', handles.axes_Coronal, ...
    'CDataMapping', 'Direct', 'CData', Slice, ...
    'AlphaData', (Slice >= 1 & Slice <= handles.nColors) * Overlay.Transp, ...
    'XData', [1, handles.Dim(2)], 'YData', [1, handles.Dim(3)], ...
    'Tag', 'image_CO', 'HitTest', 'off', 'Visible', 'on');
  % AlphaData needs full matrix?.  Can use: alpha(handles.image_CO, 0.5);
  % XData and YData are positions of centers of first and last pixels.
  Slice = squeeze(Overlay.Volume(:, handles.Coord.B(2), :))';
  handles.image_SO = image('Parent', handles.axes_Sagittal, ...
    'CDataMapping', 'Direct', 'CData', Slice, ...
    'AlphaData', (Slice >= 1 & Slice <= handles.nColors) * Overlay.Transp, ...
    'XData', [1, handles.Dim(1)], 'YData', [1, handles.Dim(3)], ...
    'Tag', 'image_SO', 'HitTest', 'off', 'Visible', 'on');
  Slice = squeeze(Overlay.Volume(:, :, handles.Coord.B(3)));
  handles.image_AO = image('Parent', handles.axes_Axial, ...
    'CDataMapping', 'Direct', 'CData', Slice, ...
    'AlphaData', (Slice >= 1 & Slice <= handles.nColors) * Overlay.Transp, ...
    'XData', [1, handles.Dim(2)], 'YData', [1, handles.Dim(1)], ...
    'Tag', 'image_AO', 'HitTest', 'off', 'Visible', 'on');

  handles.ImagesO = [handles.image_CO, handles.image_SO, handles.image_AO];
  
  % Update gui/app data
  guidata(handles.figure_BrainFigure, handles);
  setappdata(handles.figure_BrainFigure, VarName(Overlay), Overlay);
end % LoadOverlay


% Load gifti surface and display intersection on slices.
function LoadGii(GiiFile, handles)
  gii = gifti(GiiFile);
  if isempty(gii)
    warning('Could not load surface file: %s', GiiFile);
    return;
  end
  gii = CheckGiftiMatrixBug(gii);
  
  GiiSystems = [];
  for g = 1:numel(gii.private.data)
    if strfind(gii.private.data{g}.attributes.Intent, 'NIFTI_INTENT_POINTSET')
      GiiSystems = [find(strcmpi(gii.private.data{g}.space.DataSpace, handles.CoordSystems), 1), ...
        find(strcmpi(gii.private.data{g}.space.TransformedSpace, handles.CoordSystems), 1)];
      break;
    end
  end
  if isempty(GiiSystems)
    error('No points found in GIfTI file.');
  end
  
  % Remember that nii transformation matrices give 1-indexed coordinates,
  % thus we subtract 1 after going to voxel coordinates to match
  % 0-indexed display.
  
  g = find(GiiSystems(1) == handles.BaseSystems, 1, 'first');
  if ~isempty(g)
    % Raw data is already transformed it seems, so keep it.
    if numel(g) == 1 && g == 1 % Q-form
      handles.Surface.vertices = [gii.vertices, ...
        ones(size(gii.vertices, 1), 1)] / handles.VoxToQ' - 1;
    else % S-form
      handles.Surface.vertices = [gii.vertices, ...
        ones(size(gii.vertices, 1), 1)] / handles.VoxToS' - 1;
    end
  else
    g = find(GiiSystems(2) == handles.BaseSystems, 1, 'first');
    
    if isempty(g)
      % Assume data is already tranformed and use S-form.
      fprintf('GIfTI surface coordinates (both raw and transformed) have different "intent" codes \nthan NIfTI S- and Q-forms. Assuming it matches S-form without tranformation.\n');
      handles.Surface.vertices = [gii.vertices, ...
        ones(size(gii.vertices, 1), 1)] / handles.VoxToS' - 1;
      
    else
      if numel(g) == 1 && g == 1 % Q-form
        % Only the transformed intent matches, so apply the transformation.
        % The GIfTI format does not specify how the transformation is
        % supposed to be applied T * v or v' * T; but someone on their forums
        % decisively said it was supposed to be like NIfTI: T * v.
        handles.Surface.vertices = [gii.vertices, ...
          ones(size(gii.vertices, 1), 1)] * gii.mat' / handles.VoxToQ' - 1;
      else % S-form (or both)
        handles.Surface.vertices = [gii.vertices, ...
          ones(size(gii.vertices, 1), 1)] * gii.mat' / handles.VoxToS' - 1;
      end
    end
  end
  handles.Surface.vertices(:, 4) = [];
  handles.Surface.faces = gii.faces;
  
  % Calculate 3d intersection curve(s) and draw.
  % Surface coordinates are properly transformed to 0-indexed voxels.
  Plane.Point = handles.Coord.B(1);
  Plane.Normal = [1, 0, 0];
  Curve = SurfaceSlice(handles.Surface, Plane);
  % Lines must have 'HitTest' 'off' to allow clicks to the axes "under"
  % the curves.
  handles.Curve_i = plot(handles.axes_Coronal, ...
    Curve(:, 2), Curve(:, 3), 'Color', [0, 1, 0], 'HitTest', 'off');
  Plane.Normal = [0, 1, 0];
  Curve = SurfaceSlice(handles.Surface, Plane);
  handles.Curve_j = plot(handles.axes_Sagittal, ...
    Curve(:, 1), Curve(:, 3), 'Color', [0, 1, 0], 'HitTest', 'off');
  Plane.Normal = [0, 0, 1];
  Curve = SurfaceSlice(handles.Surface, Plane);
  handles.Curve_k = plot(handles.axes_Axial, ...
    Curve(:, 1), Curve(:, 2), 'Color', [0, 1, 0], 'HitTest', 'off');
  
  % Update handles structure
  guidata(handles.figure_BrainFigure, handles);
end % LoadGii



% This empty function is needed to prevent focus from going to the command
% line when pressing a key with focus on axes.  Two (or more) input
% arguments are required for callback functions.
function EmptyFunction(hObject, eventdata)
end
% Note: both KeyPressFcn and the WindowKeyPressFcn would not normally be
% used if the figure is in any of the "tool modes": pan, zoom, rotate, etc.
% In those modes, the GUI callback functions get replaced by default
% callbacks.  So to make this work we must 1. switch to the desired mode,
% 2. turn off listeners that prevent changing the callbaks again, and 3.
% reset the callbacks to these functions, in that order.
%
% --- Executes on key press with focus on figure_BrainFigure and none of its controls.
function figure_BrainFigure_KeyPressFcn(hObject, eventdata, handles)
  % hObject    handle to figure_BrainFigure (see GCBO)
  % eventdata  structure with the following fields (see FIGURE)
  %	Key: name of the key that was pressed, in lower case
  %	Character: character interpretation of the key(s) that was pressed
  %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
  % handles    structure with handles and user data (see GUIDATA)
  
  % If arrow key on axes, move crosshair (through coordinate text box
  % callback). Call Callback on text box to update coordinates and images.
  %   disp(get(gco, 'Tag'));
  %   disp(get(get(hObject, 'CurrentObject'), 'Tag'));
  %   return;
  switch get(gco, 'Tag')
    case 'axes_Axial'
      switch eventdata.Key
        case 'uparrow'
          set(handles.edit_j, 'String', num2str( ...
            str2double(get(handles.edit_j, 'String')) + 1 ));
          edit_j_Callback(handles.edit_j, 0, handles);
        case 'downarrow'
          set(handles.edit_j, 'String', num2str( ...
            str2double(get(handles.edit_j, 'String')) - 1 ));
          edit_j_Callback(handles.edit_j, 0, handles);
        case 'leftarrow'
          set(handles.edit_i, 'String', num2str( ...
            str2double(get(handles.edit_i, 'String')) - 1 ));
          edit_i_Callback(handles.edit_i, 0, handles);
        case 'rightarrow'
          set(handles.edit_i, 'String', num2str( ...
            str2double(get(handles.edit_i, 'String')) + 1 ));
          edit_i_Callback(handles.edit_i, 0, handles);
        otherwise
          return;
      end
    case 'axes_Coronal'
      switch eventdata.Key
        case 'uparrow'
          set(handles.edit_k, 'String', num2str( ...
            str2double(get(handles.edit_k, 'String')) + 1 ));
          edit_k_Callback(handles.edit_k, 0, handles);
        case 'downarrow'
          set(handles.edit_k, 'String', num2str( ...
            str2double(get(handles.edit_k, 'String')) - 1 ));
          edit_k_Callback(handles.edit_k, 0, handles);
        case 'leftarrow'
          set(handles.edit_j, 'String', num2str( ...
            str2double(get(handles.edit_j, 'String')) - 1 ));
          edit_j_Callback(handles.edit_j, 0, handles);
        case 'rightarrow'
          set(handles.edit_j, 'String', num2str( ...
            str2double(get(handles.edit_j, 'String')) + 1 ));
          edit_j_Callback(handles.edit_j, 0, handles);
        otherwise
          return;
      end
    case 'axes_Sagittal'
      switch eventdata.Key
        case 'uparrow'
          set(handles.edit_k, 'String', num2str( ...
            str2double(get(handles.edit_k, 'String')) + 1 ));
          edit_k_Callback(handles.edit_k, 0, handles);
        case 'downarrow'
          set(handles.edit_k, 'String', num2str( ...
            str2double(get(handles.edit_k, 'String')) - 1 ));
          edit_k_Callback(handles.edit_k, 0, handles);
        case 'leftarrow'
          set(handles.edit_i, 'String', num2str( ...
            str2double(get(handles.edit_i, 'String')) - 1 ));
          edit_i_Callback(handles.edit_i, 0, handles);
        case 'rightarrow'
          set(handles.edit_i, 'String', num2str( ...
            str2double(get(handles.edit_i, 'String')) + 1 ));
          edit_i_Callback(handles.edit_i, 0, handles);
        otherwise
          return;
      end
    otherwise
      return;
  end
  
end


% This would have worked, but there is a bug that clicking on an axes, then
% clicking on a text box, gco still thinks it's on the axis for the first
% button press.  This bug does not happen with KeyPressFcn, so we switched
% back to using that one.
% % --- Executes on key press with focus on figure_BrainFigure or any of its controls.
% function figure_BrainFigure_WindowKeyPressFcn(hObject, eventdata, handles)
%   % hObject    handle to figure_BrainFigure (see GCBO)
%   % eventdata  structure with the following fields (see FIGURE)
%   %	Key: name of the key that was pressed, in lower case
%   %	Character: character interpretation of the key(s) that was pressed
%   %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
%   % handles    structure with handles and user data (see GUIDATA)
%
% end







% --- Executes on button press in checkbox_Surface.
function checkbox_Surface_Callback(hObject, eventdata, handles)
  % hObject    handle to checkbox_Surface (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hint: get(hObject,'Value') returns toggle state of checkbox_Surface
  if get(hObject, 'Value')
    % On
    if ~isfield(handles, 'Curve_i')
      % No surface file loaded, pop-up dialog.
      [fileName, filePath] = uigetfile({'*.gii; *.GII; *.gii.gz', 'GIfTI Files (*.gii)'}, 'Select GIfTI file');
      % Return without error if no file selected.
      if ~exist(fullfile(filePath, fileName), 'file')
        set(hObject, 'Value', 0);
        return;
      end
      LoadGii(fullfile(filePath, fileName), handles);
    else
      % Already drawn before, but not updated while hidden, so recalculate.
      Plane.Point = handles.Coord.B;
      Plane.Normal = [1, 0, 0];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_i, 'XData', Curve(:, 2), 'YData', Curve(:, 3));
      Plane.Normal = [0, 1, 0];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_j, 'XData', Curve(:, 1), 'YData', Curve(:, 3));
      Plane.Normal = [0, 0, 1];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_k, 'XData', Curve(:, 1), 'YData', Curve(:, 2));
      
      set(handles.Curve_i, 'Visible', 'on');
      set(handles.Curve_j, 'Visible', 'on');
      set(handles.Curve_k, 'Visible', 'on');
    end
    
  else %if isfield(handles, 'Curve_i')
    % Should not happen that was checked with no surface, we'd get errors when updating images.
    % Off
    set(handles.Curve_i, 'Visible', 'off');
    set(handles.Curve_j, 'Visible', 'off');
    set(handles.Curve_k, 'Visible', 'off');
  end
  
end


% --------------------------------------------------------------------
function menu_Options_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_Options (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
end

% --------------------------------------------------------------------
function menu_Coordinates_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_Coordinates (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
end

% --------------------------------------------------------------------
function menu_Interactive_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_Interactive (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Activate or deactivate changing coordinates by clicking and sliders.
  if strcmpi(get(hObject, 'Checked'), 'on')
    % Turn off.
    set(hObject, 'Checked', 'off');
    for h = [handles.slider_C, handles.slider_S, handles.slider_A]
      set(h, 'Visible', 'off', 'Enable', 'inactive');
    end
    set(handles.figure_BrainFigure, 'KeyPressFcn', @EmptyFunction);
    for h = [handles.axes_Axial, handles.axes_Sagittal, handles.axes_Coronal]
      set(h, 'ButtonDownFcn', '');
    end
  else
    % Turn on.
    set(hObject, 'Checked', 'on');
    %     for h =
    set([handles.slider_C, handles.slider_S, handles.slider_A], 'Visible', 'on', 'Enable', 'on');
    %     end
    set(handles.figure_BrainFigure, 'KeyPressFcn', @(f, e)figure_BrainFigure_KeyPressFcn(f, e, guidata(f)));
    set(handles.axes_Axial, 'ButtonDownFcn', @(f, e)axes_Axial_ButtonDownFcn(f, e, guidata(f)));
    set(handles.axes_Sagittal, 'ButtonDownFcn', @(f, e)axes_Sagittal_ButtonDownFcn(f, e, guidata(f)));
    set(handles.axes_Coronal, 'ButtonDownFcn', @(f, e)axes_Coronal_ButtonDownFcn(f, e, guidata(f)));
  end

end

% --------------------------------------------------------------------
function menu_View_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_View (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
end

% --------------------------------------------------------------------
function menu_ImageOptions_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_ImageOptions (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
end

% --------------------------------------------------------------------
function menu_ViewOverlay_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_ViewOverlay (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  if strcmpi(get(hObject, 'Checked'), 'on')
    % Turn off.
    set(hObject, 'Checked', 'off');
    set(handles.image_CO, 'Visible', 'off');
    set(handles.image_SO, 'Visible', 'off');
    set(handles.image_AO, 'Visible', 'off');

  else
    % Turn on.
    set(hObject, 'Checked', 'on');
    if ~isfield(handles, 'image_CO')
      % No overlay file loaded, pop-up dialog.
      [fileName, filePath] = uigetfile({'*.nii; *.NII', 'NIfTI Files (*.nii)'}, 'Select NIfTI file');
      OverlayFile = fullfile(filePath, fileName);
      % Return without error if no file selected.
      if ~exist(OverlayFile, 'file')
        set(hObject, 'Checked', 'off');
        return;
      end
      LoadOverlay(OverlayFile, handles);
    else
      %       % Already drawn before, but not updated while hidden, so recalculate.
      %       Plane.Point = handles.Coord.B;
      %       Plane.Normal = [1, 0, 0];
      %       Curve = SurfaceSlice(handles.Surface, Plane);
      %       set(handles.Curve_i, 'XData', Curve(:, 2), 'YData', Curve(:, 3));
      %       Plane.Normal = [0, 1, 0];
      %       Curve = SurfaceSlice(handles.Surface, Plane);
      %       set(handles.Curve_j, 'XData', Curve(:, 1), 'YData', Curve(:, 3));
      %       Plane.Normal = [0, 0, 1];
      %       Curve = SurfaceSlice(handles.Surface, Plane);
      %       set(handles.Curve_k, 'XData', Curve(:, 1), 'YData', Curve(:, 2));
      
      set(handles.image_CO, 'Visible', 'on');
      set(handles.image_SO, 'Visible', 'on');
      set(handles.image_AO, 'Visible', 'on');
    end
  end
  
end

% --------------------------------------------------------------------
function menu_ViewCrosshair_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_ViewCrosshair (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  if strcmpi(get(hObject, 'Checked'), 'on')
    % Turn off.
    set(hObject, 'Checked', 'off');
    set(handles.Cross, 'Visible', 'off');
  else
    % Turn on.
    set(hObject, 'Checked', 'on');
    set(handles.Cross, 'Visible', 'on');
  end
end

% --------------------------------------------------------------------
function menu_ViewDirections_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_ViewDirections (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)

  if strcmpi(get(hObject, 'Checked'), 'on')
    % Turn off.
    set(hObject, 'Checked', 'off');
    set(handles.Text, 'Visible', 'off');
  else
    % Turn on.
    set(hObject, 'Checked', 'on');
    set(handles.Text, 'Visible', 'on');
  end
end

% --------------------------------------------------------------------
function menu_ViewValue_Callback(hObject, eventdata, handles)
  % hObject    handle to menu_ViewValue (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
end


function Units = SpaceUnits(nii)
  Units = bitand(7, nii.hdr.xyzt_units);
  switch Units
    case 0
      Units = '?';
    case 1
      Units = 'm';
    case 2
      Units = 'mm';
    case 3
      Units = '?m';
    otherwise
      Units = '!';
  end
end


function nColors = SetColormap(Invert, nColors)
  if nargin > 1
    warning('Not yet implemented.  Using 128 colors for now.');
  end
  nColors = 128;
%   SpectralMap = [
%          0         0         0
%     0.1020    0.0824    0.0863
%     0.2000    0.1333    0.1569
%     0.3020    0.1490    0.2235
%     0.4000    0.1333    0.3098
%     0.5020    0.0824    0.4314
%     0.6000         0    0.6000
%     0.5765         0    0.6392
%     0.5451         0    0.6784
%     0.5020         0    0.7216
%     0.4549         0    0.7608
%     0.4000         0    0.8000
%     0.3333         0    0.8392
%     0.2627         0    0.8784
%     0.1804         0    0.9216
%     0.0941         0    0.9608
%          0    0.0039    1.0000
%          0    0.1020    0.9804
%          0    0.1922    0.9608
%          0    0.2824    0.9412
%          0    0.3647    0.9216
%          0    0.4471    0.9020
%          0    0.5255    0.8784
%          0    0.5961    0.8588
%          0    0.6667    0.8392
%          0    0.7294    0.8196
%          0    0.7922    0.8000
%          0    0.8353    0.7020
%          0    0.8667    0.5843
%          0    0.9020    0.4549
%          0    0.9333    0.3176
%          0    0.9686    0.1647
%          0    1.0000    0.0039
%     0.1294    0.9882         0
%     0.2549    0.9725         0
%     0.3804    0.9608         0
%     0.5020    0.9490         0
%     0.6196    0.9333         0
%     0.7333    0.9216         0
%     0.7686    0.9176         0
%     0.8000    0.9137         0
%     0.8314    0.9098         0
%     0.8667    0.9059         0
%     0.8980    0.9020         0
%     0.9059    0.8667         0
%     0.9098    0.8275         0
%     0.9137    0.7882         0
%     0.9216    0.7490         0
%     0.9255    0.7098         0
%     0.9294    0.6667         0
%     0.9333    0.6275         0
%     0.9451    0.5412    0.0314
%     0.9569    0.4627    0.0627
%     0.9686    0.3882    0.0980
%     0.9765    0.3216    0.1294
%     0.9882    0.2588    0.1647
%     1.0000    0.2000    0.2000
%     1.0000    0.3137    0.3137
%     1.0000    0.4275    0.4275
%     1.0000    0.5412    0.5412
%     1.0000    0.6588    0.6588
%     1.0000    0.7725    0.7725
%     1.0000    0.8863    0.8863
%     1.0000    1.0000    1.0000];
  
  % Changed so extremities aren't black and white.  This and others are
  % available in ColorMaps.mat.
  GBRMap = [
         0    0.4980         0
         0    0.5145    0.0008
         0    0.5310    0.0016
         0    0.5475    0.0024
         0    0.5639    0.0031
         0    0.5804    0.0039
         0    0.5966    0.1036
         0    0.6129    0.2034
         0    0.6291    0.3031
         0    0.6454    0.4028
         0    0.6616    0.5025
         0    0.6779    0.6022
         0    0.6941    0.7020
    0.0200    0.6447    0.7318
    0.0400    0.5953    0.7616
    0.0600    0.5459    0.7914
    0.0800    0.4965    0.8212
    0.1000    0.4471    0.8510
    0.1200    0.3976    0.8808
    0.1400    0.3482    0.9106
    0.1600    0.2988    0.9404
    0.1800    0.2494    0.9702
    0.2000    0.2000    1.0000
    0.2308    0.2000    1.0000
    0.2615    0.2000    1.0000
    0.2923    0.2000    1.0000
    0.3231    0.2000    1.0000
    0.3538    0.2000    1.0000
    0.3846    0.2000    1.0000
    0.4154    0.2000    1.0000
    0.4462    0.2000    1.0000
    0.4769    0.2000    1.0000
    0.5077    0.2000    1.0000
    0.5385    0.2000    1.0000
    0.5692    0.2000    1.0000
    0.6000    0.2000    1.0000
    0.6364    0.1818    0.9091
    0.6727    0.1636    0.8182
    0.7091    0.1455    0.7273
    0.7455    0.1273    0.6364
    0.7818    0.1091    0.5455
    0.8182    0.0909    0.4545
    0.8545    0.0727    0.3636
    0.8909    0.0545    0.2727
    0.9273    0.0364    0.1818
    0.9636    0.0182    0.0909
    1.0000         0         0
    1.0000    0.1000         0
    1.0000    0.2000         0
    1.0000    0.3000         0
    1.0000    0.4000         0
    1.0000    0.5000         0
    1.0000    0.5556         0
    1.0000    0.6111         0
    1.0000    0.6667         0
    1.0000    0.7222         0
    1.0000    0.7778         0
    1.0000    0.8333         0
    1.0000    0.8889         0
    1.0000    0.9444         0
    1.0000    1.0000         0
    1.0000    1.0000    0.2497
    1.0000    1.0000    0.4993
    1.0000    1.0000    0.7490];
  
  GBRMap = interp1(1:64, GBRMap, [1:0.5:63.5, 63.75, 64]);
  
  if Invert
    GBRMap = flipdim(GBRMap, 1);
  end
  
  colormap(GBRMap);
end


% --------------------------------------------------------------------
function menu_Save_Callback(hObject, eventdata, handles)
% hObject    handle to menu_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
  if isempty(handles.SaveFile)
    [Name, Path] = uiputfile('*.png', 'Save figure');
    handles.SaveFile = fullfile(Path, Name);
  end
  
  % Give the option to change Overwrite to true.
  if ~handles.Overwrite && exist(handles.SaveFile, 'file')
    Overwrite = questdlg('Overwrite existing file?', 'Save figure - overwrite?');
    if isempty(Overwrite) || strcmpi(Overwrite, 'Cancel')
      return;
    elseif strcmpi(Overwrite, 'No')
      handles.Overwrite = false;
    else % 'Yes'
      handles.Overwrite = true;
    end
  end
  
  if handles.Overwrite || ~exist(handles.SaveFile, 'file')
    % Matlab is horrible at saving figures, shame on it!
    % Problems saving the figure:
    % 1. saveas: Resizes.
    % 2. print: Missing text if figure is not visible.
    % 3. print: Bad looking text (no font smoothing) if opengl.  painters
    %    ok but thinner fonts than on screen.
    % 4. export_fig: either aa everything, making crosshair look faint
    %    and transparent, or same as print (opengl or painters issues
    %    depending on whether there is an overlay).
    % 5. screencapture: need figure border.
    
    set(handles.figure_BrainFigure, 'Visible', 'on');
    Pos = get(handles.figure_BrainFigure, 'Position');
    screencapture(handles.figure_BrainFigure, [handles.WindowFrameSize, Pos([3, 4])], handles.SaveFile);
  else
    fprintf('Image file already exist and Overwrite is false.  Figure not saved.\n');
  end
end

