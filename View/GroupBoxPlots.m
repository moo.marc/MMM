function fid = GroupBoxPlots(Data, WhichFig, LegendCondNames, StatFun)
  % Make box and whisker plot to display results from Behavioral.m.
  %
  % fid = GroupBoxPlots(Data, WhichFig, LegendCondNames, StatFun)
  %
  % Data is one of the outputs from Behavioral.m, e.g. RT.
  % WhichFig = 1: subjects, 2: groups, 3: difference (paired) groups.
  % LegendCondNames is a cell array of cell arrays, to name groups,
  % and possibly 2 levels of conditions.
  % StatFun [default: @mean] is a function handle for how to aggregate
  % trial data within individuals, for group figures (WhichFig = 2 or 3).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-11-11
  
  if nargin < 4
    StatFun = @mean;
  end
  
  [nG, nC] = size(Data);
  nC2 = size(Data{1, 1}, 2);
  
  Spacing = 1;
  if WhichFig == 3
    nBars = nC;
  else
    nBars = nC * nC2; % nG will be side by side, for participant or group figures.
  end
  Width = 1/(nBars + Spacing);
  Offset = (0:Width:((nBars-1)*Width)) - ((nBars-1)*Width)/2;
  % 3x3x3 set of colors.
  Color = {[0, 0, 0.5], [0.2, 0, 0.5], [0.5, 0, 0.5]; ...
    [0.5, 0, 0], [0.5, 0.2, 0], [0.5, 0.5, 0]; ...
    [0, 0.5, 0], [0, 0.5, 0.2], [0, 0.5, 0.5]};
  TempCol = rgb2hsv(cell2mat(Color(:)));
  TempCol(:, 3) = min(1, TempCol(:, 3) + 0.2);
  Color(:, :, 2) = reshape(mat2cell(hsv2rgb(TempCol), ones(size(TempCol, 1), 1)), [3, 3]);
  TempCol(:, 3) = min(1, TempCol(:, 3) + 0.2);
  Color(:, :, 3) = reshape(mat2cell(hsv2rgb(TempCol), ones(size(TempCol, 1), 1)), [3, 3]);
  
  nP = zeros(nG, 1);
  for g = 1:nG
    nP(g) = size(Data{g, 1}, 1);
  end
  nPCum = [0; cumsum(nP)];
  
  % Get stat values or do subject figure if that's what was asked for.
  Stat = cell(nG, 1);
  Leg = cell(nC2, nC);
  fid = figure;
  h = zeros(nBars, 1);
  for g = 1:nG
    Five = cell(nP(g), 1);
    Out = cell(nP(g), 1);
    for c = 1:nC
      for c2 = 1:nC2
        for p = 1:nP(g)
          if WhichFig == 1 % Subjects figure.
            [Five{p}, Out{p}] = BoxWhiskerNumbers(Data{g, c}{p, c2}');
            %         if numel(Five{d}) < 3
            %           Stat{g}(c, c2, p) = NaN;
            %         else
            %           Stat{g}(c, c2, p) = Five{d}(3);
            %         end
          else
            Stat{g}(c, c2, p) = StatFun(Data{g, c}{p, c2}');
          end
        end
        if WhichFig == 1 % Subjects figure.
          % Order (bars next to each other)
          o = c2 + nC2*(c-1);
          h(o) = BoxWhiskerPlot(Five, Out, Width, Offset(o)+nPCum(g), Color{g, c, c2});
          hold on
          Leg{c2, c} = sprintf('%s%s', LegendCondNames{3}{c2}, LegendCondNames{2}{c});
        end
      end
    end
    if WhichFig == 1 % Subjects figure.
      legend(h, Leg{:});
      set(gca, 'XMinorTick', 'on');
    end
  end
  
  if WhichFig == 2
    % Group Data plot.
    for g = 1:nG
      for c = 1:nC
        for c2 = 1:nC2
          o = c2 + nC2*(c-1);
          [Five, Out] = BoxWhiskerNumbers(squeeze(Stat{g}(c, c2, :)));
          h(o) = BoxWhiskerPlot(Five, Out, Width, Offset(o)+g-1, Color{g, c, c2});
          hold on
          Leg{c2, c} = sprintf('%s%s', LegendCondNames{3}{c2}, LegendCondNames{2}{c});
        end
      end
    end
    set(gca, 'XTick', 1:nG, 'XTickLabel', LegendCondNames{1});
    legend(h, Leg{:});
    set(gca, 'XMinorTick', 'off');
    
  elseif WhichFig == 3
    % Group Data difference plot.
    if nC2 ~= 2
      error('Contrast only makes sense for 2 paired conditions.');
    end
    for g = 1:nG
      for c = 1:nC
        [Five, Out] = BoxWhiskerNumbers(squeeze( ...
          Stat{g}(c, 1, :) - Stat{g}(c, 2, :) ));
        h(c) = BoxWhiskerPlot(Five, Out, Width, Offset(c)+g-1, Color{g, c, 2});
        hold on
        Leg{1, c} = sprintf('%s', LegendCondNames{3}{c});
      end
    end
    Leg = Leg(1, :);
    title(sprintf('%s - %s', LegendCondNames{3}{1}, LegendCondNames{3}{2}));
    set(gca, 'XTick', 1:nG, 'XTickLabel', LegendCondNames{1});
    legend(h, Leg{:});
    set(gca, 'XMinorTick', 'off');
    
  end
  
  
end


