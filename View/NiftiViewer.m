function varargout = NiftiViewer(varargin)
  % GUI for displaying NIfTI files, coordinates, GIfTI overlay.
  % 
  % [SavedCoordinates, VoxelValue] = NiftiViewer('File', 'file.nii', ...
  %                                    'Surface', 'file.gii')
  %
  % Opens a NIfTI file and displays the data in voxel coordinates (as
  % stored in the file), and gives the world coordinates of the current
  % voxel.  Can also display the outline of a surface (GIfTI format).
  % Input arguments are optional, a file selection dialog will appear if
  % called without arguments.
  %
  % Returns the saved voxel coordinates, in a structure with 3 fields for
  % the 4 coordinate systems: VoxelIndexed0 and VoxelIndexed1 (indices from
  % 0 and 1 respectively), SForm and QForm.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-06
  
  % TO DO: display saved points.  check box already there, but hidden.
  
  %NIFTIVIEWER M-file for NiftiViewer.fig
  %      NIFTIVIEWER, by itself, creates a new NIFTIVIEWER or raises the existing
  %      singleton*.
  %
  %      H = NIFTIVIEWER returns the handle to a new NIFTIVIEWER or the handle to
  %      the existing singleton*.
  %
  %      NIFTIVIEWER('Property','Value',...) creates a new NIFTIVIEWER using the
  %      given property value pairs. Unrecognized properties are passed via
  %      varargin to NiftiViewer_OpeningFcn.  This calling syntax produces a
  %      warning when there is an existing singleton*.
  %
  %      NIFTIVIEWER('CALLBACK') and NIFTIVIEWER('CALLBACK',hObject,...) call the
  %      local function named CALLBACK in NIFTIVIEWER.M with the given input
  %      arguments.
  %
  %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
  %      instance to run (singleton)".
  %
  % See also: GUIDE, GUIDATA, GUIHANDLES
  
  % Edit the above text to modify the response to help NiftiViewer
  
  % Last Modified by GUIDE v2.5 10-Dec-2013 17:13:36
  
  % Begin initialization code - DO NOT EDIT
  gui_Singleton = 0;
  gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @NiftiViewer_OpeningFcn, ...
    'gui_OutputFcn',  @NiftiViewer_OutputFcn, ...
    'gui_LayoutFcn',  [], ...
    'gui_Callback',   []);
  if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
  end
  
  if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
  else
    gui_mainfcn(gui_State, varargin{:});
  end
  % End initialization code - DO NOT EDIT
  
end


% --- Executes just before NiftiViewer is made visible.
function NiftiViewer_OpeningFcn(hObject, eventdata, handles, varargin)
  % This function has no output args, see OutputFcn.
  % hObject    handle to figure
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  % varargin   unrecognized PropertyName/PropertyValue pairs from the
  %            command line (see VARARGIN)
  
  if length(varargin) == 1
    error('File name must be given after the property name, i.e. ''File''');
  elseif length(varargin) < 2
    [fileName, filePath] = uigetfile({'*.nii; *.NII', 'NIfTI Files (*.nii)'}, 'Select NIfTI file');
    NiiFile = fullfile(filePath, fileName);
  else
    v = 1;
    while v < length(varargin)
      switch varargin{v}
        case 'File'
          NiiFile = varargin{v + 1};
        case 'Surface'
          GiiFile = varargin{v + 1};
        otherwise
          error('Unrecognized variable name: %s', varargin{v});
      end
      v = v + 2;
    end
  end
  
  % Open file.
  % Make nifti handle global by adding to guidata (since using GUIDE,
  % that's the handles structure).
  %   handles.nii = [];
  handles.nii = nifti(NiiFile);
  % Compute max value for consistent scaling of data.
  handles.DataRange = [min(handles.nii.dat(:)), max(handles.nii.dat(:))];
  % Always need to save changes to guidata.
  %   guidata(hObject, handles); % Done below.
  
  % Set axes ranges.
  set(handles.axes_jk, 'XLim', [-0.5, handles.nii.dat.dim(2)-0.5], ...
    'YLim', [-0.5, handles.nii.dat.dim(3)-0.5], 'DataAspectRatio', [1 1 1], ...
    'DataAspectRatioMode', 'manual', 'XAxisLocation', 'top', 'YDir', 'normal', ...
    'CLim', [min(0, handles.DataRange(1)), handles.DataRange(2)], 'NextPlot', 'add');
  set(handles.axes_ik, 'XLim', [-0.5, handles.nii.dat.dim(1)-0.5], ...
    'YLim', [-0.5, handles.nii.dat.dim(3)-0.5], 'DataAspectRatio', [1 1 1], ...
    'DataAspectRatioMode', 'manual', 'XAxisLocation', 'top', 'YDir', 'normal', ...
    'CLim', [min(0, handles.DataRange(1)), handles.DataRange(2)], 'NextPlot', 'add');
  set(handles.axes_ij, 'XLim', [-0.5, handles.nii.dat.dim(1)-0.5], ...
    'YLim', [-0.5, handles.nii.dat.dim(2)-0.5], 'DataAspectRatio', [1 1 1], ...
    'DataAspectRatioMode', 'manual', 'XAxisLocation', 'top', 'YDir', 'normal', ...
    'CLim', [min(0, handles.DataRange(1)), handles.DataRange(2)], 'NextPlot', 'add');
  handles.i_range = get(handles.axes_ik, 'XLim');
  handles.j_range = get(handles.axes_jk, 'XLim');
  handles.k_range = get(handles.axes_jk, 'YLim');
  % This is the initial position (start in middle).
  i = round(handles.i_range(2)/2);
  j = round(handles.j_range(2)/2);
  k = round(handles.k_range(2)/2);
  % Set slider ranges and step, and initialize
  set(handles.slider_i, 'Min', 0, 'Max',  handles.nii.dat.dim(1)-1, ...
    'SliderStep', [1/(handles.nii.dat.dim(1)-1), 0.1], 'Value', i);
  set(handles.slider_j, 'Min', 0, 'Max', handles.nii.dat.dim(2)-1, ...
    'SliderStep', [1/(handles.nii.dat.dim(2)-1), 0.1], 'Value', j);
  set(handles.slider_k, 'Min', 0, 'Max', handles.nii.dat.dim(3)-1, ...
    'SliderStep', [1/(handles.nii.dat.dim(3)-1), 0.1], 'Value', k);
  
  % Initialize text boxes.
  set(handles.edit_i, 'String', num2str(i));
  set(handles.edit_j, 'String', num2str(j));
  set(handles.edit_k, 'String', num2str(k));
  
  % Refresh images, coordinates.
  RefreshCoordinates(handles);
  %   RefreshImages(handles);
  % Do initial images here and save image handles for rapid refreshing.
  colormap(gray);
  handles.image_i = image('Parent', handles.axes_jk, ...
    'CDataMapping', 'Scaled', ...
    'CData', squeeze(handles.nii.dat(i+1, :, :))', ...
    'XData', handles.j_range + [0.5, -0.5], ...
    'YData', handles.k_range + [0.5, -0.5], ...
    'Tag', 'image_i', 'HitTest', 'off');
  %     'XData', get(handles.axes_jk, 'XLim'), ...
  %     'YData', get(handles.axes_jk, 'YLim'), ...
  handles.image_j = image('Parent', handles.axes_ik, ...
    'CDataMapping', 'Scaled', ...
    'CData', squeeze(handles.nii.dat(:, j+1, :))', ...
    'XData', handles.i_range + [0.5, -0.5], ...
    'YData', handles.k_range + [0.5, -0.5], ...
    'Tag', 'image_j', 'HitTest', 'off');
  handles.image_k = image('Parent', handles.axes_ij, ...
    'CDataMapping', 'Scaled', ...
    'CData', squeeze(handles.nii.dat(:, :, k+1))', ...
    'XData', handles.i_range + [0.5, -0.5], ...
    'YData', handles.j_range + [0.5, -0.5], ...
    'Tag', 'image_k', 'HitTest', 'off');
  handles.cursor_i = rectangle(...
    'Position', [j-0.5, k-0.5, 1, 1], ...
    'Parent', handles.axes_jk, 'EdgeColor', 'red', 'HitTest', 'off');
  handles.cursor_j = rectangle(...
    'Position', [i-0.5, k-0.5, 1, 1], ...
    'Parent', handles.axes_ik, 'EdgeColor', 'red', 'HitTest', 'off');
  handles.cursor_k = rectangle(...
    'Position', [i-0.5, j-0.5, 1, 1], ...
    'Parent', handles.axes_ij, 'EdgeColor', 'red', 'HitTest', 'off');
  
  %   line(...
  %     [j_range(1), j+1, j, j; ... % x1
  %     j-1, j_range(2), j, j], ... % x2
  %     [k, k, k_range(1), k+1; ... % y1
  %     k, k, k-1, k_range(2)], ... % y2
  %     'Parent', handles.axes_jk, 'Color', 'red');

  % Constant used for matching coordinate of GIfTI surface with NIfTI
  % volume.
  handles.CoordSystems = {'NIFTI_XFORM_UNKNOWN', 'NIFTI_XFORM_SCANNER_ANAT', ...
    'NIFTI_XFORM_ALIGNED_ANAT', 'NIFTI_XFORM_TALAIRACH', 'NIFTI_XFORM_MNI_152'};
  % #define NIFTI_XFORM_UNKNOWN      0 /*! Arbitrary coordinates (Method 1). */
  % #define NIFTI_XFORM_SCANNER_ANAT 1 /*! Scanner-based anatomical coordinates */
  % #define NIFTI_XFORM_ALIGNED_ANAT 2 /*! Coordinates aligned to another file's,
  % #define NIFTI_XFORM_TALAIRACH    3 /*! Coordinates aligned to Talairach-
  % #define NIFTI_XFORM_MNI_152      4 /*! MNI 152 normalized coordinates. */
  
  % Inform of intents and units.
  SpaceUnits = bitand(7, handles.nii.hdr.xyzt_units);
  switch SpaceUnits
    case 0
      SpaceUnits = '?';
    case 1
      SpaceUnits = 'm';
    case 2
      SpaceUnits = 'mm';
    case 3
      SpaceUnits = '?m';
    otherwise
      SpaceUnits = '!';
  end
  set(handles.uipanel_s, 'Title', ['SForm (', SpaceUnits, ') ', handles.nii.mat_intent]);
  set(handles.uipanel_q, 'Title', ['QForm (', SpaceUnits, ') ', handles.nii.mat0_intent]);
  
  % Write file name, now in title bar.
  %   set(handles.text_File, 'String', NiiFile);
  set(handles.figure_NiftiViewer, 'Name', ['NiftiViewer - ', NiiFile]);
  
  
  % Number of points saved.
  handles.nP = 0;
  
  % 3d rendering.
  set(hObject, 'Renderer', 'zbuffer', 'CurrentAxes', handles.axes_3d);
  
  handles.Surf = patch('Faces', [], 'Vertices', [], ...
    'FaceColor', [0.5, 0.35, 0.25], 'EdgeColor', 'none');
  handles.Caps = patch('Faces', [], 'Vertices', [], ...
    'FaceVertexCData', [], 'FaceColor', 'interp', ...
    'EdgeColor', 'none');
  handles.CursorCube = patch('Faces', [], 'Vertices', [], ...
    'FaceColor', 'red', 'EdgeColor', 'none');
  view(3);
  axis equal tight;
  set(handles.axes_3d, 'XTick', [], 'YTick', [], 'ZTick', []);
  %   axis off; % Can't select the axes if off, which is needed for proper
  %   behavior of rotate filter function...
  %   colormap(gray(100))
  handles.Light(1) = camlight('left');
  handles.Light(2) = camlight('right');
  lighting gouraud;
  RotateH = rotate3d(handles.axes_3d);
  set(RotateH, 'RotateStyle', 'box', 'Enable', 'on', ...
    'ButtonDownFilter', @ButtonDownFilter_Callback, ...
    'ActionPostCallback', @AdjustLights);
  setAllowAxesRotate(RotateH, ...
    [handles.axes_ij, handles.axes_ik, handles.axes_jk], false);
  
  set(handles.edit_3dSize, 'String', '50');
  set(handles.edit_Threshold, 'String', '30');
  % Need to call this to set ZoomSize before setting the aspect ratio.  It
  % also draws the 3d plot.
  edit_3dSize_Callback(handles.edit_3dSize, [], handles);
  % The previous callback also modifies and updates handles.
  handles = guidata(hObject);
  
  % Adjust aspect ratio.  Setting a radio button programmatically (through
  % button or buttongroup) respects exclusivity, but it doesn't trigger the
  % group's selection change function.
  set(handles.uipanel_AR, 'SelectedObject', handles.radio_S);
  Event.EventName = 'SelectionChanged';
  Event.OldValue = handles.radio_V;
  Event.NewValue = handles.radio_S;
  uipanel_AR_SelectionChangeFcn(handles.radio_S, Event, handles); % handles was modified and saved by edit_3dSize_Callback
  
  
  % Allow key presses to be detected even in rotate (or other) mode.  This
  % must be done after switching to rotate mode.
  hManager = uigetmodemanager(hObject);
  if verLessThan('matlab', '8.4')
    % This "trick" now gives error in R2014b.
    set(hManager.WindowListenerHandles, 'Enable', 'off');
  else
    % Not sure if this makes any sense.
    hManager.WindowListenerHandles(1).Enabled = 0;
    hManager.WindowListenerHandles(2).Enabled = 0;
  end
  set(hObject, 'KeyPressFcn', @(f, e)figure_NiftiViewer_KeyPressFcn(f, e, guidata(f)));
  % Somehow, putting handles as an argument the following way doesn't use
  % the up-to-date handles structure when it is called.  E.g. if we add a
  % surface later, it won't be there when calling the callback.
  %   set(hObject, 'KeyPressFcn', {@figure_NiftiViewer_KeyPressFcn, guidata(hObject)});
  % 'WindowKeyPressFcn', {@figure_NiftiViewer_WindowKeyPressFcn, guidata(hObject)},
  %   NiftiViewer('figure_NiftiViewer_KeyPressFcn',hObject,eventdata,guidata(hObject))
  
  % Draw surface if loaded from command line.  Saves guidata again.
  if exist('GiiFile', 'var')
    LoadGii(GiiFile, handles);
    handles = guidata(hObject);
    if isfield(handles, 'Curve_i')
      set(handles.checkbox_Surface, 'Value', 1);
    end
  end
  
  %   % Update handles structure % Now done in edit_3dSize_Callback.
  %   guidata(hObject, handles);
  
  % This (and a few changes in CloseRequestFcn and OutputFcn) makes the GUI
  % modal, i.e. execution will not continue until the GUI is closed and the
  % output will be set after the user interacted with the GUI.
  % UIWAIT makes NiftiViewer wait for user response (see UIRESUME)
  uiwait(handles.figure_NiftiViewer);
  
end

% --- Executes when user attempts to close figure_NiftiViewer.
function figure_NiftiViewer_CloseRequestFcn(hObject, eventdata, handles)
  % hObject    handle to figure_NiftiViewer (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is "in UIWAIT", so "UIRESUME"
    uiresume(hObject);
  else
    % The GUI is no longer waiting.
    % Hint: delete(hObject) closes the figure
    delete(hObject);
  end
end


% --- Outputs from this function are returned to the command line.
function varargout = NiftiViewer_OutputFcn(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU,*INUSD>
  % varargout  cell array for returning output args (see VARARGOUT);
  % hObject    handle to figure
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Get default command line output from handles structure
  %   varargout{1} = handles.output;
  
  if handles.nP > 0
    varargout{1} = handles.Coordinates;
  else
    varargout{1} = [];
  end
  
  % Now delete the figure.
  delete(hObject);
  
end

% --- Executes when figure_NiftiViewer is resized.
function figure_NiftiViewer_ResizeFcn(hObject, eventdata, handles)
  % hObject    handle to figure_NiftiViewer (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
end



% ========================================================================
% Object control functions.

% --- Executes on slider movement.
function slider_i_Callback(hObject, eventdata, handles)
  % hObject    handle to slider_i (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'Value') returns position of slider
  %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
  
  % Round.
  Value = round(get(hObject, 'Value'));
  set(hObject, 'Value', Value);
  % Set corresponding text box.
  set(handles.edit_i, 'String', num2str(Value));
  % Call Callback on that text box to update coordinates and images.
  edit_i_Callback(handles.edit_i, 0, handles);
end

% --- Executes during object creation, after setting all properties.
function slider_i_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to slider_i (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: slider controls usually have a light gray background.
  if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
  end
end

% --- Executes on slider movement.
function slider_j_Callback(hObject, eventdata, handles)
  % hObject    handle to slider_j (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'Value') returns position of slider
  %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
  
  % Round.
  Value = round(get(hObject, 'Value'));
  set(hObject, 'Value', Value);
  % Set corresponding text box.
  set(handles.edit_j, 'String', num2str(Value));
  % Call Callback on that text box to update coordinates and images.
  edit_j_Callback(handles.edit_j, 0, handles);
end

% --- Executes during object creation, after setting all properties.
function slider_j_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to slider_j (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: slider controls usually have a light gray background.
  if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
  end
end

% --- Executes on slider movement.
function slider_k_Callback(hObject, eventdata, handles)
  % hObject    handle to slider_k (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'Value') returns position of slider
  %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
  
  % Round.
  Value = round(get(hObject, 'Value'));
  set(hObject, 'Value', Value);
  % Set corresponding text box.
  set(handles.edit_k, 'String', num2str(Value));
  % Call Callback on that text box to update coordinates and images.
  edit_k_Callback(handles.edit_k, 0, handles);
end

% --- Executes during object creation, after setting all properties.
function slider_k_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to slider_k (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: slider controls usually have a light gray background.
  if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
  end
end


function edit_i_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_i (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_i as text
  %        str2double(get(hObject,'String')) returns contents of edit_i as a double
  
  % Round and verify bounds.
  Value = round(str2double(get(hObject, 'String')));
  if Value < 0
    Value = 0;
  elseif Value > handles.nii.dat.dim(1) - 1
    Value = handles.nii.dat.dim(1) - 1;
  end
  set(hObject, 'String', num2str(Value));
  % Refresh slider
  set(handles.slider_i, 'Value', Value);
  % Refresh coordinates and images.
  RefreshCoordinates(handles);
  RefreshImages(handles, 1);
end

% --- Executes during object creation, after setting all properties.
function edit_i_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_i (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

function edit_j_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_j (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_j as text
  %        str2double(get(hObject,'String')) returns contents of edit_j as a double
  % Round and verify bounds.
  
  Value = round(str2double(get(hObject, 'String')));
  if Value < 0
    Value = 0;
  elseif Value > handles.nii.dat.dim(2) - 1
    Value = handles.nii.dat.dim(2) - 1;
  end
  set(hObject, 'String', num2str(Value));
  % Refresh slider
  set(handles.slider_j, 'Value', Value);
  % Refresh coordinates and images.
  RefreshCoordinates(handles);
  RefreshImages(handles, 2);
end

% --- Executes during object creation, after setting all properties.
function edit_j_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_j (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

function edit_k_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_k (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_k as text
  %        str2double(get(hObject,'String')) returns contents of edit_k as a double
  
  Value = round(str2double(get(hObject, 'String')));
  if Value < 0
    Value = 0;
  elseif Value > handles.nii.dat.dim(3) - 1
    Value = handles.nii.dat.dim(3) - 1;
  end
  set(hObject, 'String', num2str(Value));
  % Refresh slider
  set(handles.slider_k, 'Value', Value);
  % Refresh coordinates and images.
  RefreshCoordinates(handles);
  RefreshImages(handles, 3);
end

% --- Executes during object creation, after setting all properties.
function edit_k_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_k (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end


function edit_x_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_x (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_x as text
  %        str2double(get(hObject,'String')) returns contents of edit_x as a double
end

% --- Executes during object creation, after setting all properties.
function edit_x_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_x (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

function edit_y_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_y (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_y as text
  %        str2double(get(hObject,'String')) returns contents of edit_y as a double
end

% --- Executes during object creation, after setting all properties.
function edit_y_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_y (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

function edit_z_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_z (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_z as text
  %        str2double(get(hObject,'String')) returns contents of edit_z as a double
end

% --- Executes during object creation, after setting all properties.
function edit_z_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_z (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

function edit_zq_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_zq (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_zq as text
  %        str2double(get(hObject,'String')) returns contents of edit_zq as a double
end

% --- Executes during object creation, after setting all properties.
function edit_zq_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_zq (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

function edit_yq_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_yq (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_yq as text
  %        str2double(get(hObject,'String')) returns contents of edit_yq as a double
end

% --- Executes during object creation, after setting all properties.
function edit_yq_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_yq (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

function edit_xq_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_xq (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_xq as text
  %        str2double(get(hObject,'String')) returns contents of edit_xq as a double
end

% --- Executes during object creation, after setting all properties.
function edit_xq_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_xq (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end


function edit_voxelvalue_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_voxelvalue (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_voxelvalue as text
  %        str2double(get(hObject,'String')) returns contents of edit_voxelvalue as a double
end

% --- Executes during object creation, after setting all properties.
function edit_voxelvalue_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_voxelvalue (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end


% --- Executes on button press in checkbox_Cursor.
function checkbox_Cursor_Callback(hObject, eventdata, handles)
  % hObject    handle to checkbox_Cursor (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hint: get(hObject,'Value') returns toggle state of checkbox_Cursor
  %   RefreshImages(handles, 1);
  %   RefreshImages(handles, 2);
  %   RefreshImages(handles, 3);
  i = str2double(get(handles.edit_i, 'String'));
  j = str2double(get(handles.edit_j, 'String'));
  k = str2double(get(handles.edit_k, 'String'));
  if get(handles.checkbox_Cursor, 'Value')
    set(handles.cursor_i, 'EdgeColor', 'red', 'Position', [j-0.5, k-0.5, 1, 1]);
    set(handles.cursor_j, 'EdgeColor', 'red', 'Position', [i-0.5, k-0.5, 1, 1]);
    set(handles.cursor_k, 'EdgeColor', 'red', 'Position', [i-0.5, j-0.5, 1, 1]);
    % 3d plot is now 0-indexed and X first for consistency and proper orientation...
    CubeFV = Cube([i, j, k], 1);
    set(handles.CursorCube, 'FaceColor', 'red', ...
      'Vertices', CubeFV.Vertices, 'Faces', CubeFV.Faces);
  else
    set(handles.cursor_i, 'EdgeColor', 'none');
    set(handles.cursor_j, 'EdgeColor', 'none');
    set(handles.cursor_k, 'EdgeColor', 'none');
    set(handles.CursorCube, 'FaceColor', 'none');
  end
  
end

% ========================================================================
% Refresh functions.

function RefreshCoordinates(handles, FromSavedPoint)
  % Get voxel coordinates.
  % Although the transformation matrices encoded in the nifti file apply to
  % 0-indexed voxels, the nifti matlab library converts them to apply to
  % 1-indexed voxel coordinates (nii.mat, nii.mat0)!
  % We display 0-indexed coordinates, so we must shift to 1-indexed here.
  i = 1 + str2double(get(handles.edit_i, 'String'));
  j = 1 + str2double(get(handles.edit_j, 'String'));
  k = 1 + str2double(get(handles.edit_k, 'String'));
  
  % SForm world coordinates.
  Point = handles.nii.mat * [i; j; k; 1];
  set(handles.edit_x, 'String', sprintf('%1.3f', Point(1)));
  set(handles.edit_y, 'String', sprintf('%1.3f', Point(2)));
  set(handles.edit_z, 'String', sprintf('%1.3f', Point(3)));
  
  % QForm world coordinates.
  Point = handles.nii.mat0 * [i; j; k; 1];
  set(handles.edit_xq, 'String', sprintf('%1.3f', Point(1)));
  set(handles.edit_yq, 'String', sprintf('%1.3f', Point(2)));
  set(handles.edit_zq, 'String', sprintf('%1.3f', Point(3)));
  
  % Also refresh voxel value.
  set(handles.edit_voxelvalue, 'String', sprintf('%1.4f', handles.nii.dat(i, j, k)));
  
  
  if ~strcmp(get(handles.edit_iPoint, 'string'), '-') && ...
      (~exist('FromSavedPoint', 'var') || isempty(FromSavedPoint) || ...
      ~FromSavedPoint)
    % Also update saved point objects to non-saved.
    set(handles.edit_iPoint, 'string', '-');
    set(handles.button_Next, 'enable', 'off');
    if handles.nP > 0
      set(handles.button_Prev, 'enable', 'on');
    else
      set(handles.button_Prev, 'enable', 'off');
    end
    set(handles.button_Delete, 'enable', 'off');
  end
  
end

function RefreshImages(handles, Which)
  
  i = str2double(get(handles.edit_i, 'String'));
  j = str2double(get(handles.edit_j, 'String'));
  k = str2double(get(handles.edit_k, 'String'));
  
  if exist('Which', 'var')
    % Only refresh plot that changes for speed.
    %           i_range = get(handles.axes_ik, 'XLim');
    switch Which
      case 1
        set(handles.image_i, ...
          'CData', squeeze(handles.nii.dat(i+1, :, :))');
        if get(handles.checkbox_Cursor, 'Value')
          set(handles.cursor_j, 'Position', [i-0.5, k-0.5, 1, 1]);
          set(handles.cursor_k, 'Position', [i-0.5, j-0.5, 1, 1]);
        end
        if get(handles.checkbox_Surface, 'Value')
          Plane.Point = [i, j, k];
          Plane.Normal = [1, 0, 0];
          Curve = SurfaceSlice(handles.Surface, Plane);
          set(handles.Curve_i, 'XData', Curve(:, 2), 'YData', Curve(:, 3));
        end
      case 2
        set(handles.image_j, ...
          'CData', squeeze(handles.nii.dat(:, j+1, :))');
        if get(handles.checkbox_Cursor, 'Value')
          set(handles.cursor_i, 'Position', [j-0.5, k-0.5, 1, 1]);
          set(handles.cursor_k, 'Position', [i-0.5, j-0.5, 1, 1]);
        end
        if get(handles.checkbox_Surface, 'Value')
          Plane.Point = [i, j, k];
          Plane.Normal = [0, 1, 0];
          Curve = SurfaceSlice(handles.Surface, Plane);
          set(handles.Curve_j, 'XData', Curve(:, 1), 'YData', Curve(:, 3));
        end
      case 3
        set(handles.image_k, ...
          'CData', squeeze(handles.nii.dat(:, :, k+1))');
        if get(handles.checkbox_Cursor, 'Value')
          set(handles.cursor_i, 'Position', [j-0.5, k-0.5, 1, 1]);
          set(handles.cursor_j, 'Position', [i-0.5, k-0.5, 1, 1]);
        end
        if get(handles.checkbox_Surface, 'Value')
          Plane.Point = [i, j, k];
          Plane.Normal = [0, 0, 1];
          Curve = SurfaceSlice(handles.Surface, Plane);
          set(handles.Curve_k, 'XData', Curve(:, 1), 'YData', Curve(:, 2));
        end
      otherwise
        error('Bad which.');
    end
  else
    % Do all 3.
    set(handles.image_i, ...
      'CData', squeeze(handles.nii.dat(i+1, :, :))');
    set(handles.image_j, ...
      'CData', squeeze(handles.nii.dat(:, j+1, :))');
    set(handles.image_k, ...
      'CData', squeeze(handles.nii.dat(:, :, k+1))');
    if get(handles.checkbox_Cursor, 'Value')
      set(handles.cursor_i, 'Position', [j-0.5, k-0.5, 1, 1]);
      set(handles.cursor_j, 'Position', [i-0.5, k-0.5, 1, 1]);
      set(handles.cursor_k, 'Position', [i-0.5, j-0.5, 1, 1]);
    end
    if get(handles.checkbox_Surface, 'Value')
      % Calculate surface intersection and update.
      Plane.Point = [i, j, k];
      Plane.Normal = [1, 0, 0];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_i, 'XData', Curve(:, 2), 'YData', Curve(:, 3));
      Plane.Normal = [0, 1, 0];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_j, 'XData', Curve(:, 1), 'YData', Curve(:, 3));
      Plane.Normal = [0, 0, 1];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_k, 'XData', Curve(:, 1), 'YData', Curve(:, 2));
    end
    %     error('"Which" required after first initialization.');
  end
  
  % Refresh cursor in 3d plot only.
  if get(handles.checkbox_Cursor, 'Value')
    % 3d plot is now 0-indexed and X first for consistency and proper orientation...
    CubeFV = Cube([i, j, k], 1);
    set(handles.CursorCube, 'Faces', CubeFV.Faces, ...
      'Vertices', CubeFV.Vertices);
  end
  
end


% --- Executes on mouse press over axes background.
function axes_jk_ButtonDownFcn(hObject, eventdata, handles)
  % hObject    handle to axes_jk (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Get mouse coordinates.
  Mouse = get(handles.axes_jk, 'CurrentPoint');
  %   fprintf('x %1.1f, y %1.1f\n', Mouse);
  % Set corresponding text boxes.
  set(handles.edit_j, 'String', num2str(Mouse(1, 1, 1)));
  % Call Callback on those text boxes to update coordinates and images.
  edit_j_Callback(handles.edit_j, 0, handles);
  set(handles.edit_k, 'String', num2str(Mouse(1, 2, 1)));
  edit_k_Callback(handles.edit_k, 0, handles);
end

% --- Executes on mouse press over axes background.
function axes_ik_ButtonDownFcn(hObject, eventdata, handles)
  % hObject    handle to axes_ik (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Get mouse coordinates.
  Mouse = get(handles.axes_ik, 'CurrentPoint');
  % Set corresponding text boxes.
  set(handles.edit_i, 'String', num2str(Mouse(1, 1, 1)));
  % Call Callback on those text boxes to update coordinates and images.
  edit_i_Callback(handles.edit_i, 0, handles);
  set(handles.edit_k, 'String', num2str(Mouse(1, 2, 1)));
  edit_k_Callback(handles.edit_k, 0, handles);
end

% --- Executes on mouse press over axes background.
function axes_ij_ButtonDownFcn(hObject, eventdata, handles)
  % hObject    handle to axes_ij (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Get mouse coordinates.
  Mouse = get(handles.axes_ij, 'CurrentPoint');
  % Set corresponding text boxes.
  set(handles.edit_i, 'String', num2str(Mouse(1, 1, 1)));
  % Call Callback on those text boxes to update coordinates and images.
  edit_i_Callback(handles.edit_i, 0, handles);
  set(handles.edit_j, 'String', num2str(Mouse(1, 2, 1)));
  edit_j_Callback(handles.edit_j, 0, handles);
end





% ========================================================================
% New functionality: 3d rendering with transparency threshold and recording
% coordinates of multiple points.


% --- Executes on button press in button_Save.
function button_Save_Callback(hObject, eventdata, handles)
  % hObject    handle to button_Save (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  handles.nP = handles.nP + 1;
  nP = handles.nP;
  handles.Coordinates(nP).VoxelIndex0 = [str2double(get(handles.edit_i, 'String')), ...
    str2double(get(handles.edit_j, 'String')), ...
    str2double(get(handles.edit_k, 'String'))];
  handles.Coordinates(nP).VoxelIndex1 = handles.Coordinates(nP).VoxelIndex0 + 1;
  handles.Coordinates(nP).SForm = [str2double(get(handles.edit_x, 'String')), ...
    str2double(get(handles.edit_y, 'String')), ...
    str2double(get(handles.edit_z, 'String'))];
  handles.Coordinates(nP).QForm = [str2double(get(handles.edit_xq, 'String')), ...
    str2double(get(handles.edit_yq, 'String')), ...
    str2double(get(handles.edit_zq, 'String'))];
  
  set(handles.edit_iPoint, 'string', num2str(nP, '%d'));
  set(handles.button_Next, 'enable', 'off');
  if nP > 1
    set(handles.button_Prev, 'enable', 'on');
  end
  set(handles.button_Delete, 'enable', 'on');
  
  % Update handles structure
  guidata(hObject, handles); % hObject here is not the figure, but still works: parent figure is used.
  
end


% --- Executes on button press in button_Delete.
function button_Delete_Callback(hObject, eventdata, handles)
  % hObject    handle to button_Delete (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  
  handles.nP = handles.nP - 1;
  handles.Coordinates(str2double(get(handles.edit_iPoint, 'string'))) = [];
  
  set(handles.edit_iPoint, 'string', '-');
  set(handles.button_Next, 'enable', 'off');
  if handles.nP > 0
    set(handles.button_Prev, 'enable', 'on');
  else
    set(handles.button_Prev, 'enable', 'off');
  end
  set(handles.button_Delete, 'enable', 'off');
  
  % Update handles structure
  guidata(hObject, handles); % hObject here is not the figure, but still works: parent figure is used.
  
end


% --- Executes on button press in button_Prev.
function button_Prev_Callback(hObject, eventdata, handles)
  % hObject    handle to button_Prev (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  iP = str2double(get(handles.edit_iPoint, 'string'));
  if isnan(iP)
    % Was on non-saved point: '-', go to last.
    iP = handles.nP;
    set(handles.button_Delete, 'enable', 'on');
  else
    iP = iP - 1;
  end
  set(handles.edit_iPoint, 'string', num2str(iP, '%d'));
  
  if iP == 1
    set(handles.button_Prev, 'enable', 'off');
    %   else
    %     set(handles.button_Prev, 'enable', 'on'); % on already.
  end
  if iP == handles.nP - 1
    set(handles.button_Next, 'enable', 'on');
    %   else
    %     set(handles.button_Next, 'enable', 'off'); % off already.
  end
  
  set(handles.edit_i, 'String', num2str(handles.Coordinates(iP).VoxelIndex0(1), '%d'));
  set(handles.edit_j, 'String', num2str(handles.Coordinates(iP).VoxelIndex0(2), '%d'));
  set(handles.edit_k, 'String', num2str(handles.Coordinates(iP).VoxelIndex0(3), '%d'));
  set(handles.slider_i, 'Value', handles.Coordinates(iP).VoxelIndex0(1));
  set(handles.slider_j, 'Value', handles.Coordinates(iP).VoxelIndex0(2));
  set(handles.slider_k, 'Value', handles.Coordinates(iP).VoxelIndex0(3));
  
  RefreshCoordinates(handles, true);
  RefreshImages(handles);
  
  set(handles.button_Delete, 'enable', 'on');
  
end


% --- Executes on button press in button_Next.
function button_Next_Callback(hObject, eventdata, handles)
  % hObject    handle to button_Next (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  iP = 1 + str2double(get(handles.edit_iPoint, 'string'));
  %   if isnan(iP) % Should not happen here.
  set(handles.edit_iPoint, 'string', num2str(iP, '%d'));
  
  if iP == 2
    set(handles.button_Prev, 'enable', 'on');
  end
  if iP == handles.nP
    set(handles.button_Next, 'enable', 'off');
  end
  
  set(handles.edit_i, 'String', num2str(handles.Coordinates(iP).VoxelIndex0(1), '%d'));
  set(handles.edit_j, 'String', num2str(handles.Coordinates(iP).VoxelIndex0(2), '%d'));
  set(handles.edit_k, 'String', num2str(handles.Coordinates(iP).VoxelIndex0(3), '%d'));
  set(handles.slider_i, 'Value', handles.Coordinates(iP).VoxelIndex0(1));
  set(handles.slider_j, 'Value', handles.Coordinates(iP).VoxelIndex0(2));
  set(handles.slider_k, 'Value', handles.Coordinates(iP).VoxelIndex0(3));
  
  RefreshCoordinates(handles, true);
  RefreshImages(handles);
  
  set(handles.button_Delete, 'enable', 'on');
  
end


% --- Executes on button press in button_Exit.
function button_Exit_Callback(hObject, eventdata, handles)
  % hObject    handle to button_Exit (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  figure_NiftiViewer_CloseRequestFcn(handles.figure_NiftiViewer, [], handles);
  % Once the ui resumes, the output function will be executed, which will
  % close the figure.
end



function edit_iPoint_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_iPoint (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_iPoint as text
  %        str2double(get(hObject,'String')) returns contents of edit_iPoint as a double
end


% --- Executes during object creation, after setting all properties.
function edit_iPoint_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_iPoint (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end



% --- Executes on slider movement.
function slider_3d_Callback(hObject, eventdata, handles)
  % hObject    handle to slider_3d (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'Value') returns position of slider
  %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
end


% --- Executes during object creation, after setting all properties.
function slider_3d_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to slider_3d (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: slider controls usually have a light gray background.
  if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
  end
end


% % --- Executes on button press in button_Rotate.
% function button_Rotate_Callback(hObject, eventdata, handles)
%   % hObject    handle to button_Rotate (see GCBO)
%   % eventdata  reserved - to be defined in a future version of MATLAB
%   % handles    structure with handles and user data (see GUIDATA)
%
%   % Hint: get(hObject,'Value') returns toggle state of button_Rotate
% %   if get(hObject,'Value') % Toggled.
% %     h = rotate3d;
% %     set(h, 'RotateStyle', 'box', 'Enable', 'on');
% %     set(handles.axes_ij, 'Enable', 'off');
% %     set(handles.axes_ij, 'Enable', 'off');
% %     set(handles.axes_ij, 'Enable', 'off');
% %   else
% %     set(h, 'RotateStyle', 'box', 'Enable', 'on');
% %     set(handles.axes_ij, 'Enable', 'off');
% %     set(handles.axes_ij, 'Enable', 'off');
% %     set(handles.axes_ij, 'Enable', 'off');
% %   end
%
% end

% --- Executes on button press in button_Threshold.
function button_Threshold_Callback(hObject, eventdata, handles)
  % hObject    handle to button_Threshold (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  i = str2double(get(handles.edit_i, 'String'));
  j = str2double(get(handles.edit_j, 'String'));
  k = str2double(get(handles.edit_k, 'String'));
  set(handles.edit_Threshold, 'String', ...
    sprintf('%1.4f', handles.nii.dat(i+1, j+1, k+1)));
  
  edit_Threshold_Callback(handles.edit_Threshold, [], handles);
  
end


function edit_Threshold_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_Threshold (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_Threshold as text
  %        str2double(get(hObject,'String')) returns contents of edit_Threshold as a double
  
  NewThresh = str2double(get(hObject, 'String'));
  if isnan(NewThresh)
    set(hObject, 'String', '0.0000');
    %   if NewThresh < handles.DataRange(1) % It's ok.
  else
    set(hObject, 'String', sprintf('%1.4f',NewThresh));
  end
  button_Draw_Callback(handles.button_Draw, [], handles);
  
end

% --- Executes during object creation, after setting all properties.
function edit_Threshold_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_Threshold (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

% --- Executes on button press in button_Draw.
function button_Draw_Callback(hObject, eventdata, handles)
  % hObject    handle to button_Draw (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  Zoom = floor(handles.ZoomSize ./ 2);

  i = str2double(get(handles.edit_i, 'String'));
  j = str2double(get(handles.edit_j, 'String'));
  k = str2double(get(handles.edit_k, 'String'));
  Thresh = str2double(get(handles.edit_Threshold, 'String'));
  % 1-indexed array coordinates:
  SubV = [max(1, i+1-Zoom(1)), min(handles.nii.dat.dim(1), i+1+Zoom(1)), ...
    max(1, j+1-Zoom(2)), min(handles.nii.dat.dim(2), j+1+Zoom(2)), ...
    max(1, k+1-Zoom(3)), min(handles.nii.dat.dim(3), k+1+Zoom(3))];
  % Like many functions, isosurface and isocaps call the second volume
  % dimension X and the first, Y.  This is stupid and really prone to bugs.
  % Here, to keep the right orientation of the coordinate system, we must
  % flip the first 2 dimensions of V, such that when we plot, the right ear
  % looks like the right ear and not the left.
  V = permute( ...
    handles.nii.dat(SubV(1):SubV(2), SubV(3):SubV(4), SubV(5):SubV(6)), ...
    [2, 1, 3]);
  % Plot with 0-indexed coordinates for consistency, even though we're not
  % displaying the coordinates.
  [SurfF, SurfV] = isosurface(...
    (SubV(1):SubV(2)) - 1, (SubV(3):SubV(4)) - 1, (SubV(5):SubV(6)) - 1, ...
    V, Thresh);
  [CapsF, CapsV, CapsC] = isocaps(...
    (SubV(1):SubV(2)) - 1, (SubV(3):SubV(4)) - 1, (SubV(5):SubV(6)) - 1, ...
    V, Thresh);
  set(handles.Surf, 'Faces', SurfF, 'Vertices', SurfV);
  set(handles.Caps, 'Faces', CapsF, 'Vertices', CapsV, ...
    'FaceVertexCData', CapsC);
  if get(handles.checkbox_Cursor, 'Value')
    CubeFV = Cube([i, j, k], 1);
    set(handles.CursorCube, 'Faces', CubeFV.Faces, ...
      'Vertices', CubeFV.Vertices);
  end
  set(handles.axes_3d, ...
    'XLim', [i-Zoom(1), i+Zoom(1)], ...
    'YLim', [j-Zoom(2), j+Zoom(2)], ...
    'ZLim', [k-Zoom(3), k+Zoom(3)]);
  %   drawnow;
  
end




function Flag = ButtonDownFilter_Callback(obj, event_obj)
  if strcmpi(get(gca, 'Tag'), 'axes_3d')
    % Rotate has precedence.
    Flag = false;
  else
    % ButtonDownFcn has precedence.
    Flag = true;
  end
end

function AdjustLights(FigHandle, event_obj)
  % FigHandle         handle to the figure that has been clicked on
  % event_obj   object containing struct of event data (same as the
  %             event data of the 'ActionPreCallback' callback)
  
  % In this callback, we don't have the handles structure as an argument.
  handles = guidata(FigHandle);
  camlight(handles.Light(1), 'left');
  camlight(handles.Light(2), 'right');
end


% --- Executes on button press in checkbox_Surface.
function checkbox_Surface_Callback(hObject, eventdata, handles)
  % hObject    handle to checkbox_Surface (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hint: get(hObject,'Value') returns toggle state of checkbox_Surface
  if get(hObject, 'Value')
    % On
    if ~isfield(handles, 'Curve_i')
      % No surface file loaded, pop-up dialog.
      [fileName, filePath] = uigetfile({'*.gii; *.GII; *.gii.gz', 'GIfTI Files (*.gii)'}, 'Select GIfTI file');
      % Return without error if no file selected.
      if ~exist(fullfile(filePath, fileName), 'file')
        set(hObject, 'Value', 0);
        return;
      end
      LoadGii(fullfile(filePath, fileName), handles);
    else
      % Already drawn before, but not updated while hidden, so recalculate.
      i = str2double(get(handles.edit_i, 'String'));
      j = str2double(get(handles.edit_j, 'String'));
      k = str2double(get(handles.edit_k, 'String'));
      Plane.Point = [i, j, k];
      Plane.Normal = [1, 0, 0];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_i, 'XData', Curve(:, 2), 'YData', Curve(:, 3));
      Plane.Normal = [0, 1, 0];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_j, 'XData', Curve(:, 1), 'YData', Curve(:, 3));
      Plane.Normal = [0, 0, 1];
      Curve = SurfaceSlice(handles.Surface, Plane);
      set(handles.Curve_k, 'XData', Curve(:, 1), 'YData', Curve(:, 2));
      
      set(handles.Curve_i, 'Visible', 'on');
      set(handles.Curve_j, 'Visible', 'on');
      set(handles.Curve_k, 'Visible', 'on');
    end
    
  else %if isfield(handles, 'Curve_i')
    % Should not happen that was checked with no surface, we'd get errors when updating images.
    % Off
    set(handles.Curve_i, 'Visible', 'off');
    set(handles.Curve_j, 'Visible', 'off');
    set(handles.Curve_k, 'Visible', 'off');
  end
  
end


% Load gifti surface and display intersection on slices.
function LoadGii(GiiFile, handles)
  gii = gifti(GiiFile);
  if isempty(gii)
    warning('Could not load surface file: %s', GiiFile);
    return;
  end
  gii = CheckGiftiMatrixBug(gii);

  GiiSystems = [];
  for g = 1:numel(gii.private.data)
    if strfind(gii.private.data{g}.attributes.Intent, 'NIFTI_INTENT_POINTSET')
      GiiSystems = [find(strcmpi(gii.private.data{g}.space.DataSpace, handles.CoordSystems), 1), ...
        find(strcmpi(gii.private.data{g}.space.TransformedSpace, handles.CoordSystems), 1)];
      break;
    end
  end
  if isempty(GiiSystems)
    error('No points found in GIfTI file.');
  end
  
  NiiSystems = [handles.nii.hdr.qform_code, handles.nii.hdr.sform_code] + 1;
  % Remember that nii transformation matrices give 1-indexed coordinates,
  % thus we subtract 1 after going to voxel coordinates to match
  % 0-indexed display.
  
  g = find(GiiSystems(1) == NiiSystems, 1, 'first');
  if ~isempty(g)
    % Raw data is already transformed it seems, so keep it.
    if numel(g) == 1 && g == 1 % Q-form
      handles.Surface.vertices = [gii.vertices, ...
        ones(size(gii.vertices, 1), 1)] / handles.nii.mat0' - 1;
    else % S-form
      handles.Surface.vertices = [gii.vertices, ...
        ones(size(gii.vertices, 1), 1)] / handles.nii.mat' - 1;
    end
  else
    g = find(GiiSystems(2) == NiiSystems, 1, 'first');
    
    if isempty(g)
      % Assume data is already tranformed and use S-form.
      fprintf('GIfTI surface coordinates (both raw and transformed) have different "intent" codes \nthan NIfTI S- and Q-forms. Assuming it matches S-form without tranformation.\n');
      handles.Surface.vertices = [gii.vertices, ...
        ones(size(gii.vertices, 1), 1)] / handles.nii.mat' - 1;
      
    else
      if numel(g) == 1 && g == 1 % Q-form
        % Only the transformed intent matches, so apply the transformation.
        % The GIfTI format does not specify how the transformation is
        % supposed to be applied T * v or v' * T; but someone on their forums
        % decisively said it was supposed to be like NIfTI: T * v.
        handles.Surface.vertices = [gii.vertices, ...
          ones(size(gii.vertices, 1), 1)] * gii.mat' / handles.nii.mat0' - 1;
      else % S-form (or both)
        handles.Surface.vertices = [gii.vertices, ...
          ones(size(gii.vertices, 1), 1)] * gii.mat' / handles.nii.mat' - 1;
      end
    end
  end
  handles.Surface.vertices(:, 4) = [];
  handles.Surface.faces = gii.faces;
  
  % Calculate 3d intersection curve(s) and draw.
  % Surface coordinates are properly transformed to 0-indexed voxels.
  i = str2double(get(handles.edit_i, 'String'));
  j = str2double(get(handles.edit_j, 'String'));
  k = str2double(get(handles.edit_k, 'String'));
  Plane.Point = [i, j, k];
  Plane.Normal = [1, 0, 0];
  Curve = SurfaceSlice(handles.Surface, Plane);
  % Lines must have 'HitTest' 'off' to allow clicks to the axes "under"
  % the curves.
  handles.Curve_i = plot(handles.axes_jk, ...
    Curve(:, 2), Curve(:, 3), 'Color', [0, 1, 0], 'HitTest', 'off');
  Plane.Normal = [0, 1, 0];
  Curve = SurfaceSlice(handles.Surface, Plane);
  handles.Curve_j = plot(handles.axes_ik, ...
    Curve(:, 1), Curve(:, 3), 'Color', [0, 1, 0], 'HitTest', 'off');
  Plane.Normal = [0, 0, 1];
  Curve = SurfaceSlice(handles.Surface, Plane);
  handles.Curve_k = plot(handles.axes_ij, ...
    Curve(:, 1), Curve(:, 2), 'Color', [0, 1, 0], 'HitTest', 'off');
  
  % Update handles structure
  guidata(handles.figure_NiftiViewer, handles);
end


% --- Executes on button press in checkbox_Saved.
function checkbox_Saved_Callback(hObject, eventdata, handles)
  % hObject    handle to checkbox_Saved (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hint: get(hObject,'Value') returns toggle state of checkbox_Saved
end





% Note: both KeyPressFcn and the WindowKeyPressFcn would not normally be
% used if the figure is in any of the "tool modes": pan, zoom, rotate, etc.
% In those modes, the GUI callback functions get replaced by default
% callbacks.  So to make this work we must 1. switch to the desired mode,
% 2. turn off listeners that prevent changing the callbaks again, and 3.
% reset the callbacks to these functions, in that order.  This is done here
% in the opening function.
%
% This empty function is needed to prevent focus from going to the command
% line when pressing a key with focus on axes.
%
% --- Executes on key press with focus on figure_NiftiViewer and none of its controls.
function figure_NiftiViewer_KeyPressFcn(hObject, eventdata, handles)
  % hObject    handle to figure_NiftiViewer (see GCBO)
  % eventdata  structure with the following fields (see FIGURE)
  %	Key: name of the key that was pressed, in lower case
  %	Character: character interpretation of the key(s) that was pressed
  %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
  % handles    structure with handles and user data (see GUIDATA)
  
  % If arrow key on axes, move cursor (through coordinate text box
  % callback). Call Callback on text box to update coordinates and images.
  %   disp(get(gco, 'Tag'));
  %   disp(get(get(hObject, 'CurrentObject'), 'Tag'));
  %   return;
  switch get(gco, 'Tag')
    case 'axes_ij'
      switch eventdata.Key
        case 'uparrow'
          set(handles.edit_j, 'String', num2str( ...
            str2double(get(handles.edit_j, 'String')) + 1 ));
          edit_j_Callback(handles.edit_j, 0, handles);
        case 'downarrow'
          set(handles.edit_j, 'String', num2str( ...
            str2double(get(handles.edit_j, 'String')) - 1 ));
          edit_j_Callback(handles.edit_j, 0, handles);
        case 'leftarrow'
          set(handles.edit_i, 'String', num2str( ...
            str2double(get(handles.edit_i, 'String')) - 1 ));
          edit_i_Callback(handles.edit_i, 0, handles);
        case 'rightarrow'
          set(handles.edit_i, 'String', num2str( ...
            str2double(get(handles.edit_i, 'String')) + 1 ));
          edit_i_Callback(handles.edit_i, 0, handles);
        otherwise
          return;
      end
    case 'axes_jk'
      switch eventdata.Key
        case 'uparrow'
          set(handles.edit_k, 'String', num2str( ...
            str2double(get(handles.edit_k, 'String')) + 1 ));
          edit_k_Callback(handles.edit_k, 0, handles);
        case 'downarrow'
          set(handles.edit_k, 'String', num2str( ...
            str2double(get(handles.edit_k, 'String')) - 1 ));
          edit_k_Callback(handles.edit_k, 0, handles);
        case 'leftarrow'
          set(handles.edit_j, 'String', num2str( ...
            str2double(get(handles.edit_j, 'String')) - 1 ));
          edit_j_Callback(handles.edit_j, 0, handles);
        case 'rightarrow'
          set(handles.edit_j, 'String', num2str( ...
            str2double(get(handles.edit_j, 'String')) + 1 ));
          edit_j_Callback(handles.edit_j, 0, handles);
        otherwise
          return;
      end
    case 'axes_ik'
      switch eventdata.Key
        case 'uparrow'
          set(handles.edit_k, 'String', num2str( ...
            str2double(get(handles.edit_k, 'String')) + 1 ));
          edit_k_Callback(handles.edit_k, 0, handles);
        case 'downarrow'
          set(handles.edit_k, 'String', num2str( ...
            str2double(get(handles.edit_k, 'String')) - 1 ));
          edit_k_Callback(handles.edit_k, 0, handles);
        case 'leftarrow'
          set(handles.edit_i, 'String', num2str( ...
            str2double(get(handles.edit_i, 'String')) - 1 ));
          edit_i_Callback(handles.edit_i, 0, handles);
        case 'rightarrow'
          set(handles.edit_i, 'String', num2str( ...
            str2double(get(handles.edit_i, 'String')) + 1 ));
          edit_i_Callback(handles.edit_i, 0, handles);
        otherwise
          return;
      end
    otherwise
      return;
  end
  
end


% This would have worked, but there is a bug that clicking on an axes, then
% clicking on a text box, gco still thinks it's on the axis for the first
% button press.  This bug does not happen with KeyPressFcn, so we switched
% back to using that one.
% % --- Executes on key press with focus on figure_NiftiViewer or any of its controls.
% function figure_NiftiViewer_WindowKeyPressFcn(hObject, eventdata, handles)
%   % hObject    handle to figure_NiftiViewer (see GCBO)
%   % eventdata  structure with the following fields (see FIGURE)
%   %	Key: name of the key that was pressed, in lower case
%   %	Character: character interpretation of the key(s) that was pressed
%   %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
%   % handles    structure with handles and user data (see GUIDATA)
%
% end





% --- Executes when selected object is changed in uipanel_AR.
function uipanel_AR_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel_AR 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

  switch eventdata.NewValue % selected object
    case handles.radio_V
      AR = [1, 1, 1];
    case handles.radio_S
      AR = 1 ./ double(handles.nii.hdr.pixdim(2:4)); 
    case handles.radio_Q
      AR = 1 ./ double(handles.nii.hdr.pixdim(2:4)); 
    otherwise
      error('Unknown AR button.');
  end
  set(handles.axes_ij, 'DataAspectRatio', AR);
  set(handles.axes_jk, 'DataAspectRatio', AR([2, 3, 1]));
  set(handles.axes_ik, 'DataAspectRatio', AR([1, 3, 2]));
  set(handles.axes_3d, 'DataAspectRatio', AR);
  % Adjust displayed box size.  No need to redraw.
  set(handles.edit_3dSize, 'String', num2str(max(handles.ZoomSize ./ AR), '%1.1f'));

end


% --- Executes during object creation, after setting all properties.
function edit_3dSize_CreateFcn(hObject, eventdata, handles)
  % hObject    handle to edit_3dSize (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    empty - handles not created until after all CreateFcns called
  
  % Hint: edit controls usually have a white background on Windows.
  %       See ISPC and COMPUTER.
  if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
  end
end

function edit_3dSize_Callback(hObject, eventdata, handles)
  % hObject    handle to edit_3dSize (see GCBO)
  % eventdata  reserved - to be defined in a future version of MATLAB
  % handles    structure with handles and user data (see GUIDATA)
  
  % Hints: get(hObject,'String') returns contents of edit_3dSize as text
  %        str2double(get(hObject,'String')) returns contents of edit_3dSize as a double
  
  % We could also use double(nii.hdr.pixdim(2:4)) to get physical size.
  % Units must be the same anyway for S and QForm.  Otherwise, would be
  % sqrt(sum(handles.nii.mat(1:3,1:3).^2, 1)) based on matrices.
  switch get(handles.uipanel_AR, 'SelectedObject')
    case handles.radio_V
      AR = [1, 1, 1];
    case handles.radio_S
      AR = 1 ./ double(handles.nii.hdr.pixdim(2:4)); 
    case handles.radio_Q
      AR = 1 ./ double(handles.nii.hdr.pixdim(2:4)); 
    otherwise
      error('Unknown AR button.');
  end
  
  % Convert to voxel numbers and enforce limits (1 to whole volume width).
  handles.ZoomSize = ceil(str2double(get(hObject,'String')) .* AR);
  handles.ZoomSize = min(max(handles.ZoomSize, [1, 1, 1]), ...
    handles.nii.dat.dim(1:3));
  % Adjust displayed size.
  set(hObject, 'String', num2str(max(handles.ZoomSize ./ AR), '%1.1f'));
  
  % Draw.
  button_Draw_Callback(handles.button_Draw, [], handles);
  
  % Update handles structure
  guidata(hObject, handles);
  
end
