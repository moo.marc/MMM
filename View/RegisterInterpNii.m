function NewFileNames = RegisterInterpNii(Reference, Images, ...
    AppendString, Form, Method, ExtrapVal)
  % Interpolate NIfTI images to the coordinate grid of a reference image.
  %
  % NewFileNames = RegisterInterpNii(Reference, Images, ...
  %   AppendString, Form, Method, ExtrapVal) 
  % RefCoords = RegisterInterpNii(Reference, Image, [], Form)
  %
  % Interpolate one or more nifti images that are already registered to
  % each other, to the volume of a reference nifti image.  This is done
  % based on real-world coordinates within both nifti files.  Extrapolation
  % (outside the available data of the images) is set to 0.
  %
  % Images can be a single file name (string) or a list (cell array).
  %
  % AppendString will be appended to the Images file names to create the
  % new files.  If it is not provided, and a single image file is given to
  % interpolate, the coordinate array is returned and no file is created.
  % This allows performing interpolation on multiple images as part of a
  % procedure without creating intermediate files.
  %
  % Form is 'sform' or 'qform' to specify which coordinate system to use
  % from the nifti headers.  Default is qform.
  %
  % Method is the interpolation method: 'nearest', 'linear' (default),
  % 'cubic', 'spline'.
  %
  % ExtrapVal [default 0] is the value for extrapolation (outside the
  % original image range).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-10-28
  
  % [TO DO: make it work with 4d images.]
  
  if nargin < 6 || isempty(ExtrapVal)
    ExtrapVal = 0;
  end
  
  if ~exist('Reference', 'var') || isempty(Reference)
    error('Missing reference file.');
  elseif iscell(Reference)
    if numel(Reference) == 1
      Reference = Reference{1};
    else
      error('Only expecting 1 reference file.');
    end
  end
  if ~exist(Reference, 'file')
    error('Reference file not found.');
  end

  if ~exist('Images', 'var') || isempty(Images) 
    %     error('No image file to process.');
    % It's ok, just do nothing.
    return
  end
  if ~iscell(Images)
    Images = {Images};
  end
  if ~exist(Images{1}, 'file')
    error('Image file not found.');
  end

  SaveFiles = true;
  if ~exist('AppendString', 'var') || isempty(AppendString)
    if length(Images) > 1
      AppendString = '_Reg';
    else
      SaveFiles = false;
    end
  elseif ~ischar(AppendString)
    % Try converting number?
    try
      AppendString = num2str(AppendString);
    catch
      error('AppendString should be a string.');
    end
  end

  if ~exist('Form', 'var') || isempty(Form)
    Form = 'qform';
  end
  if ~strncmpi(Form, 'q', 1)
    UseQForm = false;
  else
    UseQForm = true;
  end
  clear Form
  
  if ~exist('Method', 'var') || isempty(Method)
    Method = 'linear';
  end
  
  % ----------------------------------------------------------------------
  % Open template file and get voxel coordinates and header parameters.
  % For efficiency, we will not interpolate using real-world coordinates,
  % but "voxel index" coordinates of the Images files.  Then we can use the
  % default grid for defining the interpolation function.
  RefNii = nifti(Reference);
  Nii1 = nifti(Images{1});
  % Now allow 4d images as reference or for the purpose of finding spatial
  % interpolation coordinates.
  %   if RefNii.hdr.dim(1) ~= 3 || Nii1.hdr.dim(1) ~= 3 
  %     error('Nifti file is not 3-d volume.')
  %   end
  % Remember niftimatlib transforms header parameters, including the
  % transform matrices such that voxels are assumed indexed from 1 instead
  % of 0.
  
  RefCoords = zeros([RefNii.dat.dim(1:3), 4]); % Pretty big, could be single, but most Matlab function return double... 
  [X, Y, Z] = ndgrid(1:RefNii.dat.dim(1), 1:RefNii.dat.dim(2), 1:RefNii.dat.dim(3));
  RefCoords(:, :, :, 1) = X;
  RefCoords(:, :, :, 2) = Y;
  RefCoords(:, :, :, 3) = Z;
  RefCoords(:, :, :, 4) = 1;
  clear X Y Z
  RefCoords = reshape(RefCoords, [prod(RefNii.dat.dim(1:3)), 4]);
  if UseQForm % Use qform transformation matrix. 
    RefCoords = RefCoords * RefNii.mat0' / (Nii1.mat0');
  else % Use sform transformation matrix. 
    RefCoords = RefCoords * RefNii.mat' / (Nii1.mat');
  end
  RefCoords(:, 4) = [];
  RefCoords = reshape(RefCoords(:, 1:3), [RefNii.dat.dim(1:3), 3]);
  
  
  if ~SaveFiles
    NewFileNames = RefCoords; % Supposedly Matlab doesn't make a copy if not needed, as in this case.
    return
  end

  % ----------------------------------------------------------------------
  for i = 1:length(Images)
    % Open image file to get volume and coordinates.
    Nii = nifti(Images{i});
    % Check that it has the same dimensions and coordinates as Images{1},
    % whose coordinates were used to get the interpolation coordinates.
    if any(Nii.hdr.dim(1:Nii.hdr.dim(1)+1) ~= Nii1.hdr.dim(1:Nii1.hdr.dim(1)+1))
      error('Image %d has different dimensions as the first.  It should be registered separately.', i);
    end
    if any(Nii.mat(:) ~= Nii1.mat(:))
      error('File %d has different sform coordinates.', f);
    end
    if any(Nii.mat0(:) ~= Nii1.mat0(:))
      error('File %d has different qform coordinates.', f);
    end
    
    % Interpolate. 
    
    % Tried griddedInterpolant (less overhead?).
    %     F = griddedInterpolant(Nii.dat(:, :, :));
    %     RegistVol = F(RefCoords(:, :, :, 1), RefCoords(:, :, :, 2), RefCoords(:, :, :, 3));
    %     RegistVol = F(RefCoords);
    % Unfortunately it has bugs: gives all NaN...
    % max(F(RefCoords(:, :)))
    % ans =
    %    NaN
    % max(F(RefCoords(5000000:10000000, :)))
    % ans =
    %     1.1352
    
    % We let it use the automatic grid for Nii, and now use interpn to
    % avoid Y-X flip of interp3.
    RegistVol = RegisterInterpVol(Nii.dat(:, :, :), RefCoords, Method, ExtrapVal);
    % This was verified to do "perfect" replication with 'nearest'.

  
    % --------------------------------------------------------------------
    % Save new volume with header parameters of template, except intent
    % code.  We must also keep the data type of Images, which means we
    % cannot reuse the RefNii variable and only change the file name field
    % before calling the create call.
    NewFileNames{i} = [Images{i}(1:end-4), AppendString, '.nii']; %#ok<AGROW>
    % Reusing RefNii didn't work when Nii.dat.dtype differed, even when
    % casting.
    
    %     RefNii.dat.fname = NewFileNames{i};
    %     RefNii.dat.dtype = Nii.dat.dtype;
    %     RefNii.descrip = 'NIFTI-1 file modified with RegisterInterpNii.m';
    %     RefNii.intent = Nii.intent;
    %     RefNii.cal = Nii.cal;
    
    %     if UseQForm % Copy qform transformation matrix to sform.
    %       RefNii.mat = RefNii.mat0;
    %       RefNii.mat_intent = RefNii.mat0_intent;
    %     else % Copy sform transformation matrix to qform.
    %       RefNii.mat0 = RefNii.mat;
    %       RefNii.mat0_intent = RefNii.mat_intent;
    %     end
    %
    %     create(RefNii); % Write header info to file.
    %     RefNii.dat(:,:,:) = cast(RegistVol, ...
    %       lower(strtok(Nii.dat.dtype, '-'))); % Write data to file.
    
    NewNii = nifti();
    NewNii.dat = file_array(NewFileNames{i}, RefNii.dat.dim, Nii.dat.dtype);
    NewNii.descrip = 'NIFTI-1 file modified with RegisterInterpNii.m';
    NewNii.intent = Nii.intent;
    NewNii.cal = Nii.cal;
    
    if UseQForm
      NewNii.mat = RefNii.mat0;
      NewNii.mat_intent = RefNii.mat0_intent;
      NewNii.mat0 = RefNii.mat0;
      NewNii.mat0_intent = RefNii.mat0_intent;
    else
      NewNii.mat = RefNii.mat;
      NewNii.mat_intent = RefNii.mat_intent;
      NewNii.mat0 = RefNii.mat;
      NewNii.mat0_intent = RefNii.mat_intent;
    end
    
    create(NewNii); % Write header info to file.
    NewNii.dat(:,:,:) = RegistVol; % Write data to file.
    
    % Casting unnecessary and would need to convert some type names
    % (float32 -> single, etc.)
    %     NewNii.dat(:,:,:) = cast(RegistVol, ...
    %       lower(strtok(Nii.dat.dtype, '-'))); % Write data to file.
    
  end
  
end