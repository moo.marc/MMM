function [NewIm, NewCoord] = ImageResize(Image, NewSize, Coord)
  % Resample a 2-d array (e.g. greyscale image). 
  %
  %  [NewIm, NewCoord] = ImageResize(Image, NewSize, Coord)
  %
  % Resample data with cubic interpolation, and antialiasing when
  % downsampling.  The method is largely copied from imresize in the Matlab
  % R2015a image processing toolbox, and reference therein: [Keys 1981,
  % "Cubic Convolution Interpolation for Digital Image Processing"].
  % However this function currently does not convert indexed colors to RGB
  % and back.  For now this only works on greyscale, maybe ok on indexed.
  % Dithering is also not performed.
  %
  % Can also calculate transformed 1-indexed pixel coordinates (x, y)
  % (rows).  Keeps limits the same, i.e. the middle of the edge pixels.  In
  % other words (1, 1) stays (1, 1) and size(Image) becomes NewSize.
  %
  % NaN or inf would "propagate" due to cubic interpolation.  Therefore the
  % function issues warnings if these are present, and replaces NaN by 0,
  % and +-inf by max,min(Image) respectively for interpolation, then
  % restores the values (on an "interpolated" mask).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-09-08
  
  InSize = size(Image);
  nDim = numel(InSize);
  if nDim ~=2
    error('Expecting 2d image array.');
    % For extra dim, could process in loop (inner, use same kernel).
  end
  
  % Scaling factor(s).  > 1 means more samples, i.e. enlarging the image.
  % Different factors are allowed, useful if e.g. pixels are non-isotropic
  % and going to 1:1 aspect ratio.
  Factor = (NewSize - 1) ./ (InSize - 1);
  
  if nargin > 2 && nargout > 1
    % Calculate new 1-indexed pixel coordinates for row coordinates (2
    % elements only!).
    NewCoord = bsxfun(@times, Coord - 1, Factor) + 1;
  end
  
  % Range of points needed for interpolation (and possibly filtering).
  iKernelNoFilt = [-1,2];
  iKernel = [-2,2]; % This gets extended below based on the scaling factor.
  
  for d = nDim:-1:1
    InterpCoords{d} = (0:(NewSize(d)-1)) ./ Factor(d) + 1;
    if Factor(d) < 1
      % Widen the interpolating kernel to also do anti-aliasing filtering.
      iInterp{d} = floor(iKernel(1)/Factor(d)):ceil(iKernel(2)/Factor(d));
    else
      iInterp{d} = iKernelNoFilt(1):iKernelNoFilt(2);
    end
  end
  
  Range = [min(Image(:)), max(Image(:))];

  BadThresh = 0.33;
  % Replace "bad" values for interpolation.  Make smoothed mask for
  % restoring.
  if any(isnan(Image(:)))
    fprintf('Warning: Image contains NaN, replacing by 0 for interpolation.\n');
    WhereNaN = isnan(Image);
    Image(WhereNaN) = 0;
    % There is ringing and edge artefacts with ImageResize (filtered,
    % cubic).  Try linear interp.  Use threshold instead of rounding.
    %     [X, Y] = ndgrid(1:InSize(1), 1:InSize(2)); % Default
    [X, Y] = ndgrid(InterpCoords{1}, InterpCoords{2});
    % interpn accepts X, Y in "good" ndgrid order, not like interp2, interp3.
    WhereNaN = interpn(WhereNaN, X, Y, 'linear') > BadThresh;
    %     WhereNaN = ImageResize(WhereNaN, NewSize) > BadThresh;
  else
    WhereNaN = false;
  end
  if any(Image == inf)
    fprintf('Warning: Image contains +inf, replacing by finite max(Image) for interpolation.\n');
    WhereInf = Image == inf;
    Image(WhereInf) = max(Image(~WhereInf(:)));
    if ~exist('X', 'var')
      [X, Y] = ndgrid(InterpCoords{1}, InterpCoords{2});
    end
    % interpn accepts X, Y in "good" ndgrid order, not like interp2, interp3.
    WhereInf = interpn(WhereInf, X, Y, 'linear') > BadThresh;
    %     WhereInf = ImageResize(WhereInf, NewSize) > BadThresh;
  else
    WhereInf = false;
  end
  if any(Image == -inf)
    fprintf('Warning: Image contains -inf, replacing by finite min(Image) for interpolation.\n');
    WhereNegInf = Image == -inf;
    Image(WhereNegInf) = min(Image(~WhereNegInf(:)));
    if ~exist('X', 'var')
      [X, Y] = ndgrid(InterpCoords{1}, InterpCoords{2});
    end
    % interpn accepts X, Y in "good" ndgrid order, not like interp2, interp3.
    WhereNegInf = interpn(WhereNegInf, X, Y, 'linear') > BadThresh;
    %     WhereNegInf = ImageResize(WhereNegInf, NewSize) > BadThresh;
  else
    WhereNegInf = false;
  end
  
  % Don't know a simple efficient way to loop on dimensions since we need
  % to index in that dimension position...  Could permute the array.
  d = 2;
  NewIm = zeros(InSize(1), NewSize(2));
  for p = 1:NewSize(d)
    %     x = InterpCoords{d}(p); % Where we're interpolating.
    xk = floor(InterpCoords{d}(p));
    %     s = x - xk;
    if Factor(d) < 1
      NewIm(:, p) = Image(:, max(1, min(InSize(d), xk + iInterp{d}))) * ...
        Kernel(Factor(d) * (InterpCoords{d}(p) - xk - iInterp{d}), true)'; % .* Factor(d)
      % The last Factor product would have to be before normalizing the
      % weights, so useless, and normalization is now done in the Kernel
      % function.
    else
      NewIm(:, p) = Image(:, max(1, min(InSize(d), xk + iInterp{d}))) * ...
        Kernel(InterpCoords{d}(p) - xk - iInterp{d})';
    end
  end
  
  d = 1;
  Image = NewIm;
  NewIm = zeros(NewSize);
  for p = 1:NewSize(d)
    xk = floor(InterpCoords{d}(p));
    if Factor(d) < 1
      NewIm(p, :) = Kernel(Factor(d) * (InterpCoords{d}(p) - xk - iInterp{d}), true) * ...
        Image(max(1, min(InSize(d), xk + iInterp{d})), :);
    else
      NewIm(p, :) = Kernel(InterpCoords{d}(p) - xk - iInterp{d}) * ...
        Image(max(1, min(InSize(d), xk + iInterp{d})), :);
    end
  end
  
  % Could add dithering now.
  
  % "Clamp" value range even though we use cubic interpolation: for
  % colormap, this prevents going outside the range (not displayed,
  % transparent).
  NewIm(NewIm > Range(2)) = Range(2);
  NewIm(NewIm < Range(1)) = Range(1);
  
  % Restore "bad" values.
  if any(WhereNaN(:))
    NewIm(WhereNaN) = NaN;
  end
  if any(WhereInf(:))
    NewIm(WhereInf) = inf;
  end
  if any(WhereNegInf(:))
    NewIm(WhereNegInf) = -inf;
  end
  
end

% -------------------------------------------------------------------
% Cubic interpolation kernel from Keys 1981. Normalization added to follow
% Matlab imresize.
function K = Kernel(x, DoNorm)
  if nargin < 2
    DoNorm = false;
  end
  
  x2 = x.^2;
  x = sqrt(x2); % Now absolute value.
  x3 = x2 .* x;
  
  K = (1.5 * x3 - 2.5 * x2 + 1) .* (x <= 1) + ...
    (-0.5 * x3 + 2.5 * x2 - 4 * x + 2) .* ((1 < x) & (x <= 2));
  
  % Normalize to 1.  This may be needed because of rescaling with arbitrary
  % factors and using more points.  Not sure if multiplying by .* Factor
  % would be equivalent, but probably not.  (Anyway this is what Matlab
  % does.)
  if DoNorm % Think not needed otherwise.
    K = K ./ sum(K);
  end
end




