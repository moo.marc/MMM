function Curve = SurfaceSlice(Surface, Plane)
  % Intersection of a surface and a plane in 3d.
  %
  %  Curve = SurfaceSlice(Surface, Plane)
  %
  % Returns a list of 3d coordinates of the curve defined by the
  % intersection of a surface and a plane.  Surface is a structure with
  % fields vertices and faces, Plane is a structure with fields Point (any
  % point belonging to the plane) and Normal (perpendicular vector).  The
  % surface needs not be closed.  Curve can therefore consist of disjoint
  % open segments, in which case the segments are separated by a row of NaN
  % in the list.  This allows drawing the curve with a single plot()
  % command.
  %
  % To get coordinates in 2d:
  %   Curve * [X, Y]
  % where X and Y are column unit vectors in 3d for the 2d axes.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2013-12-10

  Plane.Normal = Plane.Normal ./ sqrt(sum(Plane.Normal.^2));
  
  % Boolean indicating which side of the plane each vertex is on.  Points
  % exactly on the plane will be included on one side.  Which side doesn't
  % matter, but to avoid problems with the possibility of a triangle
  % segment exactly in the plane, we have to be consistent (always same
  % side).
  Side = (bsxfun(@minus, Surface.vertices, Plane.Point) * Plane.Normal' < 0); % [nV, 1]
  
  % Which triangles intersect the plane, have vertices on both sides.
  %   WhereTri = any(Side(Surface.faces), 2) & any(~Side(Surface.faces), 2); % [nF, 1]
  % Boolean index for lines, all columns.
  Tri = Surface.faces(any(Side(Surface.faces), 2) & any(~Side(Surface.faces), 2), :);
  nT = size(Tri, 1);
  % maybe sum would be faster
  if nT == 0
    Curve = [NaN, NaN, NaN];
    return;
  end

  % Reorder vertices so that segments 1-2 and 2-3 are the ones crossing the
  % plane. This is needed to know which second segment crosses when linking
  % below.
  WhereSeg = Side(Tri(:, 1)) == Side(Tri(:, 2));
  Tri(WhereSeg, :) = Tri(WhereSeg, [1, 3, 2]);
  WhereSeg = Side(Tri(:, 2)) == Side(Tri(:, 3));
  Tri(WhereSeg, :) = Tri(WhereSeg, [2, 1, 3]);
    
  % Link triangles to make "curves" (or "ribbons" at this point).
  nTUsed = 0; % number of triangles already used.
  Ribbon = zeros(nT, 1);
  % Indices into TCurve where the curve loops back to the initial point 
  % (closed patches) or stops (open patches).
  iEnd = [];
  while nTUsed < nT
    % Pick a starting triangle (no logical starting point, just go with
    % smallest index not yet used).
    iT = min(setdiff(1:nT, Ribbon(1:nTUsed)));
    
    for nTUsed = nTUsed+1:nT %is that legal?
      %while nTUsed < nT % Various options for this loop as it will only exit on breaks below. Could change this rule to 'true' as this should never happen.
      %     nTUsed = nTUsed + 1;
      
      Ribbon(nTUsed) = iT;
      LastTri = Tri(iT, :);
      iT = setdiff( ...
        find(sum((Tri == LastTri(2)) + (Tri == LastTri(3)), 2) == 2) , iT);
      % Should always find 0 or 1 other than iT.
      if numel(iT) > 1
        error('Weird ribbon');
      elseif isempty(iT)
        % This ribbon is open and has ended, but still missing second
        % segment of the end triangle.  It is added below, which is why we
        % have +1.
        iEnd = [iEnd; nTUsed+1, 0]; 
        break; % Start new ribbon.
      end
      if ismember(iT, Ribbon)
        % This ribbon is closed and has looped.
        if Tri(iT, 1) ~= LastTri(2) && Tri(iT, 1) ~= LastTri(3) % Debug check.  Can be removed.
          error('Shouldn not happen, bad point ordering when closing loop.');
        end
        iEnd = [iEnd; nTUsed, 1]; 
        break; % Start new ribbon.
      end
      
      % Reorder 2 segments of iT such that the one that matched is first.
      if Tri(iT, 3) == LastTri(2) || Tri(iT, 3) == LastTri(3)
        Tri(iT, :) = Tri(iT, [3, 2, 1]);
      elseif Tri(iT, 1) ~= LastTri(2) && Tri(iT, 1) ~= LastTri(3) % Debug check.  Can be removed.
        error('Should not happen, bad iT.');
      end
      if nTUsed == nT % Debug check.  Can be removed.
        error('Should not get here, incomplete ribbon.');
      end
    end
    
    % Do the other direction for open ribbon.
    if iEnd(end, 2) == 0 % empty or ~empty is always false.
      % Add second segment of the end triangle, by duplicating it and
      % adding it with reversed segments.
      nT = nT + 1;
      Tri(nT, :) = LastTri(1, [3, 2, 1]);
      nTUsed = nTUsed + 1;
      Ribbon(nTUsed) = nT;
      % Get iT where we started this ribbon, which is already in.  Then
      % find next.
      if size(iEnd, 1) == 1
        iT = Ribbon(1); 
      else
        iT = Ribbon(iEnd(end-1, 1) + 1); 
      end
      LastTri = Tri(iT, :);
      iT = setdiff( ...
        find(sum((Tri == LastTri(1)) + (Tri == LastTri(2)), 2) == 2) , iT);
      if numel(iT) > 1
        error('Weird ribbon');
      elseif isempty(iT)
        % This ribbon is open and has ended already, chose starting point earlier by chance.
        %         iEnd = [iEnd; nTUsed, 0];
        continue; % Start new ribbon.
      end
      % Reorder 2 segments of iT such that the one that matched is last.
      if Tri(iT, 1) == LastTri(2) || Tri(iT, 1) == LastTri(1)
        Tri(iT, :) = Tri(iT, [3, 2, 1]);
      elseif Tri(iT, 3) ~= LastTri(2) && Tri(iT, 3) ~= LastTri(1) % Debug check.  Can be removed.
        error('Shouldn not happen, bad iT.');
      end
      if nTUsed == nT % Debug check.  Can be removed.
        error('Should not get here, incomplete ribbon.');
      end
      for nTUsed = nTUsed+1:nT %is that legal?
        Ribbon(nTUsed) = iT;
        LastTri = Tri(iT, :);
        iT = setdiff( ...
          find(sum((Tri == LastTri(1)) + (Tri == LastTri(2)), 2) == 2) , iT);
        % Should always find 0 or 1 other than iT.
        if numel(iT) > 1
          error('Weird ribbon');
        elseif isempty(iT)
          % This ribbon is open and has ended (other side).
          iEnd = [iEnd; nTUsed, -1]; 
          break; % Start new ribbon.
        end
        if ismember(iT, Ribbon) % Debug check.  Can be removed.
          error('Should not happen, loop in open ribbon.');
        end
        
        % Reorder 2 segments of iT such that the one that matched is last.
        if Tri(iT, 1) == LastTri(2) || Tri(iT, 1) == LastTri(1)
          Tri(iT, :) = Tri(iT, [3, 2, 1]);
        elseif Tri(iT, 3) ~= LastTri(2) && Tri(iT, 3) ~= LastTri(1) % Debug check.  Can be removed.
          error('Shouldn not happen, bad iT.');
        end
        if nTUsed == nT % Debug check.  Can be removed.
          error('Should not get here, incomplete ribbon.');
        end
      end % nTUsed loop
    end % if other direction of open ribbon.
  end 
  if nTUsed ~= nT || Ribbon(nTUsed) == 0 % Debug check.  Can be removed.
    error('Should not get here, last ribbon already finished.');
  end

  % Calculate intersection of each first triangle edge.
  %   L = Surface.vertices(Tri(:, 1), :) - Surface.vertices(Tri(:, 2), :);
  %   L0 = Surface.vertices(Tri(:, 1), :);
  %   D = bsxfun(@minus, Plane.Point, L0) * Plane.Normal' ./ L * Plane.Normal';
  %   C = L0 + bsxfun(@times, D, L);

  Coords = Surface.vertices(Tri(:, 1), :) + bsxfun( @times, ...
    bsxfun(@minus, Plane.Point, Surface.vertices(Tri(:, 1), :)) * Plane.Normal' ./ ...
    ((Surface.vertices(Tri(:, 1), :) - Surface.vertices(Tri(:, 2), :)) * Plane.Normal') , ...
    (Surface.vertices(Tri(:, 1), :) - Surface.vertices(Tri(:, 2), :)) );
  
  % Make curves.  Changed from cell array to one curve with NaN's to
  % separate segments.
  iStart = [1; iEnd(1:end-1, 1) + 1];
  %   iSegments = [[1; iEnd(1:end-1, 1) + 1], iEnd(:, 1)];
  %   iSecond = find(iEnd(:, 2) == -1);
  if any(iEnd(find(iEnd(:, 2) == -1) - 1, 2) ~= 0) % Debug check.  Can be removed.
    error('Should not get here, second open ribbon part not following first part.');
  end
  nSeg = size(iEnd, 1);
  %   c = 0;
  Curve = []; %zeros(sum(iEnd(:, 2)>=0) + sum(iEnd(:, 2) >=1), 1);
  %   Curves = cell(sum(iEnd(:, 2)>=0), 1);
  for e = 1:size(iEnd, 1)
    switch iEnd(e, 2)
      case 0
        % Open curve, first part.
        % Look for second part.
        if e < nSeg && iEnd(e+1, 2) == -1
          Curve = [Curve; Coords(Ribbon(iEnd(e+1, 1):-1:iStart(e+1)), :)]; %#ok<*AGROW>
        end
        Curve = [Curve; Coords(Ribbon(iStart(e):iEnd(e, 1)), :); ...
          NaN, NaN, NaN];
        %         c = c + 1;
        %         Curves{c} = Coords(Ribbon(iStart(e):iEnd(e, 1)), :);
      case 1
        % Closed curve; add starting point at end.
        Curve = [Curve; Coords(Ribbon([iStart(e):iEnd(e, 1), iStart(e)]), :); ...
          NaN, NaN, NaN];
        %         c = c + 1;
        %         Curves{c} = Coords(Ribbon([iStart(e):iEnd(e, 1), iStart(e)]), :);
      case -1
        % Open curve, second part.  Done above.
        %         Curves{c} = [Coords(Ribbon(iEnd(e, 1):-1:iStart(e)), :) ; ...
        %           Curves{c}];
      otherwise % Debug check.  Can be removed.
        error('Should not get here, unexpected curve type.');
    end
  end
  Curve(end, :) = [];
  
end


