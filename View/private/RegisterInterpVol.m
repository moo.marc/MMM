function RegistVol = RegisterInterpVol(Image, RefCoords, Method, ExtrapVal)
  % Interpolate a 3d array to a new coordinate grid.
  %
  % RegistVol = RegisterInterpVol(Image, RefCoords, Method, ExtrapVal)
  %
  % Interpolate a volumetric image to a reference volume, specified by a 4d
  % array of coordinates RefCoords (x, y, z along fourth dim).
  % Extrapolation (outside the available data of the image) is set to
  % ExtrapVal, NaN by default.
  %
  % Method is the interpolation method: 'nearest', 'linear' (default),
  % 'cubic', 'spline'.
  %
  % For NIfTI images, the coordinates can be obtained using
  % RefCoords = RegisterInterpNii(NiiReference, NiiImage, [], Form);
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-10-28
  
  if nargin < 2 || isempty(RefCoords)
    error('Missing reference coordinates.');
  end
  if nargin < 3 || isempty(Method)
    Method = 'linear';
  end
  if nargin < 4 || isempty(ExtrapVal)
    ExtrapVal = NaN;
  end
  
  % Take care of "bad" values that would otherwise propagate if e.g. using
  % cubic interpolation.  Do this for everything except 'nearest'.
  if ~strcmpi(Method, 'nearest')
    AdjustBadVal = true;
    BadThresh = 0.33;
    % Replace "bad" values for interpolation.  Make smoothed mask for
    % restoring.
    MaskMethod = 'linear';
    if any(isnan(Image(:)))
      fprintf('Warning: Image contains NaN, replacing by 0 for interpolation.\n');
      WhereNaN = isnan(Image);
      Image(WhereNaN) = 0;
      WhereNaN = RegisterInterpVol(WhereNaN, RefCoords, MaskMethod, 0) > BadThresh;
    else
      WhereNaN = false;
    end
    if any(Image == inf)
      fprintf('Warning: Image contains +inf, replacing by finite max(Image) for interpolation.\n');
      WhereInf = Image == inf;
      Image(WhereInf) = max(Image(~WhereInf(:)));
      WhereInf = RegisterInterpVol(WhereInf, RefCoords, MaskMethod, 0) > BadThresh;
    else
      WhereInf = false;
    end
    if any(Image == -inf)
      fprintf('Warning: Image contains -inf, replacing by finite min(Image) for interpolation.\n');
      WhereNegInf = Image == -inf;
      Image(WhereNegInf) = min(Image(~WhereNegInf(:)));
      WhereNegInf = RegisterInterpVol(WhereNegInf, RefCoords, MaskMethod, 0) > BadThresh;
    else
      WhereNegInf = false;
    end
    
  else
    AdjustBadVal = false;
  end
  
  
  % Interpolate.
  
  % interp3 expects x and y inverted so we now use interpn which does not.
  RegistVol = interpn(Image, ...
    RefCoords(:, :, :, 1), RefCoords(:, :, :, 2), RefCoords(:, :, :, 3), ...
    Method, ExtrapVal);
  % This was verified to do "perfect" replication with 'nearest'.
  
 
  % Restore "bad" values.
  if AdjustBadVal
    if any(WhereNaN(:))
      RegistVol(WhereNaN) = NaN;
    end
    if any(WhereInf(:))
      RegistVol(WhereInf) = inf;
    end
    if any(WhereNegInf(:))
      RegistVol(WhereNegInf) = -inf;
    end
  end
  
end