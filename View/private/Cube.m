function FV = Cube(CenterCoordinates, EdgeLength)
  % Generate a patch structure (with Vertices and Faces fields) of a cube.
  %
  % FV = Cube(CenterCoordinates, EdgeLength)
  %
  % Note that the patch is NOT a triangulation.  The patch faces are
  % square.  Can be plotted with patch(FV, ...).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-06
 
  if ~exist('CenterCoordinates', 'var') || isempty(CenterCoordinates)
    CenterCoordinates = [0, 0, 0];
  elseif numel(CenterCoordinates) ~= 3
    error('CenterCoordinates should be a 3 element vector.');
  elseif ~all(size(CenterCoordinates) == [1, 3])
    CenterCoordinates = CenterCoordinates(:)';
  end
  if ~exist('EdgeLength', 'var') || isempty(EdgeLength)
    EdgeLength = 1;
  elseif numel(EdgeLength) ~= 1
    error('EdgeLength must be a scalar.');
  end
  
  FV.Vertices = bsxfun(@plus, CenterCoordinates, ...
    ([0, 0, 0; 0, 1, 0; 1, 1, 0; 1, 0, 0; ...
    0, 0, 1; 0, 1, 1; 1, 1, 1; 1, 0, 1] - 0.5) .* EdgeLength);
  
  FV.Faces = [1, 2, 3, 4; 5, 6, 7, 8; 1, 2, 6, 5; ...
    1, 4, 8, 5; 2, 3, 7, 6; 3, 4, 8, 7];
  
  % For plotting:
  %   h = patch(FV, 'FaceColor', 'b', 'EdgeColor', 'w');
end
