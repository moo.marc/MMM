function hPatch = BoxWhiskerPlot(Five, Outliers, Width, Offset, Color, ...
    ColorOutliers, OutlierCount)
  % Draw a box and whisker plot of provided data, in the current axes.
  %
  % BoxWhiskerPlot(Five, Outliers, Width, Offset, Color, ...
  %   ColorOutliers, OutlierCount)
  %
  % The output is a graphics handle to one of the patches, used for making
  % legends (e.g. when plotting multiple box and whisker series next to
  % each other).
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-15
  
  hPatch = 0;
  if nargin < 2
    error('"Five" and "Ouliers" inputs required.');
  end
  if nargin < 3 || isempty(Width) || ischar(Width)
    % 3rd input used to be "Title" so for backwards compatibility,
    % expect possibly a string here and ignore it.
    Width = 0.8; % Bar width proportion.
  end
  if nargin < 4 || isempty(Offset)
    Offset = 0; % Left or right nudge.
  end
  if nargin < 5 || isempty(Color)
    Color = [0.9, 0.9, 0.9]; % very light grey
  end
  if nargin < 6 || isempty(ColorOutliers)
    ColorOutliers = false;
  end
  if nargin < 7 || isempty(OutlierCount)
    OutlierCount = false;
  end
  
  if iscell(Five) % backwards compatibility
    Five = reshape([Five{:}], 5, []);
  end
  nC = size(Five, 2);
  if ~iscell(Outliers)
    if nC == 1
      Outliers = {Outliers};
    else
      error('Expecting cell array for outliers.');
    end
  end
  set(gca, 'XMinorTick', 'on', 'XLim', [0, nC + floor(Offset) + 1]);
  hold on
  
  for c = 1:nC
    % Box
    X = [c - Width/2, c + Width/2] + Offset;
    % We do a patch of 2 rectangles on top of each other.
    hPatch = patch(X([1, 2, 2, 1, 1; 1, 2, 2, 1, 1])', ... % x
      Five((c-1)*5 + [2, 2, 3, 3, 2; 3, 3, 4, 4, 3])', ... % y
      Color, 'EdgeColor', 'k');
    %       plot(X([1, 2, 2, 1, 1, 2, 2]), Five{c}([3, 3, 4, 4, 2, 2, 3]));
    % Whiskers
    %     This doesn't work as expected (anymore?).
    %     h = errorbar((c + Offset), Five{c}(3), Five{c}(3) - Five{c}(1), ...
    %       Five{c}(5) - Five{c}(3), 'k');
    %     SetErrorBarWidth(h, Width*0.8);
    X = [X(1), (X(2)+X(1))/2, X(2)];
    plot(X([1, 3, 2, 2, 1, 3]), Five((c-1)*5 + [1, 1, 1, 5, 5, 5]), 'k');
    %     MyErrorBar(X, Five{c}([1, 5]));
    % Outliers
    if ~isempty(Outliers{c})
      if ColorOutliers
        plot((c + Offset) * ones(length(Outliers{c}), 1), Outliers{c}, '*', ...
          'MarkerEdgeColor', Color, 'MarkerFaceColor', Color);
      else
        plot((c + Offset) * ones(length(Outliers{c}), 1), Outliers{c}, '*k');
      end
      if OutlierCount
        nOut = sum(Outliers{c} < Five(3, c));
        if nOut > 1
          text(c + Offset, min(Outliers{c}), [' ', num2str(nOut)]);
        end
        nOut = sum(Outliers{c} > Five(3, c));
        if nOut > 1
          text(c + Offset, max(Outliers{c}), [' ', num2str(nOut)]);
        end
      end
    end
    
  end
  
  drawnow;
end


% % Plots a single vertical error bar with whiskers.
% function h = MyErrorBar(X, Y)
%   if numel(X) == 2
%     X = [X(1), (X(2)+X(1))/2, X(2)];
%   end
%   if numel(Y) == 3
%     Y(2) = [];
%   end
%   h = plot(X([1, 3, 2, 2, 1, 3]), Y([1, 1, 1, 2, 2, 2]), 'k');
% end


