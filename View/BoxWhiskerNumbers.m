function [Five, Outliers] = BoxWhiskerNumbers(V, Precision)
  % Calculate min, quartiles, max and outliers along columns, for making box and whisker plots.
  %
  % [Five, Outliers] = BoxWhiskerNumbers(V, Precision)
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-15
  
  if ~exist('Precision', 'var') || isempty(Precision)
    Precision = 0;
  end
  
  if isempty(V)
    Five = [];
    Outliers = [];
    return;
  end
  
  if size(V, 1) == 1 && numel(V) > 1
    error('Expecting data in columns, found 1 line.');
  end
  
  Q = Quartiles(V);
  IQR = Q(3, :) - Q(1, :);
  
  nC = size(V, 2);
  Outliers = cell(nC, 1);
  Five = zeros(5, nC);
  for c = 1:nC
    Out = V( V(:, c) < Q(1, c) - 1.5 * IQR(c) - Precision, c);
    if ~isempty(Out)
      Five(1, c) = min(V( V(:, c) > max(Out) , c));
      Outliers{c} = [Outliers{c}; Out];
    else
      Five(1, c) = min(V(:, c));
    end
    
    Out = V( V(:, c) > Q(3, c) + 1.5 * IQR(c) + Precision, c);
    if ~isempty(Out)
      Five(5, c) = max(V( V(:, c) < min(Out) , c));
      Outliers{c} = [Outliers{c}; Out];
    else
      Five(5, c) = max(V(:, c));
    end
  end
  Five(2:4, :) = Q;
  
  if nC == 1 % && ~iscell(Outliers)
    Outliers = Outliers{1};
  end
end

