function [Data, Time, NumTrials] = ...
    GFP(Dataset, ChannelSubset, Plot, ActivityType, Power, Projection)
  % Plot or return channel average field power or "amplitude" (sqrt(av field pow)).
  %
  % [Data, Time, NumTrials] = GFP(Dataset, ChannelSubset, Plot, ...
  %     ActivityType, Power, Projection)
  %
  % Power [true]: If false, return "global field amplitude" = sqrt(gfp)
  % instead of global field power.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-06-07
  
  if ~exist('Dataset', 'var') || isempty(Dataset)
    error('No dataset provided.');
  elseif ~exist(Dataset, 'file')
    error('Dataset not found: %s', Dataset);
  end
  
  if ~exist('Plot', 'var') || isempty(Plot)
    if nargout < 1
      Plot = true;
    else
      Plot = false;
    end
  end
  
  if ~exist('Power', 'var') || isempty(Power)
    Power = true;
  end
  
  if ~exist('Projection', 'var') || isempty(Projection)
    DoProj = false;
  else
    DoProj = true;
  end
  
  if ~exist('ActivityType', 'var') || isempty(ActivityType)
    ActivityType = 'evoked';
  end
  
  if ~exist('ChannelSubset', 'var')
    ChannelSubset = '';
  end
  
  if DoProj || isempty(ChannelSubset) % Need all channels
    [Data, Info] = GetMEGData(Dataset, false, false, 'fT');
  else
    [Data, Info] = GetMEGData(Dataset, false, false, 'fT', ...
      ChannelSubset);
  end
  
  if DoProj % Both ways seem about same speed.
    for t = 1:Info.nTrials
      Data(:, :, t) = Data(:, :, t) * Projection';
    end
    %     Data = permute(reshape(...
    %       Projection * reshape(permute(Data, [2, 1, 3]), Info.nSens, []), ...
    %       [Info.nSens, Info.nSamplesPerTrial, Info.nTrials]), [2, 1, 3]);
  end
  
  if ~isempty(ChannelSubset)
    iChan = ~cellfun(@isempty, strfind(Info.ChanNames, ChannelSubset));
    if ~any(iChan)
      error('No channels found matching subset "%s".', ChannelSubset);
    end
    Info.ChanNames = Info.ChanNames(iChan);
    Data = Data(:, iChan, :);
  end
  
  NumTrials = Info.nTrials;
  
  switch upper(ActivityType(1))
    case {'A', 'B'} % All or both
      % Square, then trial and channel average.
      Data = sum(mean(Data.^2,3),2); 
    case 'I' % Induced
      % Subtract average (evoked) activity from each trial.
      Data = bsxfun(@minus, Data, mean(Data, 3));
      % Square, then trial and channel average.
      Data = sum(mean(Data.^2,3),2); 
    case 'E' % Evoked
      % Trial average first, then square, then channel average.
      Data = sum(mean(Data,3).^2, 2);
    otherwise
      error('Unrecognized activity type: %s.  Should be ''Induced'', ''Evoked'' or ''All''/''Both''.', ...
        ActivityType);
  end
  % Divide by number of channels to better compare if different datasets
  % have different channels, or different regions/channel subsets.  To do
  % this properly, we need to divide after summing, not after sqrt.
  Data = Data / Info.nSens;
  
  if ~Power % Amplitude
    Data = sqrt(Data);
  end
  
  Time = SampleToTime(1:Info.nSamplesPerTrial, Info.SampleRate, Info.nPreTrig);
  
  if Plot
    figure;
    plot(Time, squeeze(Data));
  end
  
end

