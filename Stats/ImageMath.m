function OutImages = ImageMath(Images, Operation, OutImages, ...
    Reference, ContrastWeights, InterpMethod, Overwrite)
  % Apply an operation to volume data and save as new NIfTI images.
  % 
  % OutImages = ImageMath(Images, Operation, OutImages, ...
  %               Reference, ContrastWeights, InterpMethod, Overwrite)
  %
  % This function works with 3d or 4d (including timeseries) NIfTI images,
  % but does not convert between the two.  See Combine4dNii or Split4dNii.
  %
  % Images: Cell array of image files, including path.  For oprations
  % within a single image, supply as a single column.  For more complex
  % operations involving multiple images, each column should represent the
  % data variables as they will be provided to the Operation function.  For
  % example, if subtracting images (Operation = @minus), it will be applied
  % as: images of the first column minus corresponding images of the second
  % column.
  %
  % Operation: Function handle (possibly passed as an anonymous function)
  %   to be applied directly to the data array(s), or one of the custom
  %   functions @average or @contrast.  Examples: 
  %   Square the data:
  %      @(X) X.^2
  %   Subtract two images: 
  %      @minus    or    @(X, Y) X - Y
  %   Threshold contrast image based on statistical parametric map:
  %      @(Contrast, SPM) Contrast .* (SPM >= Threshold)
  %
  % OutImages input argument can be given in different ways: 
  %   1. a string to be appended to the (first column) image names, e.g.
  %   '_A' would give '/path/firstimage_A.nii';
  %  2. a full path and "append" name (without extension), all files would
  %  be saved in that output folder; 
  %   3. a full path and file name with .nii extension, if more than one
  %   line in Images array and not combining images, numbers would be
  %   appended to each file;
  %  4. a cell array of length equal to the number of lines in Images, with
  %  full path and file names with .nii extension.
  %
  % Reference: If a 3d Reference image file is given, the data of each file
  % will be interpolated to the (qform) coordinates and resolution of that
  % file before applying the operation.  Otherwise if the dimensions and
  % transformation matrices don't match, an error is returned.
  % 
  % ContrastWeights: With the custom function @contrast only, this is the
  % vector of coefficients to multiply each image before adding them.  For
  % example, the weights [1, -0.5, -0.5] would subtract the average of the
  % 2nd and 3rd image from the first.  
  %
  % InterpMethod [default 'linear']: When using a reference image, this
  % interpolation method will be used.  For matching grids (but possibly
  % different storage order), 'nearest' or 'linear' will be exact.  For
  % other cases, when positive and negative values are acceptable, '*cubic'
  % may be optimal.
  %
  % OutImages output argument: Cell array of path and file names of the
  % newly created images, or empty string when failed.
  %
  % Overwrite [default false]: If false, skip if the output file exists.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-10-28

  % [Decided not to implement this here but instead as separate
  % function(s): OutImageDim [default same as input images]: 3 or 4.  Can
  % be used to combine 3d images into 4d NIfTI image, or split 4d images
  % into 3d.]

  if nargin < 7 || isempty(Overwrite)
    Overwrite = false;
  end
  if nargin < 3
    error('At least 3 input arguments expected.');
  end
  
  if ~isa(Operation, 'function_handle')
    if ischar(Operation)
      % Attempt to convert string to function.
      Operation = str2func(Operation);
    else
      error('Operation should be a function handle.');
    end
  end
  if ismember(func2str(Operation), {'average', 'contrast'})
    DoContrast = true;
    if ~exist('ContrastWeights', 'var')
      ContrastWeights = [];
    elseif strcmpi(func2str(Operation), 'average') && ~isempty(ContrastWeights)
      warning('ContrastWeights is not used with @average, use @contrast operation instead.  Ignoring weights.');
      ContrastWeights = [];
    end
  else
    DoContrast = false;
  end
  
  if ~iscell(Images)
    Images = {Images};
  end
  
  if ~exist('Reference', 'var') || isempty(Reference)
    Interpolate = false;
  else
    if iscell(Reference)
      Reference = Reference{1};
    end
    if ~exist(Reference, 'file')
      error('Template file not found for group averaging.');
    end
    Interpolate = true;
    if nargin < 6 || isempty(InterpMethod)
      InterpMethod = 'linear'; %'*cubic';
    elseif ~ischar(InterpMethod)
      error('InterpMethod should be a string, one of the valid interp3 methods.');
    end
  end
  
  % Number of independant images (lines), and number of images needed for
  % the operation (columns).
  [nI, nOp] = size(Images);
  if ~DoContrast
    if nOp ~= nargin(Operation)
      if nI == nargin(Operation)
        warning('Images seems to be transposed.  It should have %d columns, but had size [%d, %d].', ...
          nOp, nI, nOp);
        Images = Images';
        [nI, nOp] = size(Images);
      else
        error('Provided operation expects %d operands, which should be the number of columns in Images cell array.', ...
          nargin(Operation));
      end
    end
  elseif isempty(ContrastWeights)
    % Weights for contrasting images.  Can be for example number of trials.
    % We don't enforce normalization of weights here. AdjustContrastWeights
    % can be called before running this function.
    ContrastWeights(1:nOp) = 1/nOp;
  elseif numel(ContrastWeights) ~= nOp
    error('Length of provided ContrastWeights does not match the number of columns in Images cell array.');
  end
  
  % Generate output file names.
  if iscell(OutImages)
    if ~numel(OutImages) == nI
      error('OutImages should be the same length as the number of rows in Images, when provided as a cell array of path/file names.');
    end
  elseif ~ischar(OutImages)
    error('Unrecognized OutImages value, should be a string (path/name) or cell array of strings.');
  else
    [OutPath, OutName, OutExt] = fileparts(OutImages);
    OutImages = cell(nI, 1);
    for i = 1:nI
      [InPath, InName] = fileparts(Images{i, 1});
      if ~isempty(OutPath)
        Path = OutPath;
      else
        Path = InPath;
      end
      if ~isempty(OutExt)
        Name = OutName;
        if nI > 1
          Name = [Name, num2str(i)]; %#ok<AGROW>
        end
      else
        Name = [InName, OutName];
      end
      OutImages{i} = fullfile(Path, [Name, '.nii']);
    end
  end
  
  % -------------------------------------------------------------------
  if Interpolate
    % Use the reference file header parameters, except for data type and
    % intent, which are taken from the first file (below).
    RefNii = nifti(Reference);
    % Only interpolate spatially, so ignore any possible extra dims in the
    % reference image.
    RefnDim = min(3, numel(RefNii.dat.dim)); 
  end
  
  for i = 1:nI % Loop over independent (sets of) images.
    if ~Overwrite && exist(OutImages{i}, 'file')
      % Skip this image.
      fprintf('Not overwriting %s\n', OutImages{i});
      continue;
    end

    for Op = nOp:-1:1 % Loop over single image "operands".  Inverse
    % order such that the first image is the one we use to create the new
    % file at the end.
      if ~exist(Images{i, Op}, 'file')
        error('Image file not found: %s', Images{i, Op});
      end
      Nii = nifti(Images{i, Op});
      if Op == nOp
        if Interpolate
          NiinDim = numel(Nii.dat.dim);
          NewDim = RefNii.dat.dim(1:RefnDim);
          if NiinDim > RefnDim
            NewDim = [NewDim, Nii.dat.dim(RefnDim+1:min(NiinDim, 4))]; %#ok<AGROW>
          end
        else
          NewDim = Nii.dat.dim;
        end
        if DoContrast
          % Initialize volume array for contrasts/averaging.  Only write to
          % file at end for speed.
          if Interpolate
            Data = zeros(NewDim);
          else
            Data = zeros(Nii.dat.dim);
          end
        else
          % Initialize cell array for data of all image "operands".
          Data = cell(1, nOp);
        end
        if Interpolate
          % Get coordinates for interpolation.
          RefCoords = RegisterInterpNii(Reference, Images{i, Op}, [], 'qform');
        end
      else
        if Dim(1) ~= Nii.hdr.dim(1) 
          error('Dimension mismatch.');
        end
        if any(Dim ~= Nii.hdr.dim) || any(abs(Mat0(:) - Nii.mat0(:)) > eps(2))
          if Interpolate
            % Get coordinates for interpolation.
            RefCoords = RegisterInterpNii(Reference, Images{i, Op}, [], 'qform');
          else
            error('Different dimensions or qform coordinates.  Provide Reference if interpolation is desired.');
          end
        end
      end
      Dim = Nii.hdr.dim;
      Mat0 = Nii.mat0;
      
      % Get data.
      if DoContrast
        if Interpolate
          for t = 1:size(Data, 4)
            Data(:, :, :, t) = Data(:, :, :, t) + ContrastWeights(Op) * ...
              interp3(Nii.dat(:, :, :, t), RefCoords(:, :, :, 2), ...
              RefCoords(:, :, :, 1), RefCoords(:, :, :, 3), InterpMethod, 0); % 2 must be before 1.
          end
        else
          Data = Data + ContrastWeights(Op) * Nii.dat(:, :, :, :);
        end
      else
        Data{Op} = Nii.dat(:, :, :, :);
        if Interpolate
          for t = 1:size(Data, 4)
            Data{Op}(:, :, :, t) = interp3(Nii.dat(:, :, :, t), ...
              RefCoords(:, :, :, 2), RefCoords(:, :, :, 1), ...
              RefCoords(:, :, :, 3), InterpMethod, 0); % 2 must be before 1.
          end
        else
          Data{Op} = Nii.dat(:, :, :, :);
        end
      end
    end % Operand images loop.
    
    
    % Create output NIfTI file.
    if Interpolate
      NewNii = nifti;
      NewNii.dat = file_array(OutImages{i}, NewDim, Nii.dat.dtype);
      NewNii.intent = Nii.intent;
      NewNii.mat = RefNii.mat;
      NewNii.mat0 = RefNii.mat0;
      NewNii.mat_intent = RefNii.mat_intent;
      NewNii.mat0_intent = RefNii.mat0_intent;
    else
      % Create ouput nifti structure with header parameters of first file.
      NewNii = Nii;
      NewNii.dat.fname = OutImages{i};
    end
    NewNii.descrip = 'NIfTI-1 file created with ImageMath.m';
    create(NewNii); % Write header info to file.
   
    % Apply operation and save data to file.
    if DoContrast
      NewNii.dat(:, :, :, :) = Data;
    else
      NewNii.dat(:, :, :, :) = Operation(Data{:});
    end
      
    % Verify that the new image file exists, otherwise remove name.
    if ~exist(OutImages{i}, 'file')
      OutImages{i} = '';
    end
    
  end % Independent images loop.
  
end


