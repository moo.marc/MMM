function [MeanContrast, SigContrast, pValues] = PermutationTest( ...
    Data, Data2, nPerms, pValue, Paired, OneSided, ...
    MaxDims, MultipleComparisons)
  % Random permutation test with optional multiple comparison correction.
  %
  % [MeanContrast, SigContrast, pValue] = PermutationTest( ...
  %    Data, Data2, nPerms, pValue, Paired, OneSided, MaxDims, ...
  %    MultipleComparisons)
  %
  % One- or two-sample, paired or unpaired random permutation test of the
  % null hypothesis that the samples in arrays Data and Data2 along the
  % last dimension come from the same distribution(s). Samples along
  % dimensions other than the last are either treated as independent tests
  % (with possibility of multiple comparison correction) or a "global" test
  % (single-threshold max statistic permutation test) is performed.  A
  % combination of independent and global tests can be performed on
  % different dimensions of the data arrays.
  %
  %   *Inputs*
  % Data, Data2: Data arrays representing 2 groups of subjects. The data
  % must be arranged such that the last dimension represents the subjects,
  % i.e. the individuals that will be permuted between groups. E.g. for 3-d
  % voxel images, Data(:, :, :, 2) would be the data for the 2nd subject.
  % If Data2 is not provided, it is taken to be 0, i.e. a one-sample test
  % is performed on Data.
  %
  % All other input arguments are optional, default values in square
  % brackets.
  %
  % nPerms [2048]: Number of permutations to test. pValue [0.05]: The
  % desired threshold of significance for the test.
  %
  % Paired [true if same number of subjects, false otherwise]: Whether to
  % perform a paired or unpaired test.  Paired requires the same number of
  % subjects in both groups (logically the same ones).
  %
  % OneSided [false]: Whether to apply the requested pValue to the positive
  % side (tail) only, investigating only the contrast Data>Data2, or to
  % both sides.  When asking for a two-sided test (OneSided=false), the
  % distribution of absolute values is used, which is equivalent to the
  % Bonferroni correction, dividing the pValue equally between the 2 sides.
  %
  % MaxDims [[] (empty)]: Dimensions of the data arrays to be "grouped" in
  % order to perform a "max statistic" test.  This means we assume data
  % points along these dimensions belong to the same distribution.  If this
  % is not the case, strong effects will dominate and smaller effects that
  % would have been significant if tested independently may not survive.
  %
  % MultipleComparisons [1 (no correction)]: Number of comparisons to take
  % into account when applying the Sidak correction (assumes independent
  % tests) such that the familywise error rate will be given by the
  % provided pValue. Because in many cases the number of comparisons is not
  % simply equal to the number of data points, e.g. for time samples it
  % depends on the filtering bandwidth and for voxels it is at most equal
  % to the number of sensors, this value is left to the user to provide.
  % The default value is 1, i.e. no correction.
  %
  % Important.  While both previous options can be used simultaneously,
  % MultipleComparisons is meant to be used for dimensions that are not
  % included in MaxDims, as the max statistic test does not require
  % multiple comparison correction.
  %
  %   *Outputs*
  % MeanContrast: Averaged contrasted data between the two groups.
  %
  % SigContrast: MeanContrast where the pValues are smaller than the
  % provided pValue threshold, i.e. where the contrast is significant,
  % NaN elsewhere.
  %
  % pValues: Array of false positive probability values (p) for each of the
  % data points of the MeanContrast.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2016-03-14
  
  % Dependencies: d2b, b2d, MultiTestCorrection
  
  % Additional notes and references on the "maximal statistic" test:
  %
  % This method should probably be called "single-threshold max statistic
  % permutation test", with and indication of what statistic was used
  % (pseudo-Z or neural activity (nAm) differences for example, which are
  % not quite a T statistic).  In terms of references, finding the
  % "original" would require more research, but one of the more recent
  % neuroimaging papers below or the SPM book should be adequate.
  %
  % Note that this max statistic method is not an optimal approach in terms
  % of power for controlling the family-wide error rate (FWER).  However,
  % it is a valid procedure for it (i.e. deals with the multiple testing
  % issue), is often less conservative than Bonferroni (at least when the
  % variance is equal between the tested elements), and has been around for
  % a long time.
  %
  % A concise and simple enough explanation is in the SPM book chapter 21
  % Non-parametric procedures, by Nichols and Holmes 2007 ("Single
  % threshold test"). They refer to:
  %   Holmes et al., Nonparametric Analysis of Statistic Images from
  %   Functional Mapping Experiments, Journal of Cerebral Blood Flow and
  %   Metabolism, 16:7-22, 1996
  % An older reference found:
  %   Blair and Karniski, An alternative method for significance testing of
  %   waveform difference potentials, Psychophysiology, 30, 1993, 518-524
  % They present the method as new, but it's been around much longer, e.g.:  
  %   James M. Boyett and J. J. Shuster, Nonparametric One-Sided Tests in
  %   Multivariate Analysis with Medical Applications, Journal of the
  %   American Statistical Association, Vol. 72, No. 359 (Sep., 1977), pp.
  %   665-668
  % They have references to classics from the 50's and 60's about
  % permutation testing, and using the maximal statistic as a way to
  % control FWER was probably proposed even before.
  %
  % There is a well-known method called "maxT" which is a more powerful
  % extension of the single threshold method used here.  It comes from a
  % book:
  %   Westfall, P. H. and Young, S. S. (1993). Resampling-Based Multiple
  %   Testing: Examples and Methods for p-Value Adjustment. Wiley, New
  %   York.
  % A more recent reference to that specific maxT method:
  %   Westfall and Troendle, Multiple Testing with Minimal Assumptions,
  %   Biometrical Journal 50 (2008) 5, 745-755
  % We use a single threshold or single-step test, while the maxT method is
  % a step down procedure, i.e. it finds a different threshold for each
  % voxel, and is more powerful.  Some authors refer to the Westfall and
  % Young book for our single-step permutation test, calling it the
  % "single-step maxT" procedure, e.g.:
  %   Meinshausen et al., Asymptotic optimality of the Westfall-Young
  %   permutation procedure for multiple testing under dependence, Ann.
  %   Statist.Volume 39, Number 6 (2011), 2795-3443).
  %
  % Additional references in neuroimaging:
  %   Nichols TE, Holmes AP (2002): Nonparametric permutation tests for
  %   functional neuroimaging: a primer with examples. Hum Brain Mapp
  %   15:1-25.
  %    Singh KD, Barnes GR, Hillebrand A (2003): Group imaging of
  %    task-related changes in cortical synchronisation using nonparametric
  %    permutation testing. Neuroimage 19:1589-1601.
  %   Chau W, McIntosh AR, Robinson SE, Schulz M, Pantev C (2004):
  %   Improving permutation test power for group analysis of spatially
  %   filtered MEG data. Neuroimage 23:983-996.
  
  %  Things to consider:  
  % FDR for multiple comparison correction.
  %
  % Single group test can be overly conservative or invalid (> false
  % positives) if distribution is skewed (not symmetric) [e.g. Eklund
  % 2016].  2-sample test produces a symmetric distribution, but not as
  % clear for a 1-sample test, where the null hypothesis might simply be 0
  % mean.  Best to use a pivotal statistic (see below).
  %
  % A requirement is that the joint error distribution doesn't change.
  % Permuting requires "exchangeable errors" (identically distributed), and
  % sign flipping, "independent symmetrical errors" [Winkler 2014]. If
  % groups don't have the same variance (but ISE), instead of permuting
  % between groups, we should flip signs within groups, before performing
  % the contrast.  Both permuting and sign flipping can be done if error
  % distributions are both identical and ISE.  This is useful for small
  % groups, with limited number of possible permutations.  Non-identical
  % errors between voxels will still have the largest variance voxels
  % "drive" the result and prevent many voxels from possibly being
  % significant.
  % 
  % Pivotal statistic (its distribution does not depend on unknown
  % parameter) is best for permutation distribution.  "Real" t (not pseudo)
  % would remove variance differences, but not skew.  p-value is pivotal,
  % but its discreteness has been claimed to possibly cause a "loss of
  % power" [Winkler 2014, ref to Pantazis 2005], or that it requires a lot
  % of permutations: if many min-p are exactly equal, it prevents a
  % threshold that would allow a small alpha.  However, the delta-p is at
  % least as small as the uncertainty in p, so that implies that any
  % "significant" value that would be obtained with more permutations (or
  % finer p distribution), would be in a sense "close" to the threshold.
  % Therefore, apart from such a case due to really low number of
  % permutations, I don't consider this a "loss of power".
  %
  % An alternative to using a pivotal statistic is "non-stationarity"
  % correction [e.g. used for cluster size stats, refs in Winkler 2014].
  
  % Ensure different pseudo-randomness each run.
  RandomSettings = rng;
  if RandomSettings.Seed == 0
    rng('shuffle');
  end
  
  % -----------------------------------------------------------------------
  % Get and test input arguments.  And organize data.
  
  if ~exist('OneSided', 'var') || isempty(OneSided)
    OneSided = false;
  end
  
  if ~exist('MaxDims', 'var')
    MaxDims = [];
  end
  
  if ~exist('MultipleComparisons', 'var') || isempty(MultipleComparisons)
    MultipleComparisons = 1;
  elseif MultipleComparisons < 1
    error('MultipleComparisons must be 1 or more.');
  end
  
  if ~exist('pValue', 'var') || isempty(pValue)
    pValue = 0.05;
  end
  
  if ~exist('nPerms', 'var') || isempty(nPerms)
    nPerms = 2048;
  end
  
  DataSize = size(Data);
  if ~exist('Data2', 'var') || isempty(Data2)
    % This corresponds to "flipping signs" of the samples.
    Data2 = zeros(DataSize);
    if exist('Paired', 'var') && ~isempty(Paired) && ~Paired
      fprintf('One-sample paired and unpaired tests are equivalent, using faster paired test.');
    end
    Paired = true;
  end
  DataSize2 = size(Data2);
  % Everything is at least a 2-dim array to Matlab, and additional trailing
  % singleton dimensions are ignored. If there is 1 subject in one group,
  % one dimension will appear missing.
  nDataDims = max(ndims(Data), ndims(Data2));
  % Reject second dimension if column vector. This will only happen if
  % testing scalars.
  if nDataDims == 2 && DataSize(2) == 1 && DataSize2(2) == 1
    nDataDims = nDataDims - 1;
    DataSize(2) = [];
    DataSize2(2) = [];
  end
  % Check data matches between groups.
  if any(DataSize(1:nDataDims-1) ~= DataSize2(1:nDataDims-1))
    error('Data array size mismatch between groups.')
  end
  % Verify that Max dimensions correspond to data and not subjects.
  if any(MaxDims >= nDataDims)
    error('Requested MaxDims exceed data dimensions, assuming the last dim corresponds to subjects');
  end
  
  nDs1 = DataSize(nDataDims);
  nDs2 = DataSize2(nDataDims);
  nDatasets = nDs1 + nDs2;
  if nDatasets < 3
    error('You need at least 2 subjects in one group to permute.');
  end
  Asymmetrical = nDs1 ~= nDs2;
  
  if ~exist('Paired', 'var') || isempty(Paired)
    if Asymmetrical
      Paired = false;
      fprintf('Performing unpaired permutation test.\n');
    else
      Paired = true;
      fprintf('Performing paired permutation test.\n');
    end
  end
  
  if Paired && Asymmetrical
    error('Paired permutation test requires the same number of subjects in both groups, i.e. data arrays should have the same size.')
  end
  
  % Combine the data in one array.
  Data = cat(nDataDims, Data, Data2);
  clear Data2 DataSize2;
  DataSize(nDataDims) = nDatasets;
  
  
  % -----------------------------------------------------------------------
  % pValue corrections and checks.
  % Sidak "inverse correction" for multiple comparisons.
  RequestedpValue = pValue;
  pValue = MultiTestCorrection(RequestedpValue, MultipleComparisons, 1, true);
  %   pValue = 1 - (1 - pValue)^(1/MultipleComparisons);
  
  % Check if requested significance level is achievable.
  if Paired
    MaxPerms = 2^nDs1;
  else
    MaxPerms = nchoosek(nDatasets, nDs1);
  end
  if nPerms >= MaxPerms
    nPermsTemp = MaxPerms;
  else
    nPermsTemp = nPerms;
  end
  if ~OneSided && ~Asymmetrical
    % The Bonferroni correction for two-sided test is taken into account by
    % taking the absolute value below.  There is no need to change the
    % pValue here.  But we do end up effectively with half as many points
    % in the permutation distribution since each point will appear twice.
    nPermsTemp = ceil(nPermsTemp/2);
  end
  if nPermsTemp < 1/pValue
    % Check if maximum number of permutations for number of subjects.
    if nPerms >= MaxPerms
      error(['The requested (possibly corrected) significance level (pValue=%1.2g) cannot be achieved with the provided number of subjects. ', ...
        'Either reduce the number of independent data points and correspondingly the number of tests (MultipleComparisons), ', ...
        'or consider doing a Max or uncorrected test.'], RequestedpValue);
    else
      error(['The requested (possibly corrected) significance level (pValue=%1.2g) requires at least %d permutations '...
        '(which is possible with the provided number of subjects).'], RequestedpValue, ceil(1/pValue));
    end
  else
    CorrectedPrecision = MultiTestCorrection(1/nPermsTemp, MultipleComparisons, 1, false) / RequestedpValue;
    if CorrectedPrecision > 0.05
      if nPermsTemp >= MaxPerms
        warning(['The number of permutations cannot be increased and would give a poor precision (%1.0f%%) ', ...
          'in terms of the (possibly corrected) significance level (pValue=%1.2g). ', ...
          'It is recommended to either reduce the number of independent data points and correspondingly the number of tests (MultipleComparisons), ', ...
          'or consider doing a Max or uncorrected test.'], ...
          CorrectedPrecision * 100, RequestedpValue);
      else
        warning(['The requested number of permutations would give a poor precision (%1.0f%%) ', ...
          'in terms of the (possibly corrected) significance level (pValue=%1.2g). ', ...
          'It is recommended to increase the number of permutations.'], ...
          CorrectedPrecision * 100, RequestedpValue);
      end
    end
  end
  
  
  % -----------------------------------------------------------------------
  % Reshape data to simplify and speed up testing.  First dimension will be
  % "grouped" data from MaxDims.  Second dimension will be independent
  % data dimensions.  Third: subjects.  Use singleton dimensions if no
  % Max test or no independent data points.
  
  Singletons = [find(DataSize == 1), nDataDims + (1:2)];
  DataSize(end+(1:2)) = 1;
  if isempty(MaxDims)
    % Use first singleton dimension.
    MaxDims = Singletons(1);
  else
    % Ensure dimensions are sorted so we can get the correct shape again.
    MaxDims = sort(MaxDims);
  end
  NonMaxDims = setdiff(1:nDataDims-1, MaxDims);
  if isempty(NonMaxDims)
    % Use first singleton dimension, other than those in MaxDims.
    Singletons = setdiff(Singletons, MaxDims);
    NonMaxDims = Singletons(1);
  end
  DimPermutation = [MaxDims, NonMaxDims, nDataDims];
  [~, InverseDimPerm] = sort(DimPermutation);
  nMaxElements = prod(DataSize(MaxDims)); % prod([]) = 1
  nIndepElements = prod(DataSize(NonMaxDims));
  Data = reshape(permute(Data, DimPermutation), ...
    nMaxElements, nIndepElements, nDatasets, []);
  
  
  % -----------------------------------------------------------------------
  % Main section.
  
  % Compute the real contrast first.
  % This formula will do proper averages before contrast even if groups
  % have different numbers in unpaired condition.
  Coefficients(1, 1, :) = [ones(1, nDs1)./nDs1, -ones(1, nDs2)./nDs2];
  MeanContrast = sum(bsxfun(@times, Data, Coefficients), 3);
  
  % Get rid of values that are due to machine precision errors (make them zero).
  % Total error in contrast values.
  Precision = sum(eps(Data), 3);
  MeanContrast(abs(MeanContrast) <= Precision) = 0;
  
  % Stop here if not asking for anything else.
  if nargout < 2
    % Give back original data shape.
    MeanContrast = permute( reshape(MeanContrast, DataSize(DimPermutation(1:end-1))), InverseDimPerm );
    return;
  end
  
  % Generate the permutations as logical arrays indicating which group each
  % subject belongs to.
  if Paired
    % Permutation distribution is symmetrical so we can do only half as
    % many permutations.  For the one-sided test, we get 2 different points
    % from each permutation (-min and max), while for the two-sided test,
    % we use max(abs()), effectively the Bonferroni correction, which would
    % give twice each result over all permutations.
    %
    % So instead, we set the first subject in the first group and permute
    % the rest only.  The "negative" side then corresponds to all
    % permutations where the first subject would be in the second group.
    [Combinations, nPerms] = RandomSubsets(nDs1 - 1, nPerms/2);
    if OneSided
      % We get 2 points for each permutation for the distribution.
      nDistrib = 2 * nPerms;
    else
      % We only get 1 point per permutation.
      nDistrib = nPerms;
    end
    Combinations = [true(nPerms, 1), Combinations, ...
      false(nPerms, 1), ~Combinations];
  else % unpaired.
    if ~Asymmetrical
      % Distribution is symmetrical as for the paired test; only use half.
      [Combinations, nPerms] = RandomCombinations(nDatasets - 1, nDs1 - 1, nPerms/2);
      Combinations = [true(nPerms, 1), Combinations];
      if OneSided
        % We get 2 points for each permutation for the distribution.
        nDistrib = 2 * nPerms;
      else
        % We only get 1 point per permutation.
        nDistrib = nPerms;
      end
      
    else % Different number of subjects in the two groups.
      % "Opposite contrasts" are no longer part of the distribution so we
      % must use all nPerms permutations and look only at max for 1-sided
      % test.
      [Combinations, nPerms] = RandomCombinations(nDatasets, nDs1, nPerms);
      nDistrib = nPerms;
    end
  end
  
  % Get the distributions of contrast values across permutation of subjects
  % between the two groups, for each independent (i.e. non-grouped for the
  % Max test) data point.
  Distrib(nIndepElements, nDistrib) = 0;
  for p = 1:nPerms
    Coefficients(Combinations(p, :)) = 1/nDs1;
    Coefficients(~Combinations(p, :)) = -1/nDs2;
    Contrast = sum(bsxfun(@times, Data, Coefficients), 3);
    % Zero values smaller than precision.
    Contrast(abs(Contrast) <= Precision) = 0;
    if nMaxElements > 1
      % Find max (over Max group) distribution for each data point.
      % (Best always to give dimension explicitly to max and min in case of
      % row vector, though we just tested for this.)
      if OneSided
        Distrib(:, p) = max(Contrast, [], 1); % max over first dim.
        if ~Asymmetrical
          Distrib(:, p + nPerms) = -min(Contrast, [], 1);
        end
      else % 2-sided, use absolute value.
        Distrib(:, p) = max(abs(Contrast), [], 1); % max over first dim.
      end
    else % No Max test, no need for max.
      % Find distribution for each data point.
      if OneSided
        Distrib(:, p) = Contrast;
        if ~Asymmetrical
          Distrib(:, p + nPerms) = -Contrast;
        end
      else
        Distrib(:, p) = abs(Contrast);
      end
    end
  end
  % Sort the distributions.
  Distrib = sort(Distrib, 2);
  
  
  % Find significant values in mean contrast.
  % Index along distribution where the significance threshold should be.
  ThresholdIndex = floor((1 - pValue) * nDistrib) + 1;
  Thresholds(1, :) = Distrib(:, ThresholdIndex);
  % Check for duplicate value of the chosen threshold(s) preceding it in
  % the distribution, which would require finding the next larger value(s)
  % to use as threshold(s).
  if ThresholdIndex > 1 % (1 is possible if pValue = 1 or almost.)
    if any( Thresholds(:) == Distrib(:, ThresholdIndex - 1))
      NeedFixing = find(Thresholds(:) == Distrib(:, ThresholdIndex - 1));
      for i = NeedFixing(:)' % Older Matlab requires row vector.
        t = ThresholdIndex + 1;
        while t <= nDistrib && Thresholds(i) == Distrib(i, t)
          t = t + 1;
        end
        if t > nDistrib
          % No larger value found.  Nothing will be above threshold.
          Thresholds(i) = Inf;
        else
          % Found new threshold.
          Thresholds(i) = Distrib(i, t);
        end
      end
    end
  end
  
  if OneSided
    SigIndices = find( bsxfun(@gt, MeanContrast, Thresholds) ); % MeanContrast > Thresholds(OnesVectors{1}, :)
  else
    SigIndices = find( bsxfun(@gt, abs(MeanContrast), Thresholds) ); % abs(MeanContrast) > Thresholds(OnesVectors{1}, :)
  end
  
  % Create array with only significant values.
  SigContrast = NaN(nMaxElements, nIndepElements);
  SigContrast(SigIndices) = MeanContrast(SigIndices);
  
  % Calculate pValues for each data point of the mean contrast.
  if nargout > 2
    pValues(nMaxElements, nIndepElements) = 0;
    for i = 1:nIndepElements
      for o = 1:nMaxElements
        if OneSided
          Index = find(Distrib(i, :) >= MeanContrast(o, i), 1, 'first');
        else
          Index = find(Distrib(i, :) >= abs(MeanContrast(o, i)), 1, 'first');
        end
        if isempty(Index)
          % This can happen when not doing every possible permutation.
          % Would give a pValue of 0, so return instead 0 + uncertainty.
          Index = nDistrib;
        end
        pValues(o, i) = 1 - (Index - 1)/nDistrib;
      end
    end
    % Sidak correction for multiple comparisons.
    pValues = 1 - (1 - pValues).^(MultipleComparisons);
    % Give back original data shape.
    pValues = permute( reshape(pValues, DataSize(DimPermutation(1:end-1))), InverseDimPerm );
  end
  
  % Give back original data shape.
  MeanContrast = permute( reshape(MeanContrast, DataSize(DimPermutation(1:end-1))), InverseDimPerm );
  SigContrast = permute( reshape(SigContrast, DataSize(DimPermutation(1:end-1))), InverseDimPerm );
  
end



