function p = MultiTestCorrection(p, nTests, Method, Inverse)
  % Correct p-values for multiple testing, by Bonferroni or Sidak method.
  % 
  %  p = MultiTestCorrection(p, nTests, Method, Inverse)
  %
  % Given a certain number of tests or comparisons (nTests), correct
  % p-values (p) to obtain the family-wise error rate.  Uses the Bonferroni
  % correction by default (Method = 0), but can also use Sidak (Method =
  % 1).  Can go from FWER to single-test p-values by setting Inverse to
  % true (default false).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2016-03-14
  
  % TO DO: Extend to sets of p-values:
  % Holm's (closed procedure, like Bonferroni, no restriction on joint
  %   distribution of the test statistic)
  % Hochberg's (based on Simes, only non-negative dependence)
  % Westfall and Young (Max T and subset pivotality?)
  % Romano and Wolf (Max T? no condition?)
  % FDR as well.

  if ~exist('Inverse', 'var') || isempty(Inverse)
    Inverse = false;
  end
  if ~exist('Method', 'var') || isempty(Method)
    Method = 0;
  end
  
  if nTests < 1
    error('Number of tests must be 1 or more.');
  end
  
  switch Method
    case 0 % Bonferroni correction.
      if ~Inverse
        p = p .* nTests;
      else
        p = p ./ nTests;
      end
      
    case 1 % Sidak correction.
      if ~Inverse
        p = 1 - (1 - p).^(nTests);
      else
        p = 1 - (1 - p).^(1./nTests);
      end
  
    otherwise
      error('Unknown multiple test correction method, should be 0 (Bonferroni) or 1 (Sidak).');
  end
  
end