function CI = PermutationTestCI(P, N, ConfidenceLevel, Approximate)
  % Confidence intervals for permutation test statistical significance values.
  % 
  % CI = PermutationTestCI(P, N, ConfidenceLevel, Approximate)
  %
  % Given a certain number of permutations (N), converts p-values (P) into
  % confidence intervals at the desired confidence level (default 0.95,
  % i.e. 95%).  For more than a few values, the default is to use an
  % approximate formula, as the exact formula can be a bit slow.
  % 
  % The returned variable has size numel(P) by 2, P is converted to a
  % column vector regardless of its dimensions: P(:).
  % 
  % Requires Statistics Toolbox.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-05-12
  
  % Check if stats toolbox is present.
  v = ver;
  if ~ismember('Statistics Toolbox', {v.Name})
    error('This function requires the Statistics Toolbox.');
  end
  
  % Parse inputs.
  nP = numel(P);
  if ~exist('Approximate', 'var') || isempty(Approximate)
    if nP <= 10
      Approximate = false;
    else
      Approximate = true;
    end
  end
  if ~exist('ConfidenceLevel', 'var') || isempty(ConfidenceLevel)
    ConfidenceLevel = 0.95;
  elseif ConfidenceLevel > 1 && ConfidenceLevel < 100
    ConfidenceLevel = ConfidenceLevel/100;
    warning('ConfidenceLevel should be given as a proportion, not a percentage.  Assuming %1.2f.', ConfidenceLevel);
  end
  
  CI = zeros(nP, 2);
  
  if ~Approximate
    for iP = 1:nP
      % Create distribution object.
      pd = makedist('Binomial', 'N', N, 'p', P(iP));
      
      % Inverse cumulative distribution function.
      % y = cdf(pd, x, 'upper');
      CI(iP, :) = icdf(pd, [(1-ConfidenceLevel)/2, 1 - (1-ConfidenceLevel)/2]);
    end
    CI = CI ./ N;
    
  else
    % Wilson 'score' method with continuity correction (see Wallis 2013 ->
    % Newcombe 1998 -> Blyth 1983 ...?)
    pd = makedist('Normal');
    % Normal distribution threshold for confidence level.
    Z = icdf(pd, 1 - (1-ConfidenceLevel)/2);
    Z2 = Z^2;
    % Need P as column vector.
    P = P(:);
    CI = ( (P * 2 * N + Z2) * [1, 1] + ...
      [ - 1 - Z * sqrt(Z2 - 2 - 1/N + 4 * P .* (N * (1 - P) + 1)), ...
      + 1 + Z * sqrt(Z2 + 2 - 1/N + 4 * P .* (N * (1 - P) - 1)) ] ...
      ) / 2 / (N + Z2);
  end
end
