function [Model, Stats, YFit, Residuals] = MultiRegress(Y, X) 
  % Multivariate linear regression.
  %
  %  [Model, Stats, YFit, Residuals] = MultiRegress(Y, X) 
  %
  % Multiple regression of dependent data Y, size [nO - number of objects,
  % nY - number of variables], i.e. data in columns, on independent data X,
  % size [nO, nX].  (This is asymmetric, not the same relation as doing X
  % on Y.) 
  %
  % Model is a structure with fields RegCoeff, the regression coefficients,
  % size [nX, nY], and Intercept, size [1, nY], such that: 
  %  YFit = X * RegCoeff + Intercept.
  %
  % Stats contains the fields:
  %  RSq: Proportion of explained variance: ratio of explained sum of
  %    squares to total SS.  This does not have expected value of 0 for
  %    random X.
  %  RSqAdj: Unbiased estimate of explained proportion of variance, using
  %    unbiased variance with correct degrees of freedom for each: here
  %    we're using 1 - residuals variance / total.
  %  F: Unbiased estimate of the ratio of explained to unexplained
  %    variance.
  %  And global equivalents (for all dependent variables at once): GlobRSq,
  %  GlobRSqAdj, GlobF.
  %
  % Residuals = Y - YFit.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-08-17
  
  % Ordinary least squares forces the regression line to go through the
  % center of mass.  Center columns for both X and Y, such that 
  %  cYFit = cX * RegCoeff.
  MeanY = mean(Y, 1);
  cY = bsxfun(@minus, Y, MeanY);
  MeanX = mean(X, 1);
  cX = bsxfun(@minus, X, MeanX);

  % Left multiply by X' so it's square and can be inverted, thus isolating
  % RegCoeff.  This is the ordinary least squares solution, which minimizes
  % the sum of square residuals in Y only.
  Model.RegCoeff = cX' * cX \ (cX' * cY);
  
  Model.Intercept = MeanY - MeanX * Model.RegCoeff;
  
  if nargout <= 1
    return;
  end
  
  [nO, nX] = size(X);
  %   nY = size(Y, 2);
  
  cYFit = cX * Model.RegCoeff;
  % Proportion of explained variance: ratio of explained sum of squares to
  % total SS.  This does not have expected value of 0 for random X.
  Stats.RSq = diag(cYFit' * cYFit)' ./ diag(cY' * cY)'; % [1, nY]
  % Unbiased estimate of explained proportion of variance, using unbiased
  % variance with correct degrees of freedom for each: here we're using 1 -
  % residuals variance / total.  For residuals, mean y and nX regression
  % coefficients.  For total variance, just mean y.  This leaves nX for
  % explained. (nO - 1) - (nO - 1 - nX) = nX.
  Stats.RSqAdj = 1 - (1 - Stats.RSq) * (nO - 1) / (nO - 1 - nX);
  % Similarly, unbiased estimate of the ratio of explained to unexplained
  % variance.
  Stats.F = 1./(1./Stats.RSq - 1) * (nO - 1 - nX)/nX;
  
  Stats.GlobRSq = trace(cYFit' * cYFit) / trace(cY' * cY);
  Stats.GlobRSqAdj = 1 - (1 - Stats.GlobRSq) * (nO - 1) / (nO - 1 - nX); % * nY/nY in dof's.
  Stats.GlobF = 1./(1./Stats.GlobRSq - 1) * (nO - 1 - nX)/nX; % * nY/nY in dof's again.
  
  if nargout > 2
    YFit = bsxfun(@plus, cYFit, MeanY);
  end
  
  if nargout > 3
    Residuals = Y - YFit;
  end
  
end

