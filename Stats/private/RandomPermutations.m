function [Permutations, Count] = RandomPermutations(n, Count, IncludeIdent)
  % Distinct random orderings of n elements (out of n! possible). 
  %
  % [Permutations, Count] = RandomPermutations(n, Count, IncludeIdent)
  %
  % Permutations (size = [Count, n]): Integer array of orderings (rows).
  % Count may be less than requested if greater than the possible number.
  %
  % IncludeIdent [default true]: When true, the "identity" permutation is 
  % included, and since the returned set of permutations is sorted, the
  % identity will be first.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-03-15
  
  if nargin < 3 || isempty(IncludeIdent)
    IncludeIdent = true;
  end
  MaxCount = factorial(n);
  if Count >= MaxCount
    Count = MaxCount;
    if eps(Count) >= 1
      error('Asked for too many permutations: %f', Count);
    end
    Exact = true;
  else
    if Count >= MaxCount/2
      if eps(MaxCount) >= 1
        error('Asked for too many permutations: %f', Count);
      end
      % Do exact then randomly pick the rows to keep.  Faster than trying to
      % randomly pick most of the possible different permutations (many
      % duplicates to correct).
      if verLessThan('matlab', '7.13.0')
        % Older Matlab doesn't have randperm(n,k), and randperm required
        % much memory.
        PermIndices = my_randperm(MaxCount, Count); % The permutations that we will actually keep.
      else
        PermIndices = randperm(MaxCount, Count); % The permutations that we will actually keep.
      end
      Count = MaxCount;
      Exact = true;
    else
      if eps(Count) >= 1
        error('Asked for too many permutations: %f', Count);
      end
      Exact = false;
    end
  end
  
  if Exact
    % Get all permutations, then possibly take a subset.
    Permutations = perms(1:n);
    if exist('PermIndices', 'var')
      Permutations = Permutations(PermIndices, :);
      Count = length(PermIndices);
    end
  else
    % Construct random set, then possibly remove duplicates.
    % Haven't thought of a "matrix", element looping method.
    [~, Permutations] = sort(rand(Count, n), 2); % This is the old randperm(n).

    % Verify and replace duplicate permutations, but duplicates are almost
    % impossible with reasonable numbers.
    %     Permutations = unique(Permutations, 'rows'); % Uses more memory. 
    Permutations = sortrows(Permutations);
    Permutations([false; ...
      all(Permutations(1:end-1, :) == Permutations(2:end, :), 2)], :) = [];
    while size(Permutations, 1) < Count
      %       Permutations = unique([Permutations; randperm(n)], 'rows');
      Permutations = sortrows(Permutations);
      Permutations([false; ...
        all(Permutations(1:end-1, :) == Permutations(2:end, :), 2)], :) = [];
    end
    
  end % method choice
  
  if IncludeIdent
    % Already sorted.
    %     Permutations = sortrows(Permutations); 
    % Make sure identity is present.
    if any(Permutations(1, :) ~= 1:n)
      Permutations(randi([2, Count+1], 1), :) = [];
      Permutations = [1:n; Permutations]; 
    end
  end
end % function RandomPermutations



