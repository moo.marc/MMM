function y = b2d(x)
  % Convert logical (binary) array into decimal numbers.
  %
  %  y = b2d(x)
  %
  % x is assumed to have binary digits along the last dimension, least
  % significant digits first. If x is a row vector, we assume a single
  % number with bits along the column.
  %
  % Source: b2d, from Matlab File Exchange.
  % http://www.mathworks.com/matlabcentral/fileexchange/26447
  % (C) Copyright 2010 Zacharias Voulgaris
  % See licence info in External/LICENSE_FEX
  %
  % Modified to work on arrays.
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2012-04
  
  % Find which dimension to use.
  % There is no second dimension if column vector!
  nD = ndims(x); % Everything is at least a 2-dim array to Matlab, and additional trailing singleton dimensions are ignored.
  InputSize = size(x);
  if nD == 2 && InputSize(2) == 1
    nD = nD - 1;
    InputSize(2) = [];
  end
  
  % Number of binary digits.
  nBits = InputSize(nD);
  % Number of decimal numbers.
  nDec = prod(InputSize(1:nD - 1));
  
  % Reshape to consistent 2-d array.
  x = reshape(x, [nDec, nBits]);
  
  % Initialize output array with correct shape.
  y = zeros([InputSize(1:nD-1), 1]);
  
  % Compute the decimal numbers and fill ouput array.
  Bits(1, :) = 2.^(0:nBits-1);
  y(:) = sum(x .* Bits(ones(nDec, 1), :), 2);
  
end
