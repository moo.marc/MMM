function y = d2b(x, nBits)
  % Convert decimal numbers into a logical (binary) array.
  %
  %  y = d2b(x, nBits)
  %
  % The result has least significant digits first.  The function is meant
  % to work on integers and the fractional part is truncated.  Adds a
  % trailing dimension for binary digits.  Optional argument nBits pads or
  % truncates most significant bits so that the size of the added dimension
  % is nBits.
  %
  % Source: d2b, from Matlab File Exchange.
  % http://www.mathworks.com/matlabcentral/fileexchange/26447
  % (C) Copyright 2010 Zacharias Voulgaris
  % See licence info in External/LICENSE_FEX
  %
  % Modified to work on arrays.
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2012-04
  
  % Number of binary digits.
  if ~exist('nBits', 'var') || isempty(nBits)
    nBits = floor( log2(max(x(:))) ) + 1;
  end
  
  % Find which dimension to use.
  % There is no second dimension if column vector!
  nD = ndims(x); % Everything is at least a 2-dim array to Matlab, and additional trailing singleton dimensions are ignored.
  InputSize = size(x);
  if nD == 2 && InputSize(2) == 1
    nD = nD - 1;
    InputSize(2) = [];
  end
  
  % Initialize output array.
  y = false([InputSize, nBits]);
  InputDims(1:nD) = {':'};

  % In case the inputs are not integers.
  x = floor(x);

  % Compute the bits.
  for b = 1:nBits
    x = x ./ 2;
    y(InputDims{:}, b) = x ~= fix(x);
    x = floor(x);
  end
  % This was relatively equivalent in terms of speed.
  %     for b = 1:nBits
  %       r = floor(x ./ 2);
  %       y(InputDims{:}, b) = logical(x - 2*r);
  %       x = r;
  %     end
  
end

