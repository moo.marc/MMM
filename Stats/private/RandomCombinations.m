function [Combinations, Count] = RandomCombinations(n, k, Count, IncludeIdent)
  % Distinct random combinations of k out of n elements without repetition.
  %
  % [Combinations, Count] = RandomCombinations(n, k, Count, IncludeIdent)
  %
  % Combinations (size = [Count, n]) is a logical array where 'chosen'
  % elements are 'true'.  Count may be less than requested if greater than
  % the possible number.
  %
  % IncludeIdent [default true]: When true, the "identity" (1:k is true,
  % rest false) is included, and since the returned set of combinations is
  % sorted, the identity will be last.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-03-14
  
  if nargin < 4 || isempty(IncludeIdent)
    IncludeIdent = true;
  end

  Verbose = false;
  % Avoid warning from nchoosek precision limits if numbers are too big anyway.
  if eps(2^n) < 1
    MaxCount = nchoosek(n, k);
  else
    MaxCount = 1/eps('double');
  end;
  if Count >= MaxCount
    Count = MaxCount;
    if eps(Count) >= 1
      error('Asked for too many permutations: %f', Count);
    end
    Exact = true;
    if Verbose
      fprintf('Exact test with %d permutations.\n', Count);
    end
  else
    if Verbose
      fprintf('Random test with %d permutations.\n', Count);
    end
    if Count >= MaxCount/2
      if eps(MaxCount) >= 1
        error('Asked for too many permutations: %f', Count);
      end
      % Do exact then randomly pick the rows to keep.  Faster than trying to
      % randomly pick most of the possible different combinations (many
      % duplicates to correct).
      if verLessThan('matlab', '7.13.0')
        % Older Matlab doesn't have randperm(n,k), and randperm required
        % much memory.
        CombIndices = my_randperm(MaxCount, Count); % The combinations that we will actually keep.
      else
        CombIndices = randperm(MaxCount, Count); % The combinations that we will actually keep.
      end
      Count = MaxCount;
      Exact = true;
    else
      if eps(Count) >= 1
        error('Asked for too many permutations: %f', Count);
      end
      Exact = false;
    end
  end
  
  Combinations(Count, n) = false;
  if Exact
    Picks = nchoosek(1:n, k);
    for c = 1:Count % combination
      Combinations(c, Picks(c, :)) = true;
    end
    if exist('CombIndices', 'var')
      Combinations = Combinations(CombIndices, :);
      Count = length(CombIndices);
    end
  else
    
    % Haven't thought of a fast non-looping method yet, and that wouldn't
    % require verification.
    nChosen = zeros(Count, 1);
    for e = 1:n % element
      Combinations(:, e) = rand(Count, 1) < ((k - nChosen) ./ (n - e + 1));
      nChosen = nChosen + Combinations(:, e);
    end
    
    % Verify and replace duplicate combinations.
    Combinations = unique(Combinations, 'rows');
    nC = size(Combinations, 1);
    while nC < Count
      nChosen = 0;
      for e = 1:n % element
        Combinations(nC+1, e) = rand(1) < ((k - nChosen) ./ (n - e + 1));
        nChosen = nChosen + Combinations(nC+1, e);
      end
      Combinations = unique(Combinations, 'rows');
      nC = size(Combinations, 1);
    end
    
  end % method choice
  
  if IncludeIdent
    % Make sure identity is present, and sort (with unique).
    Combinations = unique([true(1,k), false(1,n-k); Combinations], 'rows');
    if size(Combinations, 1) > Count
      Combinations(randi([1, Count], 1), :) = [];
    end
  end

end % function RandomCombinations

