function [Subsets, Count] = RandomSubsets(n, Count, IncludeIdent)
  % Distinct random subsets (combinations without repetition) out of n elements (out of 2^n possible). 
  %
  % [Subsets, Count] = RandomSubsets(n, Count, IncludeIdent)
  %
  % Subsets (size = [Count, n]) is a logical array where 'chosen'
  % elements are 'true'.  Count may be less than requested if greater than
  % the possible number.
  %
  % IncludeIdent [default true]: When true, the "identity" subset (the full
  % set) is included, and since the returned set of subsets is sorted, the
  % identity will be last.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-03-14
  
  if nargin < 3 || isempty(IncludeIdent)
    IncludeIdent = true;
  end
  
  if Count >= 2^n
    Count = 2^n;
    Exact = true;
  else
    Exact = false;
  end
  
  if eps(2^n) < 1 % n < 52 % eps(2^52) = 1 and is therefore no longer accurate as an integer.
    % Use first method: binary subset.
    % This is the fastest method, even more than filling directly (like
    % in combn_mod).
    if Exact
      % All possible combinations for any number of choices among n.
      Picks = 0:2^n-1; % = zeros(100, 'uint64');
    else
      if verLessThan('matlab', '7.13.0')
        % Older Matlab doesn't have randperm(n,k), and randperm(2^n)
        % required too much memory.
        Picks = my_randperm(2^n, Count) - 1; % Random distinct integers, not sorted.
      else
        Picks = randperm(2^n, Count) - 1; % Random distinct integers, not sorted.
      end
    end
    Subsets = d2b(Picks', n); % Logical array of binary digits, least significant first.
    
  else
    % Method two: random construction, need to check for duplicates.
    if Exact || Count > 2^(n-1) % Could verify memory demand for large Count too, prevent out of memory errors.
      % Random picks might generate many duplicates, but also way too many.
      error('Asked for too many subsets: %f', Count);
    end
    Subsets = rand(Count, n) < 0.5;
    % Verify and replace duplicate subsets, but duplicates are almost
    % impossible with reasonable numbers.
    Subsets = unique(Subsets, 'rows');
    while size(Subsets, 1) < Count
      Subsets = unique([Subsets; rand(1, n) < 0.5], 'rows');
    end
    
  end % method choice

  if IncludeIdent
    % Make sure identity is present, and sort (with unique).
    Subsets = unique([Subsets; true(1,n)], 'rows');
    if size(Subsets, 1) > Count
      Subsets(randi([2, Count+1], 1), :) = [];
    end
  end
  
end % function RandomSubsets


