function [Avg, SigAvg, CI, Likelihoods] = BootstrapTest(Data, ...
    nBoots, Alpha, OneSided, Method, DataIsBootDistrib)
  % Inference test on sample mean based on bootstrap distribution.
  %
  % [Avg, SigAvg, CI, Likelihoods] = BootstrapTest(Data, ...
  %     nBoots, Alpha, OneSided, Method, DataIsBootDistrib)
  %
  % Test of the null hypothesis that the population mean is zero.  Each
  % data point is treated independently and its own bootstrap distribution
  % is computed.  If zero is found to be outside the bootstrap alpha-level
  % confidence interval, then the likelihood of obtaining such a sample
  % (with a CI that does not include 0) by chance (with random sampling
  % from the population) is less than alpha and this mean is considered
  % "significantly different from 0".
  %
  % Data (required) can have any dimension/size, but the last dimension
  % represents objects that will be bootstrapped (e.g. trials or subjects).
  %
  % nBoots [2048]: Number of resamplings to perform for the bootstrap
  % distribution.
  %
  % Alpha [0.05]: Confidence level of the test.
  %
  % OneSided [false]: If true, only positive values are considered; zero
  % must be on the left side of the alpha-level confidence interval for the
  % value to be considered significant.
  %
  % Method [3]:
  %  1 Basic bootstrap [2t -t*(1-Alpha), 2t -t*(Alpha)]
  %   2 Percentile bootstrap [t*(Alpha), t*(1-Alpha)]
  %  3 Studentized bootstrap [t -sqrt(v) z*(1-Alpha), t -sqrt(v) z*(Alpha)]
  % According to minimal research, 3 might be more accurate, though 2 may
  % outperform 3 with enough trimming (>20%).  2 may be too narrow for
  % small samples, or inaccurate when the resampling distributions is not
  % symmetrical or centered on the observed sample mean.  However, 2 will
  % respect restrictions in data range (e.g. positive values only), whereas
  % 3 is based on the normal distribution and would not, therefore possibly
  % returning impossible (e.g. negative) CI limits.  A good idea is to
  % compare 2 and 3.  In case of disagreement, other more advanced methods
  % should probably be used (BCa, ABC), but have not yet been implemented
  % here.
  %
  % DataIsBootDistrib [false]: In some cases the data cannot be obtained
  % before resampling (e.g. evoked beamformer images).  In such cases, by
  % setting DataIsBootDistrib true, the already bootstrapped distribution
  % can be passed in Data, and SigAvg will be a logical significance mask
  % that can be applied to the average (which then needs to be calculated
  % separately).
  %
  % Avg: Sample mean.
  %
  % SigAvg: Sample mean thresholded such that the likelihood is smaller
  % than alpha.  Values below threshold are replaced by NaN.
  %
  % Likelihoods: [Not yet implemented.]  Likelihood of obtaining a sample
  % whose CI includes the population mean (zero) by chance.  Thus it is
  % possible to obtain likelihood 0, but this should still be interpreted
  % as having a finite error, thus is not significant to any precision
  % desired.  [It would be better to compute the likelihood uncertainty and
  % return this instead of 0.]
  %
  % Note from the author: I am not a statistician, but from little
  % research, bootstrapping seems "dangerous" in the sense that it does not
  % give good estimates in many cases and can easily be misused or
  % misinterpreted.  Use this function with care and at your own risk.
  % 
  %
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-29
  
  % Additional notes for myself:
  
  % Confidence interval meaning: If the population parameter is within the
  % (1-alpha) CI, then the likelihood of obtaining such a sample whose
  % bootstrap CI includes the population parameter is >= (1-alpha).  This
  % is NOT the same as saying "the population parameter has probability
  % (1-alpha) of being in this CI"; this is not "proper" statistical
  % language since the population parameter and the particular CI we
  % obtained are not random variables (it either is or isn't, even though
  % we don't know which is the case). However it does seem appropriate to
  % think this way in terms of our knowledge, our "confidence" about the
  % population parameter.
  
  % Seems this could be done with permutation test, but it wouldn't exactly
  % be the same null hypothesis: the permutation null is that both groups
  % are from the same population (or for one sample we assume a symmetrical
  % distribution about 0), while for bootstrap only the means are equal. In
  % both cases, we're also assuming each object had the same probability of
  % being drawn in the parent population.  In permutation, we obtain a
  % symmetrical distribution centered at zero, in bootstrap, we obtain an
  % unconstrained distribution, though likely nearly centered at the sample
  % mean.
  
  % Ensure different pseudo-randomness each run.
  RandomSettings = rng;
  if RandomSettings.Seed == 0
    rng('shuffle');
  end
  
  % -----------------------------------------------------------------------
  % Get and test input arguments.  And organize data.
  
  if ~exist('OneSided', 'var') || isempty(OneSided)
    OneSided = true;
  end
  
  if ~exist('Alpha', 'var') || isempty(Alpha)
    Alpha = 0.05;
  elseif Alpha < 0 || Alpha > 1
    error('Alpha should be >= 0 and <= 1.');
    % 0 and 1 are useless, but having these extreme return the correct
    % results is a good test on limits and comparisons.
  end
  
  if ~exist('nBoots', 'var') || isempty(nBoots)
    nBoots = 2048;
  end
  
  if ~exist('DataIsBootDistrib', 'var') || isempty(DataIsBootDistrib)
    DataIsBootDistrib = false;
  end
  
  if ~exist('Method', 'var') || isempty(Method)
    if DataIsBootDistrib
      Method = 2;
    else
      Method = 3;
    end
  elseif DataIsBootDistrib && Method ~= 2
    error('This function can only do percentile bootstrap on already bootstrapped data.');
  end
  
  % Everything is at least a 2-dim array to Matlab, and additional trailing
  % singleton dimensions are ignored. If there is 1 subject in one group,
  % one dimension will appear missing.
  nDataDims = ndims(Data);
  DataSize = size(Data);
  % Reject second dimension if column vector. This will only happen if
  % testing scalars.
  if nDataDims == 2 && DataSize(2) == 1
    nDataDims = nDataDims - 1;
    DataSize(2) = [];
  end
  
  % Number of objects from which to resample, e.g. trials or subjects.
  nO = DataSize(nDataDims);
  if nO < 2
    error('You need at least 2 objects to do a bootstrap.');
  end
  DataSize(nDataDims) = [];
  nD = prod(DataSize);
  % Reshape data to simplify and speed up testing.  First dimension will be
  % independent data dimensions.  Second: objects.
  if numel(DataSize) > 1
    Data = reshape(Data, [nD, nO]);
    Reshaped = true;
  else
    Reshaped = false;
  end
  
  % -----------------------------------------------------------------------
  % Main section.
  
  if DataIsBootDistrib
    nBoots = nO;
    Distrib = Data;
    Avg = nan(nD, 1);
  else
    
    % Compute the real contrast first.
    Avg = mean(Data, 2);
    % Get rid of values that are due to machine precision errors (make them zero).
    % Total error in contrast values.
    Precision = sum(eps(Data), 2);
    Avg(abs(Avg) <= Precision) = 0;
    
    % Stop here if not asking for anything else.
    if nargout < 2
      if Reshaped
        % Give back original data shape.
        Avg = reshape(Avg, DataSize);
      end
      return;
    end
    
    Multisets = randi(nO, nBoots, nO);
    % My "resampling" function below was NOT equivalent to uniform random
    % resampling with repetition, and produced larger CI intervals.  randi is
    % correct.
    %   [Multisets, nBootsExact] = RandomMultisets(nO, nO, nBoots, true, true); % Distinct and include identity.
    %   if nBootsExact < nBoots
    %     nBoots = nBootsExact;
    %     fprintf('Exact bootstrapt test with %d resamplings.\n', nBootsExact);
    %   end
    % Travis' method was also correct (in terms of producing the expected
    % alpha number of false positives, but it artificially equalizes draws of
    % each element (probably not a bad thing).
    %   Multisets = (1:nO);
    %   Multisets = Multisets(ones(nBoots, 1), :);
    %   [~, Order] = sort(rand(numel(Multisets), 1));
    %   Multisets(:) = Multisets(Order);
    
    Distrib = zeros(nD, nBoots);
    switch Method
      case {1, 2}
        for b = 1:nBoots
          Distrib(:, b) = mean(Data(:, Multisets(b, :)), 2);
        end
      case 3
        for b = 1:nBoots
          Distrib(:, b) = ( mean(Data(:, Multisets(b, :)), 2) - Avg ) .* ...
            sqrt(nO ./ var(Data(:, Multisets(b, :)), 0, 2));
        end
    end
  end
  
  % Sort the distributions.
  Distrib = sort(Distrib, 2);
  
  % For one sided, still find CI (both sides), but with Alpha on each side.
  % For two sided, use half Alpha on each side.
  if ~OneSided
    Alpha = Alpha / 2;
  end
  
  % Find significant values in mean contrast.
  
  % Index along distribution where the significance threshold should be.
  % This works for Alpha=0 (index past entire distribution, but this case
  % is rejected above) or 1 (index is first element, thus entire
  % distribution accepted); this requires accepting values = to threshold.
  % This way is more conservative: it enlarges the CI, but excludes its
  % limits, or in other words, the "significance" regions are smaller, but
  % include their limits.
  iAlpha = [floor(Alpha * nBoots), ceil((1 - Alpha) * nBoots) + 1];
  if iAlpha(2) > nBoots
    CI(:, 2) = ones(nD, 1) * Inf;
  else
    CI(:, 2) = Distrib(:, iAlpha(2));
  end
  if iAlpha(1) < 1
    CI(:, 1) = ones(nD, 1) * -Inf;
  else
    CI(:, 1) = Distrib(:, iAlpha(1));
  end
  %   Check for duplicate value of the chosen threshold(s) preceding it in
  %   the distribution, which would require finding the next larger value(s)
  %   to use as threshold(s).
  if iAlpha(2) > 1 % (1 is possible if Alpha = 1 or almost.)
    if any(CI(:, 2) == Distrib(:, iAlpha(2) - 1))
      NeedFixing = find(CI(:, 2) == Distrib(:, iAlpha(2) - 1));
      % fprintf('%d upper thresholds modified due to repeated values.\n', sum(NeedFixing));
      for i = NeedFixing(:)' % Older Matlab requires row vector.
        t = iAlpha(2) + 1;
        while t <= nBoots && CI(i, 2) == Distrib(i, t)
          t = t + 1;
        end
        if t > nBoots
          % No larger value found.  Nothing will be above threshold.
          CI(i, 2) = Inf;
        else
          % Found new threshold.
          CI(i, 2) = Distrib(i, t);
        end
      end
    end
  end
  % Similarly for lower threshold.
  if iAlpha(1) < nBoots
    if any(CI(:, 1) == Distrib(:, iAlpha(1) + 1))
      NeedFixing = find(CI(:, 1) == Distrib(:, iAlpha(1) + 1));
      % fprintf('%d lower thresholds modified due to repeated values.\n', sum(NeedFixing));
      for i = NeedFixing(:)' % Older Matlab requires row vector.
        t = iAlpha(1) - 1;
        while t >= 1 && CI(i, 1) == Distrib(i, t)
          t = t - 1;
        end
        if t < 1
          % No smaller value found.  Nothing will be below threshold.
          CI(i, 1) = -Inf;
        else
          % Found new threshold.
          CI(i, 1) = Distrib(i, t);
        end
      end
    end
  end
  
  % Adjust confidence intervals depending on method.
  switch Method
    case 1 % Basic bootstrap
      % CI above are quantiles.  Basic method compares "centered"
      % sample and resampled mean distributions, thus the formula is this.
      CI = [2 * Avg - CI(:, 2), ...
        2 * Avg - CI(:, 1)]; % Have to do both columns at the same time since they depend on each other's previous values.
      % case 2 % Percentile
      % Nothing to do, thresholds above are the desired quantiles.
    case 3 % Studentized, bootstrap-t
      % CI above are quantiles of the Studentized resampled means.
      StdErr = sqrt(var(Data, 0, 2)/nO); % Could simplify nO here and above, but keep for ease of understanding.
      CI = [Avg - StdErr .* CI(:, 2), ...
        Avg - StdErr .* CI(:, 1)]; % Have to do both columns at the same time since they depend on each other's previous values.
  end
  
  SigIndices = 0 <= CI(:, 1);
  if ~OneSided
    SigIndices = SigIndices | 0 >= CI(:, 2);
  end
  %   clear CI
  
  if DataIsBootDistrib
    SigAvg = SigIndices;
  else
    % Create array with only significant values.
    SigAvg = NaN(nD, 1);
    SigAvg(SigIndices) = Avg(SigIndices);
  end
  
  % Calculate Likelihoods for each data point of the mean.
  if nargout > 2
    Likelihoods = zeros(nD, 1);
    warning('Likelihoods not yet implemented.');
    %     for i = 1:nD
    %       % As with iAlpha above, this is the conservative limit, included in
    %       % the "significance" tails, and excluded from the CI.
    %
    %       iCI = find(Distrib(i, :) <= 0, 1, 'last');
    %       if isempty(iCI)
    %         % Will give a likelihood of 0.
    %         iCI = 0;
    %       end
    %       if OneSided
    %         iCI2 = nBoots + 1;
    %       else
    %         iCI2 = find(Distrib(i, :) >= 0, 1, 'first');
    %       end
    %       Likelihoods(i) = 1 - (iCI - 1)/nBoots;
    %     end
    if Reshaped
      % Give back original data shape.
      Likelihoods = reshape(Likelihoods, DataSize);
    end
  end
  
  if Reshaped
    % Give back original data shape.
    Avg = reshape(Avg, DataSize);
    SigAvg = reshape(SigAvg, DataSize);
  end
  
end
  
  
  