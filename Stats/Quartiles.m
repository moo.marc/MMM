function [Q, I] = Quartiles(V, Dim)
  % Compute quartiles (3 values) along a specified dimension.
  %
  % [Q, I] = Quartiles(V, Dim)
  %
  % Returns 3 values along the specified dimension, the first (columns) by
  % default (i.e. returns 3 rows).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-11-10
  
  if ~exist('Dim', 'var') || isempty(Dim)
    Dim = 1;
  end
  if Dim > 2
    error('Not implemented yet for Dim > 2.');
  elseif Dim == 2
    V = V';
  end
  N = size(V, 1);
  if N == 1 && numel(V) > 1
    warning('Asked for quartiles along singleton dimension.');
  end
  if N == 0
    Q = [];
    I = [];
    return;
  elseif N == 1
    Q = [V; V; V];
    I = ones(3, N);
  else
    
    Q = zeros(3, size(V, 2));
    Err = 3 * eps(N);
    
    V = sort(V); % sorts columns.
    
    I = N * (1:3)/4;
    for i = 1:3
      if mod(I(i), 1) < Err
        I(i) = round(I(i)) + 0.5;
        Q(i, :) = ( V(floor(I(i)), :) + V(ceil(I(i)), :) ) / 2;
      else
        Q(i, :) = V(ceil(I(i)), :);
      end
    end
  end
  
  if Dim == 2
    Q = Q';
    I = I';
  end
end