function OutDatasets = DatasetMath(Datasets, Operation, AppendName)
  % Apply an operation to data and save as a new CTF dataset.
  % 
  % OutDatasets = DatasetMath(Datasets, Operation, AppendName)
  %
  % Because the operation can be non linear, reference channels are emptied
  % to avoid subsequently changing the gradiometer order, which would no
  % longer be valid.
  % 
  % Arguments:
  % 
  % Datasets: Cell array of dataset paths, or single dataset name.  For
  % oprations within a single dataset, supply as a single column.  For more
  % complex operations involving multiple dataset, each column should
  % represent the data variables as they will be provided to the Operation
  % function.  For example, if subtracting data (Operation = @minus), it
  % will be applied as: datasets of the first column minus corresponding
  % datasets of the second column.
  %
  % Operation: Function handle (possibly passed as an anonymous function)
  %   to be applied directly to the data array(s).  Examples:
  %   Square the data: 
  %      @(X) X.^2
  %   Subtract the trial average from each trial: 
  %      @(X) bsxfun(@minus, X, mean(X, 3))
  %   Subtract datasets (matched trials):
  %      @minus    or    @(X, Y) X - Y
  %   Simply make a copy (useful for converting older dataset formats):
  %      @(X) X
  %
  % AppendName: String to be appended to the (first) dataset names.  Or if
  % entirely new path and names are desired, provide them in a single
  % column cell array.
  % 
  % OutDatasets: Cell array of names of the newly created datasets, or
  % empty string when failed.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-11-27

  if nargin < 3
    error('3 input arguments expected.');
  end
  if ~isa(Operation, 'function_handle')
    error('Operation should be a function handle.');
  end
  
  % Deal with case of one dataset, not in cell array.
  if ~iscell(Datasets) && ischar(Datasets) && size(Datasets, 1) == 1
    Datasets = {Datasets};
    if ischar(AppendName) && ~isempty(fileparts(AppendName))
      AppendName = {AppendName};
    end
  end
  if ischar(AppendName) && ~isempty(strfind(AppendName, filesep))
    error('Either provide AppendName as single string to be appended, or a cell array of new dataset paths and names.');
  end
  
  % Number of independant datasets (lines), and number of datasets needed
  % for the operation (columns).
  [nD, nOp] = size(Datasets);
  if nOp ~= nargin(Operation)
    if nD == nargin(Operation)
      warning('Datasets seems to be transposed.  It should have %d columns, but had size [%d, %d].', ...
        nOp, nD, nOp);
      Datasets = Datasets';
      [nD, nOp] = size(Datasets);
    else
      error('Provided operation expects %d operands, which should be the number of columns in Datasets cell array.', ...
        nargin(Operation));
    end
  end
  
  OutDatasets = cell(nD, 1);
  for d = 1:nD
    % Load data from all datasets on the line of the cell array.  Inverse
    % order such that the first one is the one we use to create the new
    % dataset.
    for Op = nOp:-1:1
      ds = readCTFds(Datasets{d, Op});
      MEGSens = find([ds.res4.senres.sensorTypeIndex] == 5); % 5 MEG sens
      % ChanNames = cellstr(ds.res4.chanNames(MEGSens, :));
      if numel(unique([ds.res4.senres(MEGSens).grad_order_no])) > 1
        error('Channels have different grad order.');
      end
      Data{Op} = getCTFdata(ds, [], MEGSens, 'fT', 'single'); % [nSamples, nChannels, nTrials] single precision for memory.
      if isempty(Data{Op})
        error('No MEG data found in %s', Datasets{d, Op});
      end
    end
    
    % Get all channels for writing later, and replace sensor data with
    % desired operation.
    FullData = getCTFdata(ds, [], [], 'fT', 'single');
    FullData(:, MEGSens, :) = Operation(Data{:}); % All operands passed to function.
    clear Data
    
    % Clear reference channels to prevent wrong gradiant changes.
    % MEGRefs = find([ds.res4.senres.sensorTypeIndex] <= 1); % 0 mag, 1 grad
    FullData(:, [ds.res4.senres.sensorTypeIndex] <= 1, :) = 0;
    
    % Create new dataset.
    if ischar(AppendName) % verified single string.
      [DsPath, DsName] = fileparts(Datasets{d, 1});
      OutDatasets{d} = fullfile(DsPath, [DsName, AppendName, '.ds']);
    else % cell array of paths and names.
      OutDatasets{d} = AppendName{d};
    end
    writeCTFds(OutDatasets{d}, ds, FullData);
    
    % Verify that the new dataset exists, otherwise remove name.
    if ~exist(OutDatasets{d}, 'file')
      OutDatasets{d} = '';
    end
    
  end % dataset loop
  
end


