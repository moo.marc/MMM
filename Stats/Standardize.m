function Z = Standardize(X, Dim)
  % Standardization of variable(s).
  %
  % Z = Standardize(X, Dim)
  %
  % Transform variables by subtracting the sample mean and dividing by the
  % sample standard deviation. X is a data array, and Dim is the array
  % dimension that represents "observations" along which to compute the
  % mean and std. The default Dim is the longest dimension.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-02-15
  
  if nargin < 2 || isempty(Dim)
    [~, Dim] = max(size(X));
    fprintf('Standardizing along dimension %d.\n', Dim);
  end
  Z = bsxfun(@rdivide, bsxfun(@minus, X, mean(X, Dim)), std(X, [], Dim));
end
