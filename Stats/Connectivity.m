function [C, Freq] = Connectivity(Series, Freq, nT, SamplingRate, Debias)
  % Compute many phase-based connectivity measures.
  %
  % [C, Freq] = Connectivity(Series, Freq, nT, SamplingRate, Debias)
  %
  % Compute the following connectivity measures, returned as fields of the
  % structure C:
  %   Coh: magnitude square coherence,
  %   ImC: imaginary part of coherence,
  %   PLV: phase-locking value, or pairwise phase consistency (PPC) =
  %        unbiased PLV square, 
  %   PLI: phase lag index, or unbiased PLI square, 
  %   wPLI: weighted phase lag index, or debiased wPLI square.
  % For PLV, PLI and wPLI, the second option is returned when Debias is
  % true.
  %
  % Series: Source or sensor time series, as columns.  Trials can be
  % concatenated in the first dimension, or as second dimension.  Last
  % dimension is sources or sensors.
  %
  % Freq [default empty]: Frequency bins each specified as a line of two
  % columns.  If empty, bins are not used and a full spectrum is returned
  % instead.  The output Freq then contains the vector of frequencies along
  % the first dimension of C.xxx.
  %
  % nT [required if Series is not in trials]: Number of trials to divide
  % the data into.  Each trial is further processed into 3 tapers. Trials
  % of 5 seconds give a resolution of 1 Hz.
  %
  % SamplingRate [default 1]: in Hz.
  %
  % Debias [default true]: If true, return PCC instead of PLV, and the
  % unbiased/debiased versions of PLI and wPLI. [Vinck 2010, 2011]
  %
  % C.xxx: Each connectivity measure has dimensions frequency (bins),
  % sources, sources, (trials), and is symmetric in the two source
  % dimensions.  Trials are only returned separately when using frequency
  % bins (because of memory issues).  When the entire spectrum is returned,
  % trials are averaged.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-05-01
  
  % nF [default number of samples per trial]: Number of desired (full
  % spectrum) frequency points.  This is simply achieved by zero-padding or
  % truncating the input signal.  Note that the spectrum is converted to
  % one-sided, so the output will contain half.
  %
  % AdjustnF [default false]: If true, zero pad the samples to the next
  % power of 2 in length. This will affect the number of returned frequency
  % bins.
  %
  if nargin < 5 || isempty(Debias)
    Debias = true;
  end
  
  %   if nargin < 6 || isempty(AdjustnF)
  %     AdjustnF = false;
  %   end
  %
  %   if nargin < 5 || isempty(nF)
  %   nF = size(Series, 1);
  %   end
  %   if AdjustnF
  %     nF = 2 ^ ceil( log(nF) * (1 - 1e-12) / log(2) );
  %   end
  
  if nargin < 4 || isempty(SamplingRate)
    SamplingRate = 1;
  end
  
  if nargin < 3 || isempty(nT)
    if ndims(Series) == 3
      nT = size(Series, 2);
    else
      %       nTrials = 1;
      error('Multiple trials needed to compute connectivity estimate.');
    end
  end
  
  if nargin < 2 || isempty(Freq)
    DoBands = false;
    nB = 1;
  elseif size(Freq, 2) == 2
    DoBands = true;
    nB = size(Freq, 1);
  else
    error('Unrecognized frequency bands.');
  end
  
  if DoBands && ~Debias
    warning('Bias will make trial values not comparable to global values.  Consider using de/unbiased measures.');
  end
  
  tic 
  
  % Number of samples and sources.
  if ndims(Series) == 3 
    [nS, nTrTemp, nQ] = size(Series);
    if size(Series, 2) ~= nT
      Series = reshape(Series, [nS * nTrTemp, nQ]);
    end
  else
    [nS, nQ] = size(Series);
  end
  nTapers = 3;
  nW = nT * nTapers;

  if ismatrix(Series)
    % Split data into trials.
    nS = floor(nS / nT);
    % Truncate extra samples.
    Series((nS * nT + 1):end, :) = [];
    Series = permute(reshape(Series, [nS, nT, nQ]), [1, 3, 2]); % nS, nQ, nT
  end
  
  Alpha = 2.5;
  TaperWin = dpss(nS, Alpha); % nS, (unknown)
  % Concentration = 100.00%, 99.98%, 99.6%, 95%, 71%
  
  % Requires lots of memory with long trials.  Can't keep trials and tapers
  % separately and average later.

  % Actual number of output points, since we convert to one sided spectra.
  if ~rem(nS,2)
    nP = nS/2+1;
  else
    nP = (nS+1)/2;
  end
  
  OutFreq = (0:nP-1).' * (SamplingRate / nS);
  
    % Initialize required functions of the cross spectrum power density.
  if DoBands
    C.Coh = zeros(nP, nQ, nQ, 2);
    C.ImC = zeros(nP, nQ, nQ, 2);
    EAbsPow = zeros(nP, nQ, 2);
    C.PLV = zeros(nP, nQ, nQ, 2);
    C.PLI = zeros(nP, nQ, nQ, 2);
    C.wPLI = zeros(nP, nQ, nQ, 2);
    EAbsImCP = zeros(nP, nQ, nQ, 2);
    if Debias
      ESqImCP = zeros(nP, nQ, nQ, 2);
    end
    B.Coh = zeros(nB, nQ, nQ, nT+1);
    B.ImC = zeros(nB, nQ, nQ, nT+1); 
    B.PLV = zeros(nB, nQ, nQ, nT+1);
    B.PLI = zeros(nB, nQ, nQ, nT+1);
    B.wPLI = zeros(nB, nQ, nQ, nT+1);
  else
    C.Coh = zeros(nP, nQ, nQ);
    EAbsPow = zeros(nP, nQ);
    C.PLV = zeros(nP, nQ, nQ);
    C.PLI = zeros(nP, nQ, nQ);
    C.wPLI = zeros(nP, nQ, nQ);
    EAbsImCP = zeros(nP, nQ, nQ);
    if Debias
      ESqImCP = zeros(nP, nQ, nQ);
    end
  end
  CrossPow2 = zeros(nS, nQ, nQ);
  %   CrossPow = zeros(nP, nQ, nQ);
  Pow = zeros(nP, nQ);
  
  WinMeanSq = zeros(nTapers, 1);
  for w = 1:nTapers
    % Mean square of window is required for normalizing PSD amplitude.
    WinMeanSq(w) = (TaperWin(:, w).' * TaperWin(:, w)) / nS;
  end
  
fprintf('\nComputing connectivity... trial %2d / %2d\n', 0, nT);
  for t = 1:nT
    fprintf('\b\b\b\b\b\b\b\b%2d / %2d\n', t, nT);
    if DoBands
      C.Coh(:, :, :, 2) = 0; 
      EAbsPow(:, :, 2) = 0; 
      C.PLV(:, :, :, 2) = 0; 
      C.PLI(:, :, :, 2) = 0; 
      C.wPLI(:, :, :, 2) = 0; 
      EAbsImCP(:, :, :, 2) = 0; 
      if Debias
        ESqImCP(:, :, :, 2) = 0; 
      end
    end
    for w = 1:nTapers
      Fourier = fft(bsxfun(@times, TaperWin(:, w) / sqrt(WinMeanSq(w)), ...
        Series(:, :, t)), nS, 1); % nS, nQ
    
      for q = 1:nQ
        % Cross spectrum power density, complex.
        CrossPow2(:, q, :) = bsxfun(@times, Fourier(:, q), conj(Fourier));
      end
      % Convert to one-sided spectra. [SLOW 90s]
      if ~rem(nS,2)
        CrossPow = CrossPow2(1:nP, :, :);
        CrossPow(2:nP-1, :, :) = CrossPow(2:nP-1, :, :) + ...
          conj(CrossPow2(nS:-1:nP+1, :, :));
      else
        CrossPow = CrossPow2(1:nP, :, :);
        CrossPow(2:nP, :, :) = CrossPow(2:nP, :, :) + ...
          conj(CrossPow2(nS:-1:nP+1, :, :));
      end
      CrossPow = CrossPow / (nS * SamplingRate);
      for q = 1:nQ
        % Spectrum power density.
        Pow(:, q) = CrossPow(:, q, q);
      end
      
      C.Coh = bsxfun(@plus, C.Coh, CrossPow);
      EAbsPow = bsxfun(@plus, EAbsPow, abs(Pow));
      C.PLV = bsxfun(@plus, C.PLV, CrossPow ./ abs(CrossPow));
      C.PLI = bsxfun(@plus, C.PLI, imag(CrossPow) ./ abs(imag(CrossPow)));
      C.wPLI = bsxfun(@plus, C.wPLI, imag(CrossPow));
      EAbsImCP = bsxfun(@plus, EAbsImCP, abs(imag(CrossPow)));
      if Debias
        ESqImCP = bsxfun(@plus, ESqImCP, (imag(CrossPow)).^2);
      end
    end % Taper loop
    
    if DoBands
      C.ImC(:, :, :, 2) = imag(C.Coh(:, :, :, 2)) ./ sqrt( ...
        (bsxfun(@times, permute(EAbsPow(:, :, 2), [1, 3, 2]), EAbsPow(:, :, 2))) );
      C.Coh(:, :, :, 2) = abs(C.Coh(:, :, :, 2)).^2 ./ ...
        bsxfun(@times, permute(EAbsPow(:, :, 2), [1, 3, 2]), EAbsPow(:, :, 2));
      if Debias
        C.PLV(:, :, :, 2) = (C.PLV(:, :, :, 2).*conj(C.PLV(:, :, :, 2)) - nTapers) / ...
          (nTapers * (nTapers - 1));
        C.PLI(:, :, :, 2) = (C.PLI(:, :, :, 2).^2 - nTapers) / (nTapers * (nTapers - 1));
        C.wPLI(:, :, :, 2) = (C.wPLI(:, :, :, 2).^2 - ESqImCP(:, :, :, 2)) ./ ...
          (EAbsImCP(:, :, :, 2).^2 - ESqImCP(:, :, :, 2));
      else
        C.PLV(:, :, :, 2) = abs(C.PLV(:, :, :, 2)) / nTapers;
        C.PLI(:, :, :, 2) = abs(C.PLI(:, :, :, 2)) / nTapers;
        C.wPLI(:, :, :, 2) = abs(C.wPLI(:, :, :, 2) ./ EAbsImCP(:, :, :, 2));
      end
      
      for b = 1:nB
        iB = OutFreq > Freq(b, 1) & OutFreq <= Freq(b, 2);
        B.ImC(b, :, :, t) = mean(C.ImC(iB, :, :, 2), 1);
        B.Coh(b, :, :, t) = mean(C.Coh(iB, :, :, 2), 1);
        B.PLV(b, :, :, t) = mean(C.PLV(iB, :, :, 2), 1);
        B.PLI(b, :, :, t) = mean(C.PLI(iB, :, :, 2), 1);
        B.wPLI(b, :, :, t) = mean(C.wPLI(iB, :, :, 2), 1);
      end
    end
    
  end % Trial loop
  
  % Get rid of temporary space for trials calculations.
  if DoBands
    C.Coh(:, :, :, 2) = [];
    EAbsPow(:, :, 2) = [];
    C.PLV(:, :, :, 2) = [];
    C.PLI(:, :, :, 2) = [];
    C.wPLI(:, :, :, 2) = [];
    EAbsImCP(:, :, :, 2) = [];
    if Debias
      ESqImCP(:, :, :, 2) = [];
    end
  end

  %   % Expectation operator, average over trials and tapers.
  %   E = @(x) mean(x(:, :, :, :), 4);
  
  % Imaginary part of coherence, before converting to mscohere.
  C.ImC = imag(C.Coh) ./ sqrt((bsxfun(@times, permute(EAbsPow, [1, 3, 2]), EAbsPow)));
  
  % Convert coherence to magnitude square coherence.
  %   C.Coh = abs(E(CrossPow)).^2 ./ sqrt(bsxfun(@times, permute(E(abs(Pow)), [1, 3, 2]), E(abs(Pow))));
  C.Coh = abs(C.Coh).^2 ./ (bsxfun(@times, permute(EAbsPow, [1, 3, 2]), EAbsPow));
  
  %   C.PLV = abs( E(CrossPow ./ abs(CrossPow)) );
  if Debias
    % PPC
    C.PLV = (C.PLV.*conj(C.PLV) - nW) / (nW * (nW - 1));
  else
    C.PLV = abs(C.PLV) / nW;
  end
  
  %   C.PLI = abs( E(imag(CrossPow) ./ abs(imag(CrossPow))) );
  if Debias
    C.PLI = (C.PLI.^2 - nW) / (nW * (nW - 1));
  else
    C.PLI = abs(C.PLI) / nW;
  end
  
  %   C.wPLI = abs( E(imag(CrossPow)) ./ E(abs(imag(CrossPow))) );
  if Debias
    C.wPLI = (C.wPLI.^2 - ESqImCP) ./ (EAbsImCP.^2 - ESqImCP);
  else
    C.wPLI = abs(C.wPLI ./ EAbsImCP);
  end
  
  if DoBands
    t = nT + 1;
    for b = 1:nB
      iB = OutFreq > Freq(b, 1) & OutFreq <= Freq(b, 2);
      B.ImC(b, :, :, t) = mean(C.ImC(iB, :, :, 1), 1);
      B.Coh(b, :, :, t) = mean(C.Coh(iB, :, :, 1), 1);
      B.PLV(b, :, :, t) = mean(C.PLV(iB, :, :, 1), 1);
      B.PLI(b, :, :, t) = mean(C.PLI(iB, :, :, 1), 1);
      B.wPLI(b, :, :, t) = mean(C.wPLI(iB, :, :, 1), 1);
    end
    
    C = B;
  end
  
  if ~DoBands
    Freq = OutFreq;
  end
  fprintf('Connectivity completed in %1.0fs.\n', toc);
 
  
end
  