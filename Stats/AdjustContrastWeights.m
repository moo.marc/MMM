function NewWeights = AdjustContrastWeights(ContrastWeights, nTrialGroup, Verbose)
  % NewWeights = AdjustContrastWeights(ContrastWeights, nTrialsGroup, Verbose)
  %
  % Normalize ContrastWeights by rows (i.e. can have multiple contrasts) such
  % that the sum of positive coefficients is 1 and the sum of negative
  % coefficients is -1.  If nTrialsGroup are provided, weights on both the
  % positive and negative "sides" are separately further weighted by the
  % number of trials in each condition.
  %
  % For example, if there were 4 conditions and the weights were:
  %  1, 1, -1, -1
  % the first normalization would give:
  %  0.5, 0.5, -0.5, -0.5
  % And if the number of trials are:
  %  100, 50, 100, 50
  % then the second normalization would give:
  %  0.3333, 0.1667, -0.3333, -0.1667
  % 
  %
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2013-05
  
  if ~exist('Verbose', 'var') || isempty(Verbose)
    Verbose = true;
  end
  
  if exist('nTrialsGroup', 'var') && ~isempty(nTrialGroup)
    if numel(nTrialGroup) ~= size(ContrastWeights, 2)
      error('Number of group trial numbers does not match number of contrast weights.');
    end
    AdjTrial = true;
  else
    AdjTrial = false;
  end
  
  nK = size(ContrastWeights, 1);
  
  NewWeights = ContrastWeights;
  WhereP = NewWeights > 0;
  WhereN = NewWeights < 0;
  SumP = sum(NewWeights .* WhereP, 2);
  SumP(SumP == 0) = 1;
  SumN = -sum(NewWeights .* WhereN, 2);
  SumN(SumN == 0) = 1;
  
  NewWeights = bsxfun(@rdivide, NewWeights .* WhereP, SumP) + ...
    bsxfun(@rdivide, NewWeights .* WhereN, SumN);
  
  if Verbose
    for k = 1:nK
      if any(ContrastWeights(k, :) - NewWeights(k, :) > 1e-6)
        fprintf('Contrast %d: ', k);
        fprintf('%1.2f ', ContrastWeights(k, :));
        fprintf('normalized to: ');
        fprintf('%1.2f ', NewWeights(k, :));
        fprintf('\n');
      end
    end
  end
  
  % Here we're dividing by the total number of trials of all conditions,
  % this is not quite right if some contrast weights are 0; it will
  % introduce a global scale.  Readjust by calling AdjustContrastWeights
  % again without second argument.
  if AdjTrial
    NewWeights = bsxfun(@times, NewWeights,...
      nTrialGroup / sum(nTrialGroup));
    if Verbose
      for k = 1:nK
        TrialNums = [sum(nTrialGroup(WhereP(k, :))), sum(nTrialGroup(WhereN(k, :)))];
        fprintf('Contrast %d: %d trials with %d trials.\n', k, TrialNums);
      end
    end
    NewWeights = AdjustContrastWeights(NewWeights, [], Verbose);
  end
  
end



