function Markers = AddMarker(Markers, NewName, Names, ...
    TrialValidationFunction, TimeIndex, TrialStart, Overwrite, Append, ...
    ForceFirstTrial)
  % Add a marker to a MEG dataset marker structure, based on combinations of other markers.
  %
  % Markers = AddMarker(Markers, NewName, Names, ...
  %     Function, TimeIndex, TrialStart, Overwrite, Append)
  %
  % This function uses "trial logic" instead of relative marker timing:
  % TrialStart markers are provided to separate the sequence of markers
  % into trials.  All related markers that are combined must be between two
  % such TrialStart markers.
  %
  % Markers is a structure such as returned by openmrk.  Presentation log
  % files can also be converted to this structure with openlog or
  % ConvertLogToMarkers.
  %
  % NewName is a string for the new marker name.
  %
  % Names is a cell array of existing marker names in the order required by
  % TrialValidationFunction (thus a single code can be repeated).  For each
  % trial, this list will generate a logical array of whether each marker
  % was present or not.  This is the input argument to the validation
  % function.
  %
  % TrialValidationFunction [default @all]: Function handle that takes an
  % integer array of how many occurrences of each marker in Names was
  % present in the current trial (which can be used as a logical array of
  % whether each marker was present or not), and returns a single boolean
  % value indicating if a new marker should be added for this trial or not.
  % (See example below.)
  %
  % TimeIndex [default 0]: The new marker can be put at the trial start
  % (TimeIndex = 0) or coincide with another marker.  Provide a list of
  % indices into Names for which markers to use (the first one that is
  % present in the trial will be used).  If the validation function returns
  % true for a trial but none of the markers from TimeIndex are present,
  % this produces an error.
  %
  % TrialStart [default Names(1)]: List of markers that separate the
  % sequence of markers into trials.
  %
  % Overwrite [default false]: If false and NewName corresponds to an
  % existing marker, this will give an error unless Append is set to true.
  % If true, overwrites the NewName marker if it exists.
  %
  % Append [default false]: If true, reorders new markers with existing
  % ones, but does not check for duplicates.
  %
  %
  % Example:
  % NewName = 'Correct';
  % Names = {'Target1', 'Button1', 'NonTarget', 'Button1', 'Button2'};
  % TrialValidationFunction = @(x) all(x(1:2)) || (x(3) && ~x(4) && ~x(5));
  % TimeIndex = [1, 3];
  % TrialStart = {'Condition1', 'Condition2', 'PauseTrial'};
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2016-06-10
  
  
  if nargin < 3
    error('Expecting at least 3 arguments');
  end
  if nargin < 9 || isempty(ForceFirstTrial)
    ForceFirstTrial = false;
  end
  if nargin < 8 || isempty(Append)
    Append = false;
  end
  if nargin < 7 || isempty(Overwrite)
    Overwrite = false;
  end
  if nargin < 6 || isempty(TrialStart)
    if isempty(Names)
      TrialStart = {};
    else
      TrialStart = Names(1);
    end
  end
  if nargin < 5 || isempty(TimeIndex)
    TimeIndex = 0; % Use trial start.
  end
  if nargin < 4 || isempty(TrialValidationFunction)
    TrialValidationFunction = @all;
  end
  if isempty(Names) && isempty(TrialStart)
    % Nothing to do.
    return;
  end
  if isempty(NewName) || ~ischar(NewName)
    error('Expecting name for new marker.');
  end
  
  NonEmptyOnly = false; % Only give error for missing codes, not empty ones.
  iN = FindMarkerIndex(Markers, NewName, NonEmptyOnly);
  if ~iN
    iN = numel(Markers) + 1;
    Markers(iN).Name = NewName;
    Markers(iN).Samples = [];
    Markers(iN).Count = 0;
    Markers(iN).Bit = UnusedBit(Markers, false); % Don't care about duplicate bit warning here.
  elseif Overwrite
    Markers(iN).Samples = [];
    Markers(iN).Count = 0;
  elseif ~Append
    error('New marker %s already exists, please set either Overwrite or Append to true.', NewName);
  end
  
  iM = FindMarkerIndex(Markers, Names, NonEmptyOnly);
  if any(~iM)
    error('Marker not found: %s', Names{~iM});
  end
  iMStart = FindMarkerIndex(Markers, TrialStart, NonEmptyOnly);
  if any(~iMStart)
    error('Marker not found: %s', TrialStart{~iMStart}); % was TrialStart(~iMStart)?
  end
  
  %   nM = numel(iM);
  MrkLog = [];
  for m = unique(iM)
    MrkLog = [MrkLog; [m * ones(Markers(m).Count, 1), Markers(m).Samples]]; %#ok<AGROW>
  end
  for m = setdiff(iMStart, iM)
    % Make negative here (temporarily, only until sorting) such that
    % simultaneous markers to trial start markers will be after and thus
    % considered part of that trial.
    MrkLog = [MrkLog; [-m * ones(Markers(m).Count, 1), Markers(m).Samples]]; %#ok<AGROW>
  end
  
  % Sort by trial, time, bit.
  MrkLog = sortrows(MrkLog, [2, 3, 1]);
  % Remove negative signs from trial start marker indices.
  MrkLog(:, 1) = abs(MrkLog(:, 1));
  
  % Act as if there was a trial start marker right at the start of
  % recording.  This can be negative time (if pre-trigger), so just put
  % 10ms before first marker, if it's not already a trial start marker.
  if ForceFirstTrial && ~ismember(MrkLog(1, 1), iMStart)
    MrkLog = [iMStart(1), MrkLog(1, 2), MrkLog(1, 3) - 0.01; MrkLog];
  end
  
  % Use trial logic instead of time search and match.
  % [Match, Counts, MatchDelays] = MatchCodes(Markers, iP, iL, ...
  %     SearchRange, KeepDuplicates)
  
  iT = find(ismember(MrkLog(:, 1), iMStart)); % All trial start markers.
  if isempty(iT)
    warning('No trial start found.');
    return;
  end
  iT = [iT, [iT(2:end) - 1; length(MrkLog)]]; % Second column is last marker in trial.
  
  %   % This warning is now given below, only if trials are found that span
  %   % epochs.
  %   if numel(unique(MrkLog(:, 2))) > 1
  %     warning('Multiple epochs in dataset, expecting continuous.');
  %   end;
  % For epoched data, we can have more than 1 trial per epoch.  For long
  % recordings, this can be true even for raw data so we can't assume we
  % only want the first trial in each epoch.
  %   iT(1 + find( MrkLog(iT(2:end, 1), 2) == MrkLog(iT(1:end-1, 1), 2) ), :) = []; % Remove subsequent trials within each epoch.
  % Reject trials that overlap, where the last marker in the trial is not
  % in the same epoch.  Only give warning if this happens.
  SpanTrials = MrkLog(iT(:, 1), 2) ~= MrkLog(iT(:, 2), 2);
  if any(SpanTrials)
    warning('Multiple epochs in dataset; some trials span epochs and were not considered for adding markers.');
    iT(SpanTrials, :) = [];
  end
  
  % Only check trial durations within dataset epochs.
  ITIs = diff(MrkLog(iT(:, 1), 3));
  if any(ITIs(MrkLog(iT(1:end-1, 1), 2) == MrkLog(iT(2:end, 1), 2)) < -1e-3)
    error('Negative trial duration detected.');
  end
  
  for t = 1:size(iT, 1) % -1 (might want to ignore last trial if incomplete)
    %     Trial = MrkLog(iT(t, 1):iT(t, 2), :);
    Found = zeros(numel(iM), 1);
    for iiM = 1:numel(iM)
      Found(iiM) = sum(iM(iiM) == MrkLog(iT(t, 1):iT(t, 2), 1));
    end
    %     Found = ismember(iM, MrkLog(iT(t, 1):iT(t, 2), 1));
    if TrialValidationFunction(Found)
      if ~TimeIndex(1)
        Markers(iN).Count = Markers(iN).Count + 1;
        Markers(iN).Samples(Markers(iN).Count, :) = MrkLog(iT(t, 1), 2:3);
      else
        iTime = find(Found(TimeIndex), 1, 'first'); % index into TimeIndex, which indexes into Names, Found, iM
        if isempty(iTime)
          warning('Trial %d should have marker, but none of the TimeIndex markers were present.  Verify logic.', t);
        else
          %         if isempty(find(iM(TimeIndex(iTime)) == MrkLog(iT(t, 1):iT(t, 2), 1), 1, 'first'))
          %           keyboard;
          %         end
          iTime = find(iM(TimeIndex(iTime)) == MrkLog(iT(t, 1):iT(t, 2), 1), 1, 'first'); % index into log of this trial
          Markers(iN).Count = Markers(iN).Count + 1;
          Markers(iN).Samples(Markers(iN).Count, :) = MrkLog(iT(t, 1) - 1 + iTime, 2:3);
        end
      end
    end
  end
  
  % Sort if appending to existing samples.  Does not check for duplicates.
  if Append
    Markers(iN).Samples = sortrows(Markers(iN).Samples);
  end
  
end
