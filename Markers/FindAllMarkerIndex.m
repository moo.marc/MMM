function iM = FindAllMarkerIndex(Markers, Name, NonEmptyOnly, RemoveNotFound)
  % Get all indices of a (possibly duplicated) marker name in Markers structure.
  % 
  % iM = FindAllMarkerIndex(Markers, Name, NonEmptyOnly, RemoveNotFound)
  %
  % By default will return an array of indices, more than one if multiple
  % markers have the target name, or 0 if not found.  RemoveNotFound = true
  % removes the potential 0 value, thus returning an empty array.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-07-06

  if ~exist('NonEmptyOnly', 'var') || isempty(NonEmptyOnly)
    NonEmptyOnly = false;
  end
  if ~exist('RemoveNotFound', 'var') || isempty(RemoveNotFound)
    RemoveNotFound = false;
  end
  if isempty(Markers)
    iM = zeros(size(Name));
  else
    iM = find(ismember({Markers(:).Name}, Name));
    if isempty(iM)
      iM = 0;
    end
    if NonEmptyOnly
      for iiM = find(iM > 0)
        if Markers(iM(iiM)).Count == 0
          iM(iiM) = 0;
        end
      end
    end
  end
  if RemoveNotFound
    iM(iM == 0) = [];
  end
  
end
