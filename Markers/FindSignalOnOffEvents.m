function [On, Off] = FindSignalOnOffEvents(Data, SampleRate, EventRate, ...
    Inverted, CCovThresh, ErrThresh, AverageStartThresh, ...
    Visualize, AdjustOff, AllowMissing, UseZeroLevel, BelowPeakLevel, ...
    MaxGap, UseSlope, AveragingWidthTime, Repeat)
  % Finds occurrences of "on" and "off" events in an analog signal.
  %
  % [On, Off] = FindSignalOnOffEvents(Data, SamplingRate, EventRate, ...
  %     Inverted, CCovThresh, ErrThresh, AverageStartThresh, ...
  %     Visualize, AdjustOff, AllowMissing, UseZeroLevel, BelowPeakLevel, ...
  %     MaxGap, UseSlope, AveragingWidthTime)
  %
  % This function was designed to generate markers for light on and off
  % events from photosensor signals, but may work with other signals that
  % have raising and falling edges and are separated enough in time from
  % each other.  It works by doing a first pass based on threshold
  % detection, then averages the signal based on these and uses the average
  % signal shape for a second pass over the data, finding matches based on
  % thresholding cross-correlation and sum of squared errors.
  %
  % While this method is good at finding events, choosing a precise point
  % as the "start" can be difficult depending on the signal shape.  Various
  % options are given, but one should always inspect the figure that shows
  % the average signals as well as the points chosen, which are the samples
  % returned in the outputs.
  %
  %
  % Inputs, all optional except first [default values in brackets]:
  %
  % Data [required]: Continuous data stream of a single channel.
  %
  % SampleRate [600]: Data samples per second.
  %
  % EventRate [59.977]: Video frames per second.  This is used to force the
  % "on" and "off" signals to occur at certain allowed delays from one
  % another: integer multiple of frame durations.  For a signal not related
  % to video, this restriction can be ignored by setting this value to the
  % acquisition frame rate, e.g. 600.
  %
  % Inverted [false]: Set to true if the signal is high when "off" and low
  % when "on", e.g. the old photosensor.
  %
  % CCovThresh [0.6]: Threshold for cross-correlation, normalized to 1.
  % Should be above false positives but below true positives.
  %
  % ErrThresh [0.2]: Threshold for sum of squared errors, such that 1
  % represents the signal (minus zeros). Should be below false positives
  % and above true positives.
  %
  % AverageStartThresh [empty]: Can be used to calculate the start of the
  % signal, where the marker will go.  If omitted, the start will be
  % calculated automatically, where the signal appear first to be changing
  % from the noise.  However, this doesn't always work well and a simple
  % threshold value can be given instead.  A single threshold for both on
  % and off can be given, or a vector of two different thresholds.  Best to
  % look at the average signal in detail first to select the appropriate
  % values.  This can be done interactively by giving a string value to
  % this input, e.g. 'Interactive'.  The average figure will appear and a
  % prompt in the command window.  The value(s) should correspond to the
  % normalized data (-1 to +1 if UseZeroLevel is false, or scaled so that
  % the largest absolute value is 1 if UseZeroLevel is true).  Slope
  % thresholds can be given instead if UseSlope is set to true.
  %
  % Visualize [false]: If true, figures showing various steps and features
  % of the algorighm are displayed: the average on and off signals of the
  % first and second passes, with marker "start" point indicated, and the
  % cross-correlation and sum of squared errors along the data.
  %
  % AdjustOff [true]: Adjust "off" times based on frame rate. The reason is
  % that because of amplification, the new photosensor signal is cut off
  % and the drop comes late. So we round down the number of frames, then
  % convert back to number of samples rounding normally.
  %
  % AllowMissing [false]: If there are missing "on" or "off" events (i.e.
  % two successive "off" without an "on" in between or vice versa), a
  % warning is generated. If AllowMissing is false, the program then
  % displays a figure to help determine the problem and then exits. If set
  % to true, the program continues and returns normally.
  %
  % UseZeroLevel [false]: The first detection pass uses a threshold below
  % the maximum amplitude, either based on the whole range if UseZeroLevel
  % is false, which is appropriate for example for a raw photosensor
  % signal, or separately for positive and negative directions, keeping the
  % zero value as meaningful, if UseZeroLevel is set to true, which is
  % appropriate for example for a filtered photosensor signal.
  %
  % BelowPeakLevel [1/4]: Relative location of the threshold for first pass
  % detection, as a proportion of the signal range or of positive and
  % negative signal extremes separately, depending on whether UseZeroLevel
  % is false or true respectively.
  %
  % MaxGap [0.0035 (seconds = 2 samples at 600 Hz)]: During automatic
  % calcuation of signal change onset (where the marker should be), we ask
  % that the signal be changing somewhat fast enough to be distinguished
  % from the preceding noise.  If it is changing slowly, we need to ignore
  % one or more preceeding samples that are starting to change, but not
  % enough to be separate from the noise.  This is what is termed here the
  % "gap".  The gap is increased one sample at a time until the algorithm
  % finds a clear signal start or the max gap is reached.
  %
  % UseSlope [false]: When not using AverageStartThresh, by default, only
  % the value of the signal is used to find the signal change onset.
  % However, if UseSlope is set to true, the slope will also be used, but
  % this is possibly too sensitive to noise for robust and consistant start
  % detection. Without it though, a start may be found "in the noise".
  % Because there is no robust way to get consistent "start point" between
  % datasets, one should carefully inspect and compare the figures showing
  % the chosen points on the average signals.
  %
  % AveragingWidthTime [0.03]: Time in seconds, before and after the
  % detected occurrences during the first pass, for averaging.  This
  % average is then matched back with the data (2nd pass), thus it must
  % only include time where the signal will be identical at each
  % occurrence.
  %
  % Outputs: Sample numbers for On and Off events found.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-08-11
  
  % TO DO: too many inputs, change to "named", ParseVarargin style.
  
  if nargin < 2
    error('Not enough input arguments');
  end
  if size(Data, 2) > 1
    if size(Data, 1) > 1
      error('Data should be a single channel continuous data.');
    else
      % Data was row, convert to column.
      Data = Data';
    end
  end
  if ~exist('SampleRate', 'var') || isempty(SampleRate)
    SampleRate = 600;
  elseif numel(SampleRate) > 1
    error('Unrecognized SampleRate');
  end
  if ~exist('EventRate', 'var') || isempty(EventRate)
    EventRate = 59.977; % video frames per second.
    % 2013-02-14 measurement with MEG clock: 59.977 +- 0.003 Hz.
  end
  if ~exist('Inverted', 'var') || isempty(Inverted)
    Inverted = false;
  end
  % Proportion of "normalized" cross-covariance of average signal with
  % data. Should be above false positives but below true positives.
  if ~exist('CCovThresh', 'var') || isempty(CCovThresh)
    CCovThresh = 0.6;
  end
  % Normalized sum of squared errors, such that 1 represents the signal
  % (minus zeros). Should be below false positives and above true
  % positives.
  if ~exist('ErrThresh', 'var') || isempty(ErrThresh)
    ErrThresh = 0.2;
  end
  % Start point can now be based on signal itself.
  if ~exist('AverageStartThresh', 'var') || isempty(AverageStartThresh)
    CalculateStart = true;
  else
    CalculateStart = false;
    %     Thresh = 0.01; % Volts. Used for finding start of signal, where marker will go.
  end
  if ~exist('Visualize', 'var') || isempty(Visualize)
    Visualize = false;
  end
  if ~exist('AdjustOff', 'var') || isempty(AdjustOff)
    AdjustOff = true;
  end
  if ~exist('AllowMissing', 'var') || isempty(AllowMissing)
    AllowMissing = false;
  end
  if ~exist('UseZeroLevel', 'var') || isempty(UseZeroLevel)
    UseZeroLevel = false;
  end
  if ~exist('BelowPeakLevel', 'var') || isempty(BelowPeakLevel)
    BelowPeakLevel = 1/4;
  end
  if ~exist('MaxGap', 'var') || isempty(MaxGap)
    MaxGap = 0.0035; % seconds.  Will get converted to 2 samples at 600 Hz.
  end
  if ~exist('UseSlope', 'var') || isempty(UseSlope)
    UseSlope = false;
    % Possibly too sensitive to noise for robust and consistant start
    % detection. Also, depending on sampling rate, bumps may be
    % problematic.  Without it though, a start may be found "in the noise";
    % this was meant as a safeguard against "post bump starts", but
    % possibly only works on clean data...
  end
  if ~exist('AveragingWidthTime', 'var') || isempty(AveragingWidthTime)
    AveragingWidthTime = 0.030; % s
  end
  if ~exist('Repeat', 'var') || isempty(Repeat) || Repeat < 1
    Repeat = 1;
  end
  
  %   % Undocumented option: return averaged signals, for making figures.  This
  %   % interrupts the program, markers will not be saved.
  %   if ~exist('ReturnAverage', 'var') || isempty(ReturnAverage)
  %     ReturnAverage = false;
  %   end
  
  if Inverted
    Data = -Data;
  end
  
  % Get rid of zeros at end if collection stopped early.
  DataEnd = find(Data ~= 0, 1, 'last');
  if DataEnd > 0
    Data(DataEnd + 1:end) = [];
    nSamples = DataEnd;
  else
    nSamples = numel(Data);
  end
  
  % Normalize data for more consistent applicability of error thresholds in
  % last step.
  Extrema = [max(Data), min(Data)];
  if ~UseZeroLevel
    % New range will be -1 to 1.
    Data = (Data - Extrema(1)) / (Extrema(1) - Extrema(2)) * 2 + 1;
    Extrema = [1, -1];
  else
    % Scale such that maximum or minimum hits +-1.
    Data = Data / max(abs(Extrema));
    Extrema = Extrema / max(abs(Extrema));
  end
  
  
  % -------------------------------------
  % Find on and off events using threshold
  %   BelowPeakLevel = 1/4; % Now a command-line option.
  % Calculate thresholds for detection first pass.
  if ~UseZeroLevel % The 2 formula here are equivalent now that we've normalized the data.
    % Move towards middle of range.
    AmpThresh = Extrema(1) - (Extrema(1) - Extrema(2)) * [BelowPeakLevel, 1-BelowPeakLevel];
  else
    % Move towards zero by fraction.
    AmpThresh = Extrema * (1 - BelowPeakLevel);
  end
  On = 1 + find(Data(1:end-1) < AmpThresh(1) & Data(2:end) >= AmpThresh(1));
  Off = 1 + find(Data(1:end-1) > AmpThresh(2) & Data(2:end) <= AmpThresh(2));
  nOn = length(On);
  nOff = length(Off);
 
  % First signal average.
  Width = round(AveragingWidthTime * SampleRate);
  OnAverage = zeros(2 * Width + 1, 1);
  OffAverage = zeros(2 * Width + 1, 1);
  EventAverage(true);
  
  % These need to be defined in the main function since they are shared
  % between 2 subfunctions.
  OnCCov = [];
  OffCCov = [];
  OnErr = [];
  OffErr = [];
  MinOnErr = 0;
  MinOffErr = 0;
  
  for r = 1:Repeat
    % Compare average to singal and find on and off again.
    FindEventsFromAverage();
    
    % Second average. The on points are currently at the start of the
    % previous average, so go from 1 to 2*Width + 1.
    EventAverage(false);
  end
  
  %   if ReturnAverage
  %     nOn = OnAverage;
  %     nOff = OffAverage;
  %     return;
  %   end

  
  % -------------------------------------
  % Find signal change onset.  This is before matching because if matching
  % fails, event lists could still be returned.
  
  % There could be widening because of bad alignment, so not too greedy.
  if CalculateStart
    % Find start based on signal itself: from max diff going back, last one
    % (uninterrupted) larger diff and ampl than all before.
    
    MaxGap = floor(MaxGap * SampleRate);
    
    % Index into average where deflection is detected (where marker should
    % be).
    OnStart = [];
    OffStart = [];
    ADiff = diff(OnAverage);
    % If we wanted to avoid small amplitude noise for finding iAD, it would
    % require different treatment depending on UseZeroLevel and in any
    % case, next 'if' should protect against bad choice.
    %     NoiseAvoidance = 0.1;
    %     % Find greatest slope in signal rise (avoiding small amplitude noise).
    %     [~, iAD] = max(ADiff(OnAverage(2:end) > NoiseAvoidance * max(OnAverage)));
    [~, iAD] = max(ADiff);
    % Check if signal is too noisy to use slope below.
    if UseSlope
      ADDiff = diff(ADiff);
      if all(ADDiff(iAD + (-round(MaxGap/2):MaxGap)) > 0)
        UseSlope = true;
      else
        UseSlope = false;
        warning('Noisy signal detected, not using slope to find "on" start.');
      end
    end
    k = -1;
    if max(OnAverage(1:iAD)) < OnAverage(iAD+1) % This is a check against weird signal shape, where max slope point would not be above previous "noise".
      while isempty(OnStart) && k < MaxGap
        % Try again if empty with less conservative threshold: we are again
        % looking for the amplitude to be larger the same way but excluding
        % the one or two previous samples in the comparison.  This is
        % because those samples may have started to go up as part of the
        % signal but not quite fast enough to be well separated from noise.
        k = k + 1; % This was moved at the top of this loop so that when we break we don't add 1 to the k that was really used.
        for i = iAD:-1:(3+k)
          if max(OnAverage(1:i-1-k)) > OnAverage(i) || ...
              ( UseSlope && max(ADiff(1:i-2-k)) > ADiff(i-1) )
            % To not be too greedy, demand that the amplitude be larger than
            % previous max + half previous range.  Use "fixed" previous
            % range.
            NoiseThresh = max(OnAverage(1:(i-k))) + ...
              abs(max(OnAverage(1:(i-k))) - min(OnAverage(1:(i-k)))) / 2;
            for j = i:iAD
              if OnAverage(j + 1) > NoiseThresh
                OnStart = j + 1;
                break;
              end
            end
            break;
          end
        end
      end
    end
    if k > 0 && ~isempty(OnStart)
      fprintf('"on" signal start found using a gap of %1.1f ms (%d samples).\n', ...
        k/SampleRate*1000, k);
    end
    
    % Same for "off".  See detailed comments above.
    ADiff = diff(OffAverage);
    if UseSlope
      ADDiff = diff(ADiff);
      if all(ADDiff(iAD + (-round(MaxGap/2):MaxGap)) > 0)
        UseSlope = true;
      else
        UseSlope = false;
        warning('Noisy signal detected, not using slope to find "off" start.');
      end
    end
    [~, iAD] = min(ADiff);
    k = -1;
    if min(OffAverage(1:iAD)) > OffAverage(iAD+1)
      while isempty(OffStart) && k < MaxGap
        k = k + 1;
        for i = iAD:-1:(3+k)
          if min(OffAverage(1:i-1-k)) < OffAverage(i) || ...
              ( UseSlope && min(ADiff(1:i-2-k)) < ADiff(i-1) )
            NoiseThresh = min(OffAverage(1:(i-k))) - ...
              abs(max(OffAverage(1:(i-k))) - min(OffAverage(1:(i-k)))) / 2;
            for j = i:iAD
              if OffAverage(j + 1) < NoiseThresh
                OffStart = j + 1;
                break;
              end
            end
            break;
          end
        end
      end
    end
    if k > 0 && ~isempty(OffStart)
      fprintf('"off" signal start found using a gap of %1.1f ms (%d samples).\n', ...
        k/SampleRate*1000, k);
    end
    
    if isempty(OnStart) || isempty(OffStart)
      figure;
      plot(OnAverage);
      hold on
      plot(OffAverage, 'r');
      drawnow;
      if isempty(OnStart)
        error('Unable to calculate "on" signal start.');
      elseif isempty(OffStart)
        error('Unable to calculate "off" signal start.');
      end
    end
    
  else
    % Just use threshold(s)
    if ischar(AverageStartThresh)
      % User wants to pick it from the figure.
      if UseSlope
        OnADiff = diff(OnAverage);
        OffADiff = diff(OffAverage);
        figure;
        plot(OnADiff);
        hold on
        plot(OffADiff, 'r');
      else
        figure;
        plot(OnAverage);
        hold on
        plot(OffAverage, 'r');
      end
      drawnow;
      fprintf('Select threshold values from the plot and assign them with this command:\n  AverageStartThresh = [OnValue, OffValue]; dbcont;\n');
      keyboard;
    end
    if UseSlope
      OnADiff = diff(OnAverage);
      OffADiff = diff(OffAverage);
      OnStart = find(OnADiff(1:Width+1) > AverageStartThresh(1), 1, 'first');
      if numel(AverageStartThresh) > 1
        OffStart = find(OffADiff(1:Width+1) > AverageStartThresh(2), 1, 'last') + 1;
      else
        OffStart = find(OffADiff(1:Width+1) > AverageStartThresh, 1, 'last') + 1;
      end
      if isempty(OnStart) || isempty(OffStart)
        figure;
        plot(OnADiff);
        hold on
        plot(OffADiff, 'r');
        error('Bad slope threshold choice for average.');
      end
    else
      % Normalized amplitude threshold(s).  First above threshold
      % (was last below + 1, but that wouldn't work for short peaks).
      %     OnStart = find(OnAverage(1:Width+1) < Thresh(1), 1, 'last') + 1;
      OnStart = find(OnAverage(1:Width+1) > AverageStartThresh(1), 1, 'first');
      % For off, last above threshold + 1 is what works for short peaks.
      if numel(AverageStartThresh) > 1
        OffStart = find(OffAverage(1:Width+1) > AverageStartThresh(2), 1, 'last') + 1;
        %       OffStart = find(OffAverage(1:Width+1) < Thresh(2), 1, 'first');
      else
        OffStart = find(OffAverage(1:Width+1) > AverageStartThresh, 1, 'last') + 1;
        %       OffStart = find(OffAverage(1:Width+1) < Thresh, 1, 'first');
      end
      if isempty(OnStart) || isempty(OffStart)
        figure;
        plot(OnAverage);
        hold on
        plot(OffAverage, 'r');
        error('Bad threshold choice for average.');
      end
    end
  end
  
  On = On + OnStart - 1;
  if On(1) < 1
    error('Marker before first sample.');
  end
  Off = Off + OffStart - 1;
  
  % This figure is essential.  Signal start determination is really not
  % robust enough for blind use.
  StartFig = figure;
  plot(((1:(2*Width+1)) - OnStart) / SampleRate * 1000, OnAverage);
  hold on
  if AdjustOff
    Form = ':r';
  else
    Form = 'r';
  end
  plot(((1:(2*Width+1)) - OffStart) / SampleRate * 1000, OffAverage, Form);
  h = plot([0,0], get(gca, 'YLim'), 'k');
  set(get(get(h, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off'); % Exclude line from legend
  xlabel('Time relative to new markers (ms)');
  ylabel('Normalized signal amplitude');
  legend('On', 'Off');
  %   title(DsName, 'Interpreter', 'none');
  drawnow;
  
  
  % -------------------------------------
  % Match On and Off.  Give warning if missing.
  % Only keep on-off pairs, ignore off before first on or on after last off.
  FirstOff = find(Off > On(1), 1, 'first');
  if FirstOff > 1
    if FirstOff > 2
      %       fprintf('Ignoring first %d LightOff signals in %s.\n', FirstOff-1, DsName);
      fprintf('Ignoring first %d Off signals.\n', FirstOff-1);
    end
    Off(1:FirstOff-1) = [];
    nOff = nOff - (FirstOff - 1);
  elseif isempty(FirstOff)
    if ~Visualize
      DataMatchFigure()
    end
    error('No Off found.')
  end
  LastOn = find(On < Off(end), 1, 'last');
  if nOn > LastOn
    if nOn > LastOn + 1
      %       fprintf('Ignoring last %d LightOn signals in %s.\n', nOn - LastOn, DsName);
      fprintf('Ignoring last %d On signals.\n', nOn - LastOn);
    end
    On(LastOn + 1:end) = [];
    nOn = LastOn;
  end
  if nOff ~= nOn || any(Off - On < 0) || any(On(2:end) - Off(1:end-1) < 0)
    Missing = true;
    if AllowMissing
      warning('Unmatched on (%d) or off (%d).  But AllowMissing is true.', ...
        nOn, nOff);
    else
      n = min(nOn, nOff);
      if n < 2
        error('Did not find ons or offs in first pass.');
      end
      Missing = find(Off(1:n) - On(1:n) < 0, 1, 'first'); % on missing or extra off.
      if ~isempty(Missing)
        fprintf('On missing or extra off around sample %d.\n', Off(Missing));
        fprintf(' On: %d, %d, %d, %d, %d\n Off: %d, %d, %d, %d, %d', On(max(1, Missing-2) : min(Missing+2, n)), ...
          Off(max(1, Missing-2) : min(Missing+2, n)));
      end
      Missing = find(On(2:n) - Off(1:n-1) < 0, 1, 'first'); % off missing or extra on.
      if ~isempty(Missing)
        fprintf('Off missing or extra on around sample %d.\n', On(Missing+1));
        fprintf(' On: %d, %d, %d, %d, %d\n Off: %d, %d, %d, %d, %d', On(max(1, Missing-2) : min(Missing+2, n)), ...
          Off(max(1, Missing-2) : min(Missing+2, n)));
      end
      fprintf('\n');
      warning('Unmatched on (%d) and off (%d).  Events NOT returned.  Verify signals and thresholds for this dataset.', ...
        nOn, nOff);
      % Show plots to help troubleshoot.
      % Already have one if Visualize, so just add found markers.
      if ~Visualize
        DataMatchFigure()
      end
      plot(On, ones(nOn,1), '.b');
      plot(Off, ones(nOff,1), '.r');

      % IMPORTANT to remove data, otherwise, when this function is called
      % by MarkSignalOnOffEvents, they'd be saved despite not allowing
      % missing.
      On = [];
      Off = [];
      % Could be empty if didn't find missing off's, so make true again.
      Missing = true;
    end
  else
    Missing = false;
  end
  
    
  if AdjustOff
    if Missing
      warning('Cannot adjust Off times since there are unmatched on-off signals for this dataset.');
    else
      % Adjust off times based on frame rate.  Because of amplification, the
      % new photosensor signal is cut off and the drop comes late.  So we round
      % down the number of frames, then convert back to number of samples
      % rounding normally.
      Off = On + round( ...
        floor( (Off - On) ./ SampleRate .* EventRate ) ./ ...
        EventRate .* SampleRate );

      EventAverage(true);
      % Add adjusted off average to figure.
      figure(StartFig);
      plot(((0:(2*Width)) - Width) / SampleRate * 1000, OffAverage, 'r');
      plot([0,0], get(gca, 'YLim'), 'k');
      legend('On', 'Off', 'Adj.Off');
      drawnow;
    end
  end
  
  
  
  %-------------------------------------------------------------
  % Subfunctions, with global variables.
  
  function EventAverage(Centered)
    % Average.  
    OnAverage(:) = 0;
    Bad = 0;
    if Centered
      Centered = -Width;
    else
      Centered = 0;
    end
    for ii = 1:nOn
      if On(ii) + Centered < 1 || On(ii) + 2*Width + Centered > nSamples
        % Reject triggers too close to edge, at least for now.
        Bad = Bad + 1;
      else
        OnAverage = OnAverage + Data(On(ii) + Centered + (0:2*Width));
      end
    end
    OnAverage = OnAverage / (nOn - Bad);
    
    OffAverage(:) = 0;
    Bad = 0;
    for ii = 1:nOff
      if Off(ii) + Centered < 1 || Off(ii) + 2*Width + Centered > nSamples
        % Reject triggers too close to edge, at least for now.
        Bad = Bad + 1;
      else
        OffAverage = OffAverage + Data(Off(ii) + Centered + (0:2*Width));
      end
    end
    OffAverage = OffAverage / (nOff - Bad);
    
    if Visualize
      figure;
      plot(OnAverage);
      hold on
      plot(OffAverage, 'r');
      drawnow;
    end
  end
  
  function FindEventsFromAverage()
    % Compare average with data to find events again.
    OnCCov = CrossCovariance(Data, OnAverage);
    OffCCov = CrossCovariance(Data, OffAverage);
    OnErr = RunningMeanSquareError(Data, OnAverage);
    OffErr = RunningMeanSquareError(Data, OffAverage);
    
    % This adjustment is possibly no longer relevant after normalization of
    % the data and I don't remember what it was supposed to "fix": Adjust
    % ErrThresh based on min error signal: min + (1-min)*ErrThresh.
    % Now look for peak in combined ccov and err.
    %   On = intersect( find(OnCCov > CCovThresh & ...
    %     OnErr < ErrThresh - (1-ErrThresh)*min(OnErr)) , LocalMax(OnCCov) );
    %   Off = intersect( find(OffCCov > CCovThresh & ...
    %     OffErr < ErrThresh - (1-ErrThresh)*min(OffErr)) , LocalMax(OffCCov) );
    MinOnErr = min(OnErr);
    MinOffErr = min(OffErr);
    
    if Visualize
      DataMatchFigure()
    end
    
    On = intersect( LocalMax(OnCCov+1-OnErr), ...
      find(OnCCov > CCovThresh & OnErr < ErrThresh - (1-ErrThresh)*MinOnErr) );
    Off = intersect( LocalMax(OffCCov+1-OffErr), ...
      find(OffCCov > CCovThresh & OffErr < ErrThresh - (1-ErrThresh)*MinOffErr) );
    
    % Look for double triggers (two within half a frame) and attempt to
    % correct if within the same above-threshold interval.
    FrameSamples = floor(SampleRate / EventRate);
    BadDupl = false;
    OnDupl = find(diff(On) < FrameSamples / 2);
    for iOn = OnDupl(end:-1:1) % In reverse since we're removing them as we go.
      % This also allows to compare and fix more than 2 in the same interval.
      if any(OnCCov(On([iOn,iOn+1])) < CCovThresh | ...
          OnErr(On([iOn,iOn+1])) > ErrThresh - (1-ErrThresh)*MinOnErr)
        % Not in the same "above-threshold interval".
        BadDupl = true;
      else
        % keep the max of the 2.
        if OnCCov(On(iOn))+1-OnErr(On(iOn)) < OnCCov(On(iOn+1))+1-OnErr(On(iOn+1))
          On(iOn) = [];
        else
          On(iOn+1) = [];
        end
      end
    end
    OffDupl = find(diff(Off) < FrameSamples / 2);
    for iOff = OffDupl(end:-1:1) % In reverse since we're removing them as we go.
      % This also allows to compare and fix more than 2 in the same interval.
      if any(OffCCov(Off([iOff,iOff+1])) < CCovThresh | ...
          OffErr(Off([iOff,iOff+1])) > ErrThresh - (1-ErrThresh)*MinOffErr)
        % Not in the same "above-threshold interval".
        BadDupl = true;
      else
        % keep the max of the 2.
        if OffCCov(Off(iOff))+1-OffErr(Off(iOff)) < OffCCov(Off(iOff+1))+1-OffErr(Off(iOff+1))
          Off(iOff) = [];
        else
          Off(iOff+1) = [];
        end
      end
    end
    if BadDupl
      warning('Multiple "on" or "off" triggers detected within a single frame duration.\n Signal possibly too noisy.');
    end
    
    nOn = length(On);
    nOff = length(Off);
  end
  
  function DataMatchFigure()
    figure
    plot(Data, 'k'); % ./max(abs(Data))
    hold on
    plot(OnCCov, ':b');
    plot(OffCCov, ':r');
    plot(1-OnErr, 'b');
    plot(1-OffErr, 'r');
    legend('Data', 'OnCCov', 'OffCCov', '1-OnErr', '1-OffErr');
    plot([1;nSamples], CCovThresh*[1;1], ':g');
    plot([1;nSamples], (1-(ErrThresh - (1-ErrThresh)*MinOnErr))*[1;1], 'g');
    plot([1;nSamples], (1-(ErrThresh - (1-ErrThresh)*MinOffErr))*[1;1], 'g');
    drawnow;
  end
end




%-------------------------------------------------------------
% Subfunctions, independent scope.


% Find where a vector has local minima.
% function I = LocalMin(V, IncludeEdges)
%   if ~exist('IncludeEdges', 'var') || isempty(IncludeEdges)
%     IncludeEdges = true;
%   end
%   if numel(V) ~= length(V)
%     error('V must be 1d vector');
%   end
%   
%   I = find( V(1:end-2) > V(2:end-1) & V(3:end) > V(2:end-1) ) + 1;
%   if IncludeEdges
%     if V(1) < V(2)
%       I = [1; I];
%     end
%     if V(end) < V(end-1)
%       I = [I; numel(V)];
%     end
%   end
% end

% Find where a vector has local maxima.
function I = LocalMax(V, IncludeEdges)
  if ~exist('IncludeEdges', 'var') || isempty(IncludeEdges)
    IncludeEdges = true;
  end
  if numel(V) ~= length(V)
    error('V must be 1d vector');
  end
  
  I = find( V(1:end-2) < V(2:end-1) & V(3:end) < V(2:end-1) ) + 1;
  if IncludeEdges
    if V(1) > V(2)
      I = [1; I];
    end
    if V(end) > V(end-1)
      I = [I; numel(V)];
    end
  end
end

% Cross-covariance of two signals.
% xcov is available in the signal processing toolbox.
function XCov = CrossCovariance(X, Y, Normalize)
  % Normalize makes xcov with itself = 1.
  if ~exist('Normalize', 'var') || isempty(Normalize)
    Normalize = true;
  end
  
  nX = numel(X);
  nY = numel(Y);
  if nY > nX
    error('Give longer vector first.');
  end
  % Pad longer vector so we get the same length "answer".
  X = [X(:); zeros(nY-1, 1)];
  Y = Y(:); % To make sure it's a column.
  XCov = zeros(nX, 1);
  Ybar = Y - mean(Y);
  for i = 1:nX
    XCov(i) = ( X(i - 1 + (1:nY))' - mean(X(i - 1 + (1:nY))) ) * Ybar;
  end
  
  if Normalize
    XCov = XCov ./ (Ybar' * Ybar); %max(max(XCov), (Ybar' * Ybar));
  end
end


% Sum of square differences of two signals.
function Error = RunningMeanSquareError(X, Y, Normalize)
  % Normalize makes error with all zeros = 1.
  if ~exist('Normalize', 'var') || isempty(Normalize)
    Normalize = true;
  end
  
  nX = numel(X);
  nY = numel(Y);
  if nY > nX
    error('Give longer vector first.');
  end
  % Pad longer vector so we get the same length "answer".
  X = [X(:); zeros(nY-1, 1)];
  Y = Y(:); % To make sure it's a column.
  Error = zeros(nX, 1);
  
  for i = 1:nX
    Temp = ( X(i - 1 + (1:nY)) - Y );
    Error(i) = Temp' * Temp;
  end
  
  if Normalize
    Error = Error ./ (Y' * Y);
  end
end

