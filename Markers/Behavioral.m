function [Counts, RT, ITI, ET] = Behavioral(Datasets, Codes, ...
    ResponseTimeWindow, AnticipateEnd, CountMultipleResponses)
  % Get behavioral data from MEG dataset marker files.  
  %
  % This is meant to be run on original continuous datasets (or marker
  % files, log files, or marker structures), possibly with markers
  % adjusted.  It does not modify marker files.  Designed to be somewhat
  % generic as long as Presentation codes identify correct and incorrect
  % responses and there are separate single markers for the conditions that
  % need to be analyzed separately.  If the dataset is in multiple epochs
  % (e.g. long recordings that can't be recorded in a single epoch), the
  % last "task trial" (as defined by markers) of each dataset epoch might
  % be rejected for this behavioral analysis.  This will depend on whether
  % the TrialStart code (see below) is the first identified marker of the
  % epoch.
  %
  % Counts (size [#Datasets, #Conditions, 6]): First count is total number
  % of trial, then each trial is classified and counted in one of the
  % following 5 categories (some optional depending on options, see below):
  % correct, incorrect, no response, anticipation, multiple responses.
  % These are in order of priority, e.g. if a trial has both an early
  % response and multiple responses, it is only classified as anticipation.
  % Similarly a trial could have multiple late responses only, and it would
  % be classified as "no response" (during the response window).
  %
  % RT (size {#Datasets, #Conditions}): Cell array of lists of response
  % times.  Only response times of trials classified as correct and
  % incorrect are recorded, trials rejected because of early or late
  % responses are not, thus biasing in a sense the true response time
  % distribution.
  %
  % ITI (same size as RT): Inter trial interval (from trial onset to next
  % trial onset), for all trials, even rejected ones, but those that span
  % dataset epochs are set to NaN.
  %
  % ET (same size as RT): Times of "EndResponse" codes (see below), for all
  % trials, even rejected ones.
  %
  % Codes is a structure of marker names.  Each field is a list (cell
  % array) of names (as strings).  The first 3 fields are required and the
  % last 2 optional (they can be either missing or empty cell arrays).  The
  % fields are:
  %   TrialStart: Markers at the stimulus onset used for measuring response
  %     times. Each condition (trial type) should be represented by a
  %     single marker name (string).  If a condition is represented by more
  %     than one marker, either one per trial or in combinations, then a
  %     new single marker should be created, for example with AddMarker.m.
  %   CorrectResponse: Markers that represent correct responses (e.g. 
  %     button presses already interpreted by Presentation as being
  %     correct).
  %   IncorrectResponse: Similar to CorrectResponse.
  %   OtherResponse: Any other marker than the 2 lists given above, which
  %     would occur for example on additional button presses, and that
  %     would not be interpreted by Presentation.  Note that the timing
  %     will be taken into account and various response markers occuring at
  %     the same time (within 50 ms) will not count as multiple presses,
  %     except if they are correct and incorrect.
  %   EndResponse: Any other code (other than TrialStart codes) that should
  %     be interpreted as ending the period in which responses are accepted
  %     (e.g. a fixation cross). Responses after these codes can be
  %     interpreted as anticipation for the next trial (if AnticipateEnd is
  %     set true) or ignored.
  %
  % ResponseTimeWindow is a 1 or 2 element vector with positive numbers in
  % seconds indicating when responses are accepted with respect to the
  % stimulus onset.  Responses earlier than the first (or only) element are
  % counted as anticipated responses and those after the second element (if
  % present) are counted as anticipation for the next trial (if
  % AnticipateEnd is set to true) or ignored.
  %
  % AnticipateEnd (default 'true'): If 'true', responses towards the end of
  % the trial (see EndResponse and ResponseTimeWindow above) are marked as
  % anticipation for the following trial.  If 'false', they are ignored.
  %
  % CountMultipleResponses (default 'true'): Trials with multiple responses
  % are "rejected", in the sense that they are counted separately and not
  % counted as "correct". This includes responses after the response window
  % whether they would be considered anticipation or ignored.  If 'false',
  % then additional responses are ignored.  Note that anticipation and
  % no-responses during the response window trump multiple responses, such
  % that each trial is only counted for rejection once.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2016-03-21

  if nargin < 5 || isempty(CountMultipleResponses)
    CountMultipleResponses = true;
  end
  if nargin < 4 || isempty(AnticipateEnd)
    AnticipateEnd = true;
  end
  if nargin < 3 || isempty(ResponseTimeWindow)
    ResponseTimeWindow = [0.2, inf]; % seconds
  elseif numel(ResponseTimeWindow) == 1
    ResponseTimeWindow = [ResponseTimeWindow, inf];
  elseif numel(ResponseTimeWindow) > 2
    error('ResponseTimeWindow contains too many elements, should be 1 or 2.');
  end
  
  if nargin < 2 || isempty(Codes)
    error('No marker name structure provided (second argument).');
  elseif ~isfield(Codes, 'TrialStart') || ...
      ~isfield(Codes, 'CorrectResponse') || ...
      ~isfield(Codes, 'IncorrectResponse')
    error('Codes structure missing required field(s): TrialStart, CorrectResponse, IncorrectResponse');
  else
    if ~isfield(Codes, 'OtherResponse')
      Codes.OtherResponse = {};
    end
    if ~isfield(Codes, 'EndResponse')
      Codes.EndResponse = {};
    end
  end
  
  if nargin < 1 || isempty(Datasets)
    error('No dataset to process.');
  end
  if iscell(Datasets)
    nD = length(Datasets);
  else
    Datasets = {Datasets};
    nD = 1;
  end
  
  % Consider responses within this time interval as being the same press,
  % unless it is a correct and incorrect together.
  DuplicateInterval = 0.05; % 50 ms
  
  if nargout > 0
    Print = false;
  else
    Print = true;
  end
  
  if nargout > 3
    DoEndTimes = true;
  else
    DoEndTimes = false;
  end
  
  % Number of "conditions" (trial types) to treat separately.
  nC = length(Codes.TrialStart);
  
  Counts = zeros(nD, nC, 6); % Dataset, condition, response (total, correct, incorrect, miss, early, multiple)
  RT = cell(nD, nC);
  ITI = cell(nD, nC);
  if DoEndTimes
    ET = cell(nD, nC);
  end
  if Print
    RT_range = zeros(nD, nC, 4); % Dataset, condition, (min, median, mean, max)
  end
  
  % Assign unique numbers to each marker type.
  CondBit = 1:nC;
  RCBit = 20; % Response correct
  RIBit = 21; % Response incorrect
  ROBit = 25; % Response other
  EndBit = 30;
  
  
  for d = 1:nD
    if ischar(Datasets{d})
      [~, ~, Ext] = fileparts(Datasets{d});
      if strcmpi(Ext, '.mrk') || strcmpi(Ext, '.ds')
        Markers = openmrk(Datasets{d}, false, true); % not original, but quiet.
      elseif strcmpi(Ext, '.log')
        Markers = openlog(Datasets{d}); % not original, but quiet.
      else
        error('Unrecognized file format: %s', Ext);
      end
    elseif isstruct(Datasets{d}) && isfield(Datasets{d}, 'Name')
      % Assume a marker structure.
      Markers = Datasets{d};
      % Dataset name is used in error messages; must be a string.
      Datasets{d} = num2str(d);
    else
      error('Unexpected value for Datasets.');
    end
    MarkerLog = [];
    FoundResponse = false;
    
    % Get trial/time-sorted markers (could make function...)
    for m = 1:length(Markers)
      for c = 1:nC
        % Cell is necessary here otherwise when second is only one string
        % (should always be the case), it would do character array
        % comparison and assume "all" on array of logicals returned by
        % ismember.
        if ismember({Markers(m).Name}, Codes.TrialStart{c})
          %  fprintf('%s is member of %s\n', Markers(m).Name, Codes.TrialStart{c});
          MarkerLog = [MarkerLog; ...
            CondBit(c) * ones(Markers(m).Count, 1), Markers(m).Samples]; %#ok<*AGROW>
          continue;
        end
      end
      % "First" responses will be added to correct or incorrect, even if
      % the same code is also in "other": Matlab switch/case is like
      % if/elseif.
      switch Markers(m).Name
        case Codes.CorrectResponse
          MarkerLog = [MarkerLog; ...
            RCBit * ones(Markers(m).Count, 1), Markers(m).Samples];
          FoundResponse = true;
        case Codes.IncorrectResponse
          MarkerLog = [MarkerLog; ...
            RIBit * ones(Markers(m).Count, 1), Markers(m).Samples];
          FoundResponse = true;
        case Codes.OtherResponse
          MarkerLog = [MarkerLog; ...
            ROBit * ones(Markers(m).Count, 1), Markers(m).Samples];
        case Codes.EndResponse
          MarkerLog = [MarkerLog; ...
            EndBit * ones(Markers(m).Count, 1), Markers(m).Samples];
      end
    end
    
    
    if ~FoundResponse
      fprintf('No correct or incorrect responses were found in %s\n', Datasets{d});
      continue;
    end;
    if numel(unique(MarkerLog(:, 2))) > 1 % Safer to count, might be 0 or 1 for first trial from different sources (log vs mrk).
      warning('Epoched data (%d), expecting continuous (last trial of each epoch may be rejected): %s', numel(unique(MarkerLog(:, 2))), Datasets{d});
      % Some raw datasets have multiple trials, so just ignore last "task
      % trial" of each "dataset trial".
      %       continue;
    end;
    
    % Find and classify first response in each trial and get response times.
    
    % We sort last on bit column such that simultaneous markers will have a
    % specific order.  This allows us to reject responses after "end".
    MarkerLog = sortrows(MarkerLog, [2, 3, 1]);
    
    iTrials = find(ismember(MarkerLog(:, 1), CondBit)); % All trial start markers.
    if isempty(iTrials)
      warning('No trial start found in %s.', Datasets{d});
      continue;
    end
    iTrials = [iTrials, [iTrials(2:end) - 1; length(MarkerLog)]]; % Second column is last marker in trial.
    % For epoched data, we can have more than 1 trial per epoch.  For long
    % recordings, this can be true even for raw data so we can't assume we
    % only want the first trial in each epoch.
    %     iTrials(1 + find( MarkerLog(iTrials(2:end, 1), 2) == MarkerLog(iTrials(1:end-1, 1), 2) ), :) = []; % Remove subsequent trials within each epoch.
    % Reject trials that overlap, where the last marker in the trial is not
    % in the same epoch.
    iTrials(MarkerLog(iTrials(:, 1), 2) ~= MarkerLog(iTrials(:, 2), 2), :) = [];
    
    ITIs = diff(MarkerLog(iTrials(:, 1), 3))'; % row instead of column if not transposed.
    % Remove ITIs that span dataset epochs.
    ITIs(MarkerLog(iTrials(1:end-1, 1), 2) ~= MarkerLog(iTrials(2:end, 1), 2)) = NaN;
    
    Anticip = false;
    nT = size(iTrials, 1); % -1 (might want to ignore last trial if incomplete)
    for t = 1:nT
      Trial = MarkerLog(iTrials(t, 1):iTrials(t, 2), :);
      % This was already verified and dataset skipped if not single trial.
      %       if numel(unique(Trial(:, 2))) > 1 % Check that this is really only one trial.
      %         warning('Behavioral:MultipleTrials', ...
      %           'More than one trial index in trial %d, in %s.', t, Datasets{d});
      %         warning('off', 'Behavioral:MultipleTrials');
      %       end
      % Only check trial durations within dataset epochs; those that span
      % epochs are now NaN.
      if t < nT && ITIs(t) < -1e-3 
        error('Negative trial duration detected, trial %d, %s', t, Datasets{d});
      end
      
      % Condition is determined by trial start marker.
      c = Trial(1, 1);
      
      Counts(d, c, 1) = Counts(d, c, 1) + 1;
      
      if t < nT
        ITI{d, c}(end+1) = ITIs(t);
      end

      iE = find(Trial(:, 1) >= 30, 1, 'first');
      if ~isempty(iE)
        EndT = min(ResponseTimeWindow(2), Trial(iE, 3));
        if DoEndTimes
          ET{d, c}(end+1) = Trial(iE, 3) - Trial(1, 3);
        end
      else
        EndT = ResponseTimeWindow(2);
        if DoEndTimes && t < nT
          ET{d, c}(end+1) = ITI{d, c}(end);
        end
      end
      if AnticipateEnd && Anticip
        % There was an early press at the end of the previous trial.
        Counts(d, c, 5) = Counts(d, c, 5) + 1;
        Anticip = false;
        % still need iR to know if next trial is also anticip.
        iR = find(Trial(:, 1) >= 20 & Trial(:, 1) < 30);
      else
        iR = find(Trial(:, 1) >= 20 & Trial(:, 1) < 30);
        if ~isempty(iR)
          % RT not corrected for projector delay unless codes fixed first.
          ResponseTime = Trial(iR(1), 3) - Trial(1, 3);
          if ResponseTime < EndT && ...
              CountMultipleResponses && numel(iR) > 1
            % Remove duplicate responses (Presentation + direct buttons).
            r = 2;
            while numel(iR) >= r 
              if (Trial(iR(r), 3) - Trial(iR(r-1), 3)) < DuplicateInterval && ...
                  (Trial(iR(r), 1) == Trial(iR(r-1), 1) || Trial(iR(r), 1) == ROBit)
                iR(r) = [];
              else
                r = r + 1;
              end
            end
          end
          if ResponseTime < ResponseTimeWindow(1)
            % Anticipation trumps multiple responses.
            Counts(d, c, 5) = Counts(d, c, 5) + 1;
          elseif ResponseTime < EndT && ...
              CountMultipleResponses && numel(iR) > 1
            % Multiple button presses in this trial.  Not counted as
            % multiple if they were all after end, count instead as miss.
            Counts(d, c, 6) = Counts(d, c, 6) + 1;
          elseif ResponseTime >= EndT
            % No responses in the appropriate time window.
            Counts(d, c, 4) = Counts(d, c, 4) + 1;
          else
            % Keep this response.
            RT{d, c}(end+1) = ResponseTime;
            if Trial(iR(1), 1) == RCBit
              Counts(d, c, 2) = Counts(d, c, 2) + 1;
            elseif Trial(iR(1), 1) == RIBit
              Counts(d, c, 3) = Counts(d, c, 3) + 1;
              % else other, not counted, but shouldn't happen if codes organized correctly.
            end
          end % response timing switches
        else
          % No responses at all.
          Counts(d, c, 4) = Counts(d, c, 4) + 1;
        end
      end % previous trial anticipation or regular.
      if any(iR > size(Trial, 1))
        error('bad iR');
      end
      if AnticipateEnd && any((Trial(iR, 3) - Trial(1, 3)) >= EndT)
        % Anticipated response for the next trial.
        Anticip = true;
      end
      
    end % trial loop
    
    if Print
      % Calculate subject statistics.
      for c = 1:nC
        if ~isempty(RT{d, c})
          RT_range(d, c, :) = [min(RT{d, c}), median(RT{d, c}), mean(RT{d, c}), max(RT{d, c})];
        end
      end
    end
    
  end % dataset loop
  
  
  if Print
    
    % Other stats.
    Accuracy = Counts(:, :, 2) ./ Counts(:, :, 1);
    ErrorRate = Counts(:, :, 3) ./ Counts(:, :, 1);
    MissRate = Counts(:, :, 4) ./ Counts(:, :, 1);
    EarlyRate = Counts(:, :, 5) ./ Counts(:, :, 1);
    MultiRate = Counts(:, :, 6) ./ Counts(:, :, 1);
    
    fprintf('\nDataset	Condition	RT_min	RT_median	RT_mean	RT_max	Trials	Accuracy	Errors  NoResp	Early	Multiple\n');
    for d = 1:nD
      [Path, Name, Ext] = fileparts(Datasets{d}); %#ok<ASGLU>
      for c = 1:nC
        Condition = Codes.TrialStart{c};
        if iscell(Condition)
          Condition = Condition{1};
        end
        fprintf('%s	%s	%1.3f	%1.3f	%1.3f	%1.3f	%d	%1.3f	%1.3f	%1.3f	%1.3f	%1.3f\n', ...
          [Name, Ext], Condition, ...
          RT_range(d, c, :), ...
          Counts(d, c, 1), ...
          Accuracy(d, c), ErrorRate(d, c), MissRate(d, c), EarlyRate(d, c), MultiRate(d, c));
      end
    end
    
  end
  
end


