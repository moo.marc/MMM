function Bit = UnusedBit(Marker, Warn)
  % Finds the lowest integer not used as a "bit number" in a marker structure.
  %
  % Bit = UnusedBit(Marker, Warn)
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015
 
  if ~exist('Warn', 'var') || isempty(Warn)
    Warn = true;
  end
  Used = [Marker(:).Bit];
  if Warn && numel(unique(Used)) < length(Marker)
    fprintf('Duplicate bit number in Marker.\n');
  end
  for Bit = 1:length(Marker)+1
    if ~ismember(Bit, Used)
      return;
    end
  end
end
    