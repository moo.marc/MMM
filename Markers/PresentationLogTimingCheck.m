function PresentationLogTimingCheck(Log, CheckDurations, CheckUncertainties, ...
    DurationErrorThresh, UncertaintyThresh, DisplayFreq, Detailed, IgnoreLast)
  % Check Presentation log files for timing errors and list them in the command window.
  %
  %  PresentationLogTimingCheck(Log, CheckDurations, CheckUncertainties, ...
  %     DurationErrorThresh, UncertaintyThresh, DisplayFreq)
  %
  % Log can be a single log file (string), a cell array of log files or a
  % directory containing log files.  All other inputs are optional:
  % CheckDurations [true]
  % CheckUncertainties [true]
  % DurationErrorThresh [0.06, i.e. 6% of display frame duration]
  % UncertaintyThresh [0.003 (s)]
  % DisplayFreq [59.977 (Hz)]
  % Detailed [false]: if true, prints out info on each specific events,
  %   otherwise groups all into Picture and Response categories.
  % IgnoreLast [true]: Ignore the last Picture event before a pause 
  %   (currently only implemented if Detailed = false) or at the end of the
  %   log because the duration may be bad: not a multiple of frame rate,
  %   even though the uncertainty is "good" - fraction of a ms.
  %
  % Durations are very sensitive to the display frequency.  Using 60 Hz
  % instead of 59.98 would be enough of a difference to generate errors on
  % durations of about 4s.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-05-14

  if nargin < 1
    error('Expecting Log file or directory.');
  end
  if ischar(Log)
    if exist(Log, 'dir')
      LogFiles = ListFiles([Log, filesep, '*.log']);
    elseif exist(Log, 'file')
      LogFiles = {Log};
    end
  elseif ~iscellstr(Log)
    error('Unrecognized Log, should be file or directory name, or cell array of file names.');
  end
  if nargin < 2 || isempty(CheckDurations)
    CheckDurations = true;
  end
  if nargin < 3 || isempty(CheckUncertainties)
    CheckUncertainties = true;
  end
  if ~CheckDurations && ~CheckUncertainties
    fprintf('Nothing to verify.\n');
    return;
  end
  if nargin < 4 || isempty(DurationErrorThresh)
    DurationErrorThresh = 0.06; % Fraction of a display period that is considered "abnormal".  5% = 0.83ms, 6% = 1ms
  end
  if nargin < 5 || isempty(UncertaintyThresh)
    UncertaintyThresh = 0.003; % seconds.
  end
  if nargin < 6 || isempty(DisplayFreq)
    DisplayFreq = 59.977; % Hz.
  end
  if nargin < 7 || isempty(Detailed)
    Detailed = false;
  end
  if nargin < 8 || isempty(IgnoreLast)
    IgnoreLast = true;
  end

nL = length(LogFiles);

clc
if ischar(Log)
  fprintf('Verifying %d log files: %s...\n', nL, Log);
else
  fprintf('Verifying %d log files...\n', nL);
end

if Detailed
  for f = 1:nL
    EventLog = openlog(LogFiles{f});
    if CheckDurations
      for m = 1:length(EventLog)
        if IgnoreLast
          End = EventLog(m).Count - 1;
        else
          End = EventLog(m).Count;
        end
        % Check picture durations except the last one where interrupting the
        % experiment could give a "bad duration".  Also subtract the
        % uncertainty, look at "bad uncertainty" separately.
        if strncmpi(EventLog(m).Name, 'Picture', 7)
          Where = find( abs( EventLog(m).Samples(1:End, 6) * DisplayFreq - ...
            round(EventLog(m).Samples(1:End, 6) * DisplayFreq) ) - ...
            EventLog(m).Samples(1:End, 7) * DisplayFreq > ...
            DurationErrorThresh );
          if ~isempty(Where)
            FileInfo = dir(LogFiles{f});
            [~, Name] = fileparts(LogFiles{f});
            fprintf( '%s %s %s; %d bad durations, e.g. %1.4f s.\n', ...
              FileInfo.date, Name, EventLog(m).Name, numel(Where), ...
              EventLog(m).Samples(Where(1), 6) );
            drawnow;
          end
        end
      end % marker loop
    end
    
    if CheckUncertainties
      % Check uncertainties separately for pictures and responses.
      for EventType = {'Picture', 'Response'}
        for m = 1:length(EventLog)
          if IgnoreLast && EventType(1) == 'P'
            End = EventLog(m).Count - 1;
          else
            End = EventLog(m).Count;
          end
          if strncmpi(EventLog(m).Name, EventType, 7)
            Where = find( EventLog(m).Samples(1:End, 4) > UncertaintyThresh );
            if ~isempty(Where)
              FileInfo = dir(LogFiles{f});
              [~, Name] = fileparts(LogFiles{f});
              fprintf('%s %s %s; %d bad uncertainties, e.g. 1.4f s.\n', ...
                FileInfo.date, Name, EventLog(m).Name, numel(Where), ...
                EventLog(m).Samples(Where(1), 4));
              drawnow;
            end
          end
        end
      end
    end
  end
  
else % Not detailed
  
  EventType = {'Picture', 'Response'};
  for f = 1:nL
    EventListing{1} = [];
    EventListing{2} = [];
    EventLog = openlog(LogFiles{f});
    for m = 1:length(EventLog)
      if strncmpi(EventLog(m).Name, EventType{1}, 7)
        EventListing{1} = [EventListing{1}; EventLog(m).Samples]; %#ok<*AGROW>
      elseif strncmpi(EventLog(m).Name, EventType{2}, 7)
        EventListing{2} = [EventListing{2}; EventLog(m).Samples];
      end
    end
    
    e = 1;
    if IgnoreLast && ~isempty(EventListing{e})
      % Sort by time to remove the last picture before pauses and at the end.
      EventListing{e} = sortrows(EventListing{e});
      EventListing{e}(end, :) = [];
      
      % This method of removing pictures before pauses isn't perfect, one
      % could maybe pause/resume/pause quickly before another picture is
      % presented and this code would remove 2 pictures instead of just
      % one.  Not a big deal for the purpose of this function though.
      iL = FindMarkerIndex(EventLog, 'Pause', true, false);
      if numel(iL) == 1 && iL ~= 0
        for iP = 1:EventLog(iL).Count
          EventListing{e}(find( ...
            EventListing{e}(:, 1) < EventLog(iL).Samples(iP, 1) | ...
            (EventListing{e}(:, 1) == EventLog(iL).Samples(iP, 1) & ...
            EventListing{e}(:, 2) < EventLog(iL).Samples(iP, 2)) , ...
            1, 'last' ), :) = [];
        end
      end
    end
  
    if CheckDurations
      if isempty(EventListing{e})
        continue;
      end
      Where = find( abs( EventListing{e}(:, 6) * DisplayFreq - ...
        round(EventListing{e}(:, 6) * DisplayFreq) ) - ...
        EventListing{e}(:, 7) * DisplayFreq > ...
        DurationErrorThresh );
      if ~isempty(Where)
        FileInfo = dir(LogFiles{f});
        [~, Name] = fileparts(LogFiles{f});
        fprintf( '%s %s %s; %d bad durations, e.g. %1.4f s.\n', ...
          FileInfo.date, Name, 'Picture', numel(Where), ...
          EventListing{e}(Where(1), 6) );
        drawnow;
      end
    end
    
    if CheckUncertainties
      for e = 1:2
        if isempty(EventListing{e})
          continue;
        end
        Where = find( EventListing{e}(:, 4) > UncertaintyThresh );
        if ~isempty(Where)
          FileInfo = dir(LogFiles{f});
          [~, Name] = fileparts(LogFiles{f});
          fprintf('%s %s %s; %d bad uncertainties, e.g. %1.4f s.\n', ...
            FileInfo.date, Name, EventType{e}, numel(Where), ...
            EventListing{e}(Where(1), 4));
          drawnow;
        end
      end
    end
  end
  
end

fprintf('Done!\n\n');
