function NewMarkers = ConvertLogToMarkers(LogFile, Categories, ...
    TimeOffset, MarkerFile, Overwrite) 
  % Create MEG dataset markers from a Presentation log file.
  %
  % NewMarkers = ConvertLogToMarkers(LogFile, Categories, ...
  %     TimeOffset, MarkerFile, Overwrite)
  %
  % Input arguments (default in brackets):
  %
  % LogFile [required]: Presentation log file name and path.
  %
  % Categories [required]: cell array of strings with 2 lines.  The first
  % line is the desired marker names, and the second line is the matching
  % strings to search for in the presentation "event names" returned by
  % openlog, e.g.:
  %  Categories = {'Marker1', 'Marker2';
  %                'LogString1', 'LogString2'};
  % The Presentation log "event names" are a concatenation of event type
  % (e.g. 'Picture' or 'Response') and user defined codes (string or
  % number), with a space in between.  One must therefore look at the log
  % file and choose the search strings carefully to group all desired codes
  % together.  For example using 'Response' as the search string would
  % group all button presses as a single marker.  In some cases it is not
  % possible to get all and only the desired log events with a single
  % search string.  In that case multiple "categories" with the same marker
  % name can be used, these will be joined (and properly sorted) into a
  % single marker.
  %
  % TimeOffset [0]: Time delay in seconds to add to Presentation event
  % times in the marker file.  Positive means the marker time is after the
  % Presentation log time.  Presentation log times are in tenths of ms,
  % marker file times in seconds, so after having identified the same event
  % in both (e.g. start of first trial with light trigger on, and first
  % light on marker), one can set:
  %  TimeOffset = LightOnMarkerTime - TrialCodeLogTime/10000 - 0.020;
  % The last value (20 ms) corresponds to the typical fixed delay between
  % Presentation codes and the actual appearance of the projected stimulus
  % in the middle of the screen.  This must be added when using the light
  % trigger as the synchronizing event if the "usual" marker time is
  % desired.  Otherwise the markers would be at the same time as the light
  % trigger.  Ideally this value would be measured from another dataset
  % collected right before or after.  Of if not available, another
  % participant for the same experiment around the same period.  The
  % MarkerTiming function can be used to get the value.
  %
  % MarkerFile ['']: Dataset or marker file name and path.  If a marker
  % file is provided, the converted log events are added to the existing
  % markers in this file (or dataset).  Otherwise, only the converted
  % events are returned in the NewMarkers structure.
  %
  % Overwrite [false]: If true and a marker file was provided, the
  % NewMarkers structure is saved as the new marker file in the dataset.
  % If markers existed with the same names, they are replaced by the newly
  % converted events.  (As usual, savemrk keeps a backup copy of the
  % original marker file.)  If false, the marker file is not saved.
  %
  % Output argument (optional): NewMarkers: structure containing the
  % converted log file events.  See openmrk or savemrk for structure
  % details.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-07-06

  % Could round new times to sample times to match usual data, but doesn't
  % seem necessary in DataEditor, so keeping additional precision for now.
  
  %  , TrialTime, MatchCodes, MatchOffset
  % [If TrialTime is true, assumes Presentation trials match whatever
  % epoching was done and uses Presentation trial number and trial times,
  % but this should be rare... so not implemented yet.]
  
  if ~exist('LogFile', 'var') || isempty(LogFile)
    error('Please provide a Presentation log file name.');
  end
  if ~exist('TimeOffset', 'var') || isempty(TimeOffset)
    TimeOffset = 0;
  end
  if ~exist('Overwrite', 'var') || isempty(Overwrite)
    Overwrite = false;
  end
  if ~exist('Categories', 'var') || isempty(Categories)
    Categories = {};
  end
  if size(Categories, 1) ~= 2
    error('Need 2 lines in cell array Categories.');
  end
  nC = size(Categories, 2);
  if ~exist('MarkerFile', 'var') || isempty(MarkerFile)
    Markers = [];
    Overwrite = false;
  elseif  ~exist(MarkerFile, 'file') 
    % Allow saving to a datasets without a MarkerFile.
    Markers = [];
  else
    Markers = openmrk(MarkerFile);
  end
  
  if ~Overwrite && nargout < 1
    fprintf('Nothing to do, either provide an output variable or set to overwrite a marker file.\n');
    return
  end
  
  NewMarkers = struct('Name', Categories(1, :), 'Bit', 0, ...
    'Count', 0, 'Samples', []);
  
  % Read in log file.  Trial, Tr. time, Time, ...
  Events = openlog(LogFile);
  
  for c = 1:nC
    if ~isempty(Markers)
      NewMarkers(c).Bit = UnusedBit(Markers, false);
    else
      NewMarkers(c).Bit = c;
    end
    WhereMatch = find( ~ cellfun( @isempty, ...
      strfind({Events(:).Name}, Categories{2, c}) ) );
    for iE = WhereMatch
      % Keep continuous time.  Could have option to keep trial time here...
      NewMarkers(c).Samples = [NewMarkers(c).Samples; ...
        ones(Events(iE).Count, 1), Events(iE).Samples(:, 3) + TimeOffset];
      NewMarkers(c).Count = NewMarkers(c).Count + Events(iE).Count;
    end
  end
  
  for c = 1:nC
    NewMarkers(c).Samples = sortrows(NewMarkers(c).Samples);
  end
  
  % This was unnecessary, field should no longer exist ever.
  %   if isfield(Markers, 'Editable')
  %     Markers = rmfield(Markers, 'Editable');
  %   end
  
  % Merge duplicate markers if we had to search for multiple log strings.
  NewMarkers = MergeDuplicateMarkers(NewMarkers);
  nC = numel(NewMarkers);
  
  if Overwrite
    % Replace existing markers.
    for c = 1:nC
      iM = FindMarkerIndex(Markers, NewMarkers(c).Name);
      if numel(iM) == 1 && iM > 0
        NewMarkers(c).Bit = Markers(iM).Bit;
        Markers(iM) = NewMarkers(c); %#ok<*AGROW>
        %         NewMarkers(c) = []; % can't remove in this loop
      elseif numel(iM) > 1
        error('Multiple markers with same name: %s.', Markers(iM(1)).Name);
      else
        % Add new.
        Markers = [Markers, NewMarkers(c)];
      end
    end
    
    savemrk(Markers, MarkerFile);
  end
  
  warning('Always verify that newly created codes are at the right time with respect to the preexisting codes.  There has been confusion with the TimeOffset in the past, leading to wrong times.');
end




