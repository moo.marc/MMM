function iM = FindMarkerIndex(Markers, Names, NonEmptyOnly, RemoveNotFound)
  % Get index of marker name(s) in Markers structure.
  % 
  % iM = FindMarkerIndex(Markers, Names, NonEmptyOnly, RemoveNotFound)
  %
  % By default will return an array of indices, only one per name even if
  % multiple markers have the same name, or 0 if not found.  RemoveNotFound
  % = true removes those 0 values, thus matching to specific names in a
  % list is no longer possible.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2016-04-02

  if ~exist('NonEmptyOnly', 'var') || isempty(NonEmptyOnly)
    NonEmptyOnly = false;
  end
  if ~exist('RemoveNotFound', 'var') || isempty(RemoveNotFound)
    RemoveNotFound = false;
  end
  if ~iscell(Names)
    if ischar(Names)
      Names = {Names};
    else
      error('Names should be a cell array of strings.');
    end
  end
  if isempty(Markers)
    iM = zeros(size(Names));
  else
    [~, iM] = ismember(Names, {Markers(:).Name});
    if NonEmptyOnly
      for iiM = find(iM > 0)
        if Markers(iM(iiM)).Count == 0
          iM(iiM) = 0;
        end
      end
    end
  end
  if RemoveNotFound
    iM(iM == 0) = [];
  end
  
end
