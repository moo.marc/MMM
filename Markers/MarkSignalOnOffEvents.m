function [nOn, nOff] = MarkSignalOnOffEvents(Dataset, Channel, ...
    FindOptions, Overwrite, OutputDataset)
  % Wrapper function to FindSignalOnOffEvents for CTF MEG datasets.
  % 
  % MarkSignalOnOffEvents(Dataset, Channel, FindOptions)
  %
  % Inputs [default values in brackets]:
  %
  % Dataset [required]: A single dataset name, as a string.
  %
  % Channel [6]: UADC channel number (e.g. 6) or name (e.g. 'UADC006') of
  % the channel where the signal of interest is in the dataset.
  %
  % Overwrite [false]: If "OfflineLightOn" marker exists and is not empty,
  % does not compute again.
  %
  % FindOptions ['UseZeroLevel', true]: Structure with fields named after
  % the optional input variables of FindSignalOnOffEvents, except Data and
  % SampleRate (which are read from Dataset).  (Default only has one field
  % and is required if using the default channel, 6, which is filtered.)
  %
  % OutputDataset [Dataset]: If a different output dataset is given, this
  % is where the marker file will be saved.  Useful if Dataset is
  % read-only, to avoid having to make a copy and waste disk space.  Should
  % be used with caution however.
  %
  % Outputs [optional]: Number of on and off events found.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-07-04

  if nargin < 1 || isempty(Dataset) || ~exist(Dataset, 'dir')
    error('Dataset not provided or not found.');
  end
  if nargin < 5 || isempty(OutputDataset) || ~exist(OutputDataset, 'dir')
    OutputDataset = Dataset;
  else
    fprintf('Saving marker file in different dataset. Use this feature with caution!\n%s -> %s\n', ...
      Dataset, OutputDataset);
  end
  if nargin < 4 || isempty(Overwrite)
    Overwrite = false;
  end
  if nargin < 3 || isempty(FindOptions)
    FindOptions = struct('UseZeroLevel', true);
  end
  if nargin < 2 || isempty(Channel)
    Channel = 6;
  end
  
  % Get the ADC channel.
  if isnumeric(Channel)
    Channel = sprintf('UADC%03d', Channel);
  end
  ds = readCTFds(Dataset);
  SampleRate = ds.res4.sample_rate;
  nPreTrig = ds.res4.preTrigPts;
  Data = getCTFdata(ds, [], Channel); % [nSamples, nChannels, nTrials]
  if isempty(Data)
    fprintf('Photosensor channel missing for %s\n', Dataset);
    nOn = 0;
    nOff = 0;
    return;
  end
  [nSamplesPerTrial, nChannels, nTrials] = size(Data);
  if ds.res4.no_samples ~= nSamplesPerTrial
    error('Inconsistent number of samples.');
  end
  if nChannels > 1
    error('More than one light sensor channel returned.');
  end
  %   nSamples = nSamplesPerTrial * nTrials;
  if nTrials > 1
    warning('MarkSignalOnOffEvents works on continuous data only. Multiple trials detected. Assuming continuous between trials.');
    Data = Data(:);
  end
  clear ds

  % This was not done very well, FindSignal function options are not named
  % or in a structure.  Should probably upgrade to new parsing function or
  % at least structure.
  FindOptionsOrder = {'EventRate', 'Inverted', 'CCovThresh', 'ErrThresh', ...
    'AverageStartThresh', 'Visualize', 'AdjustOff', 'AllowMissing', ...
    'UseZeroLevel', 'BelowPeakLevel', 'MaxGap', 'UseSlope', ...
    'AveragingWidthTime', 'Repeat'};
  ProvidedOptions = fieldnames(FindOptions);
  PassOptions = {};
  for f = 1:numel(ProvidedOptions)
    i = find(strcmpi(ProvidedOptions{f}, FindOptionsOrder));
    if isempty(i)
      error('Unrecognized FindOption: %s', ProvidedOptions{f});
    end
    PassOptions{i} = FindOptions.(ProvidedOptions{f}); %#ok<AGROW>
  end
  
  % Verify if markers already exist.
  Markers = openmrk(Dataset, [], true); % quiet
  NewNames = {'OfflineLightOn', 'OfflineLightOff'};
  iNewM = FindMarkerIndex(Markers, NewNames, false, false);
  if ~Overwrite && sum(iNewM ~= 0) == 2 && Markers(iNewM(1)).Count > 0
    %     fprintf('OfflineLightOn marker found; not computing again.');
    nOn = Markers(iNewM(1)).Count;
    if iNewM(2) > 0
      nOff = Markers(iNewM(2)).Count;
    else
      nOff = 0;
    end
    return;
  elseif ~Overwrite && sum([Markers(iNewM(iNewM > 0)).Count]) ~= 0
    error('Overwrite is set to false, but only partial offline light markers found.');
  end

  % Call main function
  [On, Off] = FindSignalOnOffEvents(Data, SampleRate, PassOptions{:});
  nOn = length(On);
  nOff = length(Off);
  
  % Create markers.
  for i = 1:2
    if iNewM(i) == 0
      iNewM(i) = length(Markers) + 1;
      Markers(iNewM(i)).Name = NewNames{i};
      Markers(iNewM(i)).Bit = UnusedBit(Markers, false);
    end
  end
    
  Markers(iNewM(1)).Count = nOn;
  Markers(iNewM(2)).Count = nOff;
  % Modified to work with multiple trials.
  %   Time = SampleToTime((1:nSamples)', SamplingRate, nPreTrig);
  %   Markers(iNewM(1)).Samples = [ones(nOn, 1), Time(On)];
  %   Markers(iNewM(2)).Samples = [ones(nOff, 1), Time(Off)];
  [Time, Trial] = SampleToTime(On(:), SampleRate, ...
    nPreTrig, nSamplesPerTrial);
  Markers(iNewM(1)).Samples = [Trial, Time];
  [Time, Trial] = SampleToTime(Off(:), SampleRate, ...
    nPreTrig, nSamplesPerTrial);
  Markers(iNewM(2)).Samples = [Trial, Time];
  
  savemrk(Markers, OutputDataset, true);
  
end
