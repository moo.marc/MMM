function [Match, Counts, MatchDelays] = MatchCodes(Markers, iP, iL, ...
    SearchRange, KeepDuplicates)
  % Matches samples between two lists of event markers.
  %
  % [Match, Counts, MatchDelays] = MatchCodes(Markers, iP, iL, ...
  %     SearchRange, KeepDuplicates)
  %
  % Matches samples between e.g. Presentation events and light triggers.
  % SearchRange can be 0 to search for coincident codes. The second set
  % (iL) should not to contain coincident samples.
  %
  % iP and iL are the Markers index for the two sets of events.
  %
  % 'Match' has 4 columns: P (first set) Markers index, P sample index,
  % L (second set) Markers index, L sample index.
  %
  % Counts(1:4) is: # of good matches, # of P not matched, # of duplicates
  % (2 P for same L), # L not matched.
  %
  % MatchDelays is in the same order as Match.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-06-01

  
  if ~exist('KeepDuplicates', 'var') || isempty(KeepDuplicates)
    KeepDuplicates = false;
  end
  if ~exist('SearchRange', 'var') || isempty(SearchRange)
    SearchRange = 0.06;
  end
  
  iP([Markers(iP).Count] == 0) = [];
  iL([Markers(iL).Count] == 0) = [];
  iP(iP == 0) = [];
  iL(iL == 0) = [];
  
  PLog = [];
  LLog = [];
  for m = iP
    % Log trial number, sample time, marker file index and sample index.
    PLog = [PLog; Markers(m).Samples, m * ones(Markers(m).Count, 1), (1:Markers(m).Count)']; %#ok<*AGROW>
  end
  for m = iL
    LLog = [LLog; Markers(m).Samples, m * ones(Markers(m).Count, 1), (1:Markers(m).Count)'];
  end
  
  nP = size(PLog, 1);
  nL = size(LLog, 1);
  
  if nP == 0 || nL == 0
    Match = [];
    MatchDelays = [];
    Counts = [0, nP, 0, nL];
    return;
  end
  
  if any(PLog(:, 1) > 1) || any(LLog(:, 1) > 1)
    warning('This procedure works best on continuous data: trigger matching is only performed within trials.');
  end
  
  % Sort different codes in time order (trial, time, code).
  if numel(iP) > 1
    PLog = sortrows(PLog);
  end
  if numel(iL) > 1
    LLog = sortrows(LLog);
  end
  
  iMatch = zeros(nP, 2);
  MatchDelays = SearchRange * ones(nP, 2);
  LastBestL = 1;
  for p = 1:nP
    for l = LastBestL:nL
      if LLog(l, 1) < PLog(p, 1)
        % Previous trial, skip.
        continue;
      elseif LLog(l, 1) > PLog(p, 1)
        % Following trial, end search.
        break;
      end
      CurrentDelay = abs(LLog(l, 2) - PLog(p, 2));
      if CurrentDelay <= MatchDelays(p, 1)
        MatchDelays(p, 2) = MatchDelays(p, 1);
        MatchDelays(p, 1) = CurrentDelay;
        iMatch(p, 2) = iMatch(p, 1);
        iMatch(p, 1) = l;
        LastBestL = l; % This shortens the search, but requires time-ordered PLog and LLog.
        % Continue search.
      elseif CurrentDelay <= MatchDelays(p, 2)
        MatchDelays(p, 2) = CurrentDelay;
        iMatch(p, 2) = l;
        % Done search.
        break;
      end
    end % Light code loop
  end % Presentation code loop
  
  % Count bad trials.
  Counts(2) = sum(iMatch(:, 1) == 0);
  
  % Find and fix possible light triggers used by multiple presentation
  % markers.  Only look at available secondary matches on the pair for
  % now.
  Duplicates = find(iMatch(2:end, 1) == iMatch(1:end-1, 1) & ...
    iMatch(2:end, 1)); % Not zero.
  Counts(3) = numel(Duplicates);
  if ~KeepDuplicates
    for i = 1:Counts(3)
      % Check for following first.
      if iMatch(i+1, 2) > iMatch(i+1, 1) && ...
          ( i+2 > nP || (i+2 <= nP && iMatch(i+1, 2) < iMatch(i+2, 1)) )
        iMatch(i+1, 1) = iMatch(i+1, 2);
        iMatch(i+1, 2) = 0;
        MatchDelays(i+1, 1) = MatchDelays(i+1, 2);
        MatchDelays(i+1, 2) = SearchRange;
        % Otherwise check for preceeding.
      elseif iMatch(i, 2) > 0 && iMatch(i, 2) < iMatch(i, 1) && ...
          ( i == 1 || (i > 1 && iMatch(i, 2) > iMatch(i-1, 1)) )
        iMatch(i, 1) = iMatch(i, 2);
        iMatch(i, 2) = 0;
        MatchDelays(i, 1) = MatchDelays(i, 2);
        MatchDelays(i, 2) = SearchRange;
      else
        % Don't check further for now; reject one.
        Counts(2) = Counts(2) + 1;
        if LLog(iMatch(i, 1), 2) < PLog(i, 2)
          % Before both, keep first.
          iMatch(i+1, :) = 0;
          MatchDelays(i+1, :) = SearchRange;
        else
          % Between them or after, keep second.
          iMatch(i, :) = 0;
          MatchDelays(i, :) = SearchRange;
        end
      end
    end
  end
  
  % [Find matches that were too far apart on our first pass? Not
  % currently.]
  
  Counts(1) = nP - Counts(2);
  Counts(4) = nL - Counts(1);
  
  % Organize match info.
  Match = [PLog(iMatch(:, 1) > 0, 3:4), ...
    LLog(iMatch(iMatch(:, 1) > 0, 1), 3:4)];
  if nargout > 2
    MatchDelays = MatchDelays(iMatch(:, 1) > 0, 1);
  end
  
end