function MatchDelays = MarkerTiming(Datasets, ...
    MarkerNamesOn, LightNameOn, OverwriteOffline, FindOptions, ...
    Fix, Delay, MarkerNamesOff, LightNameOff, AdditionalMarkers, ...
    CheckDurations, CheckNextOn, Plot, SearchRange)
  % Verify or fix timing of dataset markers compared to light triggers.
  % 
  % MatchDelays = MarkerLightTiming(Datasets, ...
  %     MarkerNamesOn, LightNameOn, OverwriteOffline, OfflineOptions, ...
  %     Fix, Delay, MarkerNamesOff, LightNameOff, AdditionalMarkers, ...
  %     CheckDurations, CheckNextOn, Plot, SearchRange)
  %
  % This function can be used to verify or fix the timing of markers that
  % were generated from codes sent from the stimulus computer (typically
  % from Presentation).  For simple verification, only Datasets and
  % MarkerNamesOn are needed.  
  %
  % The function returns a table on the command line showing information
  % about matching between Presentation codes and light triggers, to see
  % for example if there were missing or extra codes or triggers.  Without
  % an output variable, it also shows a box and whisker plot of delays
  % between the codes and triggers. If timing is "good", this figure may
  % look strange as the box and whiskers may be "collapsed" onto a point,
  % and the y axis may show only one value.  However, if timing is not as
  % good, the distribution of delays will be visible and various datasets
  % can be compared.  If some datasets show a problem the function can be
  % used to "fix" the timing by creating new markers that are at the
  % expected time from light triggers.
  % 
  % All inputs are optional except Datasets, though without MarkerNamesOn,
  % nothing would happen.  Datasets, MarkerNamesOn, LightNameOn,
  % MarkerNamesOff, LightNameOff and AdditionalMarkers are cell arrays
  % of strings, thus require brackets {} even when only one string is
  % provided.  All these except Datasets are marker names from the dataset
  % marker files.
  %
  % MarkerNamesOn/Off should typically be a set of markers such that
  % exactly 1 of these markers occurred for each light on or off event.
  % Otherwise, matching will not work as expected and the table of results
  % would not be useful. If there are simultaneous markers that should be
  % verified or adjusted, these should be listed in AdditionalMarkers.
  % 
  % LightNameOn/Off can be omitted, or set to {'OfflineLightOn/Off'}, in
  % which case the function MarkSignalOnOffEvents is used to generate
  % markers for on and off light events, in a more accurate fashion than a
  % simple threshold, which is the case for markers created in real time
  % during acquisition. On the other hand, an existing light trigger marker
  % can also be provided. 
  % 
  % FindOptions is an optional structure with field 'Channel' which
  % specifies the channel to use in MarkSignalOnOffEvents, and other fields
  % named after the optional input variables of FindSignalOnOffEvents,
  % except Data and SampleRate (which are read from Dataset).  See these
  % functions for more details.  By default, if these markers have already
  % been created, they will be reused unless OverwriteOffline is set to
  % true, in which case they will be recalculated and overwritten.
  % 
  % Fix is false by default, but when true, all markers provided in
  % MarkerNamesOn/Off and AdditionalMarkers will be first copied with the
  % same name prepended by "Original", then the timing of each of these
  % markers will be changed so that it is a fixed delay before the
  % corresponding light trigger.  This delay is 20 ms by default, but
  % can be set with the Delay parameter.
  % 
  % Delay [0.02 s by default] is the time difference between markers and
  % light triggers, positive meaning the light comes after (this should
  % normally be the case).
  %
  % CheckDurations (default false), if set to true, will produce an
  % additional figure that shows delays between on and off light events.
  % 
  % CheckNextOn (default false), if set to true, will produce an additional
  % figure that shows delays between light on events.
  %
  % Plot (default true when no output variable, otherwise false), if set to
  % true, will show a box and whisker plot of time delays between markers
  % and light triggers, per dataset.
  %
  % SearchRange (default 0.06 s): Time before or after each marker 
  % where we try to find a matching light event.  The nearest one is kept.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-04-12
  
  % This function replaces VerifyPresentationTiming, FixPresentationTiming
  % and PresentationTiming.
  
  if ~exist('Datasets', 'var') || isempty(Datasets)
    error('No dataset provided.');
  elseif ~iscellstr(Datasets)
    error('Datasets must be provided as a cell array of strings.');
  end
  if ~exist('MarkerNamesOn', 'var') || isempty(MarkerNamesOn) % Empty check needed in case it's a different class of empty.  Needs to be cell.
    MarkerNamesOn = {};
  end
  if ~exist('MarkerNamesOff', 'var') || isempty(MarkerNamesOff) % Empty check needed in case it's a different class of empty.  Needs to be cell.
    MarkerNamesOff = {};
  end
  NewLightCodes = {'OfflineLightOn', 'OfflineLightOff'};
  if ~exist('OverwriteOffline', 'var') || isempty(OverwriteOffline)
    OverwriteOffline = false;
  end
  if ~exist('Fix', 'var') || isempty(Fix)
    Fix = false;
  end
  if ~exist('CheckDurations', 'var') || isempty(CheckDurations)
    CheckDurations = false;
  end
  if ~exist('CheckNextOn', 'var') || isempty(CheckNextOn)
    CheckNextOn = false;
  end
  if ~exist('Plot', 'var') || isempty(Plot)
    if nargout > 0
      Plot = false;
    else
      Plot = true;
    end
  end
  
  if ~exist('LightNameOn', 'var') || isempty(LightNameOn) || ...
      ismember(NewLightCodes(1), LightNameOn)
    RecreateLightCodes = true;
    LightNameOn = NewLightCodes(1);
    LightNameOff = NewLightCodes(2);
    if ~exist('FindOptions', 'var') || isempty(FindOptions)
      FindOptions = struct('UseZeroLevel', true, 'Inverted', false, ...
        'AllowMissing', false);
      Channel = 6;
      %       OfflineOptions = {6, [], false, 0.4, 0.45, 0.01}; % 0.4, 0.45, 0.6, 0.2,
      %       ADC, FrameRate, Inverted, CCovThresh, ErrThresh,
      %       Thresh, Visualize, AdjustOff, AllowMissing
    elseif ~isfield(FindOptions, 'Channel')
      Channel = 6;
      FindOptions.UseZeroLevel = true;
    else
      Channel = FindOptions.Channel;
      FindOptions = rmfield(FindOptions, 'Channel');
    end
  else
    RecreateLightCodes = false;
    if ~exist('LightNameOff', 'var') || isempty(LightNameOff)
      LightNameOff = {};
    end
    if OverwriteOffline
      warning('Not recreating offline light codes since provided LightNameOn is not ''OfflineLightOn''.');
    end
  end
  if ~iscellstr(LightNameOn) && ischar(LightNameOn)
    LightNameOn = {LightNameOn};
  end
  if ~iscellstr(LightNameOff) && ischar(LightNameOff)
    LightNameOff = {LightNameOff};
  end
  if numel(LightNameOn) > 1 || numel(LightNameOff) > 1
    error('Only one light code expected.');
  end
  if ~exist('Delay', 'var') || isempty(Delay)
    Delay = 0.020; % MarkerFile precision is unpredictable but %+20.12g.
  end
  if exist('AdditionalMarkers', 'var') && ~isempty(AdditionalMarkers)
    if ~iscellstr(AdditionalMarkers)
      error('Unrecognized additional codes.  Should be cell array of strings.');
    end
    HaveAdds = true;
  else
    HaveAdds = false;
  end
  % Time on each side of the Presentation marker to look for a light
  % trigger.  Use the closest one in time to match.  But then need to make
  % sure the same one is not used by two.
  if ~exist('SearchRange', 'var') || isempty(SearchRange)
    SearchRange = 0.06;
  end
  

  nD = numel(Datasets);

  % Figure stuff. ---------------
  if Plot
    Five = cell(nD, 1);
    MatchDelays = cell(nD, 1);
    Outliers = Five;
    if isempty(LightNameOff)
      CheckDurations = false;
    elseif CheckDurations
      DurFive = Five;
      DurOutliers = Five;
    end
    if CheckNextOn
      NextFive = Five;
      NextOutliers = Five;
    end
    Precision = 0.0001; % So that outliers are not right next to whiskers.
  elseif nargout
    MatchDelays = cell(nD, 1);
  end
  % -----------------------------

  fprintf('Index; Good P, P w/o L, Dupl, L w/o P, Good POff, POff w/o L, Good Adds, Adds w/o P, Dataset:\n');
  
  for d = 1:nD
    Markers = openmrk(Datasets{d}, [], true);
    
    if RecreateLightCodes
      iL = FindMarkerIndex(Markers, NewLightCodes, true, true); % NonEmptyOnly, RemoveNotFound
      if numel(iL) < 2 || OverwriteOffline
        % This function always overwrites the markers.
        Found = MarkSignalOnOffEvents(Datasets{d}, Channel, FindOptions, OverwriteOffline);
        if Found < 1
          % Got warning or didn't detect any light on.  Skip.
          continue;
        end
        Markers = openmrk(Datasets{d}, [], true);
      end
    end
    
    iL = FindMarkerIndex(Markers, LightNameOn, true, true); % NonEmptyOnly, RemoveNotFound
    if isempty(iL)
      fprintf('No light triggers found for %s.\n', Datasets{d});
      continue;
    end
    iP = FindMarkerIndex(Markers, MarkerNamesOn, true, true); 
    if isempty(iP)
      fprintf('No Presentation marker found for %s.\n', Datasets{d});
      continue;
    end
    iPOff = FindMarkerIndex(Markers, MarkerNamesOff, true, true); 
    if ~isempty(MarkerNamesOff) && isempty(iPOff)
      fprintf('No light off Presentation marker found for %s.\n', Datasets{d});
    end
    iLOff = FindMarkerIndex(Markers, LightNameOff, true, true);
    if isempty(iLOff) && (~isempty(iPOff) || CheckDurations)
      fprintf('No light off triggers found for %s.\n', Datasets{d});
      CheckDurations = false;
    end
      
    % Match P and L codes.
    [Match, Counts] = MatchCodes(Markers, iP, iL, SearchRange, false); % Don't keep duplicates. %, MatchDelays
    
    % Do the same for light off and its codes.
    if ~isempty(iPOff) && ~isempty(iLOff)
      [MatchOff, CountsOff] = MatchCodes(Markers, iPOff, iLOff, SearchRange, false); % Don't keep duplicates.
      Match = [Match; MatchOff]; %#ok<AGROW>
      iP = [iP, iPOff]; %#ok<AGROW>
      clear MatchOff iPOff;
    else
      CountsOff = zeros(1, 4);
    end
    
    if HaveAdds
      iA = FindMarkerIndex(Markers, AdditionalMarkers, true, true);
      if isempty(iA)
        fprintf('No additional Presentation markers found for %s.\n', Datasets{d});
        continue;
      end
      [MatchAdds, CountsAdds] = MatchCodes(Markers, iA, iP, 0, true); % Synchronous markers, keep duplicates.
    else
      CountsAdds = zeros(1, 4);
    end
    
    fprintf('%d: %d, %d, %d, %d, %d, %d, %d, %d, %s\n', d, Counts, CountsOff(1:2), CountsAdds(1:2), Datasets{d});
    
    if Fix
      % Fix the markers.  Includes on and off.
      for m = iP
        % Check that data is continuous (1 trial).
        if any(Markers(m).Samples(:, 1) > 1)
          error('Fixing Presentation timing only works on continuous data.  Found more than 1 trial in %s.', Datasets{d});
        end
        % Keep a copy of the original markers.
        BackupName = [Markers(m).Name, '_Original'];
        if ~ismember(BackupName, {Markers(:).Name})
          iM = length(Markers) + 1;
          Markers(iM) = Markers(m);
          Markers(iM).Name = BackupName;
          Markers(iM).Bit = UnusedBit(Markers, false);
        end
        % The "fixed" markers will have the usual marker name (and bit).
        Markers(m).Samples = [];
        Markers(m).Count = 0;
      end
      for i = 1:size(Match, 1)
        % Copy the appropriate sample from the matching light trigger.
        Markers(Match(i, 1)).Samples = [Markers(Match(i, 1)).Samples; ...
          Markers(Match(i, 3)).Samples(Match(i, 4), :)];
      end
      for m = iP
        % Adjust for delay and count.
        Markers(m).Count = size(Markers(m).Samples, 1);
        if Markers(m).Count > 0
          Markers(m).Samples(:, 2) = Markers(m).Samples(:, 2) - Delay;
        end
      end
      
      
      if HaveAdds
        for m = iA
          % Check that data is continuous (1 trial).
          if any(Markers(m).Samples(:, 1) > 1)
            error('Fixing Presentation timing only works on continuous data.  Found more than 1 trial in %s.', Datasets{d});
          end
          % Keep a copy of the original markers.
          BackupName = [Markers(m).Name, '_Original'];
          if ~ismember(BackupName, {Markers(:).Name})
            iM = length(Markers) + 1;
            Markers(iM) = Markers(m);
            Markers(iM).Name = BackupName;
            Markers(iM).Bit = UnusedBit(Markers, false);
          end
          % The "fixed" markers will have the usual marker name (and bit).
          Markers(m).Samples = [];
          Markers(m).Count = 0;
        end
        for i = 1:size(MatchAdds, 1)
          % Copy the appropriate sample from the light trigger that matches
          % the matching P code, if there was a light trigger for that code.
          iMatch = find(Match(:, 1) == MatchAdds(i, 3) & ...
            Match(:, 2) == MatchAdds(i, 4), 1, 'first');
          if ~isempty(iMatch)
            Markers(MatchAdds(i, 1)).Samples = [Markers(MatchAdds(i, 1)).Samples; ...
              Markers(Match(iMatch, 3)).Samples(Match(iMatch, 4), :)];
          end
        end
        for m = iA
          % Adjust for delay and count.
          Markers(m).Count = size(Markers(m).Samples, 1);
          if Markers(m).Count > 0
            Markers(m).Samples(:, 2) = Markers(m).Samples(:, 2) - Delay;
          end
        end
      end
      
      savemrk(Markers, Datasets{d});
    end

    % Figure stuff. ---------------
    if Plot || nargout
      % Not needed but to confirm it worked as expected.  Match should be the
      % same as before and Delays will all be the Delay provided exactly.
      [Match, ~, MatchDelays{d}] = MatchCodes(Markers, iP, iL, SearchRange, false); % Don't keep duplicates. %, MatchDelays
    end
    if Plot
      if CheckDurations
        % Assuming here that all on and off markers have been matched.
        if Markers(iLOff).Count ~= Markers(iL).Count
          fprintf('Different counts of light on and off, not calculating durations. %s', ...
            Datasets{d});
        else
          Durations = Markers(iLOff).Samples - Markers(iL).Samples;
          Durations(Durations(:, 1) ~= 0, 2) = NaN;
        end
      end
      if CheckNextOn
        NextOn = diff(Markers(iL).Samples(:, 2));
      end
      
      [Five{d}, Outliers{d}] = BoxWhiskerNumbers(MatchDelays{d}, Precision);
      
      if CheckDurations && ~isempty(Match)
        % Get duration numbers as well.
        [DurFive{d}, DurOutliers{d}] = BoxWhiskerNumbers( ...
          Durations(Match(:, 4), 2) , Precision);
      end
      if CheckNextOn
        [NextFive{d}, NextOutliers{d}] = BoxWhiskerNumbers( ...
          NextOn(Match(1:end-1, 4)) , Precision);
      end
    end
    % -----------------------------
    
    
    
  end % dataset loop
  
  
  % Figure stuff. ---------------
  if Plot
    figure;
    BoxWhiskerPlot(Five, Outliers, [], [], [], [], true); % Width, Offset, Color, ColorOutliers, OutlierCount)
    title('Delay of the light trigger after a stimulus onset Presentation code.');
    xlabel('Dataset');
    ylabel('Time (s)');
    
    if CheckDurations
      figure;
      BoxWhiskerPlot(DurFive, DurOutliers);
      title('Duration of white square as measured from photodiode signal.');
      xlabel('Dataset');
      ylabel('Time (s)');
    end
    if CheckNextOn
      figure;
      BoxWhiskerPlot(NextFive, NextOutliers);
      title('Time between light on triggers.');
      xlabel('Dataset');
      ylabel('Time (s)');
    end
  end
  % -----------------------------
  
  
end