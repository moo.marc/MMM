function Markers = MergeDuplicateMarkers(Markers)
  % Merge multiple markers with the same name into one.
  %
  %  Markers = MergeDuplicateMarkers(Markers)
  %
  % Samples are appended (and sorted), but possible duplicate samples are
  % not detected or removed.  Counts are updated accordingly and only one
  % marker is kept for each name.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-07-06
  
  Names = unique({Markers.Name});
  for Name = Names
    iN = FindAllMarkerIndex(Markers, Name{1});
    if numel(iN) > 1
      for i = iN(end:-1:2) % Reverse since we remove them as we go.
        Markers(iN(1)).Count = Markers(iN(1)).Count + Markers(i).Count;
        Markers(iN(1)).Samples = [Markers(iN(1)).Samples; Markers(i).Samples];
        Markers(i) = [];
      end
      Markers(iN(1)).Samples = sortrows(Markers(iN(1)).Samples);
    end
  end
  
end