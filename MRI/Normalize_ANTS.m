function [WarpedIndivAnat, WarpedImages, IndivAnatSpace] = Normalize_ANTS(...
    IndivAnatSpace, Images, WarpLevel, WarpPrefix, WarpedIndivAnat, ...
    OverwriteImages, Template, TemplateMask, TemplateGrid, InterpMethod)
  % Normalization of MRI and images to a template using ANTS.
  %
  % [WarpedSource, WarpedImages, IndivAnatSpace] = Normalize_ANTS(...
  %    IndivAnatSpace, Images, WarpLevel, WarpPrefix, WarpedIndivAnat, ...
  %    OverwriteImages, Template, TemplateMask, TemplateGrid, InterpMethod)
  %
  % Normalization of T1 MRI (IndivAnatSpace) and possibly coregistered
  % functional images (Images) to a T1 MRI template (Template) using ANTS
  % diffeomorphism. http://www.picsl.upenn.edu/ANTS/
  %
  % Under "normal" operation, IndivAnatSpace should be a NIfTI file name
  % (as a string or cell array of 1 string) of the individual MRI image.
  % But if the transformation was already computed, it can be given as a
  % structure with fields Rigid, Affine, Warp, InvWarp, each containing the
  % file name of the already computed ANTS transformation data, to apply to
  % functional images.
  %
  % The functional images are not required to have the same bounding volume
  % and voxel resolution as the source MRI or template, but must be in the
  % same coordinate system (as defined in the NIfTI header) as the source.
  % By default, the warped images will be interpolated to the coordinates
  % of the template grid, unless a different grid is provided in the form
  % of a NIfTI image (TemplateGrid), which must be in the same coordinate
  % system as Template.  This interpolation is done with one of the methods
  % available through ANTS/ITK (see ANTS documentation), e.g. 'Linear',
  % 'NearestNeighbor', 'BSpline' (default), 'LanczosWindowedSinc',
  % 'WelchWindowedSinc', 'CosineWindowedSinc', etc.
  %
  % WarpLevel (default 3) represents how much warping is allowed: 1:
  % 6-parameter rigid alignment, 2: 12-parameter affine transformation, 3:
  % full diffeomorphic transformation.
  %
  % WarpPrefix (default 'WarpANTS_') will be added to the filenames for the
  % warped Images and Source (unless a file name is supplied in
  % WarpedIndivAnat, or this is set to false). 
  %
  % WarpedIndivAnat is the desired output path for transformation files and
  % warped source image, and the "middle" part of the output file names. It
  % only applies when the source image is provided in IndivAnatSpace
  % instead of a pre-computed transformation structure.  By default, the
  % Source file name and path would be used.  Note that if the directory
  % does not exist, the code assumes the last part is the file name.  For
  % example, if the value provided is: '/data/study/condition/participant',
  % and it is not an existing directory, then the transformation files and
  % warped source image will be saved in '/data/study/condition/' and
  % contain 'participant' in the file names where the source file name
  % would normally be.
  %
  % OverwriteImages (default false).  When set to true, will reapply the
  % transformations to Source and Images and overwrite the warped files if
  % they exist.  However, it will not recompute the transformation if the
  % transformation files exist.  These need to be manually deleted first to
  % redo the computation.
  %
  % Template is the 1mm ICBM template in our repository by default.
  % Similarly for TemplateMask.  In order to not use the default
  % TemplateMask, set it to false.
  %
  % All outputs are file names, where IndivAnatSpace is a structure with
  % fields: Rigid, Affine, Warp and InvWarp.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-03-15
  
  if nargin < 1
    error('Not enough input arguments.');
  end
  if ~exist('WarpPrefix', 'var') || isempty(WarpPrefix) || ...
      ((isnumeric(WarpPrefix) || islogical(WarpPrefix)) && istrue(WarpPrefix))
    WarpPrefix = 'WarpANTS_';
  elseif ((isnumeric(WarpPrefix) || islogical(WarpPrefix)) && isfalse(WarpPrefix))
    WarpPrefix = '';
  elseif ~ischar(WarpPrefix)
    error('WarpPrefix not recognized as a character string for normalizing with ANTS.')
  end
  if iscell(IndivAnatSpace) && numel(IndivAnatSpace) == 1
    IndivAnatSpace = IndivAnatSpace{1};
  end
  if ischar(IndivAnatSpace)
    [Path, Name, Ext] = fileparts(IndivAnatSpace);
    if ~strcmpi(Ext, '.nii')
      error('Expecting NIfTI MRI file, found extension %s', Ext);
    end
    % If we're going to use this Name for output, add prefix.  Otherwise
    % this gets overwritten by the supplied output name.
    Name = [WarpPrefix, Name];
    if exist('WarpedIndivAnat', 'var') && ~isempty(WarpedIndivAnat) && ...
        ischar(WarpedIndivAnat)
      if exist(WarpedIndivAnat, 'dir')
        Path = WarpedIndivAnat;
      else
        [Path, Name] = fileparts(WarpedIndivAnat);
        if ~isempty(Path) && ~exist(Path, 'dir')
          % Attempt creating the output directory.  Error if fails.
          mkdir(Path);
        end
      end
    end
    WarpSource = true;
    IndivAnatSpace = struct('Source', IndivAnatSpace, 'Rigid', '', 'Affine', '', 'Warp', '', 'InvWarp', '');
    % The last part in the file names below is added automatically by ANTS.
    IndivAnatSpace.Rigid = fullfile(Path, [Name, '_R_', '0GenericAffine.mat']); % 6-parameter alignment
    IndivAnatSpace.Affine = fullfile(Path, [Name, '_A_', '0GenericAffine.mat']); % 12-parameter affine
    IndivAnatSpace.Warp = fullfile(Path, [Name, '_', '1Warp.nii.gz']); % Diffeomorphism
    IndivAnatSpace.InvWarp = fullfile(Path, [Name, '_', '1InverseWarp.nii.gz']); % Diffeomorphism
  else % Transformation structure.
    if any(~isfield(IndivAnatSpace, {'Rigid', 'Affine', 'Warp', 'InvWarp'}))
      error('Expecting MRI file name or ANTS transformation files in structure with fields Rigid, Affine, Warp and InvWarp.');
    end
    Path = fileparts(IndivAnatSpace.Rigid);
    % Use clear boolean for this, allow keeping the Source field.
    WarpSource = false;
    %     if isfield(IndivAnatSpace, 'Source')
    %       % Don't allow a source field to avoid confusion.  If the source
    %       % somehow needs to be warped again, it can be passed as any other
    %       % image.
    %       IndivAnatSpace = rmfield(IndivAnatSpace, 'Source');
    %     end
  end
  
  if WarpSource && ~exist(IndivAnatSpace.Source, 'file')
    error('MRI file not found: %s', IndivAnatSpace.Source);
  end
  
  if ~exist('Images', 'var') || isempty(Images)
    Images = {};
  elseif ischar(Images)
    Images = {Images};
  elseif ~iscell(Images)
    error('Unrecognized value for image files to warp with ANTS.');
  end
  if ~exist('WarpLevel', 'var') || isempty(WarpLevel)
    WarpLevel = 3; % Full diffeomorphism.
  end
  if ~exist('OverwriteImages', 'var') || isempty(OverwriteImages)
    OverwriteImages = false;
  end
  if ~exist('InterpMethod', 'var') || isempty(InterpMethod)
    InterpMethod = 'BSpline';
  end
  
  % Template files should be in the Matlab path if the repository code was
  % properly added, but use full path just to be safe.
  TemplatePath = fullfile(fileparts(fileparts(mfilename('fullpath'))), ...
    'Template');
  if ~exist('Template', 'var') || isempty(Template) || ~ischar(Template)
    Template = fullfile(TemplatePath, 'icbm152_1mm.nii');
  elseif ~exist(Template, 'file')
    error('Template not found for normalizing with ANTS: %s', Template);
  end
  if ~exist('TemplateMask', 'var') || isempty(TemplateMask) || ...
      ((isnumeric(TemplateMask) || islogical(TemplateMask)) && istrue(TemplateMask))
    TemplateMask = fullfile(TemplatePath, 'icbm152_NormMask.nii');
  elseif ((isnumeric(TemplateMask) || islogical(TemplateMask)) && isfalse(TemplateMask))
    TemplateMask = [];
  elseif ~exist(TemplateMask, 'file')
    error('Template mask not found for normalizing with ANTS.');
  end
  if ~exist('TemplateGrid', 'var') || isempty(TemplateGrid)
    TemplateGrid = Template;
  end
  if ~exist(TemplateGrid, 'file')
    error('Grid template not found for normalizing with ANTS.');
  end
  
  % ANTS outputs to the current directory so we need to move.
  CurrDir = cd;
  cd(Path);
  
  % Empty warped source name if not computing it now, it's an output.
  
  % Note that when computing from a previous transform, ANTS still outputs
  % a "full set" of transforms based on the name we want, with suffixes:
  % _0Affine, _1Warp.  So although it may be confusing how it decides to
  % combine or not transforms, the set of output transforms, with numbers,
  % should make that clear.
  %
  % As such, it would probably be safer to use the last affine output (the
  % one created when doing the diffeomorphic transform), but it was
  % verified that it is identical to the one provided as input (last affine
  % transform computed before diff transform).
  switch WarpLevel
    case 1
      Suffix = '_Rigid.nii';
      ApplyTransf = IndivAnatSpace.Rigid;
    case 2
      Suffix = '_Affine.nii';
      ApplyTransf = IndivAnatSpace.Affine;
    otherwise % case 3
      Suffix = '.nii';
      ApplyTransf = [IndivAnatSpace.Warp, ' -t ', IndivAnatSpace.Affine];
  end
  WarpedIndivAnat = [];
  
  % -------------------------------------------------------------------
  % Compute transformation if needed.  Requires MRI.
  if WarpSource
    
    NewName = [Name, Suffix];
    WarpedIndivAnat = fullfile(Path, NewName);
    
    % 1. Rigid
    % rigid whole-head, mutual information
    % Kind of randomly matches one side, doesn't seem important to be precise
    % for initializing affine.
    if ~exist(IndivAnatSpace.Rigid, 'file')
      Command = sprintf(['antsRegistration -d 3 -u 1 -n BSpline -o [%s_R_] ', ... % ,%s -x [%s]
        '-m MI[%s, %s, 1, 32] -t Rigid[0.1] -c [10000, 4e-7, 10] -f 4 -s 4vox '], ...
        Name, Template, IndivAnatSpace.Source) %#ok<*NOPRT> % TemplateHead, NewNameR,
      system(Command);
    end
    
    % 2. Affine
    if WarpLevel > 1 && ~exist(IndivAnatSpace.Affine, 'file')
      Command = sprintf(['antsRegistration -d 3 -u 1 -n BSpline -o [%s_A_] -r %s ', ... % ,%s -x [%s]
        '-m MI[%s, %s, 1, 32] -t Affine[0.1] -c [10000, 4e-7, 10] -f 3 -s 2vox '], ...
        Name, IndivAnatSpace.Rigid, Template, IndivAnatSpace.Source) % NewNameAi, TemplateHead,
      system(Command);
      if ~isempty(TemplateMask)
        % Repeat with slightly dilated inner skull mask, slightly "better" (brain inside
        % skull or better match) in majority of cases (7+, 2-, 4?)/13
        Command = sprintf(['antsRegistration -d 3 -u 1 -n BSpline -o [%s_A_] -r %s -x [%s] ', ... % ,%s
          '-m MI[%s, %s, 1, 32] -t Affine[0.1] -c [10000x10000, 4e-7, 10] -f 3x2 -s 2x1vox '], ...
          Name, IndivAnatSpace.Affine, TemplateMask, Template, IndivAnatSpace.Source) % NewNameA,
        system(Command);
      end
    end
    
    % 3. Diff
    % diffeomorphic with slightly dilated inner-skull mask, cross correlation (fast and good),
    % SyN -t SyN[0.25,3,0], SyN with regularization -t SyN[0.25,3,1],
    if WarpLevel > 2 && ~exist(IndivAnatSpace.Warp, 'file')
      if isempty(TemplateMask)
        % No mask, use full volume.  Takes longer.
        Command = sprintf(['antsRegistration -d 3 -u 1 -n BSpline -o [%s_] -r %s ', ... % ,%s
          '-m CC[%s, %s, 1, 3] -t SyN[0.25, 3, 1] -c [128x128x32, 1e-6, 10] -f 3x2x1 -s 2x1x0vox '], ...
          Name, IndivAnatSpace.Affine, Template, IndivAnatSpace.Source) % NewName.W,
      else
        % Use provided template mask.
        Command = sprintf(['antsRegistration -d 3 -u 1 -n BSpline -o [%s_] -r %s -x [%s] ', ... % ,%s
          '-m CC[%s, %s, 1, 3] -t SyN[0.25, 3, 1] -c [128x128x32, 1e-6, 10] -f 3x2x1 -s 2x1x0vox '], ...
          Name, IndivAnatSpace.Affine, TemplateMask, Template, IndivAnatSpace.Source) % NewName.W,
      end
      system(Command);
    end
    
    
    % Apply transform to source image.
    if OverwriteImages || ~exist(WarpedIndivAnat, 'file')
      Command = sprintf('antsApplyTransforms -d 3 -i %s -o %s -r %s -t %s -n %s ', ...
        IndivAnatSpace.Source, NewName, Template, ApplyTransf, InterpMethod);
      system(Command);
    end
    
    % Logic clarified, this is no longer necessary.
    %     % Don't keep the source field to avoid confusion in future calls.
    %     IndivAnatSpace = rmfield(IndivAnatSpace, 'Source');
  end
  
  % -------------------------------------------------------------------
  % Normalize images.  Use coordinate grid from TemplateGrid file, if
  % provided, otherwise uses grid from Template.
  WarpedImages = cell(size(Images));
  for f = 1:numel(Images)
    [Path, Name, Ext] = fileparts(Images{f});
    WarpedImages{f} = fullfile(Path, [WarpPrefix, Name, Ext]);
    if OverwriteImages || ~exist(WarpedImages{f}, 'file')
      if ~exist(Images{f}, 'file')
        warning('File not found, skipping %s', Images{f});
      else
        Command = sprintf('antsApplyTransforms -d 3 -i %s -o %s -r %s -t %s -n %s ', ...
          Images{f}, WarpedImages{f}, TemplateGrid, ApplyTransf, InterpMethod);
        system(Command);
        if ~exist(WarpedImages{f}, 'file')
          error('Warped image just created not found. Look for errors with antsApplyTransforms commands above.');
        end
      end
    end
  end
  
  % Go back to previous directory for convenience.
  cd(CurrDir);
  
end


