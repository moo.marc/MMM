function Fids = GetFiducials(NiiFile, FidsFile)
  % Pick coordinates of MEG fiducials on an MRI, or load if already saved.
  %
  % Fids is a structure with fields: 
  %   VoxelIndex0, VoxelIndex1, SForm, QForm: Coordinates of the fiducials
  %     as a 3x3 matrix in those 4 different NIfTI coordinate systems.
  %     [Na_x, Na_y, Na_z;  LE_x, LE_y, LE_z;  RE_x, RE_y, RE_z]
  %   VoxelSize: Factors to multiply voxel lengths to get physical
  %     dimensions (in mm, a warning is given for other units).
  %   VoxelHandedness: + or - 1 indicating the orientation of the voxel
  %     coordinate system (left or right handed).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-27
  
  if ~exist('FidsFile', 'var') || isempty(FidsFile)
    [Path, Name] = fileparts(NiiFile);
    FidsFile = fullfile(Path, [Name, '.fids.mat']);
   
  end
  
  if exist(FidsFile, 'file')
    load(FidsFile); % Fids structure.
  else
    % Fiducials = 
    fprintf('Save the three fiducial points in this order: Nasion, Left Ear, Right Ear, then exit.\n');
    Coords = NiftiViewer('File', NiiFile);
    drawnow; % Or figure stays on screen.
    
    % Do a check that left and right ears are not reversed.  Based on RAS
    % requirement of NIfTI format.
    if Coords(2).SForm(1) > Coords(3).SForm(1) % x of left > x of right.
      warning(['Unexpected coordinates for left and right ears.  ', ...
        'Make sure you save left before right!  ', ...
        'If this warning persists, there may be something wrong with the NIfTI file (not RAS oriented).']);
      % Add pause here otherwise warning can easily fly off screen unseen,
      % e.g. if this is followed by ANTS normalization.
      fprintf('If you wish to ignore the above warning, press any key to continue.\n');
      pause;
    end
    
    Fids.VoxelIndex0 = reshape([Coords(:).VoxelIndex0]', [3, 3])';
    Fids.VoxelIndex1 = reshape([Coords(:).VoxelIndex1]', [3, 3])';
    Fids.SForm = reshape([Coords(:).SForm]', [3, 3])';
    Fids.QForm = reshape([Coords(:).QForm]', [3, 3])';

    nii = nifti(NiiFile);
    if ~bitand(nii.hdr.xyzt_units, 2)
      warning('NIfTI file units are not in mm.  Results may be unexpected.');
    end
    Fids.VoxelSize = double(nii.hdr.pixdim(2:4));
    if det(nii.mat) > 0
      Fids.VoxelHandedness = 1;
    else
      Fids.VoxelHandedness = -1;
    end
    
    save(FidsFile, 'Fids');
  end
end