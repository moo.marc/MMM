function [Status, Diff] = CheckFiducials(FidsFile, Dataset, Threshold, ...
    Verbose, Visualize)
  % Compare MEG fiducial locations picked on MRI and in MEG dataset.
  %
  % Status = CheckFiducials(FidsFile, Dataset, Threshold, Verbose, Visualize)
  %
  % Returns true if distance of fids in head coordinates are within
  % Threshold (default 1), in cm; false otherwise.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-10-29
  
  if nargin < 2
    error('Not enough input arguments.');
  end
  if ~exist('Threshold', 'var') || isempty(Threshold)
    Threshold = 1; % cm
  end
  if ~exist('Verbose', 'var') || isempty(Verbose)
    Verbose = false;
  end
  if ~exist('Visualize', 'var') || isempty(Visualize)
    Visualize = false;
  end
  if ~exist(FidsFile, 'file')
    error('Fiducials file not found: %s', FidsFile);
  end
  if ~exist(Dataset, 'file')
    error('Dataset not found: %s', Dataset);
  end

  load(FidsFile); % Fids.SForm
  if ~exist('Fids', 'var') || ~isfield(Fids, 'SForm')
    error('Error loading fiducials from file: %s', FidsFile);
  end
  
  MEGHeadFids = openhc(Dataset);
  
  MRIHeadFids = ChangeCoordinates(Fids.SForm, Fids.SForm, 0.1); % from mm to cm
  
  Diff = sqrt(sum( (MEGHeadFids - MRIHeadFids).^2 , 2));
  
  if Visualize
    figure;
    for nFid = 1:3
      plot3(MEGHeadFids(1,nFid),MEGHeadFids(2,nFid),MEGHeadFids(3,nFid),'o','MarkerSize',12,'MarkerFace','r');
      hold on
    end
    for nFid = 1:3
      plot3(MRIHeadFids(1,nFid),MRIHeadFids(2,nFid),MRIHeadFids(3,nFid),'o','MarkerSize',12,'MarkerFace','g');
      hold on
    end
  end
  if Verbose
    fprintf('Difference : %f\n ',Diff);
  end
  
  if any(Diff > Threshold)
    Status = false;
    if Verbose
      warning('Fiducials mismatch exceeding %1.1g cm: Na %1.1g, LE %1.1g, RE %1.1g', ...
        Threshold, Diff);
    end
  else
    Status = true;
  end
  
end