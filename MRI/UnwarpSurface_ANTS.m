function Surface = UnwarpSurface_ANTS(IndivAnatSpace, TemplateSurface, ...
    WarpLevel, OutputPrefix, OutputFile, Overwrite, ...
    RegularizeMesh, Verify, Warp, OldANTS)
  % Get individual inner-skull surface from template.
  %
  % Surface = UnwarpSurface_ANTS(IndivAnatSpace, TemplateSurface, ...
  %     WarpLevel, OutputPrefix, OutputFile, Overwrite, ...
  %     RegularizeMesh, Verify, Warp, OldANTS)
  %
  % Transform a surface defined in the template space to the individual
  % space, based on ANTS normalization of that individual to the template
  % (performed prior to this call).
  %
  % TemplateSurface can be empty, or provided either as a GIfTI file name,
  % a surface structure with fields vertices and faces, or simply an array
  % of vertex coordinates (size [nV, 3]).  The output surface will
  % correspond to the input type (file name, structure or coordinate
  % array).  A GIfTI file is only saved in the first case (GIfTI file
  % provided as input), in the source image or transformation file
  % directory as provided in IndivAnatSpace (see below).  If empty, the
  % inner skull template surface in the repository will be used.
  %
  % IndivAnatSpace can be given as a structure with fields Rigid, Affine,
  % Warp, InvWarp, each containing the file name of the ANTS transformation
  % data.  It can also be the NIfTI file name (as a string or cell array of
  % 1 string), used for creating the normalized MRI (not the normalized MRI
  % itself), if the transformation files were created in the same directory
  % (as Normalize_ANTS does by default) and with default names.  If
  % IndivAnatSpace is given as a transformation structure, and verification
  % is desired (see Verify), then also provide the field
  % IndivAnatSpace.Source with the source NIfTI file name.
  %
  % RegularizeMesh (default false): If true, the unwarped surface
  % triangulation will be "regularized", making it more uniform, which may
  % be a good idea for BEM but would add some processing time.
  %
  % Verify (default false): If true, the unwarped surface will be overlayed
  % on top of the individual MRI and displayed in NiftiViewer.  Note that
  % since NiftiViewer is modal, execution will be halted until the window
  % is closed.
  %
  % Warp, WarpLevel and OldANTS are optional and passed directly to
  % Unwarp_ANTS.  See help Unwarp_ANTS for details.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-05-29
  
  if nargin < 2
    error('Not enough input arguments.');
  end
  % Option to "regularize" the triangulation of the unwarped surface,
  % making it more uniform, which is probably a good idea for BEM but would
  % add some processing time.
  if ~exist('RegularizeMesh', 'var') || isempty(RegularizeMesh)
    RegularizeMesh = false;
  end
  % Option to overlay the unwarped surface on top of the mri in
  % NiftiViewer.  This is false by default because it will likely be used
  % in scripts.  Easy to verify separately after.
  if ~exist('Verify', 'var') || isempty(Verify)
    Verify = false;
  end
  if ~exist('Overwrite', 'var') || isempty(Overwrite)
    Overwrite = false;
  end
  
  % Template files should be in the Matlab path if the repository code was
  % properly added, but use full path just to be safe.
  if isempty(TemplateSurface)
    TemplateSurface = fullfile(fileparts(mfilename('fullpath')), ...
      '..', 'Template', 'icbm152_InnerSkull.gii');
  end
  
  if iscell(IndivAnatSpace) && numel(IndivAnatSpace) == 1
    IndivAnatSpace = IndivAnatSpace{1};
  end
  % Look for source image, for save path and possible verification.
  if ischar(IndivAnatSpace)
    Nii = IndivAnatSpace;
  elseif isstruct(IndivAnatSpace)
    if isfield(IndivAnatSpace, 'Source')
      Nii = IndivAnatSpace.Source;
    elseif isfield(IndivAnatSpace, 'Affine')
      Nii = ListFiles(fullfile(fileparts(IndivAnatSpace.Affine), '*.nii'), 'f');
      if isempty(Nii)
        Nii = fullfile(fileparts(IndivAnatSpace.Affine), 'NotFound.nii');
        if Verify
          warning('Cannot verify unwarped surface without image.  IndivAnatSpace.Source missing.');
          Verify = false;
        end
      elseif numel(Nii) > 1 && Verify
        % Look for RAS-CTF.nii.
        for i = 1:numel(Nii)
          if strcmpi(Nii{i}(end-6:end), 'RAS-CTF.nii')
            Nii = Nii(i);
            break;
          end
        end
        Nii = Nii{1};
        warning('Found multiple potential matches for source image.  Trying: %s', Nii);
      else
        Nii = Nii{1};
      end
    end
  end
  [Path, ~, Ext] = fileparts(Nii);
  if ~strcmpi(Ext, '.nii')
    error('Expecting NIfTI MRI file, found extension %s', Ext);
  end
  if ~exist('OutputFile', 'var'); OutputFile = ''; end;
  if ~exist('OutputPrefix', 'var'); OutputPrefix = ''; end;
  % Add missing optional arguments for calling Unwarp_ANTS.
  if ~exist('Warp', 'var'); Warp = []; end;
  if ~exist('WarpLevel', 'var'); WarpLevel = []; end;
  if ~exist('OldANTS', 'var'); OldANTS = []; end;
    
  if ischar(TemplateSurface)
    [~, SurfName, SurfExt] = fileparts(TemplateSurface);
    if ~strcmpi(SurfExt, '.gii')
      error('Expecting GIfTI surface file, found extension %s', SurfExt);
    end
    if ~exist(TemplateSurface, 'file')
      error('Surface to unwarp not found: %s', TemplateSurface);
    end
    InputType = 1; % GIfTI file
    if isempty(OutputFile)
      if isempty(OutputPrefix)
        OutputPrefix = 'Unwarped_';
      end
      OutputFile = fullfile(Path, [OutputPrefix, SurfName, SurfExt]);
    elseif ~isempty(OutputPrefix)
      [OutPath, OutName, OutExt] = fileparts(OutputFile);
      OutputFile = fullfile(OutPath, [OutputPrefix, OutName, OutExt]);
    end
    if ~Overwrite && exist(OutputFile, 'file')
      warning('Not overwriting existing file: %s', OutputFile);
      Surface = OutputFile;
      if Verify
        NiftiViewer('File', Nii, 'Surface', OutputFile);
      end
      return;
    end
    Surface = gifti(TemplateSurface);
    if isempty(Surface)
      error('Could not load surface file: %s', TemplateSurface);
    end
    Surface = CheckGiftiMatrixBug(Surface);

    
    % Constant used for matching coordinate of GIfTI surface with NIfTI
    % volume.
    CoordSystems = {'NIFTI_XFORM_UNKNOWN', 'NIFTI_XFORM_SCANNER_ANAT', ...
      'NIFTI_XFORM_ALIGNED_ANAT', 'NIFTI_XFORM_TALAIRACH', 'NIFTI_XFORM_MNI_152'};
    % #define NIFTI_XFORM_UNKNOWN      0 /*! Arbitrary coordinates (Method 1). */
    % #define NIFTI_XFORM_SCANNER_ANAT 1 /*! Scanner-based anatomical coordinates */
    % #define NIFTI_XFORM_ALIGNED_ANAT 2 /*! Coordinates aligned to another file's,
    % #define NIFTI_XFORM_TALAIRACH    3 /*! Coordinates aligned to Talairach-
    % #define NIFTI_XFORM_MNI_152      4 /*! MNI 152 normalized coordinates. */
    
    GiiSystems = [];
    for v = 1:numel(Surface.private.data)
      if strfind(Surface.private.data{v}.attributes.Intent, 'NIFTI_INTENT_POINTSET')
        GiiSystems = [find(strcmpi(Surface.private.data{v}.space.DataSpace, CoordSystems), 1), ...
          find(strcmpi(Surface.private.data{v}.space.TransformedSpace, CoordSystems), 1)];
        break;
      end
    end
    if isempty(GiiSystems)
      error('No points found in GIfTI file.');
    end
    
    if GiiSystems(1) == 5
      % Raw data is already transformed it seems, so keep it.
      TemplatePoints = Surface.vertices;
    elseif GiiSystems(2) == 5
      % Only the transformed intent matches, so apply the transformation.
      % The GIfTI format does not specify how the transformation is
      % supposed to be applied T * v or v' * T; but someone on their forums
      % decisively said it was supposed to be like NIfTI: T * v.
      TemplatePoints = [Surface.vertices, ...
        ones(size(Surface.vertices, 1), 1)] * Surface.mat';
      TemplatePoints(:, 4) = [];
    end
    % Adjust intents of new file to be "aligned".  It is in RAS_CTF
    % coordinates of the subject, which is what is used for normalization.
    Surface.private.data{v}.space.DataSpace = CoordSystems{3};
    Surface.private.data{v}.space.TransformedSpace = CoordSystems{3};
    Surface.private.data{v}.space.MatrixData = eye(4);
    Surface.mat = eye(4);
    if Verify
      % Also verify if the individual nifti file intents were set to
      % "aligned".
      nii = nifti(Nii);
      NiiSystems = [nii.hdr.qform_code, nii.hdr.sform_code] + 1;
      if ~any(NiiSystems == 3)
        % Intent should be "aligned" if RAS_CTF file was used.  We could just
        % ignore since raw is aligned and will display correctly anyway with
        % the RAS_CTF file.  But could indicate an error at some earlier
        % step, e.g. wrong file used for normalization.
        warning(['Individual nii file intent not "aligned". May not overlay properly in NiftiViewer.\n', ...
          'Make sure the RAS_CTF NIfTI file was used for normalization and in the call to this function.']);
      end
    end
    
  elseif isstruct(TemplateSurface)
    if ~isfield(TemplateSurface, vertices)
      error('Unrecognized surface structure, "vertices" field not found.');
    end
    TemplatePoints = TemplateTemplatePoints;
    Surface = TemplateSurface;
    InputType = 2; % Surface structure
  else
    TemplatePoints = TemplateSurface;
    InputType = 3; % Vertex array
  end
  
  if ~ismatrix(TemplatePoints)
    error('Vertex coordinates array not a matrix, %d dimensions found.', ndims(TemplatePoints));
  end
  [nV, nD] = size(TemplatePoints);
  if nD ~= 3
    if nV == 3
      warning('Vertex coordinates array seems transposed. But this is unexpected. Attempting to compensate.');
      TemplatePoints = TemplatePoints';
      %       nV = size(TemplatePoints, 1);
    else
      error('Vertex coordinates array size unexpected.  Should be [nV, 3]');
    end
  end
  
  % Unwarp vertex coordinates from MNI to RAS_CTF.
  IndivPoints = Unwarp_ANTS(TemplatePoints, IndivAnatSpace, ...
    Warp, WarpLevel, OldANTS);
  
  % Regularize unwarped mesh.
  if RegularizeMesh
    if InputType > 2
      error('Surface face array needed to regularize unwarped mesh. Only vertex coordinates were supplied.');
    else
      IndivPoints = SurfaceSmooth(IndivPoints, Surface.faces, 1, 0.1, 10, 1);
    end
  end
  
  % Output the same format as was input.
  switch InputType 
    case 1 % GIfTI file
      % Intent and transformation matrices already adjusted above.
      Surface.vertices = IndivPoints;
      save(Surface, OutputFile, 'ASCII');
      Surface = OutputFile;
    case 2 % Surface structure
      Surface.vertices = IndivPoints;
    case 3 % Vertex array
      Surface = IndivPoints;
  end

  if Verify
    if InputType > 1
      warning('Visual verification only possible if a GIfTI file is supplied. Returning.');
    else
      NiftiViewer('File', Nii, 'Surface', OutputFile);
    end
  end
  
end
