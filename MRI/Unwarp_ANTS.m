function IndivPoints = Unwarp_ANTS(TemplatePoints, IndivAnatSpace, ...
    Warp, WarpLevel, OldANTS, Verbose)
  % Unwarp spatial coordinates from template to individual anatomical space.
  %
  % IndivPoints = Unwarp_ANTS(TemplatePoints, IndivAnatSpace, ...
  %                 Warp, WarpLevel, OldANTS, Verbose)
  %
  % Expects and returns coordinates as stored in the nifti files, i.e. RAS.
  % (Remember to change output individual coordinates when CTF (ALS)
  % coordinates are needed.  This is done e.g. in SourceTimecourse.m.)
  % TemplatePoints array should be N by 3, i.e. coordinate vectors along
  % rows.
  %
  % IndivAnatSpace can be given as a structure with fields Rigid, Affine,
  % Warp, InvWarp, each containing the file name of the ANTS transformation
  % data.  It can also be the NIfTI file name (as a string or cell array of
  % 1 string), used for creating the normalized MRI (not the normalized MRI
  % itself), if the transformation files were created in the same directory
  % (as Normalize_ANTS does by default) and with default names.
  %
  % Warp (default: false): If true, apply a forward warping transformation
  % to the points instead of the reverse "unwarping" operation (i.e. input
  % are individual coordinates and output are template coordinates).
  %
  % WarpLevel (default: 3): 1 rigid, 2 affine or 3 full diffeomorphism.
  %
  % OldANTS (default: false): If true, use older ANTS.exe program,
  % otherwise use newer AntsRegistration.
  %
  % Verbose (default: 0): 0 no output on command line, 1 only file
  % operations, > 1 all coordinates are printed.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-11-27
  
  if nargin < 2
    error('Not enough input arguments.');
  end
  if ~exist('Warp', 'var') || isempty(Warp)
    Warp = false; % i.e. unwarp
  end
  if ~exist('OldANTS', 'var') || isempty(OldANTS)
    OldANTS = false; % i.e. use new AntsRegistration
  end
  if ~exist('Verbose', 'var') || isempty(Verbose)
    Verbose = false; % i.e. no command line output.
  end
  
  if size(TemplatePoints, 2) ~= 3
    error('Expecting row coordinate vectors in TemplatePoints array.');
  end
  
  if iscell(IndivAnatSpace) && numel(IndivAnatSpace) == 1
    IndivAnatSpace = IndivAnatSpace{1};
  end
  if ischar(IndivAnatSpace)
    [Path, Name, Ext] = fileparts(IndivAnatSpace);
    if ~strcmpi(Ext, '.nii')
      error('Expecting NIfTI MRI file, found extension %s', Ext);
    end
    IndivAnatSpace = struct('Source', IndivAnatSpace, 'Rigid', '', 'Affine', '', 'Warp', '', 'InvWarp', '');
    if OldANTS % used ANTS to generate the transformation.
      WarpTransform = fullfile(Path, [Name, '_ANTS.nii']);
      IndivAnatSpace.Affine = [WarpTransform(1:end-4), 'Affine.txt'];
      IndivAnatSpace.Warp = [WarpTransform(1:end-4), 'Warp.nii'];
      IndivAnatSpace.InvWarp = [WarpTransform(1:end-4), 'InverseWarp.nii'];
    else
      % New way, used AntsRegistration to generate the transformation.
      % This is copied from Normalize_ANTS where it is defined.
      IndivAnatSpace.Rigid = fullfile(Path, [Name, '_R_', '0GenericAffine.mat']); % 6-parameter alignment
      IndivAnatSpace.Affine = fullfile(Path, [Name, '_A_', '0GenericAffine.mat']); % 12-parameter affine
      IndivAnatSpace.Warp = fullfile(Path, [Name, '_', '1Warp.nii.gz']); % Diffeomorphism
      IndivAnatSpace.InvWarp = fullfile(Path, [Name, '_', '1InverseWarp.nii.gz']); % Diffeomorphism
    end
  else
    Path = fileparts(IndivAnatSpace.Affine);
  end
  
  if ~exist('WarpLevel', 'var') || isempty(WarpLevel)
    if ~isfield(IndivAnatSpace, 'Warp') || isempty(IndivAnatSpace.Warp)
      if ~isfield(IndivAnatSpace, 'Affine') || isempty(IndivAnatSpace.Affine)
        WarpLevel = 1;
      else
        WarpLevel = 2;
      end
    else
      WarpLevel = 3;
    end
  elseif ~ismember(WarpLevel, [1, 2, 3])
    error('Unrecognized WarpLevel, must be one of: 1 (rigid), 2 (affine) or 3 (diffeomorphism)');
  end
  if OldANTS && WarpLevel == 1
    error('Rigid transformation not implemented with OldANTS method.');
  end
  
  % Create cvs file with coordinates.
  TemplateCSV = fullfile(Path, 'GroupCoord.csv');
  IndivCSV = fullfile(Path, 'IndivCoord.csv');
  
  % ANTS expects and outputs "LPS-physical" coordinates, i.e. flip X and Y.
  TemplatePoints(:, 1:2) = -TemplatePoints(:, 1:2);
  csvwrite(TemplateCSV, [0, 0, 0; TemplatePoints]);
  
  % Unwarp with ANTS.
  
  % Important: Although unintuitive, to transform point coordinates, the
  % program requires the inverse transformation that we want to do.
  switch WarpLevel
    case 1
      if ~isfield(IndivAnatSpace, 'Rigid')
        error('Missing field: IndivAnatSpace.Rigid');
      end
      if ~exist(IndivAnatSpace.Rigid, 'file')
        error('Transformation file not found: %s', IndivAnatSpace.Rigid);
      end
      if Warp % using inverse transformation.
        Command = sprintf('antsApplyTransformsToPoints -d 3 -i %s -o %s -t [%s,1] ', ...
          TemplateCSV, IndivCSV, IndivAnatSpace.Rigid);
      else % unwarp, using forward transformation.
        Command = sprintf('antsApplyTransformsToPoints -d 3 -i %s -o %s -t [%s,0]', ...
          TemplateCSV, IndivCSV, IndivAnatSpace.Rigid);
      end
    case 2
      if ~isfield(IndivAnatSpace, 'Affine')
        error('Missing field: IndivAnatSpace.Affine');
      end
      if ~exist(IndivAnatSpace.Affine, 'file')
        error('Transformation file not found: %s', IndivAnatSpace.Affine);
      end
      if Warp % using inverse transformation.
        Command = sprintf('antsApplyTransformsToPoints -d 3 -i %s -o %s -t [%s,1] ', ...
          TemplateCSV, IndivCSV, IndivAnatSpace.Affine);
      else % unwarp, using forward transformation.
        Command = sprintf('antsApplyTransformsToPoints -d 3 -i %s -o %s -t [%s,0]', ...
          TemplateCSV, IndivCSV, IndivAnatSpace.Affine);
      end
    case 3
      if ~isfield(IndivAnatSpace, 'Affine')
        error('Missing field: IndivAnatSpace.Affine');
      end
      if ~exist(IndivAnatSpace.Affine, 'file')
        error('Transformation file not found: %s', IndivAnatSpace.Affine);
      end
      if Warp % using inverse transformation.
        if ~isfield(IndivAnatSpace, 'InvWarp')
          error('Missing field: IndivAnatSpace.InvWarp');
        end
        if ~exist(IndivAnatSpace.InvWarp, 'file')
          error('Transformation file not found: %s', IndivAnatSpace.InvWarp);
        end
        Command = sprintf('antsApplyTransformsToPoints -d 3 -i %s -o %s -t [%s,1] -t %s', ...
          TemplateCSV, IndivCSV, IndivAnatSpace.Affine, IndivAnatSpace.InvWarp);
      else % unwarp, using forward transformation.
        if ~isfield(IndivAnatSpace, 'Warp')
          error('Missing field: IndivAnatSpace.Warp');
        end
        if ~exist(IndivAnatSpace.Warp, 'file')
          error('Transformation file not found: %s', IndivAnatSpace.Warp);
        end
        Command = sprintf('antsApplyTransformsToPoints -d 3 -i %s -o %s -t %s -t [%s,0]', ...
          TemplateCSV, IndivCSV, IndivAnatSpace.Warp, IndivAnatSpace.Affine);
      end
  end
  [S, Messages] = system(Command);
  if S || Verbose > 1
    fprintf(Messages);
  elseif Verbose
    iM = [find(Messages == sprintf('\n'), 1, 'first')+1, ...
      find(Messages == sprintf('\n'), 2, 'last')+1];
    if isempty(iM)
      iM = [find(Messages == sprintf('\r'), 1, 'first')+1, ...
        find(Messages == sprintf('\r'), 2, 'last')+1];
    end
    if numel(iM) >= 2
      fprintf(Messages(1:iM(1)));
      fprintf(Messages(iM(2):end));
    end
  end
  
  % Get coordinates from transformed file.
  IndivPoints = csvread(IndivCSV, 1);
  
  % ANTS expects and outputs "LPS-physical" coordinates, i.e. flip X and Y.
  IndivPoints(:, 1:2) = -IndivPoints(:, 1:2);
  
  % Remove original coordinate file.
  delete(TemplateCSV);
  
end


