function PreMRIFile = PrepareMRI(RawMRIFile, PreMRIFile, Overwrite)
  % Preprocess raw (Dicom) T1 MRI for MEG analysis.
  %
  % PreMRIFile = PrepareMRI(RawMRIFile, PreMRIFile, Overwrite)
  %
  % The MRI file is converted to Nifti format, bias-corrected and changed
  % to "CTF-RAS" coordinate system, which requires manually picking
  % locations where the MEG fiducials were placed on the head.  All
  % temporary files and the final preprocessed MRI are saved in the folder
  % provided in PreMRIFile.  If this argument is missing, default names are
  % used and files are saved in the RawMRIFile directory.  By default,
  % Overwrite = false, which means any existing file is reused with no
  % change.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-25

  
  if ~exist('RawMRIFile', 'var') || isempty(RawMRIFile) || ~exist(RawMRIFile, 'file')
    error('No MRI file to preprocess.');
  end
  if ~exist('Overwrite', 'var') || isempty(Overwrite)
    Overwrite = false;
  end
  
  if ~exist('PreMRIFile', 'var') || isempty(PreMRIFile)
    PreMRIFile = [RawMRIFile(1:end-4), '_Pre.nii'];
  end
  
  if ~Overwrite && exist(PreMRIFile, 'file')
    fprintf('MRI file already preprocessed: %s\n;', PreMRIFile);
    return;
  end
  
  % ----------------------------------------------------------------------
  % 1. Convert Dicom to Nifti.
  % Could we change this to use Fieldtrip functions?
  if isdir(RawMRIFile)
    for Ext = {'ima', 'IMA', 'dcm', 'DCM'}
      SliceList = ListFiles(fullfile(RawMRIFile, ['*.', Ext{1}]), 'f');
      if ~isempty(SliceList)
        RawMRIFile = SliceList{1};
        break;
      end
    end
  end
  if isdir(RawMRIFile)
    % Didn't find any recognized raw MRI slice files.
    error('You provided a directory instead of an MRI image (slice), and we could not find a .ima or .dcm');
  end
  [~, Name] = fileparts(RawMRIFile);
  Path = fileparts(PreMRIFile);
  TempMRIFileO = fullfile(Path, ['o', Name, '.nii']);
  TempMRIFile = fullfile(Path, [Name, '.nii']);

  % If we want to overwrite, need to remove otherwise dcm2nii appends "A"
  % at end of file.
  if Overwrite 
    if exist(TempMRIFileO, 'file')
      delete(TempMRIFileO);
    end
    if exist(TempMRIFile, 'file')
      delete(TempMRIFile);
    end
  end
  if Overwrite || ~exist(TempMRIFileO, 'file')
    Command = sprintf(...
      'dcm2nii -a n -c n -d n -e n -f y -i n -p n -n y -r y -g n -o %s %s', ...
      Path, RawMRIFile);
    system(Command);
    
    % Delete original and cropped .nii, keep orthogonalized .nii.
    delete(TempMRIFile);
    delete(fullfile(Path, ['co', Name, '.nii']));
  end
  
  % ----------------------------------------------------------------------
  % 2. N4 field bias coorection.
  N4File = [PreMRIFile(1:end-4), '_N4.nii'];
  tic;
  if Overwrite || ~exist(N4File, 'file')
    Command = sprintf('N4BiasFieldCorrection -d 3 -s 3 -i %s -o %s &', ...
      TempMRIFileO, N4File);
    % The command ends in '&', so it runs in the background allowing us to
    % pick fiducials without waiting.  Later, we check that this is done
    % before continuing.
    system(Command); % about 1 min.  
  end
  
  % 3. Pick fiducials on MRI (while bias correction runs).
  [~, Name] = fileparts(PreMRIFile);
  FidsFile = fullfile(Path, [TempMRIFileO, '.fids.mat']);
  PreFidsFile = fullfile(Path, [Name, '.fids.mat']);
  if Overwrite || ~exist(FidsFile, 'file')
    GetFiducials(TempMRIFileO, FidsFile);
  end
  
  % 4. Wait for N4 to finish if needed.
  while ~exist(N4File, 'file')
    if toc > 5*60 % 5 minutes max, adjust this value if computer needs more time...
      error('Bias-corrected MRI file not found: %s.', N4File);
    end
    pause(10); % wait 10s.
  end
  
  if exist(TempMRIFileO, 'file')
    delete(TempMRIFileO);
  end

  % ----------------------------------------------------------------------
  % 5. Convert to RAS-CTF-head coordinates.
  
  % If needed, nii2ctfnii would call GetFiducials, but now we do it above
  % while N4 is running.  
  nii2ctfnii(N4File, PreMRIFile, FidsFile); % Uses niftimatlib to open and save.  Overwrites existing files.
  if ~exist(PreMRIFile, 'file')
    error('Newly created MRI file not found: %s.', PreMRIFile);
  else
    delete(N4File);
  end
  
  % Because we only need to keep the fully pre-processed file, PreMRIFile,
  % it makes sense to keep a fiducials file that matches, with similar file
  % name and with RAS-CTF coordinates in SForm and QForm instead of the
  % coordinates from the temporary nifti file from previous steps.
  % [Previously the same file was modified, but this could also lead to
  % errors if reprocessing the raw MRI file.]  The voxel coordinates are
  % the same before and after nii2ctfnii though.  Note that this
  % PreFidsFile is not used in our usual processing.
  Fids = []; % Just to avoid Matlab editor warnings.  Loaded on next line.
  load(FidsFile);
  % The needed transformation is going to CTF coordinates in mm, then
  % rotating back to RAS.  The more robust way to do this however is to get
  % the transformation matrices from the nifti file.
  nii = nifti(PreMRIFile);
  Fids.SForm = [Fids.VoxelIndex1, ones(3, 1)] * nii.mat';
  Fids.SForm(:, 4) = [];
  Fids.QForm = [Fids.VoxelIndex1, ones(3, 1)] * nii.mat0';
  Fids.QForm(:, 4) = [];
  save(PreFidsFile, 'Fids');
  

  % 6. Now ready for normalization.
    
end
