function [PeakAmplitudes, PeakCoordinates, PeakVoxCoord, RejectedByMask] = ...
    PeakFinderNii(NiiFile, R, Thresh, Abs, Form, nDimSearch)
  % Find peak coordinates and amplitudes in a 3d nifti image
  %
  % [PeakAmplitudes, PeakCoordinates, PeakVoxCoord] = ...
  %   PeakFinderNii(NiiFile, R, Thresh, Abs, Form, nDimSearch)
  %
  % Peaks are calculated within a certain number of voxels.  E.g. if R = 1,
  % looks within a 3x3x3 cube centered around the possible peak location.
  % Also returns the peaks sorted by decreasing amplitude.  A mask (e.g.
  % significance thresholded image) can be supplied to further restrict
  % peaks, as the second cell of NiiFile (e.g. NiiFile = {'Image.nii',
  % 'Mask.nii'}).  If a fourth output argument is supplied, the peaks that
  % would be rejected by the mask are instead kept and their indices noted
  % in RejectedByMask.
  %
  % Thresh (default: -Inf, i.e. no limit): Only look for peaks of amplitude
  % above this threshold.
  %
  % Abs (default: false): if true, take the absolute value of the image,
  % such that both positive and negative peaks are found.
  %
  % Form (default: 'S'): 'S' or 'Q', which nifti coordinate system to use.
  %
  % nDimSearch (default: 3): Find peaks in this number of dimensions.  For
  % example, if a 4-d nifti file is provided but nDimSearch is 3, it will
  % separately find peaks in each "3-d slice" instead of finding peaks in
  % 4-d.  Currently only nDimSearch=3 is implemented.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-15
  
  
  if nargin < 1 || isempty(NiiFile)
    error('File not provided.');
  end
  DoMask = false;
  if iscell(NiiFile)
    switch numel(NiiFile)
      case 1
        NiiFile = NiiFile{1};
      case 2
        DoMask = true;
        MaskFile = NiiFile{2};
        NiiFile = NiiFile{1};
      otherwise
        error('NiiFile should be a single NIfTI image file, or a cell array with 2 elements (second being a mask NIfTI file).');
    end
  end
  if ~exist(NiiFile, 'file')
    error('Image file not found: %s.', NiiFile);
  end
  Nii = nifti(NiiFile);
  if DoMask
    if ~exist(MaskFile, 'file')
      error('Mask file not found: %s.', MaskFile);
    end
    Nii = nifti(MaskFile);
    Mask = Nii.dat(:, :, :, :);
    MaskDim = ndims(Mask);
  end
  if nargin < 2 || isempty(R)
    R = 2;
  elseif R < 1
    error('R must be >= 1.');
  end
  R = round(R);
  if nargin < 3 || isempty(Thresh)
    % This default is better than -Inf, because there could be a large
    % "equal area" of this value, e.g. 0.  This might be the only such area
    % and by excluding this we avoid extra processing steps.
    Thresh = min(Nii.dat(:));
  end
  
  if ~exist('Abs', 'var') || isempty(Abs)
    Abs = false;
  end
  if ~exist('Form', 'var') || isempty(Form)
    Form = 'S';
  elseif ~ismember(Form, {'S', 'Q'})
    error('Form should be ''S'' or ''Q'' to specify which nifti coordinate system (sform or qform).');
  end
  if ~exist('nDim', 'var') || isempty(nDimSearch)
    nDimSearch = 3;
  elseif nDimSearch ~= 3
    error('Currently, only searching for peaks in 3 dimensions is implemented.  Asked for %d.', ...
      nDimSearch);
  end
  
  nDims = Nii.hdr.dim(1); % find(Nii.hdr.dim > 1, 1, 'last');
  if nDims < 3
    % Code might still work, though not sure if this would ever be used and
    % what the coordinates would mean.  Not sure if Nii.dat(:, :, :) would
    % work for fewer dimensions.
    error('Unexpected: %d dimensions of data in nifti file.', nDims);
  elseif nDims > 4
    % Probably possible to make this work still treating each "3-d slice"
    % separately, but might just take very long and no use for it now.
    error('Unexpected: %d dimensions of data in nifti file.', nDims);
  end
  
  % Only 4th dim can currently be the looping dim.
  %   LoopDim = nDimSearch + (1:(nDims - nDimSearch)); SizeLoop =
  %   Nii.hdr.dim(2 + LoopDim);
  nSlices = Nii.hdr.dim(2 + nDimSearch);
  SizeV = Nii.hdr.dim(1+ (1:nDimSearch));
  
  for s = 1:nSlices
    V = Nii.dat(:, :, :, s);
    
    if Abs
      VNeg = V < 0;
      V = abs(V);
    else
      VNeg = false(SizeV);
      if min(V(:)) < 0 && Thresh < 0
        warning('Asked for peaks in the positive direction only, but image contains negative values.  Recommend setting Abs = true or a positive threshold.');
      end
    end
    
    WherePeaks = PeakFinder(V, R, Thresh);
    
    if DoMask
      if MaskDim == 4
        Mask3d = Mask(:, :, :, s);
      elseif MaskDim == 3
        Mask3d = Mask;
      else
        error('Mask should have 3 or 4 dimensions.');
      end
      if nargout < 4
        WherePeaks(isnan(Mask3d) | Mask3d==0) = false;
        RejectedByMask = zeros(sum(WherePeaks(:)), 1);
      else
        RejectedByMask = isnan(Mask3d(WherePeaks)) | Mask3d(WherePeaks) == 0;
      end
    else
      RejectedByMask = zeros(sum(WherePeaks(:)), 1);
    end
    
    % We want to know the sign of the amplitude.
    PeakAmplitudes = (-1).^VNeg(WherePeaks) .* V(WherePeaks);
    
    % Convert to 3d indices.
    [X, Y, Z] = ind2sub(SizeV, find(WherePeaks));
    PeakVoxCoord = [X, Y, Z];
    PeakCoordinates = [[X, Y, Z], ones(length(X), 1)];
    % Get coordinates.
    if strcmp(Form, 'S') % SForm world coordinates.
      PeakCoordinates = PeakCoordinates * Nii.mat';
    elseif strcmp(Form, 'Q') % QForm world coordinates.
      PeakCoordinates = PeakCoordinates * Nii.mat0';
    end
    
    % Sort peaks based on amplitude.
    PeakCoordinates = sortrows([PeakAmplitudes, PeakCoordinates(:, 1:3), ...
      PeakVoxCoord(:, 1:3), RejectedByMask], 1);
    PeakCoordinates = flipdim(PeakCoordinates, 1);
    PeakAmplitudes = PeakCoordinates(:, 1);
    PeakVoxCoord = PeakCoordinates(:, 5:7);
    RejectedByMask = PeakCoordinates(:, 8);
    PeakCoordinates = PeakCoordinates(:, 2:4);
  end
end
