function R = SecondMoment(B, Type)
  % Second moment matrix R of observations B.  No mean subtraction by default.
  %
  % R = SecondMoment(B, Type)
  %
  % B dimensions should be ordered as: samples, sensors, trials (optional).
  %
  % Type: 0: second moment (default). 1,2: covariance, i.e. mean is
  % subtracted from each channel first. For 1, a single mean over all
  % trials is used, assuming continuous data, for 2 each trial mean is
  % subtracted. 3,4: correlation, i.e. covariance divided by each channel's
  % standard deviation, using variance 1 or 2 respectively.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-10-20

  if nargin < 2 || isempty(Type) % ~exist('Type', 'var')
    Type = 0;
  end
  
  [nSampl, nSens, nT] = size(B);
  if Type > 0
    switch Type
      case {1, 3}
        Mu = 1 / (nSampl * nT) * sum(sum(B, 1), 3);
      case {2, 4}
        Mu = 1 / (nSampl) * sum(B, 1);
    end
    B = bsxfun(@minus, B, Mu);
  end
  R = zeros(nSens);
  for t = 1:nT 
    % Guessing looping over trials is faster than reshaping B, especially
    % for large datasets.
    R = R + B(:, :, t)' * B(:, :, t);
  end
  % Divide by proper degrees of freedom for unbiased variance estimate.
  switch Type
    case 0
      R = R / (nSampl * nT);
    case {1, 3}
      R = R / (nSampl * nT - 1);
    case {2, 4}
      R = R / ((nSampl - 1) * nT);
  end
  
  if Type > 2
    StDev = sqrt(diag(R)); % diag gives column vector.
    StDev = StDev * StDev';
    R = R ./ StDev;
  end
end