function F = DisjointSetForest(V, M, Rootify, R)
  % Partition volume into a disjoint-set structure of regions with equal value.
  %
  % F = DisjointSetForest(V, M, Rootify, R)
  %
  % Create a disjoint-set forest structure F from 3d volume V, based on
  % regions having the same value. The forest is a 1 column vector of
  % indices that indicate the "parent" of each "branch" (element). A branch
  % that is its own parent is a "root", i.e. the representative element of
  % that "tree" (set). (Used in PeakFinder.m)
  %
  % M: Optional mask of which elements of V to process, indicated by true
  % or non-zero values. Rest will refer to 0 in F. Default is to process
  % all elements.
  % 
  % Rootify [false]: Option to make each branch refer directly to its root.
  %
  % R [1]: Search "radius" (applied "cubically"), i.e. regions separated by
  % no more than this distance (in up to all 3 directions) will be grouped
  % in the same tree.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-02-09

  % Volume to process into sets.
  if nargin < 1 || isempty(V)
    error('Nothing to do.');
  end
  sV = size(V);
  nV = numel(V);

  % Mask of which elements to process. Rest will refer to 0 in F.
  if nargin < 2 || isempty(M)
    M = true(sV);
  elseif ~islogical(M)
    M = logical(M); % Error on NaN.
  end
  
  % Option to make each branch refer directly to its root.
  if nargin < 3 || isempty(Rootify)
    Rootify = false;
  end
  
  % Search "radius".
  if nargin < 4 || isempty(R)
    R = 1;
  end
  
  %   nM = sum(M(:));
  I = find(M);
  
  % Forest structure: parent, rank.  [This is "sparse", but unsure of an
  % efficient way to go both ways (iF <-> iV).]
  F = zeros(nV, 2);
  
  % Scan (over masked voxels only).
  for f = I(:)'
    [x,y,z] = ind2sub(sV, f);
    % Avoid redundant comparisons.  Need to include half the volume, half
    % the dividing plane, and half the dividing line.
    for k = -R+(1-z+R)*((z-R)<1):0
    for j = -R+(1-y+R)*((y-R)<1):R+(sV(2)-y-R)*((y-R)>sV(2))
    for i = -R+(1-x+R)*((x-R)<1):R+(sV(2)-y-R)*((y-R)>sV(2))
      % Exclude those "half boundary" cases.
      if (k == 0 && j > 0) || (k == 0 && j == 0 && i >= 0)
        continue;
      end
      if V(x, y, z) == V(x+i, y+j, z+k)
        g = sub2ind(sV, x+i, y+j, z+k);
        %         Union(f, g);
        rf = Find(f);
        rg = Find(g);
        if rf ~= rg
          if F(rf, 2) < F(rg, 2)
            F(rf, 1) = rg;
          elseif F(rf, 2) > F(rg, 2)
            F(rg, 1) = rf;
          else
            F(rf, 1) = rg;
            F(rg, 2) = F(rg, 2) + 1;
          end
        end
      end % found equal voxels.
    end
    end
    end
  end % masked voxel loop.
  
  % Get rid of rank.  Not sure it's actually helpful (in Matlab).
  F(:, 2) = [];
  
  if Rootify
    % Ensure each branch refers directly to its root.
    for f = I(:)'
      p = F(f);
      if F(p) ~= p % Parent is not a root.
        F(f) = Find(F(p)); 
      end
    end
  end
  
  %------------------------------------
  function Root = Find(x)
    if F(x, 1) == 0 % Because I don't initialize, expecting sparsity.
      F(x, 1) = x;
    elseif F(x, 1) ~= x
      F(x, 1) = Find(F(x, 1)); % Can this reduce rank of root?
    end
    Root = F(x, 1);
  end
  
  %   function Union(f, g)
  %     rf = Find(f);
  %     rg = Find(g);
  %     if rf ~= rg
  %       if F(rf, 2) < F(rg, 2)
  %         F(rf, 1) = rg;
  %       elseif F(rf, 2) > F(rg, 2)
  %         F(rg, 1) = rf;
  %       else
  %         F(rg, 1) = rf;
  %         F(rf, 2) = F(rf, 2) + 1;
  %       end
  %     end
  %   end
    
end