function WherePeaks = PeakFinder(V, R, Thresh, ExcludeEdge)
  % Find peak locations in a 3d array.
  %
  % WherePeaks = PeakFinder(V, R, Thresh)
  %
  % Peaks are found within a certain number of voxels. E.g. if R = 1, looks
  % within a 3x3x3 cube centered around the possible peak location. [Could
  % be extended to 4d but not yet implemented.] Returns a logical array of
  % where the peaks are, which can be converted to 3-d indices this way:
  %  [X, Y, Z] = ind2sub(SizeV, find(WherePeaks));
  %
  % Thresh [default: min(V(:)), i.e. no limit]: Only look for peaks of
  % amplitude above this threshold.
  %
  % ExcludeEdge [true]: Do now allow peaks on the volume edge.
  %
  % Note about equal values: Areas of equal values surrounded by lower
  % values are considered one peak and the single voxel at the geometrical
  % center will be kept, unless it is outside the peak area, in which case
  % it is shifted to the closest area voxel.  Two such areas (with the same
  % value) separated by a space less than R are considered part of the same
  % peak area.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-03
  
  if nargin < 1 || isempty(V)
    error('Nothing to do.');
  end
  if nargin < 2 || isempty(R)
    R = 2;
  elseif R < 1
    error('R must be >= 1.');
  end
  R = round(R);
  if nargin < 3 || isempty(Thresh)
    % This default is better than -Inf, because there could be a large
    % "equal area" of this value, e.g. 0.  This might be the only such area
    % and by excluding this we avoid extra processing steps.
    Thresh = min(V(:));
  end
  if nargin < 4 || isempty(ExcludeEdge)
    ExcludeEdge = true;
  end
  
  % NaN comparisons (<, >) are always false.  Change them to Thresh (again
  % to avoid processing this area).
  V(isnan(V)) = Thresh;
  
  SizeV = size(V); % Nii.hdr.dim(1+ (1:nDimSearch));
  if numel(SizeV) < 3
    SizeV = [SizeV, 1];
  end
  
  % Make padded volume for fast searching.
  PadV = Thresh * ones(SizeV + 2*R);
  if ExcludeEdge
    % Do not allow the very edge to have a peak, but any others ok, even
    % with large radius.  To achieve this, pad first with Thresh, and then
    % the most outer layer with Inf.
    PadV([1, end], :, :) = Inf;
    PadV(:, [1, end], :) = Inf;
    PadV(:, :, [1, end]) = Inf;
  end
  PadV((1+R:end-R), (1+R:end-R), (1+R:end-R)) = V;
  
  % 1) Find single-voxel peaks: greater than all neighbors. 
  % 2) (Within the same loop) Find voxels that are greater or equal to
  % neighbors (same radius) but then exclude those of step 1.  Here we can
  % have non-peak voxels that have equal neighbors (not necessarily
  % adjacent).
  % 3) Find all voxels with equal neighbors. This will be used to get
  % single voxels for these "equal areas", and then reject those that
  % aren't surrounded by smaller values by comparing to (2).
  
  % Start by applying the desired threshold.  Exclude it to avoid possibly
  % large Thresh "equal area" and extra steps to deal with those, since
  % it's likely there often won't be any other such areas.
  WherePeaks = V > Thresh;
  WhereGrEq = WherePeaks;
  WhereAnyEq = false(SizeV);
  for i = -R:R
    for j = -R:R
      for k = -R:R
        if i == 0 && j == 0 && k == 0
          continue;
        end
        WherePeaks = WherePeaks & PadV((1+R:end-R), (1+R:end-R), (1+R:end-R)) > ... % : has lower precedence than +, -
          PadV((1+R:end-R) + i, (1+R:end-R) + j, (1+R:end-R) + k);
        WhereGrEq = WhereGrEq & PadV((1+R:end-R), (1+R:end-R), (1+R:end-R)) >= ...
          PadV((1+R:end-R) + i, (1+R:end-R) + j, (1+R:end-R) + k);
        WhereAnyEq = WhereAnyEq | PadV((1+R:end-R), (1+R:end-R), (1+R:end-R)) == ...
          PadV((1+R:end-R) + i, (1+R:end-R) + j, (1+R:end-R) + k);
      end
    end
  end
  clear PadV
  % Apply threshold also to AllEq (couldn't before because of "or").
  WhereAnyEq = WhereAnyEq & (V >= Thresh);
  
  % Remove peaks found in step 1 from "equals".  Now they all must have an
  % equal neighbor.
  WhereGrEq(WherePeaks) = false;
  
  if sum(WhereGrEq(:)) > 1 % Shortcut if there are no "equals".
    
    % Separate "equals" into separate areas.
    EqForest = DisjointSetForest(V, WhereAnyEq, true, R);
    
    % Reject those not considered peaks, i.e. surrounded by a greater value.
    RejectedEq = unique(EqForest(WhereAnyEq(:) & ~WhereGrEq(:)));
    EqForest(ismember(EqForest, RejectedEq)) = 0;
    
    % Find geometrical center of each remaining peak area and add it to
    % WherePeaks.
    if any(EqForest)
      EqLabels = unique(EqForest > 0);
      for l = EqLabels(:)'
        [x,y,z] = ind2sub(SizeV, find(EqForest == l));
        Center = round(mean([x, y, z]));
        % Find closest.  min returns only the first index even if multiple
        % have the same min value.  So peaks will be a little biased
        % towards low voxel coordinates.
        [~, Closest] = min((x - Center(1)).^2 + (x - Center(2)).^2 + ...
          (z - Center(3)).^2);
        WherePeaks(x(Closest), y(Closest), z(Closest)) = true;
        %         if ~(EqForest(sub2ind(SizeV, Center(1), Center(2), Center(3))) == l)
        %         else
        %           WherePeaks(Center(1), Center(2), Center(3)) = true;
        %         end
      end
    end
  
  end % if there are "equals" to process.
  
end



