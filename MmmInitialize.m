function MmmInitialize()
  % Add all required folders to the MATLAB path.
  % 
  %
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-29

  % No need to add the base directory to the path.
  Dir = fileparts(mfilename('fullpath'));
  addpath( ...
    fullfile(Dir, 'Forward'), ...
    fullfile(Dir, 'Inverse'), ...
    fullfile(Dir, 'Head'), ...
    fullfile(Dir, 'IO'), ...
    fullfile(Dir, 'Markers'), ...
    fullfile(Dir, 'MRI'), ...
    fullfile(Dir, 'Stats'), ...
    fullfile(Dir, 'View'), ...
    fullfile(Dir, 'External'), ...
    fullfile(Dir, 'Misc') );

  % Search for Fieldtrip to add ctf IO routines.
  FT = which('ft_defaults');
  if ~isempty(FT)
    Dir = fileparts(FT);
    addpath(fullfile(Dir, 'external', 'ctf'));
  else
    warning('Fieldtrip not found on path, functions that interact with CTF MEG datasets may not work.');
  end
  
end