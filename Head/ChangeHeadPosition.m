function ChangeHeadPosition(Dataset, Fiducials)
  % Change the head position in a CTF MEG dataset (like CTF's changeHeadPos).
  %
  % ChangeHeadPosition(Dataset, Fiducials)
  %
  % Matlab version of the CTF command line program changeHeadPos.  Changes
  % the dataset .hc and .res4 files such that the initial head coil
  % positions are the ones provided.  This does not change the data, only
  % the sensor coordinates and orientations and the head coil coordinates.
  % Backups of the files are created first (with _Original appended before
  % the file extension), allowing easy restoration of the original dataset
  % state.  Files are double checked for errors.
  %  
  % Fiducials: coordinates of the head coils in dewar coordinates, in cm.
  % Should be shaped as a single vector (row or column):
  %   [Na_x, y, z, LE_x, y, z, RE_x, y, z]
  % or as a 3x3 matrix:
  %   [Na_x, y, z; LE_x, y, z; RE_x, y, z]
  % Note that while the CTF program changeHeadPos requires mm, we use cm
  % here to be consistent with hc and res4 files, and the CTF head
  % coordinate system.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2017-05-31

  % Results were validated against changeHeadPos on the same dataset.
  % Locations were identical within 0.0001 cm (unclear why), and
  % orientations were identical.
  
  if nargin < 2
    error('Missing inputs.');
  end
  
  [Path, Name, Ext] = fileparts(Dataset); %#ok<ASGLU>
  if strcmpi(Ext, '.ds')
    %     HCFile = [Dataset, filesep, Name, '.hc'];
    Res4File = [Dataset, filesep, Name, '.res4'];
  else
    error('Unrecognized dataset: %s.', Dataset);
  end
  
  % Backup res4 file first.  Will fail if Dataset does not exist.
  Res4FileOriginal = BackupOriginal(Res4File); %#ok<NASGU>
  
  % Load dataset info.
  % Use dewar sensor positions and orientations (pos0, ori0), in cm.
  ds = readCTFds(Dataset);
  res4 = ds.res4;
  res4Orig = res4;
  %   FiducialsOrig = ds.hc.dewar';
  clear ds
  
  % This checks that Fiducials are in cm, in dewar coords, and backs up the
  % hc file.  It also saves the adjusted new position so from here, we need
  % to catch any error to be able to revert to the original hc file, until
  % res4 is modified and saved.
  savehc(Fiducials, Dataset);
  
  if size(Fiducials, 1) ~= 3
    Fiducials = reshape(Fiducials, [3, 3])';
  end
  
  % Calculate new head coordinates of sensor positions and orientations.
  Pos = ChangeCoordinates([res4.senres.pos0]', Fiducials); %, ...
  %   Scaling, Orientation, System, Inverse, Vector, Verbose)
  
  Ori = ChangeCoordinates([res4.senres.ori0]', Fiducials, ...
    1, 1, 'CTF', false, true, false);
    
  % Distribute new values into res4 structure.
  c = 0;
  for s = 1:res4.no_channels
    res4.senres(s).pos = Pos((1:res4.senres(s).numCoils) + c, :)';
    res4.senres(s).ori = Ori((1:res4.senres(s).numCoils) + c, :)';
    c = c + res4.senres(s).numCoils;
  end
  
  % Save new .res4 file.
  MAX_COILS=8; % Hard coded in writeCTFds.
  writeRes4(Res4File, res4, MAX_COILS); 
  % No status output so double check.
  ds = readCTFds(Dataset);
  if any(ds.res4.senres(50).pos(:) ~= res4.senres(50).pos(:))
    HCFile = [Dataset, filesep, Name, '.hc'];
    HCFileOriginal = [HCFile(1:end-3), '_Original.hc'];
    if all(ds.res4.senres(50).pos(:) == res4Orig.senres(50).pos(:))
      % Somehow res4 was not updated.  Restore original hc file.
      CopyFile(HCFileOriginal, HCFile);
      %       if ~copyfile(HCFileOriginal, HCFile, 'f')
      %         error('Copy failed: %s to %s', HCFileOriginal, HCFile);
      %       end
      error('Error writing to res4 file.  Original .hc file restored (though you may want to double check).');
    else
      % Not new nor original... ?
      
      % Leave as is for now to allow investigation, but recommend restoring
      % both files.
      %       if ~copyfile(HCFileOriginal, HCFile, 'f')
      %         error('Copy failed: %s to %s', HCFileOriginal, HCFile);
      %       end
      %       if ~copyfile(Res4FileOriginal, Res4File, 'f')
      %         error('Copy failed: %s to %s', Res4FileOriginal, Res4File);
      %       end
      error(['Unexpected new location values in res4 file.  ', ...
        'Original res4 and hc files should be restored, but this was not ', ...
        'automatically done to allow investigating this unusual situation.']);
    end
  end
    
end

