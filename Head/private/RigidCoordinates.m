function [O, R] = RigidCoordinates(FidsColumns)
  % Convert head coil locations to origin position and rotation matrix.
  %
  % [O, R] = RigidCoordinates(FidsColumns)
  %
  % Copy of part of the ChangeCoordinates function, simplified and
  % optimized for speed given it gets called many times.  For additional
  % details, see HeadMotionTool.m.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-06
 
  R = zeros(3);
  O = (FidsColumns(:, 2) + FidsColumns(:, 3))/2;
  R(:, 1:2) = FidsColumns(:, 1:2) - O(:, [1, 1]);
  %R(:, 3) = cross(R(:, 1), R(:, 2));
  R(:, 3) = [R(2, 1)*R(3, 2) - R(3, 1)*R(2, 2), -R(1, 1)*R(3, 2) + R(3, 1)*R(1, 2), R(1, 1)*R(2, 2) - R(2, 1)*R(1, 2)];
  %R(:, 2) = cross(R(:, 3), R(:, 1));
  R(:, 2) = [R(2, 3)*R(3, 1) - R(3, 3)*R(2, 1), -R(1, 3)*R(3, 1) + R(3, 3)*R(1, 1), R(1, 3)*R(2, 1) - R(2, 3)*R(1, 1)];
  % Normalize x, y, z.
  R = bsxfun(@rdivide, R, sqrt(sum(R.^2, 1)));
end
