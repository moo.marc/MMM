function MedianLoc = MedianLocation(Data, GoodTrials)
  % Overall geometric median location of each head coil.
  %
  % MedianLoc = MedianLocation(Data, GoodTrials)
  %
  % For details, see HeadMotionTool.m.  
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-06
 
  nSxGT = size(Data, 1) * length(GoodTrials);
  if nSxGT == 0 % If all trials rejected.
    MedianLoc = zeros(1, 9);
  else
    MedianLoc = reshape( GeoMedian( ...
      reshape(permute( Data(:, :, GoodTrials), ...
      [1, 3, 2]), [nSxGT, 3, 3]), 1e-3 ) , [1, 9]);
  end
end
