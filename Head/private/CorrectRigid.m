function NewLocations = CorrectRigid(Locations, Rigid)
  % Adjust the head coil positions slightly based on "rigid" reference.
  %
  % NewLocations = CorrectRigid(Locations, Rigid)
  %
  % The "rigid" reference was obtained from the entire dataset.  This
  % correction then removes "jitter" in the inter-coil distances through
  % time. For details, see HeadMotionTool.m.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-02-08
 
  % Returns the inverse coordinate transformation (Rigid is in head
  % coordinates and transformed to non-head coordinates of Locations).
  switch ndims(Locations)
    case 2 % Single trial dataset.
      NeedReshape = true;
      nS = size(Locations, 1);
      nT = 1;
    case 3
      if size(Locations, 2) == 9
        NeedReshape = true;
        [nS, unused, nT] = size(Locations); % ~
      else % Single trial dataset.
        NeedReshape = false;
        nS = size(Locations, 1);
        nT = 1;
      end
    case 4
      NeedReshape = false;
      [nS, unused, unused2, nT] = size(Locations); % ~, ~
    otherwise
      error('Unexpected dimensions.');
  end
  
  % Reshape.
  Y = reshape(Rigid, [3, 3]); % Points (coils) are columns.

  NewLocations = zeros(nS, 9, nT);
  for t = 1:nT
    for s = 1:nS
      if NeedReshape
        [XO, XR] = RigidCoordinates(reshape(Locations(s, :, t), [3, 3]));
      else
        [XO, XR] = RigidCoordinates( squeeze(Locations(s, :, :, t)) );
      end
      % Inverse transformation going from Y head coordinates to X dewar.
      NewLocations(s, :, t) = reshape(XR * Y + XO(:, [1, 1, 1]), [1, 9, 1]);
    end
  end
end
