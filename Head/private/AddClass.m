function Class = AddClass(Class, Name, Trials)
  % Add a trial class to a classification structure.
  %
  % Class = AddClass(Class, Name, Trials)
  %
  % Class structure is defined in opencls.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-06
 

  if ~isempty(Trials)
    nCl = numel(Class);
    Ix = nCl + 1; % Index for new trial class.
    for c = 1:nCl
      if strcmpi(Class(c).Name, Name)
        % Class already exists.
        Ix = c;
        break
      end
    end
    if Ix > nCl
      % Create trial class.
      Class(Ix).Name = Name;
      Class(Ix).Id = max([Class(:).Id]) + 1;
      %     nCl = nCl + 1;
    end
    Class(Ix).Trials = unique([Class(Ix).Trials; ...
      Trials(:)]);
    Class(Ix).Count = numel(Class(Ix).Trials);
  end
end
