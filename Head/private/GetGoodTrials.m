function GoodTrials = GetGoodTrials(Class, nT)
  % Generate list of good trials from Class structure.
  %
  % GoodTrials = GetGoodTrials(Class, nT)
  %
  % For details, see HeadMotionTool.m.
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-06
 
  nCl = numel(Class);
  GoodTrials = (1:nT)';
  for c = 1:nCl
    if strncmpi(Class(c).Name, 'BAD', 3)
      GoodTrials = setdiff(GoodTrials, Class(c).Trials);
    end
  end
end