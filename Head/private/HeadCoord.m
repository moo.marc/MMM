function Fids = HeadCoord(CoilDist)
  % CTF head coil coordinates from distances between pairs of coils.
  %
  % Fids = HeadCoord(CoilDist)
  %
  % For details, see HeadMotionTool.m.
  % 
  %
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-06
  
  Fids = zeros(1, 9); % [Na_x, y, z, LE_x, y, z, RE_x, y, z]
  Fids(1) = sqrt( (CoilDist(1)^2 + CoilDist(3)^2 - CoilDist(2)^2/2)/2 );
  Fids(4) = (CoilDist(3)^2 - CoilDist(1)^2) / (4 * Fids(1));
  Fids(5) = sqrt( CoilDist(2)^2/4 - Fids(4)^2 );
  Fids(7:8) = -Fids(4:5);
end
