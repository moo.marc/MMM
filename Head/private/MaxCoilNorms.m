function MaxNorms = MaxCoilNorms(Locations)
  % Max of the 3 head coil displacements.
  %
  % MaxNorms = MaxCoilNorms(Locations)
  %
  % For details, see HeadMotionTool.m.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-06
 
  Norms = [sqrt(sum(Locations(:, 1:3, :).^2, 2)), ...
    sqrt(sum(Locations(:, 4:6, :).^2, 2)), ...
    sqrt(sum(Locations(:, 7:9, :).^2, 2))];
  
  MaxNorms = max(Norms, [], 2);
end