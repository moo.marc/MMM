function [x, y, z] = spher2cart(r, t, p)
  % Convert from spherical to cartesian coordinates.
  %
  % [x, y, z] = spher2cart(r, t, p)
  %
  % "physics" convention:
  % t (theta) is the zenith (from z axis),
  % p (phi) is the azimuth (in xy-plane from x).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-06

  if nargin == 1 && length(r) == 3
    % Coordinates were passed as vector.
    t = r(2);
    p = r(3);
    r = r(1);
  end

  x = r * sin(t) * cos(p);
  y = r * sin(t) * sin(p);
  z = r * cos(t);
  
  if nargout == 1
    % Output as vector.
    x = [x, y, z];
  end

end