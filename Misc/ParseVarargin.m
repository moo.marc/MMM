function Options = ParseVarargin(VarIn, Validate, EvalDefault, ...
    IgnoreUnknown, OutFormat)
  % Parse optional inputs obtained with varargin, validate and assign defaults values.
  %
  % Options = ParseVarargin(VarIn, [], [], IgnoreUnknown, OutFormat)
  % Options = ParseVarargin(VarIn, Validate, EvalDefault, IgnoreUnknown, OutFormat)
  %
  % If Validate is not provided, VarIn is minimally parsed into a single
  % structure (default) or cell array (if OutFormat is 'Cell') and returned
  % in Options.  Otherwise when doing validation, options are assigned as
  % individual variables into the calling workspace and Options is not
  % needed.  If Validation is done, variables that are not in EvalDefault
  % must be created prior to calling this function.  Furthermore it is
  % recommended to create all optional variables, typically by assigning
  % default values, before calling this function, for easier code
  % maintenance.
  % 
  % VarIn: varargin from the calling function.  The first element can be a
  % structure with fields corresponding to optional variables, followed by
  % key-value pairs, or only key-value pairs.  If a variable repeats, the
  % last occurrence only is kept.
  %
  % Validate: Structure of variable validation functions, where field names
  % are all optional variable names, and values are function handles for
  % validating input values for each variable.  If validation is not
  % required, simply use @(x) true.  Variables with evaluated defaults must
  % be after other variables they depend on.  All optional variables must
  % be in this structure (as fields).  
  %
  % EvalDefault: Structure where field names are optional variable names,
  % and values are strings to be evaluated in the calling function
  % workspace.  This is only done if Validate is present and contains those
  % variables as well.
  %
  % IgnoreUnknown: If false (default) and Validate is provided, additional
  % variables passed as inputs (in VarIn) not found in Validate will
  % generate an error.  If true, they will be ignored, and returned in
  % Options if present.
  %
  % OutFormat can be 'Cell' or 'Struct' (default), and determines the
  % class of the output variable Options.  If 'Cell', it is a N by 2 cell
  % array, with variable names in the first column and values in the
  % second.  If 'Struct', the variable names are the field names.
  % 
  % 
  % Useful functions to use in conjunction with this one:
  %   VarName = @(x) inputname(1); % Easier to maintain code.
  %   % Use as VarName(X) instead of 'X'.
  %   InlineIf = @(varargin) varargin{2 + ~varargin{1}}; % In EvalDefault.
  %   % Use as InlineIf(Condition, TrueValue, FalseValue)
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-11-12
  
  if nargin < 5 || lower(OutFormat(1)) ~= 'c'
    DoCell = false;
  else
    DoCell = true;
  end
  if nargin < 4 || isempty(IgnoreUnknown)
    IgnoreUnknown = false;
  end
  if nargin < 3
    EvalDefault = struct();
  end
  % if nargin < 2 lower.
  
  % -------------------------------------------------------------------
  % Deal with input types (struct, key-value pairs).
  if ~isempty(VarIn) && isstruct(VarIn{1})
    Options = VarIn{1};
    VarIn(1) = [];
  else
    Options = struct();
  end
  if ~isempty(VarIn)
    if mod(numel(VarIn), 2)
      error(['Optional arguments must be given either in a structure, ' ...
        'with key-value pairs, or both with the structure given before key-value pairs.']);
    end
    VarIn = reshape(VarIn, 2, [])';
    if ~iscellstr(VarIn(:, 1))
      error(['Optional arguments must be given either in a structure, ' ...
        'with key-value pairs, or both with the structure given before key-value pairs.']);
    end
    for f = 1:size(VarIn, 1)
      %       if isfield(Options, VarIn{f, 1})
      Options.(VarIn{f, 1}) = VarIn{f, 2};
    end
  end
  % We now have a single structure.

  if nargin < 2 || isempty(Validate)
    if nargout < 1
      warning('Nothing to do, please see help ParseVarargin.');
    elseif DoCell
      Options = [fieldnames(Options), struct2cell(Options)];
    end
    return;
  end
  
  % -------------------------------------------------------------------
  % Match and validate inputs.
    
  % Match inputs to expected variables.
  Variables = fieldnames(Validate);
  [VarMatch, InMatch] = MatchStrings(Variables, fieldnames(Options));
  if ~IgnoreUnknown && any(~InMatch)
    Options = fieldnames(Options);
    error('Unrecognized option(s): %s', sprintf('%s ', Options{~InMatch})); 
  end
  
  % Assign non-default values, or evaluate defaults needing it.
  %   AssignHere = @(x,y) assignin('caller', x, y); % assignin('caller' works in the calling function's workspace, not its own.
  for v = 1:size(Variables, 1)
    if VarMatch(v)
      % Validate and keep the provided value.
      if Validate.(Variables{v})(Options.(Variables{v}))
        assignin('caller', Variables{v}, Options.(Variables{v}));
      else
        error('Invalid value for %s.', Variables{v});
      end
    elseif isfield(EvalDefault, Variables{v})
      % Anonymous functions use variable values when they are created, so
      % must use eval.  It's the only way I know to delay evaluation.
      %       assignin('caller', Variables{v}, EvalDefault.(Variables{v})());
      assignin('caller', Variables{v}, evalin('caller', EvalDefault.(Variables{v})));
    end
  end

  
  if nargout > 0 && DoCell
    Options = [fieldnames(Options), struct2cell(Options)];
  end
  
end

  %   % ----------------------------------- Done directly with cell.
  %   % Deal with input types (struct, key-value pairs).
  %   Options = cell(0, 1); % If we put {} instead, Options(:, 1) gives error.
  %   if isstruct(VarIn{1})
  %     S = VarIn{1};
  %     VarIn(1) = [];
  %     F = fieldnames(S);
  %     Options = cell(numel(F), 2);
  %     for f = 1:numel(F)
  %       Options{f, 1} = F{f};
  %       Options{f, 2} = S.(F{f});
  %     end
  %     clear S F
  %   end
  %   if mod(numel(VarIn), 2)
  %     error(['Optional arguments must be given either in a structure, ' ...
  %       'with key-value pairs, or both with the structure given before key-value pairs.']);
  %   end
  %   Options = [Options; reshape(VarIn, 2, [])'];
  %   if ~iscellstr(Options(:, 1))
  %     error(['Optional arguments must be given either in a structure, ' ...
  %       'with key-value pairs, or both with the structure given before key-value pairs.']);
  %   end
  %
  %   % Match inputs to expected variables.
  %   Variables = fieldnames(Validate);
  %   [VarMatch, InMatch] = MatchStrings(Variables, Options(:, 1));
  %   if any(~InMatch)
  %     error('Unrecognized option(s): %s', sprintf('%s ', Options{~InMatch, 1})); %#ok<SPERR>
  %   end
  %
  %   % Assign non-default values, or evaluate defaults needing it. (This
  %   % can be in another function with evalin.)
  %   %   AssignHere = @(x,y) assignin('caller', x, y); % assignin('caller' works in the calling function's workspace, not its own.
  %   for v = 1:size(Variables, 1)
  %     if VarMatch(v)
  %       % Validate and keep the provided value.
  %       if isfield(Validate, Variables{v})
  %         if Validate.(Variables{v})(Options{VarMatch(v), 2})
  %           assignin('caller', Variables{v}, Options{VarMatch(v), 2});
  %         else
  %           error('Invalid value for %s.', Variables{v, 1});
  %         end
  %       else % No validation required.
  %         assignin('caller', Variables{v}, Options{VarMatch(v), 2});
  %       end
  %     elseif isfield(EvalDefault, Variables{v})
  %       assignin('caller', Variables{v}, evalin('caller', EvalDefault.(Variables{v})));
  %     end
  %   end
  

