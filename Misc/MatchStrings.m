function [RightMatches, LeftMatches, Status] = MatchStrings(Left, Right, Validate)
  % Return linear indices of (last) case insensitive matches between two cell arrays of strings.
  %
  % [RightMatches, LeftMatches, Status] = MatchStrings(Left, Right, Validate)
  %
  % Maybe the names are confusing, but RightMatches is size(Left), it is
  % the matching indices in Right for all elements in Left.  Unmatched
  % elements have the index 0.
  %
  % Validation is not done by default.  If true, the function will return
  % errors if there are duplicates in either lists, or if there are
  % elements from Left unmatched in Right.  If Validate is > 1, it requires
  % a perfect match of both lists, including ordering, otherwise returns
  % errors.  When Validate is true (but not > 1), Status will be false if
  % the lists don't match perfectly, i.e. if there are extra elements in
  % Right, or if the order is different between the lists.  In this case,
  % to reorder Right to match Left (and discard any extra Right elements),
  % use: Right = Right(LeftMatches(LeftMatches>0)).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-10-22
  
  % Not implemented options:
  % Strict can be 0 (default), 1 which returns an error if some strings in
  % Right are not found in Left, or 2 which returns an error for any string
  % on both sides without a match.  Thus when matching with a subset, Right
  % should typically be the subset of Left.
  % Could add "All" option that would return multiple matches, but no use
  % for it now.  For our need (optional argument parsing), it makes sense
  % to only keep the last match.
  
  % Select channels, if using groups or wildcards, or if subset.
  % ChannelList = ft_channelselection(ChannelList, DataLabels);
  
  if nargin < 2 || ~iscellstr(Left) || ~iscellstr(Right)
    error('Expecting Left and Right cell arrays of strings.');
  end
  
  if nargin < 3 % || isempty(Validate)
    Validate = false;
  end
  if nargout > 2 && ~Validate
    % Won't get validated.
    Status = [];
  elseif Validate
    Status = true;
  end
  
  RightMatches = zeros(size(Left));
  if isempty(Right)
    LeftMatches = [];
    if Validate && ~isempty(Left)
      error('Empty Right list.');
    end
    return;
  end
  
  % According to Fieldtrip's match_str, converting to unique numbers and
  % doing number comparisons would be faster.  lower() needed to make it
  % case insensitive.
  [~, ~, L] = unique(lower([Left(:); Right(:)]));
  nL = numel(Left);
  R = L((nL+1):end);
  nR = numel(R);
  if Validate
    if nL ~= nR
      if Validate > 1
        error('Number of channels don''t match.');
      else
        Status = false;
      end
    end
    if numel(unique(L(:))) < nL || ... % duplicates in Left
        numel(unique(R(:))) < nR % duplicates in Right
      error('Duplicate channels.');
    end
  end
  L = L(1:nL);
  
  for i = 1:nL
    %     M = find(strcmpi(Left(i), Right(:)), 1, 'last');
    M = find(L(i) == R(:), 1, 'last');
    if M % ~isempty(M)
      RightMatches(i) = M;
    end
  end
  if Validate && any(~RightMatches(:)) % some Left unmatched.
    error('Channel mismatch (Left).');
  end
  
  if nargout > 1 || Validate
    % Repeat for left matches.
    LeftMatches = zeros(size(R));
    for i = 1:nR
      %       M = find(strcmpi(Right(i), Left(:)), 1, 'last');
      M = find(R(i) == L(:), 1, 'last');
      if M % ~isempty(M)
        LeftMatches(i) = M;
      end
    end
  end
  
  if Validate > 1 && any(~LeftMatches(:)) % some Right unmatched.
    error('Channel mismatch (Right).');
  end
  
  if Validate && Status
    % At this point, we know RightMatches, and LeftMatches is
    % the order to sort Right to match Left.
    if any((1:nL)' ~= LeftMatches(LeftMatches(:)>0))
      if Validate > 1 % (if Status were false, Validate could not be > 1)
        % Require matched ordering.
        error('Channel order mismatch.');
      else
        % Return flag.
        Status = false;
        %   Right = Right(LeftMatches(LeftMatches>0));
      end
    end
  end
  
end
