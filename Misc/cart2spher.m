function [r, t, p] = cart2spher(x, y, z)
  % Convert from cartesian to spherical coordinates.
  %
  % [r, t, p] = cart2spher(x, y, z)
  %
  % "physics" convention:
  % t (theta) is the zenith (from z axis),
  % p (phi) is the azimuth (in xy-plane from x).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-06

  if nargin == 1 && size(x, 2) == 3
    % Coordinates were passed as vector.
    y = x(:, 2);
    z = x(:, 3);
    x = x(:, 1);
  elseif nargin < 3 || size(x, 2) > 1 || size(y, 2) > 1 || size(z, 2) > 1
    error('Inputs should be column vectors, or a single matrix with 3 columns.');
  end
  
  r = sqrt(x.^2 + y.^2 + z.^2);
  % Handle zero length case.
  n = size(x, 1);
  t = zeros(n, 1);
  p = zeros(n, 1);
  iOk = r ~= 0;
  %   if r == 0
  %     t = 0;
  %     p = 0;
  %   else
  t(iOk) = acos(z(iOk)./r(iOk)); % Between 0 and pi.
  p(iOk) = atan2(y(iOk), x(iOk));
  %     % Handle special azimuth cases.
  %     if x == 0
  %       if y > 0
  %         p = pi/2;
  %       else
  %         p = 3*pi/2;
  %       end
  %     else
  %       p = atan(y/x); % Between -pi/2 and pi/2, need to correct for 0 to 2*pi.
  %       if x < 0
  %         p = p + pi;
  %       elseif y < 0
  %         p = p + 2*pi;
  %       end
  %     end
  %   end
  
  if nargout == 1
    % Output as vector.
    r = [r, t, p];
  end
end
