function AddCopyright(File, iF)
  % Interactively replace author and date by copyright and license info.
  %
  % AddCopyright(File, iF)
  %
  % File can be a single m-file, a directory (which will be searched
  % recursively for all m-files), or a cell array of file names. 
  %
  % Looks within each file for the first line with the author name (hard
  % coded below), possibly followed by a date on the same or next line, and
  % replaces them with a copyright and license blurb. But if the copyright
  % symbol is present, replaces the full existing blurb instead.
  %
  % The focus changes intelligently, such that one can press space
  % repeatedly to activate the buttons for rapid processing. 
  %
  % (Suggestion: use Agent Ransack or another file searching tool to find
  % specific text and export a list of files that can then be pasted
  % directly in the Matlab workspace.)
  %
  % iF: index for where to start within the list of files, when continuing
  % through an already partially processed list.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2018-02-07
  
  Author = 'Marc Lalancette';
  Year = 2018;
  DateStr = '';
  
  SearchStr = Author;
  
  % New copyright text.
  NewBlurb = [...
    '  %% \n', ...
    '  %% � Copyright %d %s\n', ...
    '  %% The Hospital for Sick Children, Toronto, Canada\n', ...
    '  %% \n', ...
    '  %% This file is part of a free repository of Matlab tools for MEG \n', ...
    '  %% data processing and analysis <https://gitlab.com/moo.marc/MMM>.\n', ...
    '  %% You can redistribute it and/or modify it under the terms of the GNU\n', ...
    '  %% General Public License as published by the Free Software Foundation,\n', ...
    '  %% either version 3 of the License, or (at your option) a later version.\n', ...
    '  %% \n', ...
    '  %% This program is distributed WITHOUT ANY WARRANTY. \n', ...
    '  %% See the LICENSE file, or <http://www.gnu.org/licenses/> for details.\n' ...
    '  %% \n', ...
    '  %% %s\n' ]; % Space for date string.

  % Allow already built list of files, e.g. with external search tool.
  if ~iscellstr(File)
    % ListFiles
    [Path, Name, Ext] = fileparts(File);
    if strcmp(Ext, '.m')
      % Just do that one file.
      if isempty(Path) % Presume in current folder.
        File = fullfile('.', [Name, Ext]);
      end
      File = ListFiles(File); % Will put it in a cell and verify it exists.
    else % Presume folder name (and no dot in last folder?)
      File = [ListFiles(fullfile(Path, Name, filesep, '*.m'), 'f'); ...
        ListFiles(fullfile(Path, Name, filesep, '*', filesep, '*.m'), 'f')];
    end
  end
  
  nF = numel(File);
  if nargin < 2 || isempty(iF)
    iF = 0;
  end
  nReplaced = 0; 
  Edit = [];
  
  % GUI with 2 buttons: Insert and Next File
  ScreenSize = get(0,'ScreenSize');
  D = dialog('Position', [ScreenSize(3:4)/2 - [110, 35], 220, 70], ...
    'Name', 'AddCopyright', 'WindowStyle', 'normal');
  Counter = uicontrol('Parent', D, 'Style', 'Text', ...
    'Position', [90 47 50 20], 'String', sprintf('%d / %d', iF, nF));
  InsertButton = uicontrol('Parent', D, 'Position', [20 20 80 25], ...
    'String', 'Insert �', 'Callback', @Insert);
  NextFileButton = uicontrol('Parent', D, 'Position', [120 20 80 25], ...
    'String', 'Next File', 'Callback', @NextFile);

  % To delete selection: simulate delete key
  % Initialize the java engine
  import java.awt.Robot;
  import java.awt.event.InputEvent;
  % Create a robot (?)
  Towaga = Robot();
  
  function NextFile(~, ~)
    if iF > 0 && ~isempty(Edit)
      Edit.close; 
    end
    if iF == nF
      % Close dialog figure.  There is a CloseRequestFcn that's already
      % set (closereq).
      close(D);
      return
    end
    iF = iF + 1;
    Counter.String = sprintf('%d / %d', iF, nF);
    % Open (in editor) and see if already exists
    %   open(File) or
    %   edit(File)
    % Don't call matlab.desktop.editor.openDocument because it
    % does not prompt for files that don't exist.
    %   Edit = matlab.desktop.editor.openDocument(File{iF});
    Edit = matlab.desktop.editor.Document.openEditor(File{iF});
    if isempty(Edit)
      error('Error opening file %s.', File{iF});
    end
    Code = strsplit(Edit.Text, '\n');
    if ~isempty(strfind(Edit.Text, '�'))
      iLine = find(~cellfun(@isempty, strfind(Code, '�')), 1, 'First');
      if ~isempty(iLine)
        iDateLine = iLine + 12;
        iLine = iLine - 1;
      end
    else
      iLine = find(~cellfun(@isempty, strfind(Code, SearchStr)), 1, 'First');
      if ~isempty(iLine)
        % Check if next line is also a comment, should contain date.
        iDateLine = iLine + strncmp(strtrim(Code{iLine+1}), '%', 1);
      end
    end
    if ~isempty(iLine)
      % Find date string.  I use dashes, so look for at least one digit
      % followed by digits and dashes, to allow for yyyy-mm and yyyy-mm-dd.
      DateStr = regexp(Code{iDateLine}, '[0-9][0-9-]+' , 'Match', 'Once');
      % Select Name and Date line(s).
      Edit.Selection = [iLine, 1, iDateLine+1, 1];
      %     Each position is represented by its line number and position within
      %     that line (both 1-based). The end position is exclusive.
      % Probably ready to insert so give focus to insert button.
      uicontrol(InsertButton);
    else
      DateStr = datestr(datetime('now'), 'yyyy-mm-dd');
      % Probably have to manually select plase to insert, so give focus to
      % editor.
      Edit.makeActive;
    end
  end
  
  function Insert(~, ~)

    % To delete selection: simulate delete key
    Edit.makeActive; % Keypress must be captured in editor!
    import java.awt.event.KeyEvent
    Towaga.keyPress(KeyEvent.VK_DELETE);
    Towaga.keyRelease(KeyEvent.VK_DELETE);
    Towaga.waitForIdle();
    
    Edit.insertTextAtPositionInLine(sprintf(NewBlurb, Year, Author, DateStr), ...
      Edit.Selection(1), 1)
    Edit.save
    nReplaced = nReplaced + 1;
    
    % Probably done with this file, so focus on next button.
    uicontrol(NextFileButton);

  end
  
end