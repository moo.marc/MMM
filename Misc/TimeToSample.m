function [Sample, Trial] = TimeToSample(Time, SampleRate, ...
    nPreTrig, nSamplesPerTrial, Trial)
  % Convert time in seconds to samples indexed from 1 (Matlab style).
  % 
  % [Sample, Trial] = TimeToSample(Time, SampleRate, ...
  %     nPreTrig, nSamplesPerTrial, Trial)
  %
  % This function will return sample "trial indices" and trial numbers if
  % the number of samples per trial is given.  Trial numbers can be given
  % as input, but this only affects the Trial output; they get added to the
  % computed trial numbers from times past the trial duration.
  % 
  % Verified with DataEditor: first sample is time 0.0000 s with 0 s
  % pre-trigter time.  Also continuous data is possible with non-zero pre
  % trigger time and multiple trials; it simply changes the times.  
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-08-14
  
  if ~exist('nPreTrig', 'var') || isempty(nPreTrig)
    nPreTrig = 0;
  end
  if ~exist('SampleRate', 'var') || isempty(SampleRate)
    SampleRate = 600;
  end
  if ~exist('nSamplesPerTrial', 'var') || isempty(nSamplesPerTrial)
    nSamplesPerTrial = inf;
  end
  if ~exist('Trial', 'var') || isempty(Trial)
    Trial = ones(size(Time));
  end
  
  % Continuous (or trial) indices.
  Sample = round(Time * SampleRate) + 1 + nPreTrig;
  % Calculate trials, possibly added to provided ones.
  Trial = Trial + floor((Sample - 1) ./ nSamplesPerTrial);
  % If some were continuous or over trial duration, adjust.
  % Strangely, mod(a, inf) = NaN instead of a.
  if isfinite(nSamplesPerTrial)
    Sample = 1 + mod((Sample - 1), nSamplesPerTrial);
  end
end