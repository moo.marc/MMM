function [Time, Trial, Sample] = SampleToTime(Sample, SampleRate, ...
    nPreTrig, nSamplesPerTrial, Trial)
  % Convert samples indexed from 1 (Matlab style) to dataset times in seconds.
  % 
  % [Time, Trial, Sample] = SampleToTime(Sample, SampleRate, ...
  %   nPreTrig, nSamplesPerTrial, Trial)
  %
  % This function works with multiple trials.  In that case, provide the
  % number of samples per trial, and possibly trial numbers.  If trial
  % numbers are absent, trial 1 is assumed.  If samples go beyond
  % nSamplesPerTrial, trial numbers are calculated based on continuous
  % sample indices (trials joined into one).  Thus this function can also
  % be used to convert continuous indices into trials and "trial indices".
  % 
  % Verified with DataEditor: first sample is time 0.0000 s with 0 s
  % pre-trigter time.  Also continuous data is possible with non-zero pre
  % trigger time and multiple trials; it simply changes the times.  
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-08-14
  
  if ~exist('nPreTrig', 'var') || isempty(nPreTrig)
    nPreTrig = 0;
  end
  if ~exist('SampleRate', 'var') || isempty(SampleRate)
    SampleRate = 600;
  end
  if ~exist('Trial', 'var') || isempty(Trial)
    Trial = ones(size(Sample));
  end
  if ~exist('nSamplesPerTrial', 'var') || isempty(nSamplesPerTrial)
    if max(Trial(:)) > 1
      error('Number of samples per trial required for multiple trials.');
    end
    nSamplesPerTrial = inf;
  end
  
  Trial = Trial + floor((Sample - 1) ./ nSamplesPerTrial);
  % Strangely, mod(a, inf) = NaN instead of a.
  if isfinite(nSamplesPerTrial)
    Sample = 1 + mod((Sample - 1), nSamplesPerTrial);
  end
  Time = (Sample - 1 - nPreTrig) ./ SampleRate;
end
