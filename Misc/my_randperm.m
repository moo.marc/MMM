function Perm = my_randperm(n, k)
  % Return k random distinct integers from the set 1:n.
  %
  % Perm = my_randperm(n, k)
  % 
  % This is for older Matlab where randperm(n,k) doesn't exist.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-01-20
  
  % If n is large and k is small, it takes too much memory to use the old
  % randperm(n).
  if k < n/2
    if verLessThan('matlab', '7.7.0')
      Perm = unique(ceil(rand(1, k) * n));
    else
      Perm = unique(randi(n, [1, k]));
    end
    % Replace duplicates that were removed by unique().
    while numel(Perm) < k
      Perm = unique([Perm, ceil(rand(1, k - numel(Perm)) * n)]);
    end
  else
    % k comparable to n, just truncate a full permutation.
    %     Perm = randperm(n);
    [unused, Perm] = sort(rand(1,n)); %#ok<ASGLU> % This is the old randperm(n).
    Perm(k+1:end) = [];
  end
end