function B = ForwardMagneticDipole(SourceMoment, SourceLoc, ...
    SensorLoc, SensorOrient)
  % Magnetic dipole external field.
  %
  % B = ForwardMagneticDipole(SourceMoment, SourceLoc, SensorLoc, SensorOrient)
  %
  % All vectors should be given as rows of 3 components.  Multiple rows can
  % be given to compute multiple source locations or multiple sensors, but
  % not both at once.
  %
  % Locations are assumed to be given in cm.
  %
  % Assuming moment is in Am^2, returns field in T.  One can also give a
  % SourceMoment with unit norm and then the returned forward solution is
  % in units of T/(Am^2).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2016-08-26

  % Moment is assumed in Am^2 (current loop enclosing an area), and
  % locations in cm.  So we need a factor for the magnetic field: [mu] =
  % Tm/A, [B] = T, so need (100cm/m)^3
  UnitConversion = 100^3;

  if size(SourceLoc, 2) ~= 3  || size(SourceMoment, 2) ~= 3 || ...
      size(SensorLoc, 2) ~= 3 || size(SensorOrient, 2) ~= 3
    error('Location vector(s) (source, sensor or sphere) not properly transposed or wrong length.')
  end
  
  if size(SourceLoc, 1) > 1
    nL = size(SourceLoc, 1);
    % Allow as many moments, or single moment, but only one sensor.
    if size(SourceMoment, 1) > 1 && size(SourceMoment, 1) ~= nL
      error('Multiple locations and moments but numbers don''t match.');
    end
    if size(SensorLoc, 1) > 1
      error('Multiple locations and multiple sensors not allowed.');
    end
  else
    nL = 1;
  end
  if size(SensorLoc, 1) > 1
    nS = size(SensorLoc, 1);
    % Allow as many orientations, or single orientation, but only one location.
    if size(SensorOrient, 1) > 1 && size(SensorOrient, 1) ~= nS
      error('Multiple sensor locations and orientations but numbers don''t match.');
    end
    if size(SourceLoc, 1) > 1 || size(SourceMoment, 1) > 1
      error('Multiple locations and multiple sensors not allowed.');
    end
  else
    nS = 1;
  end
    
  if nL > 1 || nS > 1
    Rv = bsxfun(@minus, SensorLoc, SourceLoc);
    R = sqrt(sum(Rv.^2, 2));
    Ro = bsxfun(@rdivide, Rv, R);
    
    % First dimension: Either nL or nS.
    B = UnitConversion * (1e-7) * ... % mu0/4*pi = 10^-7 Wb/Am
      sum(bsxfun(@times, ...
        bsxfun(@rdivide, ...
          bsxfun(@minus, ...
            3 * bsxfun(@times, Ro, sum(bsxfun(@times, SourceMoment, Ro), 2)), ...
            SourceMoment), ...
          R.^3), ...
        SensorOrient), 2);
    
  else % kept simplified code for single vectors.
    % B(s) = mu0/4*pi (3 * Q'*ro * ro - Q)/r^3  where r = s - rQ is from
    % moment to measured position, ro is unit orientation vector.
    
    Rv = SensorLoc - SourceLoc; % a in Sarvas' notation.
    R = norm(Rv);
    Ro = Rv / R;
    
    B = UnitConversion * (1e-7) * ... % mu0/4*pi = 10^-7 Wb/Am = Tm/A
      ( (3 * SourceMoment * Ro' * Ro - SourceMoment) / R^3 * SensorOrient' );
    % (cm/m)^3 * Tm/A * Am^2/cm^3 = Teslas.
    % Without the unit conversion, we'd have 1e6 T.
  end
end
