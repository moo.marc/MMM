function [Forward, Orient] = ForwardSVD(Forward, Rank, Inside)
  % Perform singular value decomposition on MEG forward solution vectors.
  %
  % [Forward, Orient] = ForwardSVD(Forward, Rank, Inside)
  %
  % Forward as input has size (num sensors, 3, num locations), and as
  % output 3 is replaced by Rank.  Orient is (3, 3, num locations).  The
  % decomposition returns the vectors multiplied by their norm: If F(:,:,v)
  % = L*N*O', then the output Forward(:,:,v) = L*N and Orient(:,:,v) = O,
  % such that the columns of O are the physical orientations corresponding
  % to the new forward solution (lead field) vectors.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-09-16

  if nargin < 2 || isempty(Rank) || Rank > 3 || Rank < 1
    Rank = 3;
  end
  nLoc = size(Forward, 3);
  if nargin < 3 || isempty(Inside)
    Inside = true(nLoc, 1);
  end
  %   nSens = size(Forward, 1);
  
  Orient = zeros(3, 3, nLoc);
  for l = find(Inside)'
    [Forward(:, :, l), N, Orient(:, :, l)] = svd(Forward(:, :, l), 'econ');
    % L = LL*N*O' <=> L*O = LL*N, so columns of O are orientations.
    
    Forward(:, :, l) = Forward(:, :, l) * N;
    
    if Rank == 3
      % CAREFUL! O matrices are randomly right or left handed (positive or
      % negative basis orientation), i.e. cross(X, Y) may = + or - Z!
      % Change so all are right handed.
      if Orient(:, 3, l)' * cross(Orient(:, 1, l), Orient(:, 2, l)) < 0
        Orient(:, 3, l) = -Orient(:, 3, l);
        Forward(:, 3, l) = -Forward(:, 3, l);
      end
    end
  end
  
  % Remove extra dimensions.
  Forward(:, Rank+1:end, :) = [];
  Orient(:, Rank+1:end, :) = [];
  
end

