function B = ForwardSphere(SourceMoment, SourceLoc, ...
    SphereLoc, SensorLoc, SensorOrient, Units)
  % Spherical conductor forward solution for MEG.
  %
  % B = ForwardSphere(SourceMoment, SourceLoc, SphereLoc, SensorLoc, SensorOrient)
  %
  % All vectors should be given as rows of 3 components.  Multiple rows can
  % be given to compute multiple source locations or multiple sensors, but
  % not both at once.
  %
  % Sarvas equation (F simplified a bit).
  % Locations are assumed to be given in cm, unless Units is specified as'm'.
  %
  % Assuming moment is in Am, returns field in T.  One can also give a
  % SourceMoment with unit norm and then the returned forward solution is
  % in units of T/(Am).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-07-03

  % In Sarvas' notation, the input arguments in order are Q, rQ, 
  % the sphere center (which is the origin of his coordinate system) and r.

  if nargin > 5 && strcmpi(Units, 'm')
      UnitConversion = 1;
  else
      % Moment is assumed in Am, and locations in cm.  So we need a factor for
      % the magnetic field: [mu] = Wb/Am, [B] = T=Wb/m^2, so need (100cm/m)^2
      UnitConversion = 100^2;
  end

  if size(SourceLoc, 2) ~= 3  || size(SourceMoment, 2) ~= 3 || size(SphereLoc, 2) ~= 3 || ...
      size(SensorLoc, 2) ~= 3 || size(SensorOrient, 2) ~= 3
    error('Location vector(s) (source, sensor or sphere) not properly transposed or wrong length.')
  end
  
  if size(SourceLoc, 1) > 1
    nL = size(SourceLoc, 1);
    % Allow as many moments, or single moment, but only one sensor.
    if size(SourceMoment, 1) > 1 && size(SourceMoment, 1) ~= nL
      error('Multiple locations and moments but numbers don''t match.');
    end
    if size(SphereLoc, 1) > 1 || size(SensorLoc, 1) > 1
      error('Multiple locations and multiple sensors not allowed.');
    end
  else
    nL = 1;
  end
  if size(SensorLoc, 1) > 1
    nS = size(SensorLoc, 1);
    % Allow as many spheres, or single sphere, but only one location.
    if size(SphereLoc, 1) > 1 && size(SphereLoc, 1) ~= nS
      error('Multiple sensors and spheres but numbers don''t match.');
    end
    if size(SensorOrient, 1) > 1 && size(SensorOrient, 1) ~= nS
      error('Multiple sensor locations and orientations but numbers don''t match.');
    end
    if size(SourceLoc, 1) > 1 || size(SourceMoment, 1) > 1
      error('Multiple locations and multiple sensors not allowed.');
    end
  else
    nS = 1;
    if size(SphereLoc, 1) > 1
      error('Multiple spheres, but only 1 sensor.');
    end
  end
    
  if nL > 1 || nS > 1
    % First dimension: Either Rv, R are nL and rv, r are 1; or all are nS.
    Rv = bsxfun(@minus, SensorLoc, SourceLoc); % a in Sarvas' notation.
    rv = bsxfun(@minus, SensorLoc, SphereLoc);
    R = sqrt(sum(Rv.^2, 2));
    r = sqrt(sum(rv.^2, 2));
    
    Rvrv = sum(bsxfun(@times, Rv, rv), 2);
    % First dimension: Either nL or nS.
    F = (Rvrv + r .* R) .* R; % cm^3
    GradF = bsxfun(@times, (Rvrv./R + 2*r + R), Rv) + ...
      bsxfun(@times, ((R./r + 1) .* R), rv); % cm^2
    
    % QxrQ = cross(SourceMoment, SourceLoc - SphereLoc);
    % Only place we need rQ not as r-rQ. Doesn't depend on the sensor
    % location so is outside the coil loop in the C code.
    % The call to cross and dot functions are slow.  Write down formulae
    % directly.
    % First dimension: 1, nS or nL.
    SourceSphLoc = bsxfun(@minus, SourceLoc, SphereLoc);
    QxrQ = bsxfun(@times, SourceMoment(:, [2, 3, 1]), SourceSphLoc(:, [3, 1, 2])) - ...
      bsxfun(@times, SourceMoment(:, [3, 1, 2]), SourceSphLoc(:, [2, 3, 1])); % Am*cm
    
    B = UnitConversion * (1e-7) * ... % mu0/4*pi = 10^-7 Wb/Am
      sum(bsxfun(@times, ...
        ( bsxfun(@rdivide, QxrQ, F) - ...
        bsxfun(@times, sum(bsxfun(@times, QxrQ, rv), 2) ./ F.^2, GradF) ), ...
        SensorOrient), 2);
      %       ( (QxrQ/F - (QxrQ * rv')/F^2 * GradF) * SensorOrient' );
    % (cm/m)^2 * Wb/Am * Am*cm/cm^3 = (cm/m)^2 * Wb/cm^2 = Teslas.
    % Without the unit conversion, we'd have Wb/cm^2 * (100cm/m)^2 = 1e4 T
    % (or 1e4 T/Am = 1e2 T/(A*cm)) OK!
    
  else % kept simplified code for single vectors.
    Rv = SensorLoc - SourceLoc; % a in Sarvas' notation.
    rv = SensorLoc - SphereLoc;
    R = norm(Rv);
    r = norm(rv);
    
    F = (Rv * rv' + r * R) * R; % cm^3
    GradF = (Rv * rv'/R + 2*r + R) * Rv + ((R/r + 1) * R) * rv; % cm^2
    
    % QxrQ = cross(SourceMoment, SourceLoc - SphereLoc);
    % Only place we need rQ not as r-rQ. Doesn't depend on the sensor
    % location so is outside the coil loop in the C code.
    % The call to cross and dot functions are slow.  Write down formulae
    % directly.
    SourceSphLoc = SourceLoc - SphereLoc;
    QxrQ = [SourceMoment(2) * SourceSphLoc(3) - SourceMoment(3) * SourceSphLoc(2), ...
      -(SourceMoment(1) * SourceSphLoc(3) - SourceMoment(3) * SourceSphLoc(1)), ...
      SourceMoment(1) * SourceSphLoc(2) - SourceMoment(2) * SourceSphLoc(1)]; % Am*cm
    %   QxrQ = SourceMoment(:, [2, 3, 1]) .* SourceSphLoc(:, [3, 1, 2]) - ...
    %     SourceMoment(:, [3, 1, 2]) .* SourceSphLoc(:, [2, 3, 1]);
    
    B = UnitConversion * (1e-7) * ... % mu0/4*pi = 10^-7 Wb/Am
      ( (QxrQ/F - (QxrQ * rv')/F^2 * GradF) * SensorOrient' );
    % (cm/m)^2 * Wb/Am * Am*cm/cm^3 = (cm/m)^2 * Wb/cm^2 = Teslas.
    % Without the unit conversion, we'd have Wb/cm^2 * (100cm/m)^2 = 1e4 T
    % (or 1e4 T/Am = 1e2 T/(A*cm)) OK!
  end
end
