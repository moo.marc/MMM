function Forward = ForwardSpheres(Dataset, Grid, SpheresFile, ForwardFile, Overwrite)
  % Multiple sphere MEG forward solution at given locations (and 3 orientations at each).
  %
  % Forward = ForwardSpheres(Dataset, Grid, SpheresFile, ForwardFile, Overwrite)
  %
  % Compute the forward solution and possibly save in a .mat file.
  %
  % Units: positions in cm.  This is the case for .hdm files, and grid
  % locations can be given in 'cm', 'm', or 'mm', and specified in the
  % Grid.unit field.  Lead field is returned in fT per nAm = 1e-6 T/Am.
  %
  % Output argument Forward has size: (number of MEG sensors, 3
  % orientations, number of locations).
  %
  % Grid is based on Fieldtrip's "grid" structure, with fields: pos (nx3),
  % inside (nx1 logical), unit (string).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2015-09-15
  
  % Moment could be made an option, but easy to multiply after.
  Moment = 1e-9;
  
  if nargin < 5 || isempty(Overwrite)
    Overwrite = false;
  end
  if nargin < 4 || isempty(ForwardFile)
    DoSave = false;
  elseif ~Overwrite && exist(ForwardFile, 'file')
    % Nothing to do.
    fprintf('Found forward solution in existing file: %s.', ForwardFile);
    load(ForwardFile);
    return;
  else
    DoSave = true;
  end
  if nargin < 3
    error('Not enough input arguments: Dataset, Grid and SpheresFile expected.');
  end
  
  % ForwardSphere expects all positions in 'cm'.
  if ~isfield(Grid, 'unit')
    warning('Grid.unit missing, assuming ''cm''.');
    Grid.unit = 'cm';
  end
  switch Grid.unit
    case 'cm'
      Loc = Grid.pos;
    case 'm'
      Loc = Grid.pos * 100;
    case 'mm'
      Loc = Grid.pos * 0.1;
    otherwise
      error('Unknown Grid.unit.');
  end
  % Can get non-negligible errors if single precision locations, which can
  % be what we have when building the grid from gifti sufrace file.  So
  % convert.
  Loc = double(Loc);
  
  % Get multi-sphere head model.
  [Sphere, SphChanNames] = openhdm(SpheresFile);
  
  [~, Info] = GetMEGData(Dataset, true, true, 'fT', [], true); % InfoOnly, Geometry, -, -, IncludeRefs
  % Have to manually check which channels are sensors and references.
  % Refs should be first.
  %     nS = Info.nSens; % sum([Info.ChanNames{:}(1)] == 'M');
  %     nRefs = Info.nSens - nS;
  % Make sure reference channels are first.
  if Info.nRefs > 0 && Info.ChanNames{1}(1) == 'M'
    error('Expecting reference channels first.');
  end
  
  % Match channels.
  [Found, Where] = ismember(Info.ChanNames, SphChanNames);
  if any(~Found)
    error('Channel mismatch between dataset and spheres file.');
  end
  Sphere = Sphere(Where);
  
  % Get x, y, z oriented leadfields.  Might SVD later.
  nO = 3;
  Orient = eye(3);
  
  % Check location array size.
  if size(Loc, 2) ~= 3
    error('Expecting Grid.pos to be "n" by 3; position vectors as rows.');
  end
  nP = size(Loc, 1);
  
  %   tic;
  % Fieldtrip might need this split into a cell array by position, so
  Forward = zeros(Info.nSens, nO, nP);
  
  % Looping over locations takes much longer (about 100 times in initial
  % test with about 1500 locations inside).  On the order of a second
  % without looping.  Confirmed results are identical both ways, though
  % errors on the order of 2% when locations are single precision.
  %   for l = find(Grid.inside)'
  l = find(Grid.inside);
  for o = 1:nO
    if Info.Grad > 0
      % Need to reset for each location (if looping) and orientation.
      Br = zeros(nP, Info.nRefs);
      % Calculate forward solution for references.
      for r = 1:Info.nRefs
        for c = 1:Info.nCoils(r)
          % Sum field from all coils and dipoles.
          Br(l, r) = Br(l, r) + ...
            ForwardSphere(Moment * Orient(o, :), Loc(l, :), ...
            Sphere(r).Center, Info.ChanPos(r, :, c), ...
            Info.ChanOri(r, :, c));
          % Forward(SourceMoment, SourceLoc, SphereLoc, SensorLoc, SensorOrient)
        end
      end
    end
    % Calculate forward solution for sensors.
    for s = 1:Info.nSens
      sInfo = s + Info.nRefs;
      if Info.Grad > 0
        % Substract from field of primary sensor according to sensor coefficients.
        % This was fixed to use coefficients adjusted to work with Tesla
        % values (by multiplying them by a ratio of gains).
        Forward(s, o, l) = squeeze(Forward(s, o, l)) - Br(l, :) * Info.GradCoefs(:, s);
      end
      for c = 1:Info.nCoils(sInfo)
        % Sum field from all coils.
        Forward(s, o, l) = squeeze(Forward(s, o, l)) + ...
          ForwardSphere(Moment * Orient(o, :), Loc(l, :), ...
          Sphere(sInfo).Center, Info.ChanPos(sInfo, :, c), ...
          Info.ChanOri(sInfo, :, c));
      end
    end
  end
  
  Forward = 1e15 * Forward; % Go from T to fT.
  
  if DoSave
    save(ForwardFile, 'Forward');
    fprintf('Forward solution computed with spherical head model saved: %s\n', ...
      ForwardFile);
  end
end
