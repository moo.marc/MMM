function SpheresFile = FitSpheres(Dataset, SurfaceFile, Model, ...
    Overwrite, SaveCTFhdm, Show, Verbose)
  % Create a multiple-sphere head model fitted to a surface (CTF .hdm format).
  %
  % SpheresFile = FitSpheres(Dataset, SurfaceFile, Model, ...
  %     Overwrite, SaveCTFhdm, Show, Verbose)
  %
  % This head model is used to compute the MEG forward solution.
  %
  % Dataset: CTF dataset where the .hdm file(s) will be saved.  This is
  % required as the head coordinates are used and will be different for
  % each dataset; thus .hdm files are dataset-specific.  Fieldtrip is used
  % to read the sensor information from the dataset.
  %
  % SurfaceFile: GIfTI file of the (typically inner-skull) surface in
  % individual RAS_CTF coordinates.  If missing, a dialog is presented to
  % select a file.
  %
  % Model: structure with fields Function (function handle to the
  % fitting method) and Options (fitting function options, depends on the
  % method used) and optionally FileName (string for output .hdm file, no
  % path and no extension - they are removed).
  %
  % Overwrite [default false]: If true, overwrite existing .hdm file(s).
  % SaveCTFhdm [default true]: If true, save CTF header in .hdm file(s).
  % Show [default false]: If true, plots fitted sphere coordinates.
  % Verbose [default 1]: Amount of text displayed in command window. 0
  %   nothing, 1 only file operations, 2 more.
  %
  % For details and comparison of fitting methods, see: Lalancette et al.,
  % Evaluation of multiple-sphere head models for MEG source localization,
  % Phys. Med. Biol. 56 (2011) 5621�5635, doi:10.1088/0031-9155/56/17/010
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-11-24
  
  % In this version, the MRI file used for normalization and thus for the
  % inner skull surface are already in RAS_CTF coordinates.  This means we
  % no longer need to use a Fiducials.mat file.
  
  % [TO DO: could skip unnecessary processing when only displaying...]
  
  % --------------------------------------
  % Parse inputs, including Model structure.
  if ~exist('Model', 'var') || isempty(Model)
    Model.Function = @MultiSphere_Local;
    Model.Options = [9, 1, 1, 0]; %[PatchR, Square, Weight, FromSens]
    Model.FileName = sprintf('MultiSphere_Local_%gcm', 9);
    %     error('Missing or empty "Model" argument.')
  elseif ~isfield(Model, 'Function') || ~isfield(Model, 'Options')
    error('Model structure is missing one or more required fields: Function, Options.')
  end
  nModels = numel(Model);
  
  if ~exist('Overwrite', 'var') || isempty(Overwrite)
    Overwrite = false;
  end
  if ~exist('SaveCTFhdm', 'var') || isempty(SaveCTFhdm)
    SaveCTFhdm = true;
  end

  if ~exist('Show', 'var') || isempty(Show)
    Show = false;
  end
  
  if ~exist('Verbose', 'var') || isempty(Verbose)
    Verbose = 1;
  end
  
  
  % --------------------------------------
  % Get sensor locations, orientations and reference coefficients from
  % Dataset file.  Now changed to use Fieldtrip fileio functions.
  
  sens = ft_read_sens(Dataset, 'fileformat', 'ctf_ds');
  nChan = size(sens.tra, 1);
  % Build new sensor structure.
  Coil(1: 2) = struct('Location', zeros(1, 3), 'Orientation', zeros(1, 3), ...
    'Radius', 0);
  Sensor(1: nChan) = struct('Label', '     ', 'Coil', Coil);
  
  Flips = 0;
  Flops = 0;
  for s = 1:nChan
    iCoils = find(sens.tra(s, :) == 1, 2, 'first');
    % This works because sensors are first, and for reference channels
    % there are no balancing coefficients.
    Sensor(s).Label = sens.label{s};
    for c = 1:numel(iCoils)
      Sensor(s).Coil(c).Location(:) = sens.coilpos(iCoils(c), :); % (:) needed to keep it as line vector.
      % Orientation is already corrected to point outwards.  No need to
      % multiply by -sign(proper_gain).
      % Easier to have both orientations separate for adding up the fields.
      Sensor(s).Coil(c).Orientation(:) = sens.coilori(iCoils(c), :);
    end
    if numel(iCoils) < 2
      % Remove extra coil since functions count this to know how many.
      Sensor(s).Coil(2) = [];
    end
    % Verify that the sensor orientation points outwards.
    if strncmpi(sens.chantype{s}, 'meg', 3) && ...
        Sensor(s).Coil(1).Orientation * Sensor(s).Coil(1).Location' < 0
      Flips = Flips + 1;
    end
    if numel(iCoils) > 1 && ...
        Sensor(s).Coil(1).Orientation * Sensor(s).Coil(2).Orientation' > 0
      Flops = Flops + 1;
    end
  end
  if Flips > 0
    error('Coil orientation problem: %1.0f coils point (somewhat) inwards.\n', Flips)
  end
  if Flops > 0
    error('Coil orientation problem: %1.0f gradiometers with same coil orientations.\n', Flops)
  end
  % Gradiometer selection is not important here.  We calculate spheres for
  % every sensor and reference channel separately.
  
  
  % --------------------------------------
  % Find sphere that's close to sensors without including them.
  % Used for sphere validation.
  SensorSphere.Center = [0, 0, 5]; % mean(SensorLocations, 1);
  SensorSphere.Center = fminsearch(@InsideMeanSquareDist, SensorSphere.Center);
  SensorSphere.Radius = min( sqrt(sum( ...
    bsxfun(@minus, sens.chanpos, SensorSphere.Center).^2 , 2)) );
  % Results: Center: [-0.70 0.04 5.43], Radius: 10.39
  clear sens Coil s Flips Flops
  
  % ---------------------------------------------------------------------
  % Subfunction to fit a sphere to the sensors, while not overlapping them.
  % Need to calculate this everytime in head coordinates.
  function D = InsideMeanSquareDist(Center)
    Distances = sqrt(sum( bsxfun(@minus, sens.chanpos, Center).^2 , 2));
    Rad = min(Distances);
    D = mean((Distances - Rad).^2);
  end
  % ---------------------------------------------------------------------
  
  
  % --------------------------------------
  % Get head shape.
  
  if Verbose > 1
    fprintf('Loading subject inner skull surface.\n')
  end
  
  if ~exist('SurfaceFile', 'var') || isempty(SurfaceFile) || ~ischar(SurfaceFile)
    [fileName, filePath] = ...
      uigetfile({'*.gii'}, 'Select GIfTI surface file (.gii).');
    SurfaceFile = fullfile(filePath, fileName);
  else
    [~, ~, SurfExt] = fileparts(SurfaceFile);
    if ~strcmpi(SurfExt, '.gii')
      error('Expecting GIfTI surface file, found extension %s', SurfExt);
    end
  end
  
  gii = gifti(SurfaceFile);
  if isempty(gii)
    error('Could not load surface file: %s', SurfaceFile);
  end
  gii = CheckGiftiMatrixBug(gii);

  Vertices = gii.vertices; % nV x 3
  Faces = gii.faces; % nF x 3
  nV = size(gii.vertices, 1);
  %   nF = size(gii.faces, 1);
  
  
  % ----------------------------------- (Coordinate transformation stuff)
  % Here we assume that our usual pipeline is used and "aligned" means
  % subject RAS_CTF coordinates.
  
  CoordSystems = {'NIFTI_XFORM_UNKNOWN', 'NIFTI_XFORM_SCANNER_ANAT', ...
    'NIFTI_XFORM_ALIGNED_ANAT', 'NIFTI_XFORM_TALAIRACH', 'NIFTI_XFORM_MNI_152'};
  % #define NIFTI_XFORM_UNKNOWN      0 /*! Arbitrary coordinates (Method 1). */
  % #define NIFTI_XFORM_SCANNER_ANAT 1 /*! Scanner-based anatomical coordinates */
  % #define NIFTI_XFORM_ALIGNED_ANAT 2 /*! Coordinates aligned to another file's,
  % #define NIFTI_XFORM_TALAIRACH    3 /*! Coordinates aligned to Talairach-
  % #define NIFTI_XFORM_MNI_152      4 /*! MNI 152 normalized coordinates. */
  
  GiiSystems = [];
  for v = 1:numel(gii.private.data)
    if strfind(gii.private.data{v}.attributes.Intent, 'NIFTI_INTENT_POINTSET')
      GiiSystems = [find(strcmpi(gii.private.data{v}.space.DataSpace, CoordSystems), 1), ...
        find(strcmpi(gii.private.data{v}.space.TransformedSpace, CoordSystems), 1)];
      break;
    end
  end
  if isempty(GiiSystems)
    error('No points found in GIfTI file.');
  end
  
  if GiiSystems(1) == CoordSystems{3}
    % Raw data is already aligned it seems, so keep it.
    %       Vertices = gii.vertices;
  elseif GiiSystems(2) == CoordSystems{3}
    % Only the transformed intent matches, so apply the transformation.
    % The GIfTI format does not specify how the transformation is
    % supposed to be applied T * v or v' * T; but someone on their forums
    % decisively said it was supposed to be like NIfTI: T * v.
    Vertices = [Vertices, ones(nV, 1)] * gii.mat';
    Vertices(:, 4) = [];
  end
  
  % We need CTF coordinates, so rotate and scale to cm.
  
  % This R is the same as used in nii2ctfnii, we now apply its inverse
  % (transpose), but since we apply it to row vectors, it's transposed
  % back.
  R = zeros(3);
  R(1, 2) = -1;
  R(2, 1) = 1;
  R(3, 3) = 1;
  
  Vertices = Vertices * R / 10;
  % ------------------------------- (End coordinate transformation stuff)
  
  
  % Calculate dA normal vectors to each vertex (not needed in all methods).
  
  if Verbose > 1
    fprintf('Calculating normal surface element vectors.\n')
  end
  dA = SurfaceNormals(Vertices, Faces, false, 1); % Don't normalize, right handed.
  % For any new format, or in case of weird behavior by the volume
  % current method, verify normals with PlotShape option.  Because of
  % previous errors, this is now always verified below, once the
  % InitialSphere is computed.
  
  PlotShape = false;
  if PlotShape
    figure();
    %    hold off % doesn't work for 3d apparenlty
    clf % clear current figure window.
    AlphaScalar = 0.5;
    %set(gcf, 'Renderer', 'zbuffer', 'RendererMode', 'Manual')
    patch('Faces', Faces, 'Vertices', Vertices, ...
      'FaceColor', [1,0.6,0.4], 'EdgeColor', 'none', ...
      'FaceAlpha', AlphaScalar, 'EdgeAlpha', AlphaScalar);
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
    hold on
    axis equal
    quiver3(Vertices(:,1), Vertices(:,2), Vertices(:,3), ...
      dA(:,1), dA(:,2), dA(:,3), 'b');
    %     plot3(Fiducials(:, 1), Fiducials(:, 2), Fiducials(:, 3), '.r')
    Position = [43.275, 100.04, 49.143];
    set(gca, ...
      'CameraPosition', Position)
    axis tight
    drawnow
  end
  %   clear Fiducials
  
  
  % --------------------------------------
  % Compute spheres.
  % Get the best center and corresponding radius for each sensor.
  % Do simplex minimization with appropriate error function for each
  % method.
  
  % Initial sphere location: use local patch method.
  InitialSphere = MultiSphere_Local([], Sensor(1), ...
    Vertices, nV, dA, [50, 1, 1, 0]);
  
  % Now verify dA are pointing out and not in.
  if sum(sum( (dA .* (Vertices - InitialSphere.Center(ones(1, nV), :)) ), 2) < 0) >= nV/5
    error('More than 20%% normal vectors to the conductor surface seem to be pointing inwards.  \nVerify normals with PlotShape option.')
  end
  
  % Preallocate the multi-sphere structure array.
  Sphere(1:nModels, 1:nChan) = InitialSphere;
  
  for m = 1:nModels
    
    % Check for supplied output file name.
    if isfield(Model, 'FileName') && ~isempty(Model(m).FileName)
      % Remove path and extension if present.
      [unused, SpheresFile] = fileparts(Model(m).FileName); %#ok<ASGLU>
      SpheresFile = [SpheresFile, '.hdm']; %#ok<AGROW>
    else
      SpheresFile = sprintf('%s_%s_%s.hdm', ShapeName, ...
        func2str(Model(m).Function), ...
        strrep(num2str(Model(m).Options,'%g'), ' ', ''));
      % strrep needed since num2str has inconsistent space padding between
      % versions.
    end
    % Head models depend on sensor locations, which change from dataset to
    % dataset in head coordinates, so save .hdm files in dataset.
    SpheresFile = fullfile(Dataset, SpheresFile);
    
    if exist(SpheresFile, 'file') && ~Overwrite 
      if Verbose > 0
        fprintf('Found head model file %d: %s\n', m, SpheresFile)
      end
      if Show
        Sphere(m, :) = openhdm(SpheresFile);
      end
    else
      % Compute spheres.
      
      if Verbose > 1
        fprintf('Fitting spheres for head model %d.\n', m)
      end
      
      % Skip if Model(m).Function doesn't exist, for example: Primary
      if ~exist(func2str(Model(m).Function), 'file')
        warning('Skipping unknown sphere fitting function: %s', func2str(Model(m).Function));
        continue
      end
      if ( isequal(Model(m).Function, @MultiSphere_Local) ...
          && Model(m).Options(1) > 20 ) || ...
          isequal(Model(m).Function, @SingleSphere) || ...
          isequal(Model(m).Function, @SingleSphereExternal)
        % Single sphere model.  In this case, just compute sphere once.
        
        % Call sphere calculation function.
        c = 1;
        Sphere(m, c) = Model(m).Function(InitialSphere, Sensor(c), ...
          Vertices, nV, dA, Model(m).Options);
        % Check for invalid sphere.
        if norm(Sphere(m, c).Center - SensorSphere.Center) > SensorSphere.Radius
          warning('Sphere center for reference %1.0f, method %1.0f is outside the single sphere.', ...
            c, m);
        end
        if Sphere(m, c).Radius < 1
          warning('Bad convergence with small radius for ref. %1.0f, method %1.0f.', ...
            c, m);
        end
        % Copy to all references and sensors.
        for c = 2:nChan
          Sphere(m, c) = Sphere(m, 1);
        end
        
      else % Not single sphere.
        
        % Need spheres for references too.
        tic
        for c = 1:nChan % Slow. Need Parallel Processing Toolbox to thread. Makes a big difference.
          % Call sphere calculation function.
          Sphere(m, c) = Model(m).Function(InitialSphere, Sensor(c), ...
            Vertices, nV, dA, Model(m).Options);
          % Check for invalid sphere.
          if norm(Sphere(m, c).Center - SensorSphere.Center) > SensorSphere.Radius %#ok<*PFBNS>
            warning('Sphere center for reference %1.0f, method %1.0f is outside the single sphere.', ...
              c, m);
          end
          if Sphere(m, c).Radius < 1
            warning('Bad convergence with small radius for ref. %1.0f, method %1.0f.', ...
              c, m);
          end
          
        end % Channel loop
        
        if Verbose > 1
          if toc > 60
            fprintf('Total channel loop time: %1.1f min.\n', toc/60);
          else
            fprintf('Total channel loop time: %1.0f s.\n', toc);
          end
        end
      end % single or multiple sphere model
      
      % Save multi-sphere head model to .hdm file in dataset directory.
      if SaveCTFhdm
        % Save as CTF .hdm format with header so they can be used in
        % dipole fit.
        CTFHeader.File = SpheresFile;
        CTFHeader.ShapeFile = SurfaceFile;
        if isequal(@MultiSphere_Local, Model(m).Function)
          CTFHeader.PatchR = Model(m).Options(1);
        end
      else
        CTFHeader = [];
      end
      savehdm(SpheresFile, Sphere(m, :), {Sensor(:).Label}, ...
        Overwrite, Verbose, CTFHeader);
    end % Compute spheres
    
    if Show % Show even if not overwriting.
      Visualize();
    end
  end % Models (m) loop
  
  if ~exist(SpheresFile, 'file')
    SpheresFile = [];
    if Verbose
      fprintf('Warning: Newly created head model not found.\n');
    end
  elseif Verbose > 1
    fprintf('Finished creating head models.\n')
  end
  
  
  % ---------------------------------------------------------------------
  % Subfunction to give visual feedback on fit.
  function Visualize()
    figure()
    %     clf
    Centers = reshape([Sphere(m,:).Center], 3, [])';
    %plot(NormLines(Centers))
    plot(Centers)
    hold on
    plot([Sphere(m,:).Radius], ':')
    legend('Center x', 'Center y', 'Center z', 'Radius', 'Location', 'West');
    xlabel('Sensor');
    ylabel('Location (cm)');
    %     SensorLabels = {Sensor(1:nChan).Label};
    %     Ticks = [1, find(~strncmpi(SensorLabels(2:end), SensorLabels(1:end-1), 3))];
    %     Ticks(end-2:end) = [];
    %     TickLabels = {'Ref'};
    %     for t = 1:length(Ticks)
    %       TickLabels(t+1) = {SensorLabels{Ticks(t)+1}(2:3)};
    %     end
    %     TickLabels(end) = {'Z'};
    set(gca, 'XLim', [0, nChan+1]) %, ...
    %     'XTick', [1, Ticks], 'XTickLabel', TickLabels, ...
    %       'Position', [0.08, 0.17, 0.9, 0.8]);
    %     if PlotSpheres3D
    %       figure()
    %       hold on
    %       patch('Faces', Faces, 'Vertices', Vertices, ...
    %         'FaceColor', [1,0.6,0.4], 'EdgeColor', 'none', ...
    %         'FaceAlpha', 0.5, 'EdgeAlpha', 0.5);
    %       axis equal
    %       xlabel('X');
    %       ylabel('Y');
    %       zlabel('Z');
    %       for ss = 1:nChan
    %         [IcosaV, IcosaF] = SphereSurface(Sphere(m, ss).Radius, Sphere(m, ss).Center, 3);
    %         patch('Faces', IcosaF, 'Vertices', IcosaV, ...
    %           'FaceColor', [0.1,0.2,0.7], 'EdgeColor', 'none', ...
    %           'FaceAlpha', 0.05, 'EdgeAlpha', 0.05);
    %       end
    %       set(gca, 'CameraPosition', [10, 100, 70]);
    %     end
    
    drawnow
  end
  
  
end % Main function.






