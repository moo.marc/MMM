function WM = WeightedMedian(List, Weights)
  % Find the weighted median of a list of real numbers.
  %
  % WM = WeightedMedian(List, Weights)
  %
  % Minimizes the (optionally weighted) sum of absolute difference between
  % list elements and a fixed point.  
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2013
  
  % When there are no (or equal) weights, the logic behind the median
  % minimizing the sum of distances is that we pair up end points: the
  % sum of distances from any (third) point will be minimized if it is
  % anywhere in between them. We continue pairing the next "end" points
  % until there are an equal number on each side (median).
  %
  % Now, with weights, we can again pair up end points, but there will be
  % a remainder if one point has a larger weight.  Simply think of this
  % as an extra point and continue pairing up (partial) points until
  % there are again an equal number of these partial points on each side.
  % In general, the partial sum of weights on one side won't exactly add
  % up to half, so the weighted median will be ON the last remaining
  % (partial) point. Otherwise, as in the median, it is anywhere in
  % between the middle two (partial) points.
  
  if ~isvector(List)
    error('WeightedMedian requires a vector.')
  elseif ~exist('Weights', 'var') || length(Weights) == 1
    % warning('No weights or equal weights: using regular median.');
    WM = median(List);
    return
  elseif size(Weights) ~= size(List)
    error('Weights and List sizes don''t match.')
  elseif any(Weights < 0)
    error('Negative weights are not allowed.')
  end
  
  % First sort the list and weights.
  [List, Order] = sort(List);
  Weights = Weights(Order);
  % n = length(List);
  
  Total = sum(Weights); % Weights might not be normalized.
  if Total == 0
    warning('All weights = 0, median undetermined.');
    WM = NaN;
    return
  end
  
  PartialSum = 0;
  w = 0;
  while PartialSum < Total/2
    w = w + 1;
    PartialSum = PartialSum + Weights(w);
  end
  
  if PartialSum == Total/2
    % Use weighted mean here, but any number between List(w: w+1) is
    % equally valid.
    WM = WeightedMean(List(w: w+1), Weights(w: w+1));
  else
    WM = List(w);
  end
  
end 
