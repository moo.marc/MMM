function WM = WeightedMean(List, Weights)
  % Find the weighted mean of a list of real numbers.
  %
  % WM = WeightedMean(List, Weights)
  %
  % Minimizes the (optionally weighted) sum of absolute difference between
  % list elements and a constant.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2013
  
  if ~isvector(List)
    error('WeightedMean requires a vector.')
  elseif ~exist('Weights', 'var') || length(Weights) == 1
    % warning('No weights or equal weights: using regular mean.');
    WM = mean(List);
  elseif any(size(Weights) ~= size(List))
    error('Weights and List sizes don''t match.')
  elseif any(Weights < 0)
    error('Negative weights are not allowed.')
  elseif size(List, 2) == 1 % Column vectors.
    WM = Weights' * List/ sum(Weights);
  else % Row vectors.
    WM = Weights * List'/ sum(Weights);
  end
end

