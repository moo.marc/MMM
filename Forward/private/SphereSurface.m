function [Vertices, Faces, VertexNormals] = SphereSurface(r, o, n)
  % Create a sphere triangulated surface.
  %
  % Surface = Sphere(r, o, n)
  % [Vertices, Faces, VertexNormals] = SphereSurface(r, o, n)
  %
  % Creates a spherical mesh by iteratively refining an icosahedron and
  % projecting the points on the sphere.  The surface normals at vertices
  % are scaled to the area element they represent (a third of each adjacent
  % triangle).  If the radius is 1, this equals the solid angle element of
  % each vertex.
  %
  % r: Sphere radius.
  % o: Sphere origin.
  % n: Number of mesh refining iterations.
  %
  % If only one output argument is supplied, a "patch structure" is
  % returned with the 3 fields: vertices, faces, VertexNormals. (Thus it
  % can be passed directly to the patch function.)
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-06
  
  % (Used by VolumeCurrentWeighting method only for head models.)
 
  % Parse input variables.
  if ~exist('r', 'var') || isempty(r)
    r = 1;
  end
  if ~exist('o', 'var') || isempty(o)
    o = [0, 0, 0];
  elseif any(size(o) ~= [1, 3])
    error('Origin should be a 1 by 3 vector: %f %f', size(o));
  end
  if ~exist('n', 'var') || isempty(n)
    n = 0;
  end
  
  
  % First vertex, others obtained by permutations and sign changes.
  phi = (1 + sqrt(5)) / 2; % Golden ratio
  Vertex = [0, 1, phi];
  Vertex = r * Vertex / norm(Vertex);
  
  CyclicalPerm = eye(3);
  CyclicalPerm = CyclicalPerm([3, 1, 2], :);
  
  % Preallocate full array.
  Vertices = ones(12, 1) * Vertex;
  % Flipping y, z and (y and z) gives 3 more vertices.
  Vertices([2, 4], 2) = - Vertices([2, 4], 2);
  Vertices([3, 4], 3) = - Vertices([3, 4], 3);
  % Cycle the coordinates to get the rest.
  Vertices(5:8, :) = Vertices(1:4, :) * CyclicalPerm;
  Vertices(9:12, :) = Vertices(5:8, :) * CyclicalPerm;
  
  % Now describe the faces.
  % Faces = FindIsocahedronFaces(Vertices, r)
  Faces = ...
    [1     2     9
    1    11     2
    1     5     6
    1     9     5
    1     6    11
    2     8     7
    2     7     9
    2    11     8
    3    10     4
    3     4    12
    3     6     5
    3     5    10
    3    12     6
    4     7     8
    4    10     7
    4     8    12
    5     9    10
    6    12    11
    7    10     9
    8    11    12];
  
  % Refine the mesh.
  %       v3
  %       /\
  %      /  \
  %  v31/____\v23
  %    /\    /\
  %   /  \  /  \
  %  /____\/____\
  % v1    v12	  v2
  
  for Iteration = 1:n
    
    % Preallocate.
    nF = size(Faces, 1);
    nV = size(Vertices, 1);
    NewFaces = zeros(4 * nF, 3);
    nVNew = 3 * nF / 2;
    Vertices(nV + 1: nV + nVNew, :) = zeros(nVNew, 3);
    Vi = nV + 1; % Index to add elements in Vertices.
    
    % When a new vertex is calculated (say between v1 and v2) and added to
    % the NewVertices matrix, we write its index in this sparse matrix at
    % position (v1, v2) so we don't have to search for it later.  Each new
    % vertex is part of 2 old faces (6 new ones).
    VIs = sparse(nV, nV);
    
    for f = 1:nF
      
      % Get the triangle vertex indices
      v1 = Faces(f, 1);
      v2 = Faces(f, 2);
      v3 = Faces(f, 3);
      
      % Create the new vertices if not done already.
      if ~VIs(v1, v2)
        VIs(v1, v2) = Vi;
        VIs(v2, v1) = Vi;
        Vertices(Vi, :) = (Vertices(v1, :) + Vertices(v2, :)) ./2;
        Vertices(Vi, :) = ProjectOnSphere(Vertices(Vi, :), r);
        Vi = Vi + 1;
      end
      if ~VIs(v2, v3)
        VIs(v2, v3) = Vi;
        VIs(v3, v2) = Vi;
        Vertices(Vi, :) = (Vertices(v2, :) + Vertices(v3, :)) ./2;
        Vertices(Vi, :) = ProjectOnSphere(Vertices(Vi, :), r);
        Vi = Vi + 1;
      end
      if ~VIs(v1, v3)
        VIs(v1, v3) = Vi;
        VIs(v3, v1) = Vi;
        Vertices(Vi, :) = (Vertices(v1, :) + Vertices(v3, :)) ./2;
        Vertices(Vi, :) = ProjectOnSphere(Vertices(Vi, :), r);
        Vi = Vi + 1;
      end
      
      % Create new faces, with proper orientation (see picture).
      NewFaces(f*4 - 3, :) = [ v1, VIs(v1, v2), VIs(v3, v1) ];
      NewFaces(f*4 - 2, :) = [ VIs(v1, v2), v2, VIs(v2, v3) ];
      NewFaces(f*4 - 1, :) = [ VIs(v2, v3), v3, VIs(v3, v1) ];
      NewFaces(f*4, :) = [ VIs(v1, v2), VIs(v2, v3), VIs(v3, v1) ];
      
    end % f loop
    
    Faces = NewFaces;
    
  end % Iteration
  
  nF = size(Faces, 1);
  nV = size(Vertices, 1);
  
  % Translate.
  
  Vertices = Vertices + o(ones(nV, 1), :);
  
  if nargout == 2
    return;
  end
  
  % Now calculate normal vectors at each vertex, with norm equal to the
  % surface element it represents, to be used in surface integration.
  
  % CORRECTED (see below) OLD WAY: Average the dS's of each face adjacent
  % to a vertex.  Careful, the original isocahedron vertices have only 5
  % neighboring faces, all others have 6. But they are the first 12 ones in
  % the array, so we don't have to keep the count. BETTER: We know the
  % orientation of dS, it's that of the vertex position vector (since the
  % sphere is centered at the origin.  So we only need its length, for
  % which we use previous method: add a third of each adjacent face. Also,
  % the faces have their vertices in counterclockwise order (when viewed
  % from outside the sphere above that face) so all normals will point
  % outwards.
  VertexNormals = zeros(size(Vertices));
  for f = 1:nF
    VertexNormals(Faces(f, :), :) = VertexNormals(Faces(f, :), :) + ...
      ones(3, 1) * cross(...
      (Vertices(Faces(f, 2), :) - Vertices(Faces(f, 1), :)), ...
      (Vertices(Faces(f, 3), :) - Vertices(Faces(f, 2), :)) ...
      ) / 2 / 3;
  end
  for v = 1:nV
    VertexNormals(v, :) = norm(VertexNormals(v, :))/norm(Vertices(v, :)) * Vertices(v, :);
  end
  
  if nargout == 1
    Surface.vertices = Vertices;
    Surface.faces = Faces;
    Surface.VertexNormals = VertexNormals; % Not sure if normals in patch objects need to be unit length.  This might produce strange effects for lighting?
    Vertices = Surface;
  end
  
end



% ------------------------------------------------------------------------
% The midpoint between two vertices is not on the sphere surface, so
% project it back to the surface with this function.
function NewVertex = ProjectOnSphere(Vertex, r)
  % Convert to spherical coordinates, adjust radius and convert back.
  [WrongRadius, t, p] = cart2spher(Vertex); %#ok<ASGLU>
  NewVertex = spher2cart(r, t, p);
end



% ------------------------------------------------------------------------
% Use this code to find the faces if something changes.  Can then
% hardcode the results as above.
function Faces = FindIsocahedronFaces(Vertices, r) %#ok<DEFNU>
  
  if ~exist('r', 'var')
    r = 1;
  end
  
  % First, calculate distances to find which vertices are close.
  D = zeros(12, 12);
  for v = 1:12
    for w = 1:12
      D(v, w) = norm(Vertices(w, :) - Vertices(v, :)); % Symmetric.
    end
  end
  % Distances for r=1 are 0, 1.0515, 1.7013, 2.
  Near = r * 1.1;
  NearVertices = D < Near;
  
  % Now go through half the array (to avoid duplicate faces).  For each
  % close vertex, find a third common close one (at most 2 of them).
  Faces = zeros(20, 3);
  Fi = 1;
  for v1 = 1:12
    v2s = v1 + find(NearVertices(v1, v1+1:end));
    for v2i = 1:length(v2s)
      v2 = v2s(v2i);
      v3s = v2 + find(NearVertices(v1, v2+1:end) & NearVertices(v2, v2+1:end));
      for v3i = 1:length(v3s)
        Faces(f, :) = [v1, v2, v3s(v3i)];
        Fi = Fi + 1;
      end
    end
  end
  
  % Now need to orient the vertices so the normal will point outwards.
  for Fi = 1:20
    Normal = cross(...
      (Vertices(Faces(Fi, 2), :) - Vertices(Faces(Fi, 1), :)), ...
      (Vertices(Faces(Fi, 3), :) - Vertices(Faces(Fi, 2), :)) ...
      );
    Normal = Normal/norm(Normal);
    if norm( Vertices(Faces(Fi, 1), :)/r + Normal ) < 1
      temp = Faces(Fi, 3);
      Faces(Fi, 3) = Faces(Fi, 2);
      Faces(Fi, 2) = temp;
    end
  end
end


% Now separate function.
% function [r, t, p] = cart2spher(x, y, z)
%   % Convert from cartesian to spherical coordinates.
%   %
%   % [r, t, p] = cart2spher(x, y, z)
%   %
%   % "physics" convention:
%   % t (theta) is the zenith (from z axis),
%   % p (phi) is the azimuth (in xy-plane from x).
% 
%   if nargin == 1 && length(x) == 3
%     % Coordinates were passed as vector.
%     y = x(2);
%     z = x(3);
%     x = x(1);
%   end
%   
%   r = sqrt(x^2 + y^2 + z^2);
%   % Handle zero length case.
%   if r == 0
%     t = 0;
%     p = 0;
%   else
%     t = acos(z/r); % Between 0 and pi.
%     % Handle special azimuth cases.
%     if x == 0
%       if y > 0
%         p = pi/2;
%       else
%         p = 3*pi/2;
%       end
%     else
%       p = atan(y/x); % Between -pi/2 and pi/2, need to correct for 0 to 2*pi.
%       if x < 0
%         p = p + pi;
%       elseif y < 0
%         p = p + 2*pi;
%       end
%     end
%   end
%   
%   if nargout == 1
%     % Output as vector.
%     r = [r, t, p];
%   end
% end


% Now separate function.
% function [x, y, z] = spher2cart(r, t, p)
%   % Convert from spherical to cartesian coordinates.
%   %
%   % [x, y, z] = spher2cart(r, t, p)
%   %
%   % "physics" convention:
%   % t (theta) is the zenith (from z axis),
%   % p (phi) is the azimuth (in xy-plane from x).
% 
%   if nargin == 1 && length(r) == 3
%     % Coordinates were passed as vector.
%     t = r(2);
%     p = r(3);
%     r = r(1);
%   end
% 
%   x = r * sin(t) * cos(p);
%   y = r * sin(t) * sin(p);
%   z = r * cos(t);
%   
%   if nargout == 1
%     % Output as vector.
%     x = [x, y, z];
%   end
% 
% end
