function Sphere = ...
    MultiSphere_VolumeCurrent(SingleSphere, Sensor, Vertices, nV, dA, Options) %#ok<INUSD>
  % Fit a sphere based on currents throughout the conductor volume.
  %
  % Sphere = MultiSphere_VolumeCurrent(SingleSphere, Sensor, Vertices, nV, dA, Options)
  %
  % 
  % This is used by FitSpheres.m to create a multiple-sphere head model
  % for computing the MEG forward solution.  See that file for a published
  % study that compares this and other sphere-fitting methods.
  % 
  % Minimize the non-overlapping volumes between the sphere and shape,
  % weighing each volume element by the sensitivity of the given sensor to
  % currents of optimal orientation at that location. 
  %
  % For details on this new fitting approach, see: Lalancette et al.,
  % Evaluation of multiple-sphere head models for MEG source localization,
  % Phys. Med. Biol. 56 (2011) 5621�5635, doi:10.1088/0031-9155/56/17/010
  %
  % SingleSphere: Optional 4-element vector (x,y,z,radius) used to
  % initialize the minimization search.
  %
  % Sensor: Structure containing the sensor geometry (coils, locations,
  % orientations).
  %
  % Vertices: (nV x 3) array of shape points.
  %
  % nV: Optional, number of shape points.
  % 
  % dA: (nV x 3) array of normal vectors at each vertex.
  %
  % (Options is not used here, but kept for compatibility with other
  % fitting methods.)
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-07
  
  % This method is based on the idea of minimizing the difference between
  % the volume current contributions to the magnetic field detected by the
  % sensor, from the real head shape and approximating sphere. Since
  % Huang's method fails because it assumes uniform magnitude for the
  % equivalent surface currents, we retain a volume integral, assuming
  % instead uniform magnitude for the volume currents, with directions that
  % maximize the contribution of each volume element, i.e. the direction of
  % maximum sensitivity for the given sensor.
  %
  % However, when taking the difference between shape and sphere volumes,
  % we end up with one or more regions with positive or negative sign
  % depending on which surface is on the "exterior" (not considering the
  % case where the two volumes don't intersect here).  These positive and
  % negative regions will cancel, and this will not be the case in general
  % for a real source distribution.  We therefore take the absolute value
  % of that difference.  We can interpret the result as minimizing the
  % non-overlapping volumes "weighted" by the (maximum) sensitivity of the
  % sensor to currents at each location.
  %
  % Complications:
  % 1. Since the shape can be convex and more than one shape point is
  % mapped to the same sphere point, we use the (dot product of the)
  % surface normal vectors to assign a positive or negative sign to each
  % integral element, such that the non-overlapping volume is properly
  % integrated over (only once and nothing else).
  % 2. However, when the sphere center is outside the shape, mapping
  % each shape point to the closest sphere point does not cover the volume
  % properly.  In that case, we must add the same integral over the entire
  % sphere (a few drawings make this clear).  This requires us to create a
  % sphere mesh.
  % 3. The volume integral is performed over "conical" volume elements from
  % the center of the sphere to the shape (or sphere) surface.  The
  % analytic expression for the "radial" integral is rather complicated so
  % we use numerical integration.  This might slow down the process.

  % This can likely be accelerated enormously and made much more accurate
  % by converting to a surface integral.  We need to apply the proper
  % transformation to the vector field.  I think this will give back the
  % tangential current surface weighing method, but not with uniform
  % currents.  Should implement and compare!  Possible complication is
  % taking the absolute value of volume elements, otherwise as explained in
  % paper, optimizes for currents everywhere at the same time...
  
  if ~exist('dA', 'var')
    error('Not enough parameters in function call.');
  end
  % Avoid other checks for speed.
  %   if ~exist('SingleSphere', 'var') || isempty(SingleSphere)
  %     SingleSphere.Center = sum(Vertices, 1) / nV;
  %     SingleSphere.Radius = mean( sqrt(sum( ...
  %       (Vertices - SingleSphere.Center(ones(nV, 1), :)).^2 , 2)) );
  %   end
 
  % Number of points for "radial" integral, including limits.
  nI = 10;
  
  Orientation = Sensor.Coil(1).Orientation';
  EndPoints = sqrt(2);

  
  % Create sphere mesh that will be "inflated" and translated when the
  % sphere center is outside the head shape.
  [IcosaV(:, 1, :), IcosaF, IcosadA] = SphereSurface(1, [], 2); %#ok<ASGLU>
  % IcosaV = unit sphere dA, |IcosadA| = sphere dOhm, faces not used.
  nSV = size(IcosaV, 1);
  
  % Call minimization function to find best sphere.
  %options = optimset('MAXFUNEVALS',5000,'MAXITER',1000);
  %[Sphere4, junk, junk, output] = ...
  Sphere4 = ...
    fminsearch(@VolumeContribution, ...
    [SingleSphere.Center, SingleSphere.Radius]);%, options);
  %output
  Sphere.Center = Sphere4(1:3);
  Sphere.Radius = abs(Sphere4(4));
  %Sphere

  
  
  % -----------------------------------------------------------------------
  % Minimization subfunction.  Unit volume currents (with optimal
  % orientations) contribution to magnetic field detected by sensor from
  % non-overlapping head shape and sphere volumes.
  function Integral = VolumeContribution(Sphere4)
    %tic
    % Original code with vertex loop took over 5 seconds.
    % Without loops and writing out dot and norms explicitely: 60 times
    % faster. (0.088s)
    % Still slow, with nI = 10, 0.26s. -> 2-3 hours
    %   nI = 15 -> 3.3 h.
    % After removing repmat, ni = 10, 0.24s; nI = 15, 0.31s.

    % Radius must be positive because of sign of integral depends on it.
    Sphere4(4) = abs(Sphere4(4));
    
    % Vector from sphere center to sensor, repeated.
    %     R0(1, 1, :) = Sensor.Location - Sphere4(1:3);
    
    % First, check if sphere center is inside shape.
    % Find closest shape point to sphere center.
    % Note: minimizing square distance.
    %     [Distance, ClosestI] = min( sum(...
    %       (Vertices - repmat(Sphere4(1:3), nV, 1)).^2 ...
    %       , 2) ); %#ok<ASGLU>
    [Distance, ClosestI] = min( (Vertices(:, 1) - Sphere4(1)).^2 + ...
      (Vertices(:, 2) - Sphere4(2)).^2 + ...
      (Vertices(:, 3) - Sphere4(3)).^2 ); %#ok<ASGLU>
    
      % Initialize.
      Integral = 0;
      
    % Check surface normal: if roughly pointing towards sphere center, then
    % assume center is outside. 
    %
    % Could this check ever be wrong?  Add warning since apparently this
    % never happened before (there was a fatal bug that was never
    % triggered).
    if (dA(ClosestI, 1) * (Vertices(ClosestI, 1) - Sphere4(1)) + ...
        dA(ClosestI, 2) * (Vertices(ClosestI, 2) - Sphere4(2)) + ...
        dA(ClosestI, 3) * (Vertices(ClosestI, 3) - Sphere4(3)) ) < 0
      warning('Sphere center outside conductor.');
      %       keyboard;
      
      % Solid angle elements. 
      dOhm = (IcosadA(:, 1).^2 + IcosadA(:, 2).^2 + IcosadA(:, 3));
    
      % Integrate over it.  (More detailed comments in other integral below.)
      % Radii for trapezoidal integral.
      dr = Sphere4(4) / (nI-1);
      r = 0:dr:Sphere4(4); % 1 x nI
      for c = 1:length(Sensor.Coil)
        % Vector from sphere center to sensor
        R0(1, 1, :) = Sensor.Coil(c).Location - Sphere4(1:3);
        
        % Vector from volume element to sensor.
        R = R0(ones(1, nSV),ones(1, nI), :) - ...
          r(ones(1, nSV), :, [1, 1, 1]) .* IcosaV(:, ones(1, nI), :); % size: nSV x nI x 3
        R(:, 1, :) = R(:, 1, :) * EndPoints;
        R(:, end, :) = R(:, end, :) * EndPoints;
        RSquare = R(:, :, 1).^2 + R(:, :, 2).^2 + R(:, :, 3).^2;
        Integral = Integral + (-1)^(c+1) * sum( ... % over nSV vertices
          dOhm .* dr .* sum( ... % over nI radii
          r(ones(1, nSV), :).^2 .* ... % nSV x nI
          ( 1 - ...  % next 3 lines: cos^2
          (R(:, :, 1) * Orientation(1) + ...
          R(:, :, 2) * Orientation(2) + ...
          R(:, :, 3) * Orientation(3)).^2 ./ RSquare ) ./ ... % nSV x nI
          RSquare, 2) );
      end
    end
    % Clearing slows down a bit, even if arrays are resized.
    % clear SphereV SpheredA dOhm dr r R RSquare
    
    % Integrate over non-overlapping volumes.  If center is outside shape
    % this actually integrates over the whole shape, but with an extra
    % negative sign for the overlapping part, which cancels the overlapping
    % part we included in the whole sphere integral above.
    
    % Sphere surface normal unit vectors for each head shape vertex.
    n(:, 1, 1:3) = Vertices - Sphere4(ones(1, nV), 1:3);
    nNorm = sqrt(n(:, 1, 1).^2 + n(:, 1, 2).^2 + n(:, 1, 3).^2);
    n = n ./ nNorm(:, 1, [1, 1, 1]);
    % Solid angle elements.  Project area element of shape onto sphere
    % tangent plane (dA.n) and divide by squared distance to sphere center.
    %
    % This is where the negative sign comes from for removing duplicate
    % volumes when the sphere center is outside the conductor.  However,
    % due to the fixed number of integration points, that would not fall in
    % the same locations for the two parts that are supposed to cancel, the
    % cancellation may not be very good at all.  Hopefully, this will
    % continue to never be needed (sphere always inside conductor).
    dOhm = (dA(:, 1) .* n(:, 1, 1) + ...
      dA(:, 2) .* n(:, 1, 2) + ...
      dA(:, 3) .* n(:, 1, 3)) ...
      ./ nNorm.^2;
    
    % Radii for trapezoidal integral.  By explicitely finding rmin and
    % max, we don't have to apply abs on the integral below.
    % It would be: I = I + dOhm * abs(trapz())
    rmin = min(nNorm, Sphere4(4));
    rmax = max(nNorm, Sphere4(4));
    dr = (rmax - rmin) / (nI-1);
    r = rmin(:, ones(1, nI)) + dr * (0:(nI-1));
    
    for c = 1:length(Sensor.Coil)
      % Vector from sphere center to sensor
      R0(1, 1, :) = Sensor.Coil(c).Location - Sphere4(1:3);
      
      % Vector from volume element to sensor. (n needs to be unitary here.)
      % This is the slowest step, over 50% of the function time.
      R = R0(ones(1, nV), ones(1, nI), :) - ...
        r(:, :, [1, 1, 1]) .* n(:, ones(1, nI), :); % size: nV x nI x 3
      R(:, 1, :) = R(:, 1, :) * EndPoints;
      R(:, end, :) = R(:, end, :) * EndPoints;
      RSquare = R(:, :, 1).^2 + R(:, :, 2).^2 + R(:, :, 3).^2;
      % Integrate[ dr r^2 dOhm sin^2(R, O) / R^2 ]
      % Explicit formula for trapezoidal integral:
      % sum(f(xi)dx) - f(x1)dx/2 - f(xend)dx/2
      % End points of the radius integral are adjusted by multiplying their R
      % by sqrt(2) above, this cancels in the cos^2 term but gives 1/2 in the
      % other RSquare in the denominator.
      %
      % Because we use the orientation to define the optimal current
      % direction: RxO, that would give us a current in opposite directions
      % for the two coils.  So add an extra "flipping" factor (-1)^(c+1).
      % Since the second coil is further, have its contribution be
      % negative, and the integral should remain positive.
      Integral = Integral + (-1)^(c+1) * sum( ... % over nV vertices
        dOhm .* dr .* sum( ... % over nI radii
        r.^2 .* ... % size: nV x nI
        ( 1 - ...  % next 3 lines: cos^2
        (R(:, :, 1) * Orientation(1) + ...
        R(:, :, 2) * Orientation(2) + ...
        R(:, :, 3) * Orientation(3)).^2 ./ RSquare ) ./ ... % size: nV x nI
        RSquare, 2) );
    end
    %toc
    
    % Because it is not always clear which coil will detect the greatest
    % field (especially for reference gradiometers), we must take the
    % absolute value of the integral.  Otherwise, minimization will produce
    % large negative results in some cases.
    if Integral < 0
      Integral = - Integral;
    end

  end
  
end


