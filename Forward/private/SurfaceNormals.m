function [VN, FN, VdA, FdA] = SurfaceNormals(Vertices, Faces, Normalize, Handedness)
  % Normal vectors at vertices and faces of a triangulated surface.
  %
  % [VN, FN, VdA, FdA] = SurfaceNormals(Vertices, Faces, Normalize) [VN,
  % FN, VdA, FdA] = SurfaceNormals(Patch, [], Normalize)
  %
  %  Vertices [nV, 3]: Point 3d coordinates. Faces [nF, 3]: Triangles, i.e.
  %  3 point indices. Patch: Instead of Vertices and Faces, a single
  %  structure can be given
  %    with fields 'vertices' and 'faces'.  In this case, leave Faces empty
  %    [].
  %  Normalize (default false): If true, VN and FN vectors are unit length.
  %    Otherwise, their lengths are the area element for each vertex (a
  %    third of each adjacent face) and face respectively.
  %  VN, FN: Vertex and face normals. 
  %  VdA, FdA: Vertex and face areas, which is the norm of VN and FN 
  %    vectors if Normalize is false.
  %  Handedness (default 1): + or - 1 to indicate the order of triangle
  %    points.  1 indicates that the normal vector will point towards the
  %    side where the points are viewed as going counterclockwise (normals
  %    computed as edge(1 to 2) x edge(2 to 3) ). For our typical use, this
  %    means that 1 is used if the normals point outwards, and -1 would be
  %    used if they'd point inwards otherwise. This may have to be verified
  %    for each new format or application.
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2019-04-03

  if nargin < 2 || isempty(Faces)
    Faces = Vertices.faces;
    Vertices = Vertices.vertices;
  end
  
  if ~exist('Normalize', 'var') || isempty(Normalize)
    Normalize = false;
  end
  if ~exist('Handedness', 'var') || isempty(Handedness)
    Handedness = 1;
  end

  nV = size(Vertices, 1);
  VN = zeros(nV, 3);
  % Get face normal vectors with length the size of the face area.
  FN = sign(Handedness) * ...
    CrossProduct( (Vertices(Faces(:, 2), :) - Vertices(Faces(:, 1), :)), ...
    (Vertices(Faces(:, 3), :) - Vertices(Faces(:, 2), :)) ) / 2;
  
  % For vertex normals, add adjacent face normals, then normalize.  Also
  % add 1/3 of each adjacent area element for vertex area.
  FdA = sqrt(FN(:,1).^2 + FN(:,2).^2 + FN(:,3).^2);
  VdA = zeros(nV, 1);
  for ff = 1:size(Faces, 1)
    VN(Faces(ff, :), :) = VN(Faces(ff, :), :) + FN([ff, ff, ff], :);
    VdA(Faces(ff, :), :) = VdA(Faces(ff, :), :) + FdA(ff)/3;
  end
  if Normalize
    VN = bsxfun(@rdivide, VN, sqrt(VN(:,1).^2 + VN(:,2).^2 + VN(:,3).^2));
    FN = bsxfun(@rdivide, FN, FdA);
  else
    VN = bsxfun(@times, VN, VdA ./ sqrt(VN(:,1).^2 + VN(:,2).^2 + VN(:,3).^2));
  end
end

function c = CrossProduct(a, b)
  c = [a(:,2).*b(:,3)-a(:,3).*b(:,2), ...
    a(:,3).*b(:,1)-a(:,1).*b(:,3), ...
    a(:,1).*b(:,2)-a(:,2).*b(:,1)];
end

