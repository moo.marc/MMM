function Sphere = MultiSphere_Local(SingleSphere, Sensor, Vertices, nV, dA, Options)
  % Fit a sphere based on small surface area (patch) near a sensor.
  %
  % Sphere = MultiSphere_Local(SingleSphere, Sensor, Vertices, nV, dA, Options)
  %
  % This is used by FitSpheres.m to create a multiple-sphere head model
  % for computing the MEG forward solution.  See that file for a published
  % study that compares this and other sphere-fitting methods.
  % 
  % Minimizes the sum of distance between sphere and shape points over a
  % small area near the sensor location.
  %
  % SingleSphere: Optional 4-element vector (x,y,z,radius) used to
  % initialize the minimization search.
  %
  % Sensor: Structure containing the sensor geometry (coils, locations,
  % orientations).
  %
  % Vertices: (nV x 3) array of shape points.
  %
  % nV: Optional, number of shape points.
  % 
  % dA: (nV x 3) array of normal vectors at each vertex.
  %
  % Options: 4-element vector [PatchR, DistanceType, AreaWeights, FromSens]
  % Default [9, 1, 1, 0]
  %   PatchR: Patch radius in cm,
  %   DistanceType: Abs(D), D^2, Radius-free, R-free^2, R-free-angles^2,
  %     Lutkenhoner(nu=2)^2.
  %   AreaWeights: Whether to apply dA as weights to vertex distances.
  %   FromSens: Patch from sensor instead of from closest shape point.
  % 
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-25

  Min_nPoints = 10; % Minimum number of points required for fitting.

  if ~exist('Vertices', 'var')
    error('Not enough parameters in function call.');
  end
  
  if ~exist('nV', 'var')
    nV = size(Vertices, 1);
  end
  if nV < Min_nPoints
    error('Not enough vertices in provided head shape mesh (or vertices matrix transposed).')
  end
  
  if ~exist('Options', 'var') || isempty(Options)
    Options = [9, 1, 1, 0];
  else
    % At least one option specified, assume radius.
    % If options are missing, put defaults.
    Options = [Options, 1, 1, 0];
  end
  % Also disregard superfluous options.
  Options = num2cell(Options(1:4));
  [PatchR, DistanceType, AreaWeights, FromSens] = Options{:};
  % DistanceType: Abs(D), D^2, Radius-free, R-free^2, R-free-angles^2,
  %   Lutkenhoner(nu=2)^2.

  if size(Sensor.Coil(1).Location, 2) ~= size(Vertices, 2)
    error('Location vector size wrong.  May be transposed.')
  end

  if ~exist('SingleSphere', 'var') || isempty(SingleSphere)
    % Compute (simply) a single sphere.
    % Make sure class is double, or fminsearch gives error.
    SingleSphere.Center = double(sum(Vertices, 1) / nV);
    SingleSphere.Radius = mean( sqrt(sum( ...
      (Vertices - SingleSphere.Center(ones(nV, 1), :)).^2 , 2)) );
  end
  
  % Source: fitMultipleSpheres.m
  % Find closest mesh point to sensor.  Note: minimizing square distance.
  [Distance, PatchCenterI] = min( sum(...
    (Vertices - Sensor.Coil(1).Location(ones(nV, 1), :)).^2 ...
    , 2) );
  Distance = sqrt(Distance);
  if FromSens
    PatchCenter = Sensor.Coil(1).Location;
    % We don't want to adjust the patch radius individually for sensors,
    % but it will have to be larger than when not using FromSens.
    if PatchR >= 2 * SingleSphere.Radius
      angle = 180;
    else
      angle = asind(PatchR / (2 * SingleSphere.Radius));
    end
    %  fprintf('Old radius: %1.0f, ', PatchR);
    PatchR = sqrt( PatchR^2 + Distance^2 - ...
      2 * PatchR * Distance * cosd(90 + angle) );
    %  fprintf('new radius: %1.1f.\n', PatchR);
    %  fprintf('From shape: %1.0f, from sensor: %1.0f with angle: %1.0f.\n', nP1, nP, angle);
  else
    PatchCenter = Vertices(PatchCenterI, :);
  end

  % Find points within patch radius.
  PatchI = sqrt(sum( (Vertices - PatchCenter(ones(nV, 1), :)).^2 , 2)) ...
    < PatchR;
  Patch = Vertices(PatchI, :);
  nP = size(Patch, 1); % Number of points in patch.
  if nP < Min_nPoints
    error('Insufficient number of points in patch.');
  end
  
  if AreaWeights || ismember(DistanceType, [2, 3, 4])
    if ~exist('dA', 'var')
      error('Option to weight by area element set, but missing dA.')
    elseif size(dA, 2) ~=3
      error('dA size is wrong, perhaps transposed.')
    end
    
    % Use the area elements as weights for the points. (Reset a few lines
    % down if shouldn't use them, but needed anyway for normalizing dA.)
    Weights = sqrt(sum(dA(PatchI, :).^2, 2));
    if ismember(DistanceType, [2, 3, 4])
      % Need to normalize normals.
      dA = dA(PatchI, :) ./ Weights(:, ones(1, 3));
    else
      clear dA
    end
  end
  if AreaWeights
    % Normalize weights to avoid extreme numbers, probably not necessary.
    Weights = Weights / max(Weights);
  else
    Weights = 1;
    % Only used in WeightedMean and WeightedMedian, both functions allow
    % empty or a scalar weight, meaning no weighting.
  end

  % Simplex minimization.
  Opt = optimset('MAXFUNEVALS',5000,'MAXITER',1000);

  switch DistanceType
    case 0
      % Distance between shape point and sphere.
      Sphere.Center = fminsearch(@WMeanDistance, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMedian( sqrt(sum( ...
        (Patch - Sphere.Center(ones(nP, 1), :)).^2 , 2)), Weights );

    case 1
      % Square distance between shape point and sphere.
      Sphere.Center = fminsearch(@WMeanSquareDistance, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMean( sqrt(sum( ...
        (Patch - Sphere.Center(ones(nP, 1), :)).^2 , 2)), Weights );

    case 2
      % Distance between sphere center and line along shape surface normal.
      % No radius involved, use same as case 0.
      Sphere.Center = fminsearch(@WMNormalDistance, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMedian( sqrt(sum( ...
        (Patch - Sphere.Center(ones(nP, 1), :)).^2 , 2)), Weights );

    case 3
      % Square distance between sphere center and line along shape surface
      % normal.  Radius-free, use same as case 1.
      Sphere.Center = fminsearch(@WMSquareNormalDistance, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMean( sqrt(sum( ...
        (Patch - Sphere.Center(ones(nP, 1), :)).^2 , 2)), Weights );

    case 4
      % Square deviation between radial and normal directions.
      % Radius-free, use same as case 1.  
      Sphere.Center = fminsearch(@WMSquareAngleDeviation, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMean( sqrt(sum( ...
        (Patch - Sphere.Center(ones(nP, 1), :)).^2 , 2)), Weights );

    case 5
      % Similar to case 2, but extra square exponents. Lutkenhoner (1990).
      Sphere.Center = fminsearch(@WMSquareNu2, SingleSphere.Center, Opt);
      Sphere.Radius = sqrt(WeightedMean(sum( ...
        (Patch - Sphere.Center(ones(nP, 1), :)).^2 , 2), Weights));

    otherwise
      error('Unknown distance type.');
  end
  
  %   if verbose
  %     fprintf('Found %d points, sphere = %.2f %.2f %.2f, radius = %.2f cm.\n', ...
  %       nP, Sphere.Center, Sphere.Radius);
  %   end


  % -----------------------------------------------------------------------
  % Calculate the mean distance between the points of the patch and the
  % sphere which is centered at the provided point.  The radius that
  % minimizes that distance is the median (not mean!) distance between the
  % center and points. So no need to search for it. 
  %   function D = MeanDistance(Center)
  %     Distances = sqrt(sum( (Patch - Center(ones(nP, 1), :)).^2 , 2));
  %     R = median(Distances);
  %     D = mean(abs(Distances - R));
  %   end
  
  % -----------------------------------------------------------------------
  % Calculate the mean square distance between the points of the patch and
  % the sphere which is centered at the provided point.  The radius that
  % minimizes that distance is the mean distance between the center and
  % points. So no need to search for it.
  %   function D = MeanSquareDistance(Center)
  %     SquareDist = sum( (Patch - Center(ones(nP, 1), :)).^2 , 2);
  %     R = mean(sqrt(SquareDist));
  %     D = mean(SquareDist) - R^2; % Variance formula.
  %   end
  
  % -----------------------------------------------------------------------
  % Calculate the mean distance between the points of the patch and the
  % sphere which is centered at the provided point.  The radius that
  % minimizes that distance is the median (not mean!) distance between the
  % center and points. So no need to search for it. 
  function D = WMeanDistance(Center)
    Distances = sqrt(sum( (Patch - Center(ones(nP, 1), :)).^2 , 2));
    R = WeightedMedian(Distances, Weights); % Radius that minimizes this function.
    D = WeightedMean(abs(Distances - R), Weights);
  end
  
  % -----------------------------------------------------------------------
  % Calculate the mean square distance between the points of the patch and
  % the sphere which is centered at the provided point.  The radius that
  % minimizes that distance is the mean distance between the center and
  % points. So no need to search for it.
  function D = WMeanSquareDistance(Center)
    SquareDist = sum( (Patch - Center(ones(nP, 1), :)).^2 , 2);
    R = WeightedMean(sqrt(SquareDist), Weights); % Radius that minimizes this function.
    D = WeightedMean(SquareDist, Weights) - R^2; % Weighted variance formula.
  end
    
  % -----------------------------------------------------------------------
  % Calculate the mean distance between the provided sphere center
  % and the lines normal to the surface vertices.  Radius-free method.
  function D = WMNormalDistance(Center)
    CtoV = Patch - Center(ones(nP, 1), :);
    Distances = sqrt(sum(CtoV.^2, 2) - ...
      (CtoV(:, 1) .* dA(:, 1) + CtoV(:, 2) .* dA(:, 2) + CtoV(:, 3) .* dA(:, 3)).^2 );
    D = WeightedMean(Distances, Weights);
  end
    
  % -----------------------------------------------------------------------
  % Calculate the mean distance between the provided sphere center
  % and the lines normal to the surface vertices.  Radius-free method.
  function D = WMSquareNormalDistance(Center)
    CtoV = Patch - Center(ones(nP, 1), :);
    SquareDist = sum(CtoV.^2, 2) - ...
      (CtoV(:, 1) .* dA(:, 1) + CtoV(:, 2) .* dA(:, 2) + CtoV(:, 3) .* dA(:, 3)).^2 ;
    D = WeightedMean(SquareDist, Weights);
  end
  
  % -----------------------------------------------------------------------
  % Calculate the mean deviation between the radial direction as determined
  % from the provided sphere center and the normal to the surface vertices,
  % from Lutkenhoner (1990). Basically same as previous except normalized
  % by distance from center to vertex: only angles matters. Radius-free
  % method.
  function D = WMSquareAngleDeviation(Center)
    CtoV = Patch - Center(ones(nP, 1), :);
    SquareSine = 1 - ...
      (CtoV(:, 1) .* dA(:, 1) + CtoV(:, 2) .* dA(:, 2) + CtoV(:, 3) .* dA(:, 3)).^2 ...
      ./ sum(CtoV.^2, 2);
    D = WeightedMean(SquareSine, Weights);
  end

  % -----------------------------------------------------------------------
  % Calculate the mean of the difference between (the square distance
  % between points on the patch and the sphere center) and the square
  % sphere radius. Method suggested by Lutkenhoner (1990). Not justified
  % other than the minimization of this function could be solved somewhat
  % easily without searching. But a search is easier to write and fast
  % anyway.
  function D = WMSquareNu2(Center)
    SquareDist = sum( (Patch - Center(ones(nP, 1), :)).^2 , 2);
    R = sqrt(WeightedMean(SquareDist, Weights)); % Radius that minimizes this function, as given in paper.
    D = WeightedMean((SquareDist - R^2).^2, Weights);
  end
    
end


