function [Sphere, Weights] = ...
    MultiSphere_SurfaceCurrent(SingleSphere, Sensor, Vertices, nV, dA, Options)
  % Fit a sphere based on currents on the conductor surface.
  %
  % [Sphere, Weights] = ...
  %   MultiSphere_SurfaceCurrent(SingleSphere, Sensor, Vertices, nV, dA, Options)
  %
  % This is used by FitSpheres.m to create a multiple-sphere head model
  % for computing the MEG forward solution.  See that file for a published
  % study that compares this and other sphere-fitting methods.
  % 
  % Minimizes the sum of distance between sphere and shape points over the
  % entire surface, using Huang's calculation as a weight applied on
  % distance, such that surface elements that contribute more to the field
  % are given more importance in the least-squares distance fit. As in
  % Brainstorm (normal surface currents). There are other options like
  % using tangential currents, using a "radius-free" distance to minimize,
  % etc.
  %
  % SingleSphere: Optional 4-element vector (x,y,z,radius) used to
  % initialize the minimization search.
  %
  % Sensor: Structure containing the sensor geometry (coils, locations,
  % orientations).
  %
  % Vertices: (nV x 3) array of shape points.
  %
  % nV: Optional, number of shape points.
  % 
  % dA: (nV x 3) array of normal vectors at each vertex.
  %
  % Options: 4-element vector [WeightType, DistanceType, EqualdA, WeightPower]
  % Default [1, 2, 0, 1]
  %   WeightType: 1: Normal current (BrainStorm), 2: Tangential current
  %   DistanceType: 1=Abs(D), 2=D^2, 3=Radius-free, 4=R-free^2.
  %   EqualdA: If 0, apply extra weight factor accounting for varying area
  %     at each vertex.
  %   WeightPower: Power to apply to weights, to "strengthen" their effect.
  %     (Brainstorm uses 2).
  %
  % 
  % � Copyright 2018 Marc Lalancette
  % The Hospital for Sick Children, Toronto, Canada
  % 
  % This file is part of a free repository of Matlab tools for MEG 
  % data processing and analysis <https://gitlab.com/moo.marc/MMM>.
  % You can redistribute it and/or modify it under the terms of the GNU
  % General Public License as published by the Free Software Foundation,
  % either version 3 of the License, or (at your option) a later version.
  % 
  % This program is distributed WITHOUT ANY WARRANTY. 
  % See the LICENSE file, or <http://www.gnu.org/licenses/> for details.
  % 
  % 2014-02-25
  
Min_nPoints = 10; % Minimum number of points required for fitting.

  if ~exist('Vertices', 'var')
    error('Not enough parameters in function call.');
  end
  
  if ~exist('nV', 'var') || isempty(nV)
    nV = size(Vertices, 1);
  end
  if nV < Min_nPoints
    error('Not enough vertices in provided head shape mesh (or vertices matrix transposed).')
  end
  
  if ~exist('Options', 'var') || isempty(Options)
    Options = [1, 2, 0, 1];
  else
    % At least one option specified, if some are missing, put some.
    Options = [Options, 1, 1, 1];
    % Also disregard superfluous options.
    Options = num2cell(Options(1:4));
  end
  [WeightType, DistanceType, EqualdA, WeightPower] = Options{:};
  % WeightType: 1: Normal current (BrainStorm), 2: Tangential current
  % DistanceType: 1: Distance, 2: Square distance, 3: Radius-free distance,
  %   4: Square R-free distance.

  if size(Sensor.Coil(1).Location, 2) ~= size(Vertices, 2)
    error('Location vector size wrong.  May be transposed.')
  end

  if ~exist('SingleSphere', 'var') || isempty(SingleSphere)
    SingleSphere.Center = sum(Vertices, 1) / nV;
    SingleSphere.Radius = mean( sqrt(sum( ...
      (Vertices - SingleSphere.Center(ones(nV, 1), :)).^2 , 2)) );
  end


  % Calculate weights.
  
  % Need to separate the |dA| weight from the rest here; don't want to
  % change the way it is applied, e.g. when we square the weights.
  NormdA = sqrt(sum(dA.^2, 2));
  % Normalize surface normal vectors.
  dA = dA ./ NormdA(:, ones(1, 3));
  % Calculate real weights for gradiometer, not just based on average coil
  % location.
  Weights = zeros(nV, 1);
  for c = 1:numel(Sensor.Coil)
    R = Sensor.Coil(c).Location(ones(nV, 1), :) - Vertices;
    switch WeightType
      case 1
        % Use the contribution of the equivalent surface current (normal to
        % the surface) as a weight function.
        Weights = Weights + cross(dA, R, 2) * ...
          Sensor.Coil(c).Orientation' ./ sum(R.^2, 2).^(3/2);
      case 2
        % Use the contribution of a tangential unit dipole as the weight.
        % Because we use the orientation to define the tangential current
        % direction: [(RxO) x dA] x dA, that would give us a current in
        % opposite directions for the two coils.  So add an extra
        % "flipping" factor (-1)^(c+1).  (Second coil negative: not really
        % any good reason here, but same as VolumeCurrent method, in which
        % case it makes sense.)
        RxO = cross(R, Sensor.Coil(c).Orientation(ones(nV, 1), :), 2);
        Weights = Weights + (-1)^(c+1) * ...
          (sum(RxO.^2, 2) - dot(RxO, dA, 2).^2) ./ sum(R.^2, 2).^2;
      otherwise
        error('Invalid weight type.')
    end
  end
  
  % Apply power to strengthen weight effect.
  if WeightPower ~= 1 % For speed, since not really used.
    Weights = Weights .^ WeightPower;
  end
  
  % Apply extra weight factor accounting for varying area at each vertex.
  if ~EqualdA
    Weights = Weights .* NormdA;
  end
  
  % Normalize weights. 
  Weights = abs(Weights);
  Weights = Weights / max(Weights);
  
 

  % Grid search didn't help.
%   % Get orthonormal vector basis for grid search.  We want to move the
%   % center first along the line from the shape point closest to the sensor
%   % to the SingleSphere.Center.  Then four more points around that line.
%   % Find closest mesh point to sensor.  Note: minimizing square distance.
%   [junk ClosestI] = min( sum(...
%     (Vertices - repmat(Sensor.Location, nV, 1)).^2 ...
%     , 2) ); %#ok<ASGLU>
%   ClosestPoint = Vertices(ClosestI, :);
% 
%   GridX = SingleSphere.Center - ClosestPoint;
%   GridX = GridX / sqrt(sum(GridX.^2));
%   % Now need 2 orthogonal vectors.  Find smallest component and make it
%   % big, then cross.
%   [junk, ISmall] = min(abs(GridX)); %#ok<ASGLU>
%   GridY = [0, 0, 0];
%   GridY(ISmall) = 1;
%   GridY = cross(GridX, GridY);
%   GridZ = cross(GridX, GridY);
%   
%   % Build up the initial sphere "grid".
%   Grid = repmat(ClosestPoint, 8, 1) + ...
%     [3; 6; 9; 12; 6; 6; 6; 6] * GridX + ...
%     [0; 0; 0; 0; 3; 0; -3; 0] * GridY + ...
%     [0; 0; 0; 0; 0; 3; 0; -3] * GridZ;
%   % Also try single sphere.
%   Grid(end+1, :) = SingleSphere.Center;
% 
%   % Proceed with grid search before simplex minimization.
%   NGrid = size(Grid, 1);
%   Errors = zeros(NGrid, 1);
%   for g = 1:NGrid
%     Errors(g) = WMeanSquareDistance(Grid(g, :));
%   end
%   [junk, BestI] = min(Errors); %#ok<ASGLU>
%   
%   % Overwrite SingleSphere for now.
%   SingleSphere.Center = Grid(BestI, :);

  Opt = optimset('MAXFUNEVALS',5000,'MAXITER',1000);
  switch DistanceType
    case 1
      % Distance between shape point and sphere.
      Sphere.Center = fminsearch(@WMeanDistance, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMedian( sqrt(sum( ...
        (Vertices - Sphere.Center(ones(nV, 1), :)).^2 , 2)), Weights );
      %     %testing R minimization
      %     test = fminsearch(@TEST, [Sphere.Center, Sphere.Radius+1], Opt)
      %     keyboard

    case 2
      % Square distance between shape point and sphere.
      Sphere.Center = fminsearch(@WMeanSquareDistance, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMean( sqrt(sum( ...
        (Vertices - Sphere.Center(ones(nV, 1), :)).^2 , 2)), Weights );

    case 3
      % Distance between sphere center and line along shape surface normal.
      % No radius involved, use same as case 0.
      Sphere.Center = fminsearch(@WMNormalDistance, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMedian( sqrt(sum( ...
        (Vertices - Sphere.Center(ones(nV, 1), :)).^2 , 2)), Weights );

    case 4
      % Square distance between sphere center and line along shape surface
      % normal.  No radius involved, use same as case 1.
      Sphere.Center = fminsearch(@WMSquareNormalDistance, SingleSphere.Center, Opt);
      Sphere.Radius = WeightedMean( sqrt(sum( ...
        (Vertices - Sphere.Center(ones(nV, 1), :)).^2 , 2)), Weights );

  end

  % -----------------------------------------------------------------------
  % For testing my R formula calculations, i.e. see if fminsearch finds the
  % same radius.  Result: yes, all is good.
  %   function D = TEST(CenterR) %#ok<DEFNU>
  %     Center = CenterR(1:3);
  %     R = CenterR(4);
  %     Dist = sqrt(sum( (Vertices - Center(ones(nV, 1), :)).^2 , 2));
  %     %R = WeightedMean(sqrt(SquareDist), Weights);
  %     D = WeightedMean(abs(Dist-R), Weights);
  %   end
  
  % -----------------------------------------------------------------------
  % Calculate the mean distance between the points of the patch and the
  % sphere which is centered at the provided point.  The radius that
  % minimizes that distance is the median (not mean!) distance between the
  % center and points. So no need to search for it. 
  function D = WMeanDistance(Center)
    Distances = sqrt(sum( (Vertices - Center(ones(nV, 1), :)).^2 , 2));
    R = WeightedMedian(Distances, Weights);
    D = WeightedMean(abs(Distances - R), Weights);
  end
    
  % -----------------------------------------------------------------------
  % Calculate the mean square distance between the points of the patch and
  % the sphere which is centered at the provided point.  The radius that
  % minimizes that distance is the mean distance between the center and
  % points. So no need to search for it.
  function D = WMeanSquareDistance(Center)
    SquareDist = sum( (Vertices - Center(ones(nV, 1), :)).^2 , 2);
    R = WeightedMean(sqrt(SquareDist), Weights);
    D = WeightedMean(SquareDist, Weights) - R^2; % Weighted variance formula.
  end
  
  % -----------------------------------------------------------------------
  % Calculate the mean distance between the provided sphere center
  % and the lines normal to the surface vertices.  Radius-free method.
  function D = WMNormalDistance(Center)
    CtoV = Vertices - Center(ones(nV, 1), :);
    Distances = sqrt(sum(CtoV.^2, 2) - ...
      (CtoV(:, 1) .* dA(:, 1) + CtoV(:, 2) .* dA(:, 2) + CtoV(:, 3) .* dA(:, 3)).^2 );
    D = WeightedMean(Distances, Weights);
  end
    
  % -----------------------------------------------------------------------
  % Calculate the mean distance between the provided sphere center
  % and the lines normal to the surface vertices.  Radius-free method.
  function D = WMSquareNormalDistance(Center)
    CtoV = Vertices - Center(ones(nV, 1), :);
    SquareDist = sum(CtoV.^2, 2) - ...
      (CtoV(:, 1) .* dA(:, 1) + CtoV(:, 2) .* dA(:, 2) + CtoV(:, 3) .* dA(:, 3)).^2 ;
    D = WeightedMean(SquareDist, Weights);
  end
  
end


