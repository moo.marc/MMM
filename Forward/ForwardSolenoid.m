function [B, BDip] = ForwardSolenoid(SolenoidMoment, SolenoidLoc, ...
        SolenoidR, SolenoidL, SensorLoc, SensorOrient)
    % Magnetic field of a finite ideal solenoid, and optionally the dipolar limit.
    % 
    % [B, BDip] = ForwardSolenoid(SolenoidMoment, SolenoidLoc, ...
    %     SolenoidR, SolenoidL, SensorLoc, SensorOrient)
    %
    % Inputs are row vectors.  Multiple rows can be provided for SensorLoc
    % and SensorOrient.  SI units: distances in m, field in T.
    %
    % Moment magnitude for the solenoid is n * I * area, where n is the
    % number of turns, I the current and area = pi * SolenoidR^2.  Or if a
    % unit moment is provided, the output is in T/(Am.^2).
    %
    % Formulae from [Derby 2010 - Cylindrical magnets and ideal solenoids].
    %
    % Marc Lalancette, 2019-03-07
    
    % "Hat" indicates unit vectors.
    
    if size(SolenoidLoc, 2) ~= 3  || size(SolenoidMoment, 2) ~= 3 || ...
            size(SensorLoc, 2) ~= 3 || size(SensorOrient, 2) ~= 3
        error('Input vector(s) (source or sensor) not properly transposed or wrong length.')
    end
    if size(SolenoidLoc, 1) > 1 || size(SolenoidMoment, 1) > 1
        error('Multiple solenoids (location and moment) not allowed.');
    end
    
    % Convert coordinates and orientations to solenoid cylindrical coords.
    % Cylindrical symmetry, only 2 components (z: axial, rho: radial).
    Rv = bsxfun(@minus, SensorLoc, SolenoidLoc);
    %     R = norm(Rv);
    %     RHat = Rv / R;
    % Moment magnitude for the solenoid is n * I * area, where n is the
    % number of turns, I the current and area = pi * SolenoidR^2.
    mu = norm(SolenoidMoment);
    zHat = SolenoidMoment / mu;
    z = Rv * zHat';
    rhoHat = Rv - z * zHat;
    rho = sqrt(sum(rhoHat.^2, 2));
    rhoHat = bsxfun(@rdivide, rhoHat, rho);
    
    % From here, all scalars until final B.
    zp = z + SolenoidL/2;
    zm = z - SolenoidL/2;
    kp = sqrt( (zp.^2 + (SolenoidR - rho).^2) ./ (zp.^2 + (SolenoidR + rho).^2) );
    km = sqrt( (zm.^2 + (SolenoidR - rho).^2) ./ (zm.^2 + (SolenoidR + rho).^2) );
    g = (SolenoidR - rho) ./ (SolenoidR + rho);
    
    Bz = SolenoidR ./ (SolenoidR + rho) .* ...
        ( zp ./ sqrt(zp.^2 + (SolenoidR + rho).^2) .* GCEInt(kp, g.^2, 1, g) - ...
        zm ./ sqrt(zm.^2 + (SolenoidR + rho).^2) .* GCEInt(km, g.^2, 1, g) );
    Br = ( SolenoidR ./ sqrt(zp.^2 + (SolenoidR + rho).^2) .* GCEInt(kp, 1, 1, -1) - ...
        SolenoidR ./ sqrt(zm.^2 + (SolenoidR + rho).^2) .* GCEInt(km, 1, 1, -1) );
    
    % B0 = mu0/pi * n/L * I = mu0/(4*pi) * 4/(area*L) * mu.
    % mu0/4*pi = 10^-7 Wb/Am = Tm/A
    B = 10^-7 * 4 / (pi * SolenoidR.^2 * SolenoidL) * mu * ... 
        sum((Bz * zHat + bsxfun(@times, Br, rhoHat)) .* SensorOrient, 2);
    
    % Also return the dipolar limit (infinitely small solenoid with the
    % same total magnetic moment).
    if nargout > 1
        r5 = (sqrt(z.^2 + rho.^2)).^5;
        BzDip = (2 * z.^2 - rho.^2) ./ r5;
        BrDip = 3 * rho .* z ./ r5;
        BDip = 10^-7 * mu * ...
            sum((BzDip * zHat + bsxfun(@times, BrDip, rhoHat)) .* SensorOrient, 2);
    end
end

function C = GCEInt(kc, p, c, s)
    % Generalized Complete Elliptic Integral
    % From [Derby 2010 - Cylindrical magnets and ideal solenoids].
    % Vectorized.
    
    if kc == 0
        C = NaN;
        return;
    end
    Tol = 1e-6;
    %     p = p;
    %     c = c;
    %     s = s;
    if p > 0
        p = sqrt(p);
        s = s ./ p;
    else
        %         f = kc^2;
        %         q = 1 - f;
        %         g = 1 - p;
        %         f = kc^2 - p;
        %         q = (1 - f) * (ss - c * p);
        p = sqrt((kc.^2 - p) ./ (1 - p));
        Prevc = c;
        c = (c - s) ./ (1 - p);
        s = -(1 - (kc.^2 - p)) .* (s - Prevc .* p) / ((1 - p).^2 .* p) + c * p;
    end
    em = 1;
    k = abs(kc);
    kk = k .* em;
    Prevc = c;
    c = c + s ./ p;
    %     g = kk / p;
    s = 2 * (s + Prevc .* kk ./ p);
    p = kk ./ p + p;
    PrevEm = em;
    em = k + em;
    
    while any(abs(PrevEm - k) > PrevEm * Tol)
        k = 2 * sqrt(kk);
        kk = k .* em;
        Prevc = c;
        c = c + s ./ p;
        %        g = kk / p;
        s = 2 * (s + Prevc .* kk ./ p);
        p = kk ./ p + p;
        PrevEm = em;
        em = k + em;
    end
    C = pi/2 * (s + c .* em) ./ (em .* (em + p));
end


